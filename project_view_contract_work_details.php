<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	if(isset($_GET['payment_contract_id']))
	{
		$contract_payment_id = $_GET['payment_contract_id'];
	}
	else
	{
		$contract_payment_id = "";
	}

	$search_project   	 = "";

	if(isset($_POST["search_project"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_vendor   	 = "";

	if(isset($_POST["search_vendor"]))
	{
		$search_vendor   = $_POST["search_vendor"];
	}


	// Temp data
	$project_payment_contract_mapping_search_data = array("payment_id"=>$contract_payment_id,"active"=>"1");
	$project_task_boq_actual_list = i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);

	if($project_task_boq_actual_list["status"] == SUCCESS)
	{
		$project_task_boq_actual_list_data = $project_task_boq_actual_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_boq_actual_list["data"];
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$users);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
	 else
	{

	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Actual Contract Work List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task Actual Contract Work List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_view_manpower_details.php">
			  <input type="hidden" name="payment_manpower_id" value="<?php echo $payment_id; ?>" />

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

             <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task</th>
					<th>Contract Process</th>
					<th>Contract Task</th>
					<th>Vendor</th>
					<th>Date</th>
					<th>UOM</th>
					<th>Number</th>
					<th>Length</th>
					<th>Breadth</th>
					<th>Depth</th>
					<th>Total Measurement</th>
					<th>Rate</th>
					<th>Total</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>

				</tr>
				</thead>
				<tbody>
				<form method="post" action="project_approved_contract_work.php">
				<input type="hidden" name="hd_vendor_id" value="<?php echo $search_vendor; ?>" />
				<?php
				if($project_task_boq_actual_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_task_boq_actual_list_data); $count++)
					{
						?>
						<input type="hidden" name="hd_boq_count" value="<?php echo count($project_task_boq_actual_list_data); ?>" />
						<?php
						$sl_no++;
						$measurement = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"];
						$rate = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
						$total = $measurement * $rate;
						$total_amount = $total_amount + $total;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_process_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>

					<input type="hidden" name="hd_vendor_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_vendor_id"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_manpower_agency_name"]; ?></td>

					<input type="hidden" name="hd_contract_date_<?php echo $count; ?>" value="<?php echo date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count][
						"project_task_actual_boq_date"])); ?>" />
					<td><?php echo get_formatted_date($project_task_boq_actual_list_data[$count]["project_task_actual_boq_date"],"d-M-Y"); ?></td>

					<input type="hidden" name="hd_unit_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_uom"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["stock_unit_name"]; ?></td>

					<input type="hidden" name="hd_number_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?></td>

					<input type="hidden" name="hd_length_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?></td>

					<input type="hidden" name="hd_breadth_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?></td>

					<input type="hidden" name="hd_depth_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?></td>

					<input type="hidden" name="hd_measurement_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?></td>

					<input type="hidden" name="hd_rate_count_<?php echo $count; ?>" value="<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?>" />
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?></td>

					<td><?php echo  $total ; ?></td>

					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"]; ?></td>

					<td><?php echo $project_task_boq_actual_list_data[$count]["user_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count][
					"project_task_actual_boq_added_on"])); ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>

            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_actual_manpower(man_power_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function project_approve_task_actual_manpower(man_power_id,task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_man_power.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
</script>

  </body>

</html>
