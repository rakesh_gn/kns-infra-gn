<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th May 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Get Project List
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_filter_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_filter_list["status"] == SUCCESS)
	{
		$project_management_master_filter_list_data = $project_management_master_filter_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_filter_list["data"];
	}

	if($project_management_master_filter_list['status'] == SUCCESS)
	{
		for($count = 0; $count < count($project_management_master_filter_list['data']); $count++)
		{
			$project_id = $project_management_master_filter_list['data'][$count]['project_management_master_id'];

			/* Material - Start */
			// Plan
			$material_plan_value = i_get_planned_material_value($project_id,'1');

			// Actuals
			$material_actual_value = i_get_actual_material_value($project_id,'1');

			$material_variance = $material_actual_value - $material_plan_value;
			/* Material - End */

			/* Machine - Start */
			// Plan
			$machine_plan_value = round(i_get_planned_machine_value($project_id,'1'));

			// Actuals
			$machine_actual_value = round(i_get_actual_machine_value($project_id,'1'));

			$machine_variance = $machine_actual_value - $machine_plan_value;

			/* Machine - End */

			/* Manpower - Start */
			// Plan
			$manpower_plan_value = round(i_get_planned_manpower_value($project_id,'1'));

			// Actuals
			$manpower_actual_value = round(i_get_actual_manpower_value($project_id,'1'));

			$manpower_variance = $manpower_actual_value - $manpower_plan_value;

			/* Manpower - End */

			/* Contract - Start */
			// Plan
			$contract_plan_value = round(i_get_planned_contract_value($project_id,'1'));

			// Actuals
			$contract_actual_value = round(i_get_actual_contract_value($project_id,'1'));

			$contract_variance = $contract_actual_value - $contract_plan_value;

			/* Contract - End */

			// Add Project Finance Dashboard fields
			$project_finance_dashboard_iresult = i_add_project_finance_dashboard($project_id,$material_plan_value,$material_actual_value,$material_variance,$manpower_plan_value,$manpower_actual_value,$manpower_variance,$machine_plan_value,$machine_actual_value,$machine_variance,$contract_plan_value,$contract_actual_value,$contract_variance,'','');

			if($project_finance_dashboard_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = " Finance Dashboard Successfully Added";
			}

			else
			{
				$alert = "Please fill all the mandatory fields";
				$alert_type = 0;
			}
		}
	}

}
else
{
	header("location:login.php");
}
?>
