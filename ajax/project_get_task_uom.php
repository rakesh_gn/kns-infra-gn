<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	if(isset($_POST['process']))
	{
		$process      = $_POST["process"];
	}
	else
	{
		$process      = '';
	}
	
	if(isset($_POST['task']))
	{
		$task      = $_POST["task"];
	}
	else
	{
		$task      = '';
	}
	
	// List of added tasks
	$project_task_master_search_data = array("active"=>'1',"process"=>$process,"task_master_id"=>$task);	
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);	
	if($project_task_master_list["status"] == SUCCESS)	
	{
		$project_task_master_list_data = $project_task_master_list["data"];
		$uom_id = $project_task_master_list_data[0]["project_task_master_uom"];
		$uom_name = $project_task_master_list_data[0]["project_uom_name"];
	}
	
	$result = array("uom_id"=>$uom_id,"uom_name"=>$uom_name);
	
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>