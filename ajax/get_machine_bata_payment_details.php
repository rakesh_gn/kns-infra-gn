<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$payment_id  = $_GET["payment_id"];
	// Get machine payment mapping for project name
	$project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$payment_id);
	$project_machine_issue_payment_list = db_get_project_machine_bata_issue_payment($project_machine_issue_payment_search_data);
	$project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
	 $output = array("instrument_details"=>$project_machine_issue_payment_list_data[0]["project_machine_bata_issue_payment_instrument_details"],
	"remarks"=>$project_machine_issue_payment_list_data[0]["project_machine_bata_issue_payment_remarks"],
	"payment_added_on"=>$project_machine_issue_payment_list_data[0]["project_machine_bata_issue_payment_added_on"],
	"payment_mode"=>$project_machine_issue_payment_list_data[0]["payment_mode_name"],
	"payment_added_by"=>$project_machine_issue_payment_list_data[0]["user_name"]);
echo json_encode($output);
}
else
{
	echo FAILURE;
	header("location:login.php");
}
?>
