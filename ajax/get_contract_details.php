<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$payment_id  = $_GET["payment_id"];
	$issued_amount = 0;
	$project_payment_contract_mapping_search_data = array("payment_id"=>$payment_id);
	$payment_contract_mapping_list= i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);
	if($payment_contract_mapping_list["status"] == SUCCESS)
	{
		$payment_contract_mapping_list_data = $payment_contract_mapping_list["data"];
		$contract_id = $payment_contract_mapping_list_data[0]["project_payment_contract_mapping_contract_id"];
		$project_task_actual_boq_search_data = array("boq_id"=>$contract_id);
		$actual_payment_contract_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);
		if($actual_payment_contract_list["status"] == SUCCESS)
		{
			$project = $actual_payment_contract_list["data"][0]["project_master_name"];
		}
	}
	else
	{
		$project = 'INVALID PROJECT';
	}

	$project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$payment_id);
	$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
	if($project_contract_issue_payment_list["status"] == SUCCESS)
	{
		$project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
		for($issue_count = 0 ; $issue_count < count($project_contract_issue_payment_list_data) ; $issue_count++)
		{
			$issued_amount = $issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
		}
	}
	if(isset($_GET["payment_status"])){
	$output = array("project"=>$project,"issued_amount"=>$issued_amount,"instrument_details"=>$project_contract_issue_payment_list_data[0]["project_contract_issue_payment_instrument_details"],
	"remarks"=>$project_contract_issue_payment_list_data[0]["project_contract_issue_payment_remarks"],
	"payment_added_on"=>$project_contract_issue_payment_list_data[0]["project_contract_issue_payment_added_on"],
	"payment_mode"=>$project_contract_issue_payment_list_data[0]["payment_mode_name"],
	"payment_added_by"=>$project_contract_issue_payment_list_data[0]["user_name"]);
}
else{
	$output= array("project"=>$project,"issued_amount"=>$issued_amount);
}
	echo json_encode($output);
}
else
{
	header("location:login.php");
}
?>
