<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$indent_id      = $_REQUEST["indent_id"];
	$material_id    = $_REQUEST["material_id"];

	// Get Indent Item Details
	$stock_indent_search_data = array("indent_id"=>$indent_id,"active"=>'1',"material_id"=>$material_id);
	$indent_item_list = i_get_indent_items_list($stock_indent_search_data);
	if($indent_item_list["status"] == SUCCESS)
	{
		$indent_item_list_data = $indent_item_list["data"];
		$indent_qty = $indent_item_list_data[0]["stock_indent_item_quantity"];
	}
	else
	{
		$alert = $alert."Alert: ".$indent_item_list["data"];
		$indent_qty = "";
	}
	echo $indent_qty ;
}
else
{
	header("location:login.php");
}
?>
