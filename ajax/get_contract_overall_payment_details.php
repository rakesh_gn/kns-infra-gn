<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'general_config.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_master_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    $search_vendor = $_GET["vendor_id"];

    // Get Project  Payment ManPower modes already added
    $project_actual_contract_payment_search_data = array("active"=>'1',"vendor_id"=>$search_vendor);
   $project_actual_contract_payment_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
   if($project_actual_contract_payment_list['status'] == SUCCESS)
   {
       $project_actual_contract_payment_list_data = $project_actual_contract_payment_list['data'];
   }

      $total_issued_amount = 0;
      $total_deduction = 0;
      $total_balance = 0;
      $total_amount = 0;
     if($project_actual_contract_payment_list["status"] == SUCCESS)
               {
                   $sl_no = 0;
                   for($count = 0; $count < count($project_actual_contract_payment_list_data); $count++)
                   {
                       if($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Accepted' ||
                       $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Approved' ||
                       $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Payment Issued'){
                       $sl_no++;
                       //Get Delay
                       $start_date = date("Y-m-d");
                       $end_date = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_approved_on"];
                       $delay = get_date_diff($end_date,$start_date);

                       //Get total amount
                       //$amount = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];

                       $amount_before_tds = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];
                       $contract_tds = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_tds"];
                       $tds_amount = ($contract_tds/100) * $amount_before_tds;
                       $amount = $amount_before_tds - $tds_amount;
                       //Get security deposit
                       $security_deposit = $project_actual_contract_payment_list_data[$count]["project_actual_contract_deposit_amount"];

                       if ($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_deposit_status"] == 'Bill Generated' ||
                        $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_deposit_status"] == 'Payment Issued') {
                            $amount = $amount - $security_deposit;
                           }

                       //Get Project Machine Vendor master List
                       $issued_amount = 0;
                       $deduction = 0;
                       $project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]);
                       $project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
                       if($project_contract_issue_payment_list["status"] == SUCCESS)
                       {
                           $project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
                           for($issue_count = 0 ; $issue_count < count($project_contract_issue_payment_list_data) ; $issue_count++)
                           {
                               $issued_amount = $issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
                               $deduction = $deduction + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_deduction"];
                               $total_issued_amount = $total_issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
                           }
                       }
                       else
                       {
                           $issued_amount = 0;
                           $deduction = 0;
                       }
                       $balance_amount = round(($amount - $issued_amount),2);

                       if($balance_amount != 0)
                       {
                           // Get Project details

                               $total_issued_amount = $total_issued_amount;
                               $total_deduction  = $total_deduction + $deduction;
                               $total_amount = $total_amount + $amount;
                           }
                       }
                    }
                   }

               $total_balance  = $total_amount-$total_issued_amount;

    $output= array('total_amount' => $total_amount, 'issued_amount'=>$total_issued_amount,'balance_amount'=>$total_balance);
    echo json_encode($output);
} else {
    echo "FAILURE";
}

?>
