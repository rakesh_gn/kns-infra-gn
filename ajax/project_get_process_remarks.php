<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['process_id'])) {
        $process_id = $_REQUEST['process_id'];
    } else {
        $process_id = "";
    }

    $process_remarks_search_data = array("process_id"=>$process_id,"status"=>"Pending");
    $process_remarks_list = db_get_process_view_remarks($process_remarks_search_data);
    if($process_remarks_list["status"] == DB_RECORD_ALREADY_EXISTS)
    {
      $process_remarks_list_data = $process_remarks_list["data"];
    }
    else {

    }

  echo json_encode($process_remarks_list_data);
} else {
    header("location:login.php");
}
