
<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $rowdata = $_POST["val"];
	$vendor_id = $_POST["vendor"];
	$amount = $_POST["amount"];
	$quote_no = $_POST["quote_no"];
	$remarks = $_POST["remarks"];
	$received_date = $_POST["received_date"];
	$length = count($rowdata);

  if($length > 0){
    $start_date = "0000-00-00";
    $end_date = "0000-00-00";
    $no_of_men_hrs = "";
    $no_of_women_hrs = "";
    $no_of_mason_hrs = "";
    $total_amount = "";
    $bill_no= p_generate_quotation_no();

    //update by Rakesh
    //as of Requirment
    //Date : 31-Jan-2019
		/***--old code--**
    $quotation_compare_iresult = i_add_stock_quotation_compare($bill_no,$rowdata[0]['material_id'],$amount,$quote_no,$vendor_id,$rowdata[0]['indent_item_qty'],$rowdata[0]['project_id'],$received_date,'',$remarks,$user);*/

    /*$quotation_compare_iresult = i_add_stock_quotation_compare($bill_no,$rowdata[0]['indent_id'],$amount,$quote_no,$vendor_id,
      $rowdata[0]['indent_item_qty'],$rowdata[0]['project_id'],$received_date,'',$remarks,$user);*/
    //end of update

    $stock_quotation_compare_search_data1  = array('indent_id' =>$rowdata[0]['indent_id'] ,'material_id' =>$rowdata[0]['material_id'] );

    $stock_quotation_compare_result = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data1);

    if($stock_quotation_compare_result["status"] == 0) {
       $output = array('status'=> 400,'message' => 'Quote already created','data' => $stock_quotation_compare_search_data["data"]);

       echo json_encode($output);
    }else{

      $quotation_compare_iresult = i_add_stock_quotation_compare($bill_no,$rowdata[0]['indent_id'],$amount,$quote_no,$vendor_id,$rowdata[0]['indent_item_qty'],$rowdata[0]['project_id'],$received_date,'',$remarks,$user,$rowdata[0]['material_id']);

      if ($quotation_compare_iresult["status"] == SUCCESS) {
          $quote_id = $quotation_compare_iresult["data"];
      }
      $total_indent_item_qty = 0 ;
      for ($item_count = 0 ; $item_count < $length ; $item_count++) {
      $indent_item_id        = $rowdata[$item_count]['indent_item_id'];
      $material_id           = $rowdata[$item_count]['material_id'];
      $indent_id             = $rowdata[$item_count]['indent_id'];
      $indent_no             = $rowdata[$item_count]['indent_no'];
      $material_qty          = $rowdata[$item_count]['indent_item_qty'];
      $project_id            = $rowdata[$item_count]['project_id'];
      $project_name          = $rowdata[$item_count]['project_name'];
      $material_name         = $rowdata[$item_count]['material_name'];
      $material_code         = $rowdata[$item_count]['material_code'];
      $total_indent_item_qty += $material_qty ;
      $quotation_items_mapping_iresult = db_add_stock_quotation_items_mapping($quote_id,$indent_item_id,$material_id,$indent_id,$indent_no,$material_qty,$project_id,$project_name,$material_name,$material_code);
      if($quotation_items_mapping_iresult["status"] == SUCCESS) {
      }
    }
   //Update Payment Manpower
   $quotation_compare_update_data = array("item_quote_qty"=>$total_indent_item_qty);
   $payment_manpower_uresults = i_update_quotation_compare($quote_id,'',$quotation_compare_update_data);
   $output = array('status'=> 200,'quote' => $quote_id);
    echo json_encode($output);

    }

      
}
}
else{
   echo "FAILURE";
 }
?>
