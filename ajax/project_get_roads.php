<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['process_id'])) {
        $process_id = $_REQUEST['process_id'];
    } else {
        $process_id = "";
    }

    if (isset($_REQUEST['project_id'])) {
        $project_id = $_REQUEST['project_id'];
    } else {
        $project_id = "";
    }

    // Get Already added process
    $project_process_master_list = db_get_project_process_list('plan_process_id',$process_id);
    if ($project_process_master_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_process_master_list_data = $project_process_master_list["data"];
        $process_master_id = $project_process_master_list_data[0]["process_master_id"];
    }
    // Get process details
		$project_process_master_search_data = array("process_id"=>$process_master_id,"active"=>'1');
		$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
		if($project_process_master_list['status'] == SUCCESS)
		{
			$project_plan_process_list_sec_data = $project_process_master_list["data"];
			$process_road = $project_plan_process_list_sec_data[0]["project_process_master_road"];
		}
		else
		{
			$process_road  = "";
		}
    $project_site_location_mapping_master_list_data = array();
    if($process_road == 1)
    {
      $project_site_location_mapping_master_search_data = array("is_road"=>'1',"active"=>'1',"project_id"=>$project_id);
      $project_site_location_mapping_master_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
      if($project_site_location_mapping_master_list['status'] == SUCCESS)
      {
     	 $project_site_location_mapping_master_list_data = $project_site_location_mapping_master_list['data'];
      }
      else {
        $project_site_location_mapping_master_list_data[0]["project_site_location_mapping_master_id"] = "No Roads";
        $project_site_location_mapping_master_list_data[0]["project_site_location_mapping_master_name"] = "No Roads";
      }
    }
    else
    {
      $project_site_location_mapping_master_list_data[0]["project_site_location_mapping_master_id"] = "No Roads";
      $project_site_location_mapping_master_list_data[0]["project_site_location_mapping_master_name"] = "No Roads";
    }
    echo json_encode($project_site_location_mapping_master_list_data);
} else {
    header("location:login.php");
}
