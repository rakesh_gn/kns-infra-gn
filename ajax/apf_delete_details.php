<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$apf_id         = $_POST["apf_id"];
	$active         = $_POST["action"];
	
	$apf_details_update_data = array("active"=>$active);
	$apf_details_result =i_delete_apf_details($apf_id,$apf_details_update_data);
	
	if($apf_details_result["status"] == FAILURE)
	{
		echo $apf_details_result["data"];
	}
	else
	{
		echo "SUCCESS";
		for($process_count = 0; $process_count < count($apf_details_result["data"]); $process_count++)
		{
			$apf_process_search_data = array();
			$apf_process_list = i_get_apf_process($apf_process_search_data);
		}
	}
}
else
{
	header("location:login.php");
}
?>