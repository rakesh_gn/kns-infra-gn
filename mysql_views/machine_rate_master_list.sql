-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2018 at 07:03 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `machine_rate_master_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `machine_rate_master_list`  AS  select `PMRM`.`project_machine_rate_master_id` AS `project_machine_rate_master_id`,`PMRM`.`project_machine_rate_machine_id` AS `project_machine_rate_machine_id`,`PMRM`.`project_machine_rate` AS `project_machine_rate`,`PMRM`.`project_machine_kns_fuel` AS `project_machine_kns_fuel`,`PMRM`.`project_machine_vendor_fuel` AS `project_machine_vendor_fuel`,`PMRM`.`project_machine_kns_bata` AS `project_machine_kns_bata`,`PMRM`.`project_machine_vendor_bata` AS `project_machine_vendor_bata`,`PMRM`.`project_machine_rate_master_active` AS `project_machine_rate_master_active`,`PMRM`.`project_machine_rate_master_remarks` AS `project_machine_rate_master_remarks`,`PMRM`.`project_machine_rate_master_added_by` AS `project_machine_rate_master_added_by`,`PMRM`.`project_machine_rate_master_added_on` AS `project_machine_rate_master_added_on`,`PMRM`.`project_machine_rate_master_updated_by` AS `project_machine_rate_master_updated_by`,`PMRM`.`project_machine_rate_master_updated_on` AS `project_machine_rate_master_updated_on`,`AU`.`user_name` AS `updated_by`,`U`.`user_id` AS `user_id`,`U`.`user_name` AS `user_name`,`PMM`.`project_machine_master_id` AS `project_machine_master_id`,`PMM`.`project_machine_master_name` AS `project_machine_master_name`,`PMM`.`project_machine_master_id_number` AS `project_machine_master_id_number`,`PMM`.`project_machine_master_vendor` AS `project_machine_master_vendor`,`PMM`.`project_machine_type` AS `project_machine_type`,`PMM`.`project_machine_master_use` AS `project_machine_master_use`,`PMM`.`project_machine_master_machine_type` AS `project_machine_master_machine_type`,`PMM`.`project_machine_master_active` AS `project_machine_master_active`,`PMM`.`project_machine_master_remarks` AS `project_machine_master_remarks`,`PMM`.`project_machine_master_added_by` AS `project_machine_master_added_by`,`PMM`.`project_machine_master_added_on` AS `project_machine_master_added_on`,`PMVM`.`project_machine_vendor_master_id` AS `project_machine_vendor_master_id`,`PMVM`.`project_machine_vendor_master_name` AS `project_machine_vendor_master_name`,`PMVM`.`project_machine_vendor_master_active` AS `project_machine_vendor_master_active`,`PMVM`.`project_machine_vendor_master_remarks` AS `project_machine_vendor_master_remarks`,`PMVM`.`project_machine_vendor_master_added_by` AS `project_machine_vendor_master_added_by`,`PMVM`.`project_machine_vendor_master_added_on` AS `project_machine_vendor_master_added_on`,`PMTM`.`project_machine_type_master_id` AS `project_machine_type_master_id`,`PMTM`.`project_machine_type_master_name` AS `project_machine_type_master_name`,`PMTM`.`project_machine_type_master_active` AS `project_machine_type_master_active`,`PMTM`.`project_machine_type_master_remarks` AS `project_machine_type_master_remarks`,`PMTM`.`project_machine_type_master_added_by` AS `project_machine_type_master_added_by`,`PMTM`.`project_machine_type_master_added_on` AS `project_machine_type_master_added_on` from (((((`project_machine_rate_master` `PMRM` join `users` `U` on((`U`.`user_id` = `PMRM`.`project_machine_rate_master_added_by`))) join `project_machine_master` `PMM` on((`PMM`.`project_machine_master_id` = `PMRM`.`project_machine_rate_machine_id`))) join `project_machine_vendor_master` `PMVM` on((`PMVM`.`project_machine_vendor_master_id` = `PMM`.`project_machine_master_vendor`))) left join `users` `AU` on((`AU`.`user_id` = `PMRM`.`project_machine_rate_master_updated_by`))) join `project_machine_type_master` `PMTM` on((`PMTM`.`project_machine_type_master_id` = `PMM`.`project_machine_master_machine_type`))) ;

--
-- VIEW  `machine_rate_master_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
