-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 14, 2018 at 09:19 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `crm_site_travel`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `crm_site_travel`  AS  select `CSVP`.`crm_site_visit_plan_id` AS `crm_site_visit_plan_id`,`CSVP`.`crm_site_visit_plan_date` AS `crm_site_visit_plan_date`,`CSVP`.`crm_site_visit_plan_time` AS `crm_site_visit_plan_time`,`CE`.`enquiry_number` AS `enquiry_number`,`CSVP`.`crm_site_visit_pickup_location` AS `crm_site_visit_pickup_location`,`CSVP`.`crm_site_visit_plan_confirmation` AS `crm_site_visit_plan_confirmation`,`CSVP`.`crm_site_visit_plan_drive` AS `crm_site_visit_plan_drive`,`CSVP`.`crm_site_visit_plan_status` AS `crm_site_visit_plan_status`,`CSVP`.`crm_site_visit_status` AS `crm_site_visit_status`,`CE`.`name` AS `name`,`CE`.`cell` AS `cell`,`PM`.`project_name` AS `project_name`,`PM`.`project_id` AS `project_id`,`U`.`user_name` AS `assigner`,`U`.`user_id` AS `assigner_id`,`U1`.`user_id` AS `assignee_id`,`U1`.`user_name` AS `assignee` from ((((`crm_site_visit_plan` `CSVP` join `crm_enquiry` `CE` on((`CE`.`enquiry_id` = `CSVP`.`crm_site_visit_plan_enquiry`))) join `crm_project_master` `PM` on((`PM`.`project_id` = `CSVP`.`crm_site_visit_plan_project`))) join `users` `U` on((`U`.`user_id` = `CSVP`.`crm_site_visit_plan_added_by`))) join `users` `U1` on((`U1`.`user_id` = `CE`.`assigned_to`))) ;

--
-- VIEW  `crm_site_travel`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
