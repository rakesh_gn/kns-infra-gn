-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 21, 2019 at 04:25 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_legal`
--

-- --------------------------------------------------------

--
-- Structure for view `po_material_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `po_material_list`  AS  select `smm`.`stock_material_code` AS `material_code`,`smm`.`stock_material_name` AS `material_name`,`smm`.`stock_material_price` AS `material_price`,`spoi`.`stock_purchase_order_item_id` AS `spoi_id`,`spoi`.`stock_purchase_order_item` AS `material_id`,`spo`.`stock_purchase_order_project` AS `project_id`,`spoi`.`stock_purchase_order_item_quantity` AS `po_material_qty`,`spoi`.`stock_grn_qty` AS `stock_grn_quantity`,`spoi`.`stock_grn_remaning_qty` AS `stock_grn_remaning_qty`,`spo`.`stock_purchase_order_quotation_id` AS `po_quotation_id`,`spo`.`stock_purchase_order_id` AS `po_order_id`,`spo`.`stock_purchase_order_number` AS `stock_purchase_order_number` from ((`stock_purchase_order_items` `spoi` join `stock_purchase_order` `spo` on((`spo`.`stock_purchase_order_id` = `spoi`.`stock_purchase_order_id`))) left join `stock_material_master` `smm` on((`smm`.`stock_material_id` = `spoi`.`stock_purchase_order_item`))) where ((`spoi`.`stock_purchase_order_item_status` = 'Waiting') or (`spoi`.`stock_purchase_order_item_status` = 'Pending') or (`spoi`.`stock_purchase_order_item_status` = 'Approved')) ;

--
-- VIEW  `po_material_list`
-- Data: None
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
