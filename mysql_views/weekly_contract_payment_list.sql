-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2018 at 07:56 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vafa_5`
--

-- --------------------------------------------------------

--
-- Structure for view `weekly_contract_payment_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `weekly_contract_payment_list`  AS  select `PACP`.`project_actual_contract_payment_id` AS `project_actual_contract_payment_id`,`PACP`.`project_actual_contract_id` AS `project_actual_contract_id`,`PACP`.`project_actual_contract_payment_from_date` AS `project_actual_contract_payment_from_date`,`PACP`.`project_actual_contract_payment_to_date` AS `project_actual_contract_payment_to_date`,`PACP`.`project_actual_contract_payment_vendor_id` AS `project_actual_contract_payment_vendor_id`,`PACP`.`project_actual_contract_payment_amount` AS `project_actual_contract_payment_amount`,`PACP`.`project_actual_contract_payment_tds` AS `project_actual_contract_payment_tds`,`PACP`.`project_actual_contract_deposit_amount` AS `project_actual_contract_deposit_amount`,`PACP`.`project_actual_contract_payment_number` AS `project_actual_contract_payment_number`,`PACP`.`project_actual_contract_payment_length` AS `project_actual_contract_payment_length`,`PACP`.`project_actual_contract_payment_breadth` AS `project_actual_contract_payment_breadth`,`PACP`.`project_actual_contract_payment_depth` AS `project_actual_contract_payment_depth`,`PACP`.`project_actual_contract_payment_total_measurement` AS `project_actual_contract_payment_total_measurement`,`PACP`.`project_actual_contract_payment_uom` AS `project_actual_contract_payment_uom`,`PACP`.`project_actual_contract_payment_rate` AS `project_actual_contract_payment_rate`,`PACP`.`project_actual_contract_payment_status` AS `project_actual_contract_payment_status`,`PACP`.`project_actual_contract_payment_bill_no` AS `project_actual_contract_payment_bill_no`,`PACP`.`project_actual_contract_payment_billing_address` AS `project_actual_contract_payment_billing_address`,`PACP`.`project_actual_contract_payment_active` AS `project_actual_contract_payment_active`,`PACP`.`project_actual_contract_payment_remarks` AS `project_actual_contract_payment_remarks`,`PACP`.`project_actual_contract_payment_accepted_by` AS `project_actual_contract_payment_accepted_by`,`PACP`.`project_actual_contract_payment_accepted_on` AS `project_actual_contract_payment_accepted_on`,`PACP`.`project_actual_contract_payment_approved_by` AS `project_actual_contract_payment_approved_by`,`PACP`.`project_actual_contract_payment_approved_on` AS `project_actual_contract_payment_approved_on`,`PACP`.`project_actual_contract_payment_added_by` AS `project_actual_contract_payment_added_by`,`PACP`.`project_actual_contract_payment_added_on` AS `project_actual_contract_payment_added_on`,`PACP`.`project_actual_contract_payment_deposit_status` AS `project_actual_contract_payment_deposit_status`,`PACP`.`project_actual_contract_payment_deposit_accepted_by` AS `project_actual_contract_payment_deposit_accepted_by`,`PACP`.`project_actual_contract_payment_deposit_accepted_on` AS `project_actual_contract_payment_deposit_accepted_on`,`PACP`.`project_actual_contract_payment_deposit_bill_no` AS `project_actual_contract_payment_deposit_bill_no`,`PMA`.`project_manpower_agency_id` AS `project_manpower_agency_id`,`PMA`.`project_manpower_agency_name` AS `project_manpower_agency_name`,`PMA`.`project_manpower_agency_active` AS `project_manpower_agency_active`,`SUMM`.`stock_unit_id` AS `stock_unit_id`,`SUMM`.`stock_unit_name` AS `stock_unit_name`,`SCM`.`stock_company_master_id` AS `stock_company_master_id`,`SCM`.`stock_company_master_name` AS `stock_company_master_name`,`SCM`.`stock_company_master_contact_person` AS `stock_company_master_contact_person`,`SCM`.`stock_company_master_address` AS `stock_company_master_address`,`SCM`.`stock_company_master_tin_no` AS `stock_company_master_tin_no`,`SCM`.`stock_company_master_active` AS `stock_company_master_active`,`U`.`user_name` AS `added_by`,`AU`.`user_name` AS `approved_by` from (((((`project_actual_contract_payment` `PACP` join `users` `U` on((`U`.`user_id` = `PACP`.`project_actual_contract_payment_added_by`))) join `project_manpower_agency` `PMA` on((`PMA`.`project_manpower_agency_id` = `PACP`.`project_actual_contract_payment_vendor_id`))) join `stock_unit_measure_master` `SUMM` on((`SUMM`.`stock_unit_id` = `PACP`.`project_actual_contract_payment_uom`))) left join `users` `AU` on((`AU`.`user_id` = `PACP`.`project_actual_contract_payment_approved_by`))) left join `stock_company_master` `SCM` on((`SCM`.`stock_company_master_id` = `PACP`.`project_actual_contract_payment_billing_address`))) ;

--
-- VIEW  `weekly_contract_payment_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
