-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 21, 2019 at 04:24 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_legal`
--

-- --------------------------------------------------------

--
-- Structure for view `stock_approved_quotations_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_approved_quotations_list`  AS  select `sqc`.`stock_quotation_id` AS `quote_id`,`sqc`.`stock_quotation_material_id` AS `material_id`,`sqc`.`stock_quotation_indent_id` AS `indent_id`,`sqc`.`stock_quotation_amount` AS `quote_amount`,`sqc`.`stock_quotation_no` AS `user_quote_no`,`sqc`.`stock_quotation_received_date` AS `recieved_date`,`sqc`.`stock_quote_no` AS `system_quote_no`,`sqc`.`stock_quotation_vendor` AS `quote_venor_id`,`sqc`.`stock_quotation_quantity` AS `quote_qty`,`sqc`.`stock_quotation_po_qty` AS `po_qty`,`sqc`.`stock_quotation_project` AS `quote_project`,`sqc`.`stock_quotation_status` AS `quote_status`,`sqc`.`stock_quotation_doc` AS `quote_doc`,`sqc`.`stock_quotation_remarks` AS `quote_remarks`,`sqc`.`stock_quotation_active` AS `quote_active`,`sqc`.`stock_quotation_approved_by` AS `approved_by`,`sqc`.`stock_quotation_approved_on` AS `approved_on`,`sqc`.`stock_quotation_added_by` AS `added_by`,`sqc`.`stock_quotation_added_on` AS `added_on`,`sqc`.`stock_quotation_po_qty` AS `quote_po_qty`,`u`.`user_id` AS `user_id`,`u`.`user_name` AS `user_name`,`svm`.`stock_vendor_name` AS `stock_vendor_name`,`smm`.`stock_material_code` AS `stock_material_code`,`smm`.`stock_material_name` AS `stock_material_name`,`smm`.`stock_material_price` AS `stock_material_price`,`slm`.`stock_project_name` AS `stock_project_name`,`uom`.`stock_unit_name` AS `stock_unit_name`,`si`.`stock_indent_no` AS `system_indent_no`,`si`.`stock_indent_added_by` AS `indent_requested_by`,`si`.`stock_indent_added_on` AS `indent_requested_on`,`sqc`.`stock_quotation_vendor` AS `quote_vendor_id`,`sqc`.`stock_quotation_po_value` AS `quote_po_value`,`ms`.`material_stock_quantity` AS `project_material_stock_qty`,`msp`.`stock_percentage` AS `project_material_min_qty` from ((((((((`stock_quotation_compare` `sqc` left join `users` `u` on((`u`.`user_id` = `sqc`.`stock_quotation_added_by`))) join `stock_vendor_master` `svm` on((`svm`.`stock_vendor_id` = `sqc`.`stock_quotation_vendor`))) join `stock_material_master` `smm` on((`sqc`.`stock_quotation_material_id` = `smm`.`stock_material_id`))) left join `stock_indent` `si` on((`sqc`.`stock_quotation_indent_id` = `si`.`stock_indent_id`))) left join `stock_project` `slm` on((`slm`.`stock_project_id` = `sqc`.`stock_quotation_project`))) join `stock_unit_measure_master` `uom` on((`uom`.`stock_unit_id` = `smm`.`stock_material_unit_of_measure`))) left join `material_stock` `ms` on(((`sqc`.`stock_quotation_project` = `ms`.`material_stock_project`) and (`sqc`.`stock_quotation_material_id` = `ms`.`material_id`)))) left join `material_stock_percentage` `msp` on(((`sqc`.`stock_quotation_project` = `msp`.`stock_percentage_project_id`) and (`sqc`.`stock_quotation_material_id` = `msp`.`stock_percentage_material_id`)))) ;

--
-- VIEW  `stock_approved_quotations_list`
-- Data: None
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
