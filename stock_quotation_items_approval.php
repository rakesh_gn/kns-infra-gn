<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('STOCK_QUOTE_APPROVE', '365');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
  
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', STOCK_QUOTE_APPROVE, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', STOCK_QUOTE_APPROVE, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', STOCK_QUOTE_APPROVE, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', STOCK_QUOTE_APPROVE, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', STOCK_QUOTE_APPROVE, '6', '1');
    ?>

    <script>
      window.permissions = {
          view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          approve: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

    <?php
    if (isset($_REQUEST["search_status"])) {
      $search_status  = $_REQUEST["search_status"];
    }
    else {
      $search_status = '';
    }
		if(isset($_GET['hd_material_id']))
		{
			if($_GET["stxt_material"] != '')
			{
				$search_material 	  = $_GET["hd_material_id"];
				$search_material_name = $_GET["stxt_material"];
			}
			else
			{
				$search_material 	  = "";
				$search_material_name = "";
			}
		}
		else
		{
			$search_material 	  = "";
			$search_material_name = "";
		}
  	// }
    // Get material modes already added
  	$stock_material_search_data = array("material_active"=>'1');
  	$material_list = i_get_stock_material_master_list($stock_material_search_data);
  	if($material_list['status'] == SUCCESS)
  	{
  		$material_list_data = $material_list['data'];
  	}

  	else
  	{
  		$alert = $material_list["data"];

  		$alert_type = 0;
  	}

    // Get Project List
  	$stock_project_search_data = array();
  	$project_management_master_list = i_get_project_list($stock_project_search_data);
  	if($project_management_master_list['status'] == SUCCESS)
  	{
  		$project_management_master_list_data = $project_management_master_list['data'];
  	}
  	else
  	{
  		$alert = $project_management_master_list["data"];
  		$alert_type = 0;
  	}

    // Get Venor Item Mapping already added
  	$stock_vendor_item_mapping_search_data = array("item_id"=>$search_material);
  	$vendor_master_list = i_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);
  	if($vendor_master_list['status'] == SUCCESS)
  	{
  		$vendor_master_list_data = $vendor_master_list['data'];
  	}
  	else
  	{
  		$alert = $vendor_master_list["data"];
  		$alert_type = 0;
  	}
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Quotation Process</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/stock_quotation_items_approval.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:80%; left: 10%; height: 50% ;padding-left: 17px; background-color:transparent">
   <div class="modal-dialog" role="document" style="width:100%">
      <div class="modal-content" style="width:100%">
      </div>
    </div>
  </div>
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Quotation process</h3>
            </div>
            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                 <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />

         			  <span style="padding-right:15px; float:left;">

         					<input type="text" name="stxt_material" class="form-control" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search Material by name or code" value="<?php echo $search_material_name; ?>" />

         					<div id="search_results" class="dropdown-content"></div>

         			  </span>
                <select name="search_status" id="search_status" style="max-width:250px" class="form-control input-sm">
                    <option value="Waiting">Pending</option>
                    <option value="Approved">Approved</option>
                    <option value="Rejected">Rejected</option>
                </select>
                <button id="submit_button" type="button" class="btn btn-primary" onclick="redrawTable()">Submit</button>
              </form>
            </div>
            <br>
          </div>
          <?php if($view_perms_list['status'] == SUCCESS){ ?>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example" style="width:100%">
               <thead>
                 <tr>
                    <th colspan="14">&nbsp;</th>
                    <th colspan="3" class="center blue">Actions</th>
                 </tr>
                 <tr>

                   <th>#</th>
                   <th>Material Name</th>
                   <th>Material Code</th>
                   <th>Project</th>
                   
                   <th>Indent Qty</th>
                   <th>UOM</th>
                   <th>Quotation No</th>
                   <th>Indent No</th>
                   <th>Vendor Quote No</th>
                   <th>Vendor</th>
                   <th>Recieved date</th>
                   <th>Quote Added By</th>
                   <th>Vendor Price</th>
                   <th>Added on</th>
                   <th>View</th>
                   <th>A</th>
                   <th>R</th>

                </tr>
             </thead>
               </tbody>
             </table>
            </div>
          <?php }
        else{ ?>
          <div class="widget-content">
            <h1>Access Denied</h1>
            <h3>You dont have permissions to view</h3>
          </div>
          <?php } ?>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
<script>
function get_material_list()
{
	var searchstring = document.getElementById('stxt_material').value;
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}
		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);
	}
	else
	{
		document.getElementById('search_results').style.display = 'none';
	}
}
function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;



	document.getElementById('search_results').style.display = 'none';

}
</script>
</body>
</html>
