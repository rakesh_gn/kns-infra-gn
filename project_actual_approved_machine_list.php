<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID', '254');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID, '6', '1');

?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_vendor = "";
    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor = $_REQUEST["search_vendor"];
    }

     $search_machine = "";
    if(isset($_REQUEST["stxt_machine"]))
  	{
  		$search_machine = $_REQUEST["stxt_machine"];
  	}

    $search_process = "";
    if (isset($_REQUEST["search_process"])) {
        $search_process = $_REQUEST["search_process"];
    }
    $search_task = "";
    if (isset($_REQUEST['search_task'])) {
        $search_task = $_REQUEST['search_task'];
    }

    $start_date = "";
    if (isset($_REQUEST["dt_start_date"])) {
        $start_date = $_REQUEST["dt_start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["dt_end_date"])) {
        $end_date = $_REQUEST["dt_end_date"];
    }

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    // Project Manpower Vendor Master List
    $project_machine_vendor_master_search_data = array("active"=>'1');
    $project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
    if ($project_machine_vendor_list["status"] == SUCCESS) {
        $project_machine_vendor_list_data = $project_machine_vendor_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_machine_vendor_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project,"user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    $project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
    $project_task_master_list = i_get_project_task_master($project_task_master_search_data);
    if ($project_task_master_list["status"] == SUCCESS) {
        $project_task_master_list_data = $project_task_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_task_master_list["data"];
    }
    $company_list = i_get_company_list(array());
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Project Actual Machine List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js?22062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
		 <script src="datatable/project_approved_machine_list_datatable.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Bill No</h4>
          </div>
          <div class="modal-body">
            <form id="add-bill-form">
                <div class="form-group">
                  <label for="BillAddress" class="control-label">Bill Address</label>
                  <select name="billing_address" required id="billing_address" class="form-control">
                  <option value="">- - -Select Billing Address- - -</option>
                  <?php foreach($company_list['data'] as $item){  ?>
                  <option value="<?php echo $item['stock_company_master_id']; ?>">
                    <?php echo $item['stock_company_master_name']; ?>
                  </option>
                  <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="remarks" class="control-label">Remarks</label>
                  <input type="text" class="form-control" id="remarks" name="remarks" class="form-control">
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="generateMachineId()">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Daily Machine List</h3>
                    <button type="button" id="generateBillBtn"
                            onclick="generateBill()" name="items_submit"
                            class="btn btn-success pull-right btn-sm"
                            style="margin: 5px 5px 0px 0px;">Generate Bill</button>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <select id="search_vendor" name="search_vendor" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Vendor - -</option>
                        <?php
                              for ($vendor_count = 0; $vendor_count < count($project_machine_vendor_list_data); $vendor_count++) {
                                  ?>
                        <option value="<?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]; ?>" <?php if ($search_vendor == $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]) {
                                      ?> selected="selected" <?php
                                  } ?>><?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
                        <?php
                              } ?>
                        </select>

                      <select id="search_project" name="search_project" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Project - -</option>
                        <?php
                                        for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                            ?>
                        <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                                ?> selected="selected" <?php
                                            } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
                        <?php
                                        } ?>
                        </select>
                          <input class="form-control" type="hidden" name="hd_machine_id" id="hd_machine_id" value="<?php echo $search_machine;?>" />
                          <input class="form-control" type="text" name="stxt_machine"  autocomplete="off" id="stxt_machine" onkeyup="return get_machine_list();" placeholder="Search Machine by name or code" />
                          <div id="search_results" class="dropdown-content"></div>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo $start_date; ?>"  class="form-control input-sm"/>
                      <input type="date" id="end_date" name="dt_end_date" value="<?php echo $end_date; ?>"  class="form-control input-sm"/>
                      <button type="button" onclick="tableDraw()" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                <tr>
                <th>#</th>
                <th>Agency</th>
                <th>Project</th>
                <th>Process</th>
                <th>Task Name</th>
                <th>Uom</th>
                <th>Work Type</th>
                <th>Road</th>
                <th>Machine Type</th>
                <th>Machine</th>
                <th>Hours Worked</th>
                <th>Off Time</th>
                <th>Machine Rate</th>
                <th>Measurment</th>
                <th>%</th>
                <th>Additional Cost</th>
                <th>Machine Bata</th>
                <th>Machine Issued Fuel</th>
                <th>Remarks</th>
                <th>Approved On</th>
                <th>Approved By</th>
                <th>Actual Start Date</th>
                <th>Actual End Date</th>
                <th>Total Amount</th>
                <th><input type="checkbox" id="selectAllCheckbox"/ onchange="toggleSelection(this)"></th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
