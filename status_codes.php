<?php
/*
FILE		: statuscodes.php
CREATED ON	: 14-April-2014
CREATED BY	: Nitin Kashyap
PURPOSE   	: All status codes such as success, different error codes etc.
*/

// General Status Codes - Start
define('SUCCESS',0);
define('FAILURE',1);
define('EXCEPTION',2);
define('NO_DATA',-1);
define('UNSUPPORTED_FEATURE',-2);
// General Status Codes - End

// Database Status Codes - Start
define('DB_SUCCESS',-101);
define('DB_CONN_FAILURE',-102);
define('DB_RECORD_ALREADY_EXISTS',-103); // By karthik
define('DB_NO_RECORD',-104); // By karthik
// Database Status Codes - End

// URL related Status Codes - Start
define('PROBABLE_INVALID_URL',-501);
define('INVALID_URL',-502);
// URL related Status Codes - End

// User status codes - Start
define('INVALID_PASSWORD',-601);
define('INVALID_USERNAME',-602);
// User status codes - End
?>