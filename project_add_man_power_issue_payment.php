
<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 13-April-2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET['payment_manpower_id']))
	{
		$payment_manpower_id = $_GET['payment_manpower_id'];
	}
	else
	{
		$payment_manpower_id = '';
	}
	
	if(isset($_GET['vendor_id']))
	{
		$vendor_id = $_GET['vendor_id'];
	}
	else
	{
		$vendor_id = '';
	}
	/* QUERY STRING - END */
	// Capture the form data
	if(isset($_POST["add_project_man_power_issue_payment_submit"]))
	{
        $payment_manpower_id  = $_POST["hd_payment_id"];
		$amount               = $_POST["amount"];
		$deduction            = $_POST["deduction"];
		$total_payable_amount = $_POST["hd_payable_amount"];
		$balance_amount       = $_POST["hd_balance_amount"];
		$vendor_id            = $_POST["hd_vendor_id"];
		$payment_mode         = $_POST["ddl_mode"];
		$instrument_details   = $_POST["txt_details"];
		$remarks 	          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($amount != "") && ($vendor_id != "") && ($payment_mode != ""))
		{
			$project_man_power_issue_payment_iresult = i_add_project_man_power_issue_payment($payment_manpower_id,$total_payable_amount,$deduction,$vendor_id,$payment_mode,$instrument_details,$remarks,$user);
			
			if($project_man_power_issue_payment_iresult["status"] == SUCCESS)
			{
				if($balance_amount == $amount)
				{
					$project_actual_payment_manpower_update_data = array("status"=>'Payment Issued');
					i_update_project_actual_payment_manpower($payment_manpower_id,$project_actual_payment_manpower_update_data);
					header("location:project_accept_payment_manpower_list.php");
				}
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $project_man_power_issue_payment_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	//Get Project Man Power Issue Payment List
	$project_man_power_issue_payment_search_data = array("active"=>'1');
	$project_man_power_issue_payment_list = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
	if($project_man_power_issue_payment_list["status"] == SUCCESS)
	{
		$project_man_power_issue_payment_list_data = $project_man_power_issue_payment_list["data"];
	}
	
	// Get project_machine_vendor_master modes already added
	$project_machine_vendor_master_search_data = array("active"=>'1');
	$project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
	if($project_machine_vendor_master_list['status'] == SUCCESS)
	{
		$project_machine_vendor_master_list_data = $project_machine_vendor_master_list['data'];
	}
	else
	{
		$alert = $project_machine_vendor_master_list["data"];
		$alert_type = 0;
	}
	
	//Get Payment Mode 
	$payment_mode_list =  i_get_payment_mode_list('','1');
	if($payment_mode_list['status'] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list['data'];		
	}	
	else
	{
		$alert = $payment_mode_list["data"];
		$alert_type = 0;		
	}
	
	// Get Project  Payment ManPower modes already added
	$project_actual_payment_manpower_search_data = array("active"=>'1',"status"=>"Accepted","manpower_id"=>$payment_manpower_id,"start"=>'-1');
	$project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
	if($project_actual_payment_manpower_list['status'] == SUCCESS)
	{
		$project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
		$amount = $project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_amount"];
		$vendor = $project_actual_payment_manpower_list_data[0]["project_manpower_agency_name"];
	}
	else
	{
		$amount ="";
		$vendor ="";
	}
	
	// Get Project  Payment ManPower modes already added
	$issued_amount = 0;
	$deduction = 0;
	$project_man_power_issue_payment_search_data = array("man_power_id"=>$payment_manpower_id);
	$project_actual_payment_issue_list = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
	if($project_actual_payment_issue_list['status'] == SUCCESS)
	{
		$project_actual_machine_payment_issue_list_data = $project_actual_payment_issue_list["data"];
		for($issue_count = 0 ; $issue_count < count($project_actual_machine_payment_issue_list_data) ; $issue_count++)
		{
			$issued_amount = $issued_amount + $project_actual_machine_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_amount"];
			$deduction = $deduction + $project_actual_machine_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_deduction"];
		}
	}
	else
	{
		$issued_amount = 0;
	}
	$balance_amount = round(($amount - $issued_amount),2);
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Project Man Power Issue Payment </title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Project Man Power Issue Payment &nbsp;&nbsp;&nbsp;&nbsp;Total Amount : <?php echo $amount ; ?> &nbsp;&nbsp;&nbsp;&nbsp;Total Issued Amount : <?php echo $issued_amount ; ?> &nbsp;&nbsp;&nbsp;&nbsp;Deduction: <?php echo $deduction ; ?> &nbsp;&nbsp;&nbsp;&nbsp;Vendor : <?php echo $vendor ; ?> &nbsp;&nbsp;&nbsp;&nbsp; Balance: <?php echo $balance_amount ; ?> </h3><!--<span style="float:right; padding-right:20px;"><a href="project_man_power_issue_payment_list.php">Project Man Power Issue Payment List</a>--></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Man Power Issue Payment</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_project_man_power_issue_payment_form" class="form-horizontal" method="post" action="project_add_man_power_issue_payment.php">
								<input type="hidden" value="<?php echo $payment_manpower_id; ?>" name="hd_payment_id" />
								<input type="hidden" value="<?php echo $vendor_id; ?>" name="hd_vendor_id" />
								<input type="hidden" value="<?php echo $balance_amount; ?>" name="hd_balance_amount" />
									<fieldset>					
									
										<div class="control-group">											
											<label class="control-label" for="amount">Amount</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="amount" id="amount" onkeyup="return check_validity('<?php echo $amount ;?>','<?php echo $issued_amount ;?>','<?php echo $deduction ;?>');" placeholder="Amount">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="deduction">Deduction</label>
											<div class="controls">
												<input type="number" class="span6" step="0.01" name="deduction" onkeyup="return payable_amount('<?php echo $amount ;?>');"  id="deduction" placeholder="Deduction">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<input type="hidden" name="hd_payable_amount" id="hd_payable_amount" />
										<div class="control-group">											
											<label class="control-label" for="total_payable_amount">Payable Amount</label>
											<div class="controls">
												<input type="number" min="0.01" step="0.01" class="span6" disabled name="total_payable_amount" id="total_payable_amount" placeholder="Payable Amount">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_mode">Payment Mode*</label>
											<div class="controls">
												<select name="ddl_mode" required>
												<option>- - -Select Mode- - -</option>
												<?php
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
													?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>"><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_details">Instrument Details</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_details">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                        <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_project_man_power_issue_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      		
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function check_validity(total_amount,issued_amount,deduction)
{
	var amount = parseInt(document.getElementById('amount').value);
	
	var cur_amount = parseInt(amount);
	var total_amount  = parseInt(total_amount);
	var deduction  = parseInt(deduction);
	var issued_amount      = parseInt(issued_amount);
	if((cur_amount + issued_amount) > total_amount)
	{
		document.getElementById('amount').value = 0;
		alert('Cannot release greater than total value');
	}
}
function payable_amount()
{
	var deduction = parseInt(document.getElementById('deduction').value);
	var cur_amount = parseFloat(document.getElementById('amount').value);
	
	var deduction = parseInt(deduction);
	//var cur_amount  = parseInt(cur_amount);
	
	if(deduction > cur_amount)
	{
		document.getElementById('deduction').value = 0;
		alert('Cannot release greater than Amount');
	}
	alert(cur_amount);
	total_payable_amount = cur_amount - deduction;
	document.getElementById('total_payable_amount').value = total_payable_amount;
	document.getElementById('hd_payable_amount').value = total_payable_amount;
		
}
</script>


  </body>

</html>