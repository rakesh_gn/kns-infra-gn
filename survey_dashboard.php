<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16-Oct-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('DASHBOARD_SURVEY_FUNC_ID','346');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list = i_get_user_perms($user,'',DASHBOARD_SURVEY_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Get Survey Details already added
	$survey_details_search_data = array("active"=>'1');
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Dashboard</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Survey Dashboard List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Delay Reason</th>
					<th>Village</th>
					<th>Survey No</th>
					<th>Total Extent</th>
					<th>Start Date</th>
					<th colspan="1" style="text-align:center;">Actions</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_details_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($survey_details_list_data); $count++)
					{
						$sl_no++;	
						
						    // Get Survey File already added
							$survey_file_search_data = array("active"=>'1',$survey_details_list_data[$count]["survey_details_id"]);
							$survey_file_list = i_get_survey_file($survey_file_search_data);
							if($survey_file_list['status'] == SUCCESS)
							{
								$survey_file_list_data = $survey_file_list["data"];
								$extent    = $survey_file_list_data[0]["survey_master_extent"];
								$survey_no = $survey_file_list_data[0]["survey_master_survey_no"];
							}
							else
							{
								$extent = '';
								$survey_no = '';
							}
							
							// Get process for this project
							$survey_process_search_data = array("survey_id"=>$survey_details_list_data[$count]["survey_details_id"]);
							$survey_process = i_get_survey_process($survey_process_search_data);
							if($survey_process['status'] == SUCCESS)
							{
								$survey_process_data = $survey_process["data"];
								$survey_process_id = $survey_process_data[0]["survey_process_id"];
								
								// Get Survey Process Delay modes already added
								$survey_process_delay_search_data = array("active"=>'1',"process_id"=>$survey_process_id);
								$survey_process_delay_list = i_get_survey_process_delay($survey_process_delay_search_data);
								if($survey_process_delay_list['status'] == SUCCESS)
								{
									$survey_process_delay_list_data = $survey_process_delay_list['data'];
									$delay_reason_name = $survey_process_delay_list_data[0]["survey_delay_reason_master_name"];
								}
								else
								{
									$delay_reason_name = "";
								}
								
							}
						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $delay_reason_name ; ?></td>
					<td><?php echo $survey_details_list_data[$count]["village_name"]; ?></td>
					<td><?php echo $survey_no; ?></td>
					<td><?php echo $extent; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_details_list_data[$count][
					"survey_details_planned_start_date"])); ?></td>
					<td><a href="survey_file_list.php?survey_id=<?php echo $survey_details_list["data"][$count]["survey_details_id"]; ?>">Survey List</a></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Survey Dashboard added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
