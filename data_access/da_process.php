<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. In db_get_process_list, name search should be wildcard search
*/

/*
PURPOSE : To add new process type
INPUT 	: Process Name, Module, Is this a bulk process, Added By
OUTPUT 	: Process Type id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_process_type_legal($process_name,$module,$is_bulk,$added_by)
{
	// Query
    $process_type_iquery = "insert into process_master (process_name,process_module,process_is_bulk,process_type_active,process_added_by,process_added_on) values (:process_name,:module,:is_bulk,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_type_istatement = $dbConnection->prepare($process_type_iquery);
        
        // Data
        $process_type_idata = array(':process_name'=>$process_name,':module'=>$module,':is_bulk'=>$is_bulk,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $process_type_istatement->execute($process_type_idata);
		$process_type_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_type_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get processes type list
INPUT 	: Process Name, Module, Added By, Start Date(for added on), End Date(for added on), Status
OUTPUT 	: List of processes
BY 		: Nitin Kashyap
*/
function db_get_process_type_list($process_name,$module,$added_by,$start_date,$end_date,$status="1",$is_bulk="")
{
	$get_process_list_squery_base = "select * from process_master PM left outer join users U on U.user_id = PM.process_added_by";
	
	$get_process_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_process_list_sdata = array();
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_name=:process_name";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_name=:process_name";				
		}
		
		// Data
		$get_process_list_sdata[':process_name']  = $process_name;
		
		$filter_count++;
	}
	
	if($module != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_module=:module";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_module=:module";				
		}
		
		// Data
		$get_process_list_sdata[':module']  = $module;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_added_by=:added_by";				
		}
		
		// Data
		$get_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_added_on >= :start_date";				
		}
		
		//Data
		$get_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_added_on <= :end_date";				
		}
		
		//Data
		$get_process_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_type_active = :status";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_type_active = :status";				
		}
		
		//Data
		$get_process_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($is_bulk != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." where process_is_bulk = :is_bulk";								
		}
		else
		{
			// Query
			$get_process_list_squery_where = $get_process_list_squery_where." and process_is_bulk = :is_bulk";				
		}
		
		//Data
		$get_process_list_sdata[':is_bulk']  = $is_bulk;
		
		$filter_count++;
	}
	
	$get_process_list_squery = $get_process_list_squery_base.$get_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_process_list_sstatement = $dbConnection->prepare($get_process_list_squery);
		
		$get_process_list_sstatement -> execute($get_process_list_sdata);
		
		$get_process_list_sdetails = $get_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get list of process plans
INPUT 	: Process Plan ID, File ID, Process Type, Party Name, Survey Number, Village, Extent, File Type, Assigned To, Added By, Start Date(for added on), End Date(for added on), Status (completed or not), Order for sorting
OUTPUT 	: List of legal process plans
BY 		: Nitin Kashyap
*/
function db_get_legal_process_plan_list($process_plan_id,$file_id,$type,$party_name,$survey_no,$village,$extent,$file_type,$assigned_to,$added_by,$start_date,$end_date,$status,$order,$active="1")
{
	$get_process_plan_list_squery_base = "select * from process_plan_legal PPL";
	
	$get_process_plan_list_squery_join = " inner join process_master PM on PM.process_master_id = PPL.process_plan_type inner join users U on U.user_id = PPL.process_plan_legal_assigned_to inner join files F on F.file_id = PPL.process_plan_legal_file_id";
	
	$get_process_plan_list_squery_where = "";
	
	$get_task_plan_legal_list_squery_order = "";
	
	$filter_count = 0;
	
	// Data
	$get_process_plan_list_sdata = array();
	
	if($process_plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_id=:process_plan_id";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_id=:process_plan_id";				
		}
		
		// Data
		$get_process_plan_list_sdata[':process_plan_id']  = $process_plan_id;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_file_id=:file_id";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_file_id=:file_id";				
		}
		
		// Data
		$get_process_plan_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_type=:type";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_type=:type";				
		}
		
		// Data
		$get_process_plan_list_sdata[':type']  = $type;
		
		$filter_count++;
	}
	
	if($party_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where F.file_land_owner=:party_name";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and F.file_land_owner=:party_name";				
		}
		
		// Data
		$get_process_plan_list_sdata[':party_name']  = $party_name;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where F.file_survey_number=:survey_no";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and F.file_survey_number=:survey_no";				
		}
		
		// Data
		$get_process_plan_list_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where F.file_village=:village";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and F.file_village=:village";				
		}
		
		// Data
		$get_process_plan_list_sdata[':village']  = $village;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where F.file_extent=:extent";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and F.file_extent=:extent";				
		}
		
		// Data
		$get_process_plan_list_sdata[':extent']  = $extent;
		
		$filter_count++;
	}
	
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_assigned_to=:assigned_to";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_assigned_to=:assigned_to";				
		}
		
		// Data
		$get_process_plan_list_sdata[':assigned_to']  = $assigned_to;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_added_by=:added_by";				
		}
		
		// Data
		$get_process_plan_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_added_on >= :start_date";				
		}
		
		//Data
		$get_process_plan_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_added_on <= :end_date";				
		}
		
		//Data
		$get_process_plan_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_plan_legal_completed = :status";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_plan_legal_completed = :status";				
		}
		
		//Data
		$get_process_plan_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." where process_legal_active = :active";								
		}
		else
		{
			// Query
			$get_process_plan_list_squery_where = $get_process_plan_list_squery_where." and process_legal_active = :active";				
		}
		
		//Data
		$get_process_plan_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	// Sorting Order
	if($order != "")
	{
		if($order == 1)
		{
			$get_task_plan_legal_list_squery_order = " order by process_plan_legal_id asc limit 0,1";
		}
		else		
		{
			$get_task_plan_legal_list_squery_order = " order by process_plan_legal_id desc limit 0,1";
		}
	}
	
	$get_process_plan_list_squery = $get_process_plan_list_squery_base.$get_process_plan_list_squery_join.$get_process_plan_list_squery_where.$get_task_plan_legal_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_process_plan_list_sstatement = $dbConnection->prepare($get_process_plan_list_squery);
		
		$get_process_plan_list_sstatement -> execute($get_process_plan_list_sdata);
		
		$get_process_plan_list_sdetails = $get_process_plan_list_sstatement -> fetchAll();
		
		if(FALSE === $get_process_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_process_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_process_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new legal process plan
INPUT 	: Process Type, File ID, Start Date, Assigned To, Added By
OUTPUT 	: Process Plan id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_process_plan_legal($process_type,$file_id,$start_date,$assigned_to,$added_by)
{
	// Query
    $process_plan_iquery = "insert into process_plan_legal (process_plan_type,process_plan_legal_file_id,process_plan_legal_start_date,process_plan_legal_completed,process_plan_legal_assigned_to,process_legal_active,process_plan_legal_added_by,process_plan_legal_added_on) values (:type,:file_id,:start_date,:completed,:assigned_to,:active,:added_by,:now)";    

    try
    {
        $dbConnection = get_conn_handle();
        
        $process_plan_istatement = $dbConnection->prepare($process_plan_iquery);
        
        // Data		
        $process_plan_idata = array(':type'=>$process_type,':file_id'=>$file_id,':start_date'=>$start_date,':completed'=>'0',':assigned_to'=>$assigned_to,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		
		$dbConnection->beginTransaction();
        $process_plan_istatement->execute($process_plan_idata);
		$process_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_plan_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE: To update process plan
INPUT  : Process Plan ID, Completed
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_process_plan($process_plan_id,$completed)
{
	// Query
    $process_plan_uquery = "update process_plan_legal set process_plan_legal_completed = :completed where process_plan_legal_id=:process_plan_id";   
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_plan_ustatement = $dbConnection->prepare($process_plan_uquery);
        
        // Data
        $process_plan_udata = array(':completed'=>$completed,':process_plan_id'=>$process_plan_id);		
        
        $process_plan_ustatement -> execute($process_plan_udata);
		
		$updated_rows = $process_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To enable/disable process plan
INPUT  : Process Plan ID, File ID, Active
OUTPUT : SUCCESS if process plan enabled/disabled; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_enable_disable_process_plan($process_plan_id,$file_id,$active)
{
	// Query 
	if($file_id != "")
	{
		$process_plan_uquery = "update process_plan_legal set process_legal_active = :active where process_plan_legal_file_id = :file_id";   
		// Data
        $process_plan_udata = array(':file_id'=>$file_id,':active'=>$active);
	}
	else if($process_plan_id != "")
	{
		$process_plan_uquery = "update process_plan_legal set process_legal_active = :active where process_plan_legal_id=:process_plan_id";   
		// Data
        $process_plan_udata = array(':process_plan_id'=>$process_plan_id,':active'=>$active);
	}
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_plan_ustatement = $dbConnection->prepare($process_plan_uquery);       		
        
        $process_plan_ustatement -> execute($process_plan_udata);
		
		$updated_rows = $process_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To enable/disable process type
INPUT  : Process Type ID, Active
OUTPUT : SUCCESS if process type enabled/disabled; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_enable_disable_process_type($process_type_id,$active)
{
	// Query 	
	$process_type_uquery = "update process_master set process_type_active = :active where process_master_id = :process_type";   
	// Data
	$process_type_udata = array(':process_type'=>$process_type_id,':active'=>$active);
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_type_ustatement = $dbConnection->prepare($process_type_uquery);       		
        
        $process_type_ustatement -> execute($process_type_udata);
		
		$return["status"] = DB_NO_RECORD;
		$return["data"]   = '';
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To initiate bulk processing of files
INPUT 	: Process Type, Start Date, Assigned To, Added By
OUTPUT 	: Bulk Process ID, success or failure message
BY 		: Sonakshi
*/
function db_add_legal_bulk_process($legal_bulk_process_type,$legal_process_start_date,$legal_process_assigned_to,$legal_bulk_process_added_by)
{
	//Query
	$legal_bulk_iquery="insert into legal_bulk_process (legal_bulk_process_type,legal_process_start_date,legal_process_assigned_to,legal_bulk_process_completed,legal_bulk_process_active,legal_bulk_process_added_by,legal_bulk_process_added_on) values (:legal_bulk_process_type,:legal_process_start_date,:legal_process_assigned_to,:is_completed,:legal_bulk_process_active,:legal_bulk_process_added_by,:legal_bulk_process_added_on)";
	
	try
	{
        $dbConnection = get_conn_handle();
        
        $legal_bulk_istatement = $dbConnection->prepare($legal_bulk_iquery);
        
        // Data
        $legal_bulk_idata = array(':legal_bulk_process_type'=>$legal_bulk_process_type,':legal_process_start_date'=>$legal_process_start_date,':legal_process_assigned_to'=>$legal_process_assigned_to,':is_completed'=>'0',':legal_bulk_process_active'=>'1',':legal_bulk_process_added_by'=>$legal_bulk_process_added_by,':legal_bulk_process_added_on'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $legal_bulk_istatement->execute($legal_bulk_idata);
		$legal_bulk_process_type = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $legal_bulk_process_type;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "not inserted";
    }
    
    return $return;
}
	
/*
PURPOSE : To get bulk process file list
INPUT 	: Bulk Process Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Sonakshi
*/
function db_get_legal_bulk_process($legal_bulk_search_data)
{
	// Extract all input parameters
	if(array_key_exists("bulk_process_id",$legal_bulk_search_data))
	{
		$legal_bulk_process_id = $legal_bulk_search_data["bulk_process_id"];
	}
	else
	{
		$legal_bulk_process_id = "";
	}
	
	if(array_key_exists("legal_bulk_process_type",$legal_bulk_search_data))
	{
		$legal_bulk_process_type = $legal_bulk_search_data["legal_bulk_process_type"];
	}
	else
	{
		$legal_bulk_process_type = "";
	}
	
	if(array_key_exists("legal_process_start_date",$legal_bulk_search_data))
	{
		$legal_process_start_date = $legal_bulk_search_data["legal_process_start_date"];
	}
	else
	{
		$legal_process_start_date = "";
	}
	
	if(array_key_exists("legal_process_assigned_to",$legal_bulk_search_data))
	{
		$legal_process_assigned_to = $legal_bulk_search_data["legal_process_assigned_to"];
	}
	else
	{
		$legal_process_assigned_to = "";
	}
	
	if(array_key_exists("legal_bulk_process_active",$legal_bulk_search_data))
	{
		$legal_bulk_process_active = $legal_bulk_search_data["legal_bulk_process_active"];
	}
	else
	{
		$legal_bulk_process_active = "";
	}
	
	if(array_key_exists("legal_bulk_process_added_by",$legal_bulk_search_data))
	{
		$legal_bulk_process_added_by = $legal_bulk_search_data["legal_bulk_process_added_by"];
	}
	else
	{
		$legal_bulk_process_added_by = "";
	}
	
	if(array_key_exists("legal_bulk_process_added_on",$legal_bulk_search_data))
	{
		$legal_bulk_process_added_on = $legal_bulk_search_data["legal_bulk_process_added_on"];
	}
	else
	{
		$legal_bulk_process_added_on = "";
	}
	
	if(array_key_exists("completed",$legal_bulk_search_data))
	{
		$legal_bulk_process_completed = $legal_bulk_search_data["completed"];
	}
	else
	{
		$legal_bulk_process_completed = "";
	}
	
	if(array_key_exists("file_no",$legal_bulk_search_data))
	{
		$legal_bulk_process_file_no = $legal_bulk_search_data["file_no"];
	}
	else
	{
		$legal_bulk_process_file_no = "";
	}
	
	if(array_key_exists("file_id",$legal_bulk_search_data))
	{
		$legal_bulk_process_file_id = $legal_bulk_search_data["file_id"];
	}
	else
	{
		$legal_bulk_process_file_id = "";
	}
	
	if(array_key_exists("survey_no",$legal_bulk_search_data))
	{
		$legal_bulk_process_survey_no = $legal_bulk_search_data["survey_no"];
	}
	else
	{
		$legal_bulk_process_survey_no = "";
	}
	
	$get_legal_bulk_squery_base = "select * from legal_bulk_process LBP inner join process_master PM on PM.process_master_id = LBP.legal_bulk_process_type inner join users U on U.user_id = LBP.legal_process_assigned_to";
	
	$get_legal_bulk_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_legal_bulk_list_sdata = array();
	
	if($legal_bulk_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_id=:process_id";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_id=:process_id";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':process_id']  = $legal_bulk_process_id;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_type=:legal_bulk_process_type";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_type=:legal_bulk_process_type";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_bulk_process_type']  = $legal_bulk_process_type;
		
		$filter_count++;
	}
	
	if($legal_process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_process_start_date=:legal_process_start_date";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_process_start_date=:legal_process_start_date";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_process_start_date']  = $legal_process_start_date;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_active=:legal_bulk_process_active";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_active=:legal_bulk_process_active";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_bulk_process_active']  = $legal_bulk_process_active;
		
		$filter_count++;
	}
	
	if($legal_process_assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_process_assigned_to=:legal_process_assigned_to";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_process_assigned_to=:legal_process_assigned_to";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_process_assigned_to']  = $legal_process_assigned_to;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_added_by=:legal_bulk_process_added_by";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_added_by=:legal_bulk_process_added_by";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_bulk_process_added_by']  = $legal_bulk_process_added_by;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_added_on=:legal_bulk_process_added_on";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_added_on=:legal_bulk_process_added_on";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':legal_bulk_process_added_on']  = $legal_bulk_process_added_on;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_completed != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_completed=:completed";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_completed=:completed";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':completed']  = $legal_bulk_process_completed;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_file_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and F.file_number like :file_no)";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and F.file_number like :file_no)";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':file_no']  = '%'.$legal_bulk_process_file_no.'%';
		
		$filter_count++;
	}
	
	if($legal_bulk_process_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and LBPF.legal_bulk_process_file_id = :file_id)";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and LBPF.legal_bulk_process_file_id = :file_id)";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':file_id']  = $legal_bulk_process_file_id;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." where legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and F.file_survey_number like :survey_no)";								
		}
		else
		{
			// Query
			$get_legal_bulk_squery_where = $get_legal_bulk_squery_where." and legal_bulk_process_id IN (select legal_bulk_process_files_process_id from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id where LBPF.legal_bulk_process_file_active = '1' and F.file_survey_number like :survey_no)";				
		}
		
		// Data
		$get_legal_bulk_list_sdata[':survey_no']  = '%'.$legal_bulk_process_survey_no.'%';
		
		$filter_count++;
	}
	
	$get_legal_bulk_list_squery = $get_legal_bulk_squery_base.$get_legal_bulk_squery_where;			
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_legal_bulk_list_sstatement = $dbConnection->prepare($get_legal_bulk_list_squery);
		
		$get_legal_bulk_list_sstatement -> execute($get_legal_bulk_list_sdata);
		
		$get_legal_bulk_list_sdetails = $get_legal_bulk_list_sstatement-> fetchAll();
		
		if(FALSE === $get_legal_bulk_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_legal_bulk_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_legal_bulk_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add files for a bulk process
INPUT 	: File ID,Process ID,Assigned TO.
OUTPUT 	: File Process Mapping ID, success or failure message
BY 		: Sonakshi
*/

function db_add_legal_bulk_files($legal_bulk_process_files_process_id,$legal_bulk_process_file_id,$legal_bulk_process_files_assigned_to,$legal_bulk_process_files_added_by)
{
	$legal_bulk_files_iquery= "insert into legal_bulk_process_files (legal_bulk_process_files_process_id,legal_bulk_process_file_id,legal_bulk_process_files_assigned_to,legal_bulk_process_file_active,legal_bulk_process_files_added_by,legal_bulk_process_files_added_on) values(:legal_bulk_process_files_process_id,:legal_bulk_process_file_id,:legal_bulk_process_files_assigned_to,:active,:legal_bulk_process_files_added_by,:legal_bulk_process_files_added_on)";
	
	try{
		
		$dbconnection = get_conn_handle();
		$legal_files_istatement=$dbconnection->prepare($legal_bulk_files_iquery);
		
		$legal_files_idata = array(':legal_bulk_process_files_process_id'=>$legal_bulk_process_files_process_id,':legal_bulk_process_file_id'=>$legal_bulk_process_file_id,':legal_bulk_process_files_assigned_to'=>$legal_bulk_process_files_assigned_to,':active'=>'1',':legal_bulk_process_files_added_by'=>$legal_bulk_process_files_added_by,':legal_bulk_process_files_added_on'=>date("Y-m-d H:i:s"));
		
		$dbconnection->beginTransaction();
		$legal_files_istatement->execute($legal_files_idata);
		$legal_bulk_process_files_process_id = $dbconnection->lastInsertId();
		$dbconnection->commit(); 
        
		$return["status"] = SUCCESS;
		$return["data"]   = $legal_bulk_process_files_process_id;		
	}
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}
	
/*
PURPOSE : To get bulk process files
INPUT 	: Process Files Search Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Sonakshi
*/
function db_get_legal_bulk_files($legal_bulk_files_search_data)
{
	// Extract all input parameters
	if(array_key_exists("legal_bulk_process_files_id",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_files_id = $legal_bulk_files_search_data["legal_bulk_process_files_id"];
	}
	else
	{
		$legal_bulk_process_files_id = "";
	}
	
	if(array_key_exists("legal_bulk_process_files_process_id",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_files_process_id = $legal_bulk_files_search_data["legal_bulk_process_files_process_id"];
	}
	else
	{
		$legal_bulk_process_files_process_id = "";
	}
	
	if(array_key_exists("legal_bulk_process_file_id",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_file_id = $legal_bulk_files_search_data["legal_bulk_process_file_id"];
	}
	else
	{
		$legal_bulk_process_file_id = "";
	}
	
	if(array_key_exists("legal_bulk_process_files_assigned_to",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_files_assigned_to = $legal_bulk_files_search_data["legal_bulk_process_files_assigned_to"];
	}
	else
	{
		$legal_bulk_process_files_assigned_to = "";
	}
	
	if(array_key_exists("process_status",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_status = $legal_bulk_files_search_data["process_status"];
	}
	else
	{
		$legal_bulk_process_status = "";
	}
	
	if(array_key_exists("active",$legal_bulk_files_search_data))
	{
		$active = $legal_bulk_files_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("legal_bulk_process_files_added_by",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_files_added_by = $legal_bulk_files_search_data["legal_bulk_process_files_added_by"];
	}
	else
	{
		$legal_bulk_process_files_added_by = "";
	}
	
	if(array_key_exists("legal_bulk_process_files_added_on",$legal_bulk_files_search_data))
	{
		$legal_bulk_process_files_added_on = $legal_bulk_files_search_data["legal_bulk_process_files_added_on"];
	}
	else
	{
		$legal_bulk_process_files_added_on = "";
	}
	
	$get_legal_files_squery_base = "select * from legal_bulk_process_files LBPF inner join files F on F.file_id = LBPF.legal_bulk_process_file_id inner join legal_bulk_process LBP on LBP.legal_bulk_process_id = LBPF.legal_bulk_process_files_process_id left outer join village_master VM on VM.village_id = F.file_village";
	
	$get_legal_files_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_legal_files_sdata = array();
	
	if($legal_bulk_process_files_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_files_id=:legal_bulk_process_files_id";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_files_id=:legal_bulk_process_files_id";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_files_id']  = $legal_bulk_process_files_id;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_files_process_id!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_files_process_id=:legal_bulk_process_files_process_id";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_files_process_id=:legal_bulk_process_files_process_id";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_files_process_id']  = $legal_bulk_process_files_process_id;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_file_id=:legal_bulk_process_file_id";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_file_id=:legal_bulk_process_file_id";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_file_id']  = $legal_bulk_process_file_id;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_files_assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_files_assigned_to=:legal_bulk_process_files_assigned_to";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_files_assigned_to=:legal_bulk_process_files_assigned_to";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_files_assigned_to']  = $legal_bulk_process_files_assigned_to;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_completed=:legal_bulk_process_status";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_completed=:legal_bulk_process_status";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_status']  = $legal_bulk_process_status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_file_active = :active";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_file_active = :active";				
		}
		
		// Data
		$get_legal_files_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_files_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_files_added_by=:legal_bulk_process_files_added_by";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_files_added_by=:legal_bulk_process_files_added_by";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_files_added_by']  = $legal_bulk_process_files_added_by;
		
		$filter_count++;
	}
	
	if($legal_bulk_process_files_added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." where legal_bulk_process_files_added_on=:legal_bulk_process_files_added_on";								
		}
		else
		{
			// Query
			$get_legal_files_squery_where = $get_legal_files_squery_where." and legal_bulk_process_files_added_on=:legal_bulk_process_files_added_on";				
		}
		
		// Data
		$get_legal_files_sdata[':legal_bulk_process_files_added_on']  = $legal_bulk_process_files_added_on;
		
		$filter_count++;
	}
	
	$get_legal_files_squery = $get_legal_files_squery_base.$get_legal_files_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_legal_files_sstatement = $dbConnection->prepare($get_legal_files_squery);
		
		$get_legal_files_sstatement -> execute($get_legal_files_sdata);
		
		$get_legal_files_sdetails = $get_legal_files_sstatement -> fetchAll();
		
		if(FALSE === $get_legal_files_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_legal_files_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_legal_files_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE: To update bulk process plan
INPUT  : Process Plan ID, Completed
OUTPUT : SUCCESS if bulk process plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_bulk_process_plan($process_plan_id,$completed)
{
	// Query
    $process_plan_uquery = "update legal_bulk_process set legal_bulk_process_completed = :completed where legal_bulk_process_id=:process_plan_id";   
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_plan_ustatement = $dbConnection->prepare($process_plan_uquery);
        
        // Data
        $process_plan_udata = array(':completed'=>$completed,':process_plan_id'=>$process_plan_id);	
        
        $process_plan_ustatement -> execute($process_plan_udata);
		
		$updated_rows = $process_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new documents
INPUT 	: Document Process ID, Document Type, Added By
OUTPUT 	: Document Id, success or failure message
BY 		: Sonakshi D
*/
function db_add_legal_bulk_documents($bulk_document_process_id,$bulk_document_type,$bulk_document_remarks,$bulk_document_added_by)
{
	$legal_bulk_document_iquery = "insert into legal_bulk_process_documents (bulk_document_process_id,bulk_document_type,bulk_document_status,bulk_document_remarks,bulk_document_added_by,bulk_document_added_on,bulk_document_updated_by,bulk_document_updated_on) values (:bulk_document_process_id,:bulk_document_type,:bulk_document_status,:bulk_document_remarks,:bulk_document_added_by,:bulk_document_added_on,:updated_by,:updated_on)";
	
	 try
    {
        $dbConnection = get_conn_handle();
        
        $legal_bulk_document_istatement = $dbConnection->prepare($legal_bulk_document_iquery);
        
        // Data
        $legal_bulk_document_idata = array(':bulk_document_process_id'=>$bulk_document_process_id,':bulk_document_type'=>$bulk_document_type,':bulk_document_status'=>'0',':bulk_document_remarks'=>$bulk_document_remarks,':bulk_document_added_by'=>$bulk_document_added_by,':bulk_document_added_on'=>date("Y-m-d H:i:s"),':updated_by'=>'',':updated_on'=>'');	
		
		$dbConnection->beginTransaction();
        $legal_bulk_document_istatement->execute($legal_bulk_document_idata);
		$bulk_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $bulk_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Legal Documents
INPUT 	: Document Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Sonakshi D
*/
function db_get_legal_bulk_documents($legal_process_search_data)
{
	// Extract all input parameters
	if(array_key_exists("bulk_document_process_id",$legal_process_search_data))
	{
		$bulk_document_process_id = $legal_process_search_data["bulk_document_process_id"];
	}
	else
	{
		$bulk_document_process_id = "";
	}
	
	if(array_key_exists("bulk_document_type",$legal_process_search_data))
	{
		$bulk_document_type = $legal_process_search_data["bulk_document_type"];
	}
	else
	{
		$bulk_document_type = "";
	}
	
	if(array_key_exists("bulk_document_status",$legal_process_search_data))
	{
		$bulk_document_status = $legal_process_search_data["bulk_document_status"];
	}
	else
	{
		$bulk_document_status = "";
	}
	
	if(array_key_exists("bulk_document_added_by",$legal_process_search_data))
	{
		$bulk_document_added_by = $legal_process_search_data["bulk_document_added_by"];
	}
	else
	{
		$bulk_document_added_by = "";
	}
	if(array_key_exists("bulk_document_added_on",$legal_process_search_data))
	{
		$bulk_document_added_on = $legal_process_search_data["bulk_document_added_on"];
	}
	else
	{
		$bulk_document_added_on = "";
	}
	
	$get_legal_process_list_squery_base = "select * from legal_bulk_process_documents";
	
	$get_legal_process_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_legal_process_list_sdata = array();
	
	if($bulk_document_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." where bulk_document_process_id=:bulk_document_process_id";
		}
		else
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." and bulk_document_process_id=:bulk_document_process_id";
		}
		
		// Data
		$get_legal_process_list_sdata[':bulk_document_process_id']  = $bulk_document_process_id;
		
		$filter_count++;
	}
	
	if($bulk_document_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." where bulk_document_type=:bulk_document_type";
		}
		else
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." and bulk_document_type=:bulk_document_type";				
		}
		
		// Data
		$get_legal_process_list_sdata[':bulk_document_type']  = $bulk_document_type;
		
		$filter_count++;
	}
	
	if($bulk_document_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." where bulk_document_added_by=:bulk_document_added_by";
		}
		else
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." and bulk_document_added_by=:bulk_document_added_by";				
		}
		
		// Data
		$get_legal_process_list_sdata[':bulk_document_added_by']  = $bulk_document_added_by;
		
		$filter_count++;
	}
	
	if($bulk_document_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." where bulk_document_status=:bulk_document_status";
		}
		else
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." and bulk_document_status=:bulk_document_status";				
		}
		
		// Data
		$get_legal_process_list_sdata[':bulk_document_status']  = $bulk_document_status;
		
		$filter_count++;
	}
	
	if($bulk_document_added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." where bulk_document_added_on=:bulk_document_added_on";
		}
		else
		{
			// Query
			$get_legal_process_list_squery_where = $get_legal_process_list_squery_where." and bulk_document_added_on=:bulk_document_added_on";				
		}
		
		// Data
		$get_legal_process_list_sdata[':bulk_document_added_on']  = $bulk_document_added_on;
		
		$filter_count++;
	}
	
	$get_legal_process_list_squery = $get_legal_process_list_squery_base.$get_legal_process_list_squery_where;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_legal_process_list_sstatement = $dbConnection->prepare($get_legal_process_list_squery);
		
		$get_legal_process_list_sstatement -> execute($get_legal_process_list_sdata);
		
		$get_legal_process_list_sdetails = $get_legal_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_legal_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_legal_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_legal_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE: To update bulk document status
INPUT  : Document ID, Status, Updated By
OUTPUT : SUCCESS if document updated; DB_NO_RECORD if document ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_bulk_document($document_id,$status,$updated_by)
{
	// Query
    $bulk_document_uquery = "update legal_bulk_process_documents set bulk_document_status = :status,bulk_document_updated_by = :updated_by,bulk_document_updated_on = :updated_on where bulk_document_id=:document_id";   
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $bulk_document_ustatement = $dbConnection->prepare($bulk_document_uquery);
        
        // Data
        $bulk_document_udata = array(':status'=>$status,':updated_by'=>$updated_by,':updated_on'=>date("Y-m-d H:i:s"),':document_id'=>$document_id);		
        
        $bulk_document_ustatement -> execute($bulk_document_udata);
		
		$updated_rows = $bulk_document_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new process user maping
INPUT 	: Process Name, user
OUTPUT 	: Process user maping id, success or failure message
BY 		: Sonakshi D
*/
function db_add_process_user($process_name,$user,$added_by)
{
	// Query
    $process_user_iquery = "insert into process_user_maping (process_user_maping_process_type,process_user_maping_user_id,process_user_maping_added_by,process_user_maping_added_on) values (:process_name,:user,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_user_istatement = $dbConnection->prepare($process_user_iquery);
        
        // Data
        $process_user_idata = array(':process_name'=>$process_name,':user'=>$user,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $process_user_istatement->execute($process_user_idata);
		$process_user_maping_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_user_maping_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get processes user maping list
INPUT 	: Process Name,User, Added By, Start Date(for added on), End Date(for added on), Status
OUTPUT 	: List of processes User Map
BY 		: Sonakshi D
*/
function db_get_process_user_list($process_name,$user,$added_by,$start_date,$end_date)
{
	$get_process_user_list_squery_base = "select * from process_user_maping PUM inner join process_master PM on PUM.process_user_maping_process_type=PM.process_master_id inner join users U on PUM.process_user_maping_user_id=U.user_id";
	
	$get_process_user_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_process_user_list_sdata = array();
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." where process_user_maping_process_type=:process_name";								
		}
		else
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." and process_user_maping_process_type=:process_name";				
		}
		
		// Data
		$get_process_user_list_sdata[':process_name']  = $process_name;
		
		$filter_count++;
	}
	
	if($user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." where process_user_maping_user_id=:user";								
		}
		else
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." and process_user_maping_user_id=:user";				
		}
		
		// Data
		$get_process_user_list_sdata[':user']  = $user;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." where process_user_maping_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." and process_user_maping_added_by=:added_by";				
		}
		
		// Data
		$get_process_user_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." where process_user_maping_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." and process_user_maping_added_on >= :start_date";				
		}
		
		//Data
		$get_process_user_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." where process_user_maping_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_process_user_list_squery_where = $get_process_user_list_squery_where." and process_user_maping_added_on <= :end_date";				
		}
		
		//Data
		$get_process_user_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_process_user_list_squery_order = ' order by process_user_maping_added_on desc';
	$get_process_user_list_squery = $get_process_user_list_squery_base.$get_process_user_list_squery_where.$get_process_user_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_process_user_list_sstatement = $dbConnection->prepare($get_process_user_list_squery);
		
		$get_process_user_list_sstatement -> execute($get_process_user_list_sdata);
		
		$get_process_user_list_sdetails = $get_process_user_list_sstatement -> fetchAll();
		
		if(FALSE === $get_process_user_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_process_user_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_process_user_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add Completed process
INPUT 	: File ID,Added By
OUTPUT 	: Completed Process Plan id, success or failure message
BY 		: Sonakshi
*/
function db_add_completed_process_plan($file_id,$added_by)
{
	// Query
    $completed_process_plan_iquery = "insert into completed_process_plan_legal (completed_process_plan_legal_file_id,completed_process_plan_legal_complete,completed_process_plan_legal_added_by,completed_process_plan_legal_added_on) values (:file_id,:complete,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $completed_process_plan_istatement = $dbConnection->prepare($completed_process_plan_iquery);
        
        // Data
        $completed_process_plan_idata = array(':file_id'=>$file_id,':complete'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $completed_process_plan_istatement->execute($completed_process_plan_idata);
		$completed_process_plan_legal_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $completed_process_plan_legal_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get completed process plan
INPUT 	: File ID, Added By, Status (1 or 0), Start Date(for added on), End Date(for added on), Status
OUTPUT 	: List of Completed process plan
BY 		: Sonakshi
*/
function db_get_completed_process_plan_list($file_id,$added_by,$status,$start_date,$end_date)
{
	$get_completed_process_plan_list_squery_base = "select * from completed_process_plan_legal";
	
	$get_completed_process_plan_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_completed_process_plan_list_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." where completed_process_plan_legal_file_id=:file_id";								
		}
		else
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." and completed_process_plan_legal_file_id=:file_id";				
		}
		
		// Data
		$get_completed_process_plan_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." where completed_process_plan_legal_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." and completed_process_plan_legal_added_by=:added_by";				
		}
		
		// Data
		$get_completed_process_plan_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." where completed_process_plan_legal_complete = :status";								
		}
		else
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." and completed_process_plan_legal_complete = :status";				
		}
		
		// Data
		$get_completed_process_plan_list_sdata[':status'] = $status;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." where completed_process_plan_legal_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." and completed_process_plan_legal_added_on >= :start_date";				
		}
		
		//Data
		$get_completed_process_plan_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." where completed_process_plan_legal_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_completed_process_plan_list_squery_where = $get_completed_process_plan_list_squery_where." and completed_process_plan_legal_added_on <= :end_date";				
		}
		
		//Data
		$get_completed_process_plan_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_completed_process_plan_list_squery = $get_completed_process_plan_list_squery_base.$get_completed_process_plan_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_completed_process_plan_list_sstatement = $dbConnection->prepare($get_completed_process_plan_list_squery);
		
		$get_completed_process_plan_list_sstatement -> execute($get_completed_process_plan_list_sdata);
		
		$get_completed_process_plan_list_sdetails = $get_completed_process_plan_list_sstatement -> fetchAll();
		
		if(FALSE === $get_completed_process_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_completed_process_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_completed_process_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE: To update process plan completion status
INPUT  : File ID, Status
OUTPUT : SUCCESS if process plan completion updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_process_plan_completion($file_id,$status)
{
	// Query
    $process_plan_uquery = "update completed_process_plan_legal set completed_process_plan_legal_complete = :status where completed_process_plan_legal_file_id = :file_id";   
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $process_plan_ustatement = $dbConnection->prepare($process_plan_uquery);
        
        // Data
        $process_plan_udata = array(':status'=>$status,':file_id'=>$file_id);		
        
        $process_plan_ustatement -> execute($process_plan_udata);
		
		$updated_rows = $process_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>