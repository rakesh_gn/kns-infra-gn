<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

PURPOSE : To add new Stock Quotation Compare

INPUT 	: Indent No,Amount,Quotation No,Quotation Vendor,Quotation Quantity,Location,Received Date,Remarks,Added By,Added On

OUTPUT 	: Quotation Id, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_quotation_compare($auto_quote_no,$indent_id,$amount,$quotation_no,$quotation_vendor,$quotation_qty,$project,$received_date,$doc,$remarks,$added_by,$material_id)
{

	// Query
    $stock_quotation_compare_iquery = "insert into stock_quotation_compare (stock_quote_no,stock_quotation_indent_id,stock_quotation_amount,stock_quotation_no,stock_quotation_vendor,stock_quotation_quantity,stock_quotation_po_qty,stock_quotation_project,stock_quotation_received_date,stock_quotation_status,stock_quotation_remarks,stock_quotation_active,stock_quotation_added_by,stock_quotation_added_on,stock_quotation_material_id) values (:auto_quote_no,:indent_id,:amount,:quotation_no,:quotation_vendor,:quotation_qty,:po_qty,:project,:received_date,:status,:remarks,:active,:added_by,:added_on,:material_id)";


    try

    {

        $dbConnection = get_conn_handle();

        $stock_quotation_compare_istatement = $dbConnection->prepare($stock_quotation_compare_iquery);



        // Data
        $stock_quotation_compare_idata = array(':auto_quote_no'=>$auto_quote_no,':indent_id'=>$indent_id,':amount'=>$amount,':quotation_no'=>$quotation_no,':quotation_vendor'=>$quotation_vendor,':quotation_qty'=>$quotation_qty,':po_qty'=>'0',':project'=>$project,':received_date'=>$received_date,':status'=>'Waiting',':remarks'=>$remarks,':active'=>'1',':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"),'material_id'=>$material_id);

		$dbConnection->beginTransaction();

        $stock_quotation_compare_istatement->execute($stock_quotation_compare_idata);

		$stock_quotation_compare_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_quotation_compare_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}

/*

PURPOSE : To add new Stock Quotation Items Mapping

INPUT 	: Indent No,Amount,Quotation No,Quotation Vendor,Quotation Quantity,Location,Received Date,Remarks,Added By,Added On

OUTPUT 	: Quotation Id, success or failure message

BY 		: Sonakshi

*/
function db_add_stock_quotation_items_mapping($quote_id,$indent_item_id,$material_id,$indent_id,$indent_no,$material_qty,$project,$project_name,$material_name,$material_code)
{
	// Query
    $stock_quotation_compare_iquery = "insert into stock_quotation_items_mapping_list (quote_quotation_id,quote_indent_item_id,quote_material_id,quote_indent_id,quote_indent_no,quote_material_qty,quote_project_id,quote_project_name,quote_material_name,quote_material_code)
     values (:quote_id,:indent_item_id,:material_id,:indent_id,:indent_no,:material_qty,:project,:project_name,:material_name,:material_code)";
    try
    {	
    	
        $dbConnection = get_conn_handle();
        $stock_quotation_compare_istatement = $dbConnection->prepare($stock_quotation_compare_iquery);
        // Data
        $stock_quotation_compare_idata = array(':quote_id'=>$quote_id,
                                                ':indent_item_id'=>$indent_item_id,
                                                ':material_id'=>$material_id,
                                                ':indent_id'=>$indent_id,
                                                ':indent_no'=>$indent_no,
                                                ':material_qty'=>$material_qty,
                                                ':project'=>$project,
                                                ':project_name'=>$project_name,
                                                ':material_name'=>$material_name,
                                                ':material_code'=>$material_code);
      $dbConnection->beginTransaction();
      $stock_quotation_compare_istatement->execute($stock_quotation_compare_idata);
		  $stock_quotation_compare_id = $dbConnection->lastInsertId();
		  $dbConnection->commit();
      $return["status"] = SUCCESS;
		  $return["data"]   = $stock_quotation_compare_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		    $return["data"]   = "";
    }
    return $return;
}



/*

PURPOSE : To get Stock Quotation Compare List

INPUT 	:  Quotation Id,Indent No,Amount,Quotation No,Quotation Vendor,Received Date,Added by,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Quotation Compare

BY 		: Lakshmi

*/

function db_get_stock_quotation_compare_list($stock_quotation_compare_search_data)

{

    if(array_key_exists("quotation_id",$stock_quotation_compare_search_data))

	{

		$quotation_id = $stock_quotation_compare_search_data["quotation_id"];

	}

	else

	{

		$quotation_id= "";

	}

	//update by Rakesh
	//Purpose - search for purchase order id
	if(array_key_exists("po_id",$stock_quotation_compare_search_data))

	{

		$po_id = $stock_quotation_compare_search_data["po_id"];

	}

	else

	{

		$po_id= "";

	}

	if(array_key_exists("indent_id",$stock_quotation_compare_search_data))

	{

		$indent_id = $stock_quotation_compare_search_data["indent_id"];

	}

	else

	{

		$indent_id= "";

	}

	if(array_key_exists("amount",$stock_quotation_compare_search_data))

	{

		$amount = $stock_quotation_compare_search_data["amount"];

	}

	else

	{

		$amount= "";

	}

	if(array_key_exists("quotation_no",$stock_quotation_compare_search_data))

	{

		$quotation_no = $stock_quotation_compare_search_data["quotation_no"];

	}

	else

	{

		$quotation_no= "";

	}



	if(array_key_exists("quotation_vendor",$stock_quotation_compare_search_data))

	{

		$quotation_vendor = $stock_quotation_compare_search_data["quotation_vendor"];

	}

	else

	{

		$quotation_vendor= "";

	}


	if(array_key_exists("project",$stock_quotation_compare_search_data))
	{
		$project = $stock_quotation_compare_search_data["project"];
	}

	else

	{
		$project = "";
	}



	if(array_key_exists("received_date",$stock_quotation_compare_search_data))

	{

		$received_date = $stock_quotation_compare_search_data["received_date"];

	}

	else

	{

		$received_date= "";

	}

	if(array_key_exists("status",$stock_quotation_compare_search_data))

	{

		$status = $stock_quotation_compare_search_data["status"];

	}

	else

	{

		$status= "";

	}



	if(array_key_exists("active",$stock_quotation_compare_search_data))

	{

		$active= $stock_quotation_compare_search_data["active"];

	}

	else

	{

		$active= "";

	}



	if(array_key_exists("added_by",$stock_quotation_compare_search_data))

	{

		$added_by= $stock_quotation_compare_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$stock_quotation_compare_search_data))

	{

		$start_date= $stock_quotation_compare_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}



	if(array_key_exists("end_date",$stock_quotation_compare_search_data))

	{

		$end_date= $stock_quotation_compare_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	if(array_key_exists("material_id",$stock_quotation_compare_search_data))

	{

		$material_id= $stock_quotation_compare_search_data["material_id"];

	}

	else

	{

		$material_id= "";

	}
		

	//update by Rakesh as of new Requirment
	//31-01-2019
	$get_stock_quotation_compare_list_squery_base = 
	"select * from stock_quotation_compare SQC 
	left outer join users U on U.user_id = SQC.stock_quotation_added_by 
	inner join stock_vendor_master SVM on SVM.stock_vendor_id = SQC.stock_quotation_vendor 
	inner join stock_material_master  SMM on SQC.stock_quotation_material_id = SMM.stock_material_id left 
	outer join stock_project SLM on SLM.stock_project_id = SQC.stock_quotation_project
	inner join stock_unit_measure_master UOM on UOM.stock_unit_id=SMM.stock_material_unit_of_measure
	inner join stock_indent SI on SI.stock_indent_id=SQC.stock_quotation_indent_id";
	//end of update

	//old code ->
	/*$get_stock_quotation_compare_list_squery_base = "select * from stock_quotation_compare SQC left outer join users U on U.user_id = SQC.stock_quotation_added_by inner join stock_vendor_master SVM on SVM.stock_vendor_id = SQC.stock_quotation_vendor inner join stock_material_master  SMM on SQC.stock_quotation_indent_id = SMM.stock_material_id left outer join stock_project SLM on SLM.stock_project_id = SQC.stock_quotation_project
	inner join stock_unit_measure_master UOM on UOM.stock_unit_id=SMM.stock_material_unit_of_measure";*/
	
	



	$get_stock_quotation_compare_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_quotation_compare_list_sdata = array();



	if($quotation_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_id=:quotation_id";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_id=:quotation_id";

		}

		// Data

		$get_stock_quotation_compare_list_sdata[':quotation_id'] = $quotation_id;



		$filter_count++;

	}

	//update
	if($po_id != ""){

		if($filter_count == 0){

			//Query
			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_po_id=:po_id";

		}

		else{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_po_id=:po_id";
		}

		// Data

		$get_stock_quotation_compare_list_sdata[':po_id'] = $po_id;
		$filter_count++;

	}

	if($indent_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_indent_id=:indent_id";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_indent_id=:indent_id";

		}



		// Data

		$get_stock_quotation_compare_list_sdata[':indent_id']  = $indent_id;



		$filter_count++;

	}

	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_material_id=:material_id";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_material_id=:material_id";

		}



		// Data

		$get_stock_quotation_compare_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}

	 if($amount != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_amount=:amount";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_amount:amount";

		}



		// Data

		$get_stock_quotation_compare_list_sdata[':amount']  = $amount;



		$filter_count++;

	}

	if($quotation_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_no=:quotation_no";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_no=:quotation_no";

		}



		// Data

		$get_stock_quotation_compare_list_sdata[':quotation_no']  = $quotation_no;



		$filter_count++;

	}



	if($quotation_vendor != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_vendor=:quotation_vendor";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_vendor=:quotation_vendor";

		}



		// Data

		$get_stock_quotation_compare_list_sdata[':quotation_vendor']  = $quotation_vendor;



		$filter_count++;

	}


	if($project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_project = :quotation_project";
		}

		else

		{

			// Query
			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_project = :quotation_project";
		}



		// Data
		$get_stock_quotation_compare_list_sdata[':quotation_project'] = $project;


		$filter_count++;

	}



	if($received_date!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_received_date = :received_date";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_received_date = :received_date";

		}



		//Data

		$get_stock_quotation_compare_list_sdata[':received_date']  = $received_date;



		$filter_count++;

	}

	if($status!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_status = :status";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_status = :status";

		}



		//Data

		$get_stock_quotation_compare_list_sdata[':status']  = $status;



		$filter_count++;

	}



	if($active!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_active = :active";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_active = :active";

		}



		//Data

		$get_stock_quotation_compare_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_added_by = :added_by";

		}



		//Data

		$get_stock_quotation_compare_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_added_on >= :start_date";

		}



		//Data

		$get_stock_quotation_compare_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quotation_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quotation_added_on <= :end_date";

		}



		//Data

		$get_stock_quotation_compare_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}

  if(array_key_exists("sort",$stock_quotation_compare_search_data))
	{
		if($stock_quotation_compare_search_data['sort'] == '1')
		{
  			$get_quotation_list_squery_order = " order by stock_quotation_id desc limit 0,1";
		}
		else
		{
			$get_quotation_list_squery_order = " order by stock_quotation_added_on DESC";
		}
	}
  else{
    $get_quotation_list_squery_order = " order by stock_quotation_added_on DESC";
  }

	if(array_key_exists("empty_check",$stock_quotation_compare_search_data))
	{
		if($filter_count == 0)
		{
			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." where stock_quote_no != ''";
		}
		else
		{
			$get_stock_quotation_compare_list_squery_where = $get_stock_quotation_compare_list_squery_where." and stock_quote_no != ''";
		}
		$filter_count++;
	}

	$get_stock_quotation_compare_list_squery = $get_stock_quotation_compare_list_squery_base.$get_stock_quotation_compare_list_squery_where.$get_quotation_list_squery_order;


	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_quotation_compare_list_sstatement = $dbConnection->prepare($get_stock_quotation_compare_list_squery);



		$get_stock_quotation_compare_list_sstatement -> execute($get_stock_quotation_compare_list_sdata);


		$get_stock_quotation_compare_list_sdetails = $get_stock_quotation_compare_list_sstatement -> fetchAll();


		if(FALSE === $get_stock_quotation_compare_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_quotation_compare_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_quotation_compare_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



/*

PURPOSE : To update Quotation Compare

INPUT 	: Quotation ID, Quotation Compare Update Array

OUTPUT 	: Quotation ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_quotation_compare($quotation_id,$indent_id,$quotation_compare_update_data)

{
	
	if(array_key_exists("amount",$quotation_compare_update_data))

	{

		$amount = $quotation_compare_update_data["amount"];

	}

	else

	{

		$amount = "";

	}

	if(array_key_exists("quotation_no",$quotation_compare_update_data))

	{

		$quotation_no = $quotation_compare_update_data["quotation_no"];

	}

	else

	{

		$quotation_no = "";

	}

	if(array_key_exists("quotation_vendor",$quotation_compare_update_data))

	{

		$quotation_vendor = $quotation_compare_update_data["quotation_vendor"];

	}

	else

	{

		$quotation_vendor = "";

	}

	if(array_key_exists("received_date",$quotation_compare_update_data))

	{

		$received_date = $quotation_compare_update_data["received_date"];

	}

	else

	{

		$received_date = "";

	}



	if(array_key_exists("status",$quotation_compare_update_data))

	{

		$status = $quotation_compare_update_data["status"];

	}

	else

	{

		$status = "";

	}



	if(array_key_exists("po_qty",$quotation_compare_update_data))

	{

		$po_qty = $quotation_compare_update_data["po_qty"];

	}

	else

	{

		$po_qty = "";

	}

  if(array_key_exists("item_quote_qty",$quotation_compare_update_data))

	{

		$item_quote_qty = $quotation_compare_update_data["item_quote_qty"];

	}

	else

	{

		$item_quote_qty = "";

	}


	if(array_key_exists("active",$quotation_compare_update_data))

	{

		$active = $quotation_compare_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$quotation_compare_update_data))

	{

		$remarks = $quotation_compare_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("approved_by",$quotation_compare_update_data))

	{

		$approved_by = $quotation_compare_update_data["approved_by"];

	}

	else

	{

		$approved_by = "";

	}



	if(array_key_exists("approved_on",$quotation_compare_update_data))

	{

		$approved_on = $quotation_compare_update_data["approved_on"];

	}

	else

	{

		$approved_on = "";

	}





	if(array_key_exists("added_by",$quotation_compare_update_data))

	{

		$added_by = $quotation_compare_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("added_on",$quotation_compare_update_data))

	{

		$added_on = $quotation_compare_update_data["added_on"];

	}

	else

	{

		$added_on = "";

	}

	if(array_key_exists("po_value",$quotation_compare_update_data))

	{

		$po_value = $quotation_compare_update_data["po_value"];

	}

	else

	{

		$po_value = "";

	}

	if(array_key_exists("stock_quotation_po_id",$quotation_compare_update_data))

	{

		$po_id = $quotation_compare_update_data["stock_quotation_po_id"];

	}

	else

	{

		$po_id = "";

	}

	if(array_key_exists("vendor_id",$quotation_compare_update_data))

	{

		$vendor_id = $quotation_compare_update_data["vendor_id"];

	}

	else

	{

		$vendor_id = "";

	}



	// Query

    $quotation_compare_update_uquery_base = "update stock_quotation_compare set";



	$quotation_compare_update_uquery_set = "";



	$quotation_compare_update_uquery_where = " where stock_quotation_id=:quotation_id OR  stock_quotation_indent_id=:indent_id";



	$quotation_compare_update_udata = array(":quotation_id"=>$quotation_id,":indent_id"=>$indent_id);

	//search 

	if(array_key_exists("search_by_vendor",$quotation_compare_update_data))

	{

		$quotation_compare_update_uquery_where 
		= $quotation_compare_update_uquery_where." AND stock_quotation_vendor=:vendor_id";
		$quotation_compare_update_udata = array(":quotation_id"=>$quotation_id,":indent_id"=>$indent_id,":vendor_id"=>$vendor_id);

	}

	


	$filter_count = 0;



	if($amount != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_amount=:amount,";

		$quotation_compare_update_udata[":amount"] = $amount;

		$filter_count++;

	}



	if($quotation_no != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_no=:quotation_no,";

		$quotation_compare_update_udata[":quotation_no"] = $quotation_no;

		$filter_count++;

	}



	if($status != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_status=:status,";

		$quotation_compare_update_udata[":status"] = $status;

		$filter_count++;

	}



	if($po_qty != "")

	{

		/*$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_po_qty = stock_quotation_po_qty + :po_qty,";*/
		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_po_qty = :po_qty,";

		$quotation_compare_update_udata[":po_qty"] = $po_qty;

		$filter_count++;

	}

  if($item_quote_qty != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_quantity = :item_quote_qty,";

		$quotation_compare_update_udata[":item_quote_qty"] = $item_quote_qty;

		$filter_count++;

	}


	if($received_date != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_received_date=:received_date,";

		$quotation_compare_update_udata[":received_date"] = $received_date;

		$filter_count++;

	}



	if($active != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_active = :active,";

		$quotation_compare_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_remarks = :remarks,";

		$quotation_compare_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($approved_by != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_approved_by=:approved_by,";

		$quotation_compare_update_udata[":approved_by"] = $approved_by;

		$filter_count++;

	}



	if($approved_on != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_approved_on=:approved_on,";

		$quotation_compare_update_udata[":approved_on"] = $approved_on;

		$filter_count++;

	}





	if($added_by != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_added_by=:added_by,";

		$quotation_compare_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_added_on=:added_on,";

		$quotation_compare_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}

	if($po_value != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_po_value=:po_value,";

		$quotation_compare_update_udata[":po_value"] = $po_value;

		$filter_count++;

	}

	if($po_id != "")

	{

		$quotation_compare_update_uquery_set = $quotation_compare_update_uquery_set." stock_quotation_po_id=:po_id,";

		$quotation_compare_update_udata[":po_id"] = $po_id;

		$filter_count++;

	}


	if($filter_count > 0)

	{

		$quotation_compare_update_uquery_set = trim($quotation_compare_update_uquery_set,',');

	}



	$quotation_compare_update_uquery = $quotation_compare_update_uquery_base.$quotation_compare_update_uquery_set.$quotation_compare_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();


        $quotation_compare_update_ustatement = $dbConnection->prepare($quotation_compare_update_uquery);

        //quotation_compare_update_uquery_where
       //echo json_encode($quotation_compare_update_udata);exit();
        $quotation_compare_update_ustatement -> execute($quotation_compare_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $quotation_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Quote Reset

INPUT 	: Material ID, Location Date Time, PO ID, Remarks, Added by

OUTPUT 	: Reset ID, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_quote_reset($material_id,$project,$date_time,$po_id,$remarks,$added_by)
{

	// Query
    $stock_quote_reset_iquery = "insert into stock_quote_reset (stock_quote_reset_material_id,stock_quote_reset_project,stock_quote_reset_date_time,stock_quote_reset_po_id,stock_quote_reset_active,stock_quote_reset_remarks,stock_quote_reset_added_by,stock_quote_reset_added_on) values (:material_id,:project,:date_time,:po_id,:active,:remarks,:added_by,:added_on)";


    try

    {

        $dbConnection = get_conn_handle();

        $stock_quote_reset_istatement = $dbConnection->prepare($stock_quote_reset_iquery);



        // Data
        $stock_quote_reset_idata = array(':material_id'=>$material_id,':project'=>$project,':date_time'=>$date_time,':po_id'=>$po_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));


		$dbConnection->beginTransaction();

        $stock_quote_reset_istatement->execute($stock_quote_reset_idata);

		$stock_quote_reset_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_quote_reset_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Quote Reset List

INPUT 	: Reset ID, Material ID, Date Time, PO ID, Active, Added by, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Quote Reset

BY 		: Lakshmi

*/

function db_get_stock_quote_reset($stock_quote_reset_search_data)

{

	if(array_key_exists("reset_id",$stock_quote_reset_search_data))

	{

		$reset_id = $stock_quote_reset_search_data["reset_id"];

	}

	else

	{

		$reset_id= "";

	}



	if(array_key_exists("material_id",$stock_quote_reset_search_data))

	{

		$material_id= $stock_quote_reset_search_data["material_id"];

	}

	else

	{

		$material_id= "";

	}



	if(array_key_exists("date_time",$stock_quote_reset_search_data))

	{

		$date_time= $stock_quote_reset_search_data["date_time"];

	}

	else

	{

		$date_time= "";

	}



	if(array_key_exists("po_id",$stock_quote_reset_search_data))

	{

		$po_id= $stock_quote_reset_search_data["po_id"];

	}

	else

	{

		$po_id= "";

	}

	if(array_key_exists("project",$stock_quote_reset_search_data))

	{

		$project= $stock_quote_reset_search_data["project"];

	}

	else

	{

		$project= "";

	}



	if(array_key_exists("active",$stock_quote_reset_search_data))

	{

		$active= $stock_quote_reset_search_data["active"];

	}

	else

	{

		$active= "";

	}



	if(array_key_exists("added_by",$stock_quote_reset_search_data))

	{

		$added_by= $stock_quote_reset_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$stock_quote_reset_search_data))

	{

		$start_date= $stock_quote_reset_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}



	if(array_key_exists("end_date",$stock_quote_reset_search_data))

	{

		$end_date= $stock_quote_reset_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_quote_reset_list_squery_base = "select * from stock_quote_reset";



	$get_stock_quote_reset_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_quote_reset_list_sdata = array();



	if($reset_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_id = :reset_id";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_id = :reset_id";

		}

		// Data

		$get_stock_quote_reset_list_sdata[':reset_id'] = $reset_id;



		$filter_count++;

	}



	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_material_id = :material_id";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_material_id = :material_id";

		}



		// Data

		$get_stock_quote_reset_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($date_time != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_date_time = :date_time";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_date_time = :date_time";

		}



		// Data

		$get_stock_quote_reset_list_sdata[':date_time']  = $date_time;



		$filter_count++;

	}



	if($po_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_po_id = :po_id";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_po_id = :po_id";

		}



		// Data

		$get_stock_quote_reset_list_sdata[':po_id']  = $po_id;



		$filter_count++;

	}

	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_project = :project";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_project = :project";

		}



		// Data

		$get_stock_quote_reset_list_sdata[':project']  = $project;



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_active = :active";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_active = :active";

		}



		// Data

		$get_stock_quote_reset_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_added_by = :added_by";

		}



		//Data

		$get_stock_quote_reset_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_added_on >= :start_date";

		}



		//Data

		$get_stock_quote_reset_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." where stock_quote_reset_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_quote_reset_list_squery_where = $get_stock_quote_reset_list_squery_where." and stock_quote_reset_added_on <= :end_date";

		}



		//Data

		$get_stock_quote_reset_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_quote_reset_list_squery_order = " order by stock_quote_reset_date_time desc";

	$get_stock_quote_reset_list_squery = $get_stock_quote_reset_list_squery_base.$get_stock_quote_reset_list_squery_where.$get_stock_quote_reset_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_quote_reset_list_sstatement = $dbConnection->prepare($get_stock_quote_reset_list_squery);



		$get_stock_quote_reset_list_sstatement -> execute($get_stock_quote_reset_list_sdata);



		$get_stock_quote_reset_list_sdetails = $get_stock_quote_reset_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_quote_reset_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_quote_reset_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_quote_reset_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Stock Quote Reset

INPUT 	: Material ID, Location ID, Stock Quote Reset Update Array

OUTPUT 	: Material ID; Message of success or failure

BY 		: Lakshmi

*/
function db_update_stock_quote_reset($material_id,$project_id,$stock_quote_reset_update_data)
{

	if(array_key_exists("reset_date_time",$stock_quote_reset_update_data))

	{

		$date_time = $stock_quote_reset_update_data["reset_date_time"];

	}

	else

	{

		$date_time = "";

	}



	if(array_key_exists("po_id",$stock_quote_reset_update_data))

	{

		$po_id = $stock_quote_reset_update_data["po_id"];

	}

	else

	{

		$po_id = "";

	}



	if(array_key_exists("active",$stock_quote_reset_update_data))

	{

		$active = $stock_quote_reset_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$stock_quote_reset_update_data))

	{

		$remarks = $stock_quote_reset_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("updated_by",$stock_quote_reset_update_data))

	{

		$updated_by = $stock_quote_reset_update_data["updated_by"];

	}

	else

	{

		$updated_by = "";

	}



	$updated_on = date('Y-m-d H:i:s');



	// Query

    $stock_quote_reset_update_uquery_base = "update stock_quote_reset set";



	$stock_quote_reset_update_uquery_set = "";


	$stock_quote_reset_update_uquery_where = " where stock_quote_reset_material_id = :material_id and stock_quote_reset_project = :project";

	$stock_quote_reset_update_udata = array(":material_id"=>$material_id,":project"=>$project_id);


	$filter_count = 0;



	if($date_time != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_date_time = :date_time,";

		$stock_quote_reset_update_udata[":date_time"] = $date_time;

		$filter_count++;

	}



	if($po_id != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_po_id = :po_id,";

		$stock_quote_reset_update_udata[":po_id"] = $po_id;

		$filter_count++;

	}



	if($active != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_active = :active,";

		$stock_quote_reset_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_remarks = :remarks,";

		$stock_quote_reset_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($updated_by != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_added_by = :updated_by,";

		$stock_quote_reset_update_udata[":updated_by"] = $updated_by;

		$filter_count++;

	}



	if($updated_on != "")

	{

		$stock_quote_reset_update_uquery_set = $stock_quote_reset_update_uquery_set." stock_quote_reset_added_on = :updated_on,";

		$stock_quote_reset_update_udata[":updated_on"] = $updated_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$stock_quote_reset_update_uquery_set = trim($stock_quote_reset_update_uquery_set,',');

	}



	$stock_quote_reset_update_uquery = $stock_quote_reset_update_uquery_base.$stock_quote_reset_update_uquery_set.$stock_quote_reset_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $stock_quote_reset_update_ustatement = $dbConnection->prepare($stock_quote_reset_update_uquery);



        $stock_quote_reset_update_ustatement -> execute($stock_quote_reset_update_udata);



        $return["status"] = SUCCESS;
		$return["data"]   = $material_id;
    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}
/*
PURPOSE : To get Project Actual Payment ManPower list
INPUT 	: ManPower ID, Vendor ID, Amount, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Process Task
BY 		: Lakshmi
*/
function db_get_stock_items_quotations($stock_items_quotation_search_data)
{
	if(array_key_exists("quote_id",$stock_items_quotation_search_data))
	{
		$quote_id = $stock_items_quotation_search_data["quote_id"];
	}
	else
	{
		$quote_id= "";
	}
  $get_stock_item_quotation_list_squery_base = " select * from stock_quotation_items_mapping_list SQIML inner join stock_quotation_compare SQC on SQC.stock_quotation_id=SQIML.quote_quotation_id inner join stock_material_master SMM on SMM.stock_material_id=SQIML.quote_material_id inner join stock_indent_items SII on SII.stock_indent_item_id=SQIML.quote_indent_item_id inner join users U on U.user_id=SII.stock_indent_item_added_by";
	$get_stock_item_quotation_list_squery_where = "";

	$filter_count = 0;
	// Data
	$get_stock_item_quotation_list_sdata = array();

  if($quote_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_item_quotation_list_squery_where = $get_stock_item_quotation_list_squery_where." where quote_quotation_id=:quote_id";
		}
		else
		{
			// Query
			$get_stock_item_quotation_list_squery_where = $get_stock_item_quotation_list_squery_where." and quote_quotation_id=:quote_id";
		}
    // Data
		$get_stock_item_quotation_list_sdata[':quote_id'] = $quote_id;
		$filter_count++;
	}
	$get_stock_items_quotation_list_squery = $get_stock_item_quotation_list_squery_base.$get_stock_item_quotation_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_stock_items_quotation_list_sstatement = $dbConnection->prepare($get_stock_items_quotation_list_squery);

		$get_stock_items_quotation_list_sstatement -> execute($get_stock_item_quotation_list_sdata);

		$get_stock_item_quotation_list_sdetails = $get_stock_items_quotation_list_sstatement -> fetchAll();

		if(FALSE === $get_stock_item_quotation_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_item_quotation_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_item_quotation_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }
?>
