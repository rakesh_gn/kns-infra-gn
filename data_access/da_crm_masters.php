<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new bank
INPUT 	: Bank Name, Added By
OUTPUT 	: Bank id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_bank($bank_name,$added_by)
{
	// Query
    $bank_iquery = "insert into crm_bank_master (crm_bank_name,crm_bank_active,crm_bank_added_by,crm_bank_added_on) values (:bank_name,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $bank_istatement = $dbConnection->prepare($bank_iquery);
        
        // Data
        $bank_idata = array(':bank_name'=>$bank_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $bank_istatement->execute($bank_idata);
		$bank_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $bank_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get bank list
INPUT 	: Bank Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of banks
BY 		: Nitin Kashyap
*/
function db_get_bank_list($bank_name,$active,$added_by,$start_date,$end_date)
{
	$get_bank_list_squery_base = "select * from crm_bank_master CBM inner join users U on U.user_id = CBM.crm_bank_added_by";
	
	$get_bank_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_bank_list_sdata = array();
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_name=:bank_name";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_name=:bank_name";				
		}
		
		// Data
		$get_bank_list_sdata[':bank_name']  = $bank_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_active=:active";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_active=:active";				
		}
		
		// Data
		$get_bank_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_by=:added_by";				
		}
		
		// Data
		$get_bank_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_on >= :start_date";				
		}
		
		//Data
		$get_bank_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_on <= :end_date";				
		}
		
		//Data
		$get_bank_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_bank_list_squery = $get_bank_list_squery_base.$get_bank_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_bank_list_sstatement = $dbConnection->prepare($get_bank_list_squery);
		
		$get_bank_list_sstatement -> execute($get_bank_list_sdata);
		
		$get_bank_list_sdetails = $get_bank_list_sstatement -> fetchAll();
		
		if(FALSE === $get_bank_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_bank_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_bank_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new customer interest status
INPUT 	: Status Name, Added By
OUTPUT 	: Status id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_interest_status($interest_status_name,$added_by)
{
	// Query
    $interest_status_iquery = "insert into crm_customer_interest_status (crm_cust_interest_status_name,crm_cust_interest_status_active,crm_cust_interest_status_added_by,crm_cust_interest_status_added_on) values (:interest_status_name,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $interest_status_istatement = $dbConnection->prepare($interest_status_iquery);
        
        // Data
        $interest_status_idata = array(':interest_status_name'=>$interest_status_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $interest_status_istatement->execute($interest_status_idata);
		$interest_status_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $interest_status_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get customer interest status list
INPUT 	: Status Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of customer interest status
BY 		: Nitin Kashyap
*/
function db_get_customer_interest_list($status_name,$active,$added_by,$start_date,$end_date)
{
	$get_cust_int_status_list_squery_base = "select * from crm_customer_interest_status CIS inner join users U on U.user_id = CIS.crm_cust_interest_status_added_by";
	
	$get_cust_int_status_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_cust_int_status_list_sdata = array();
	
	if($status_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." where crm_cust_interest_status_name=:cust_int_status_name";								
		}
		else
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." and crm_cust_interest_status_name=:cust_int_status_name";				
		}
		
		// Data
		$get_cust_int_status_list_sdata[':cust_int_status_name']  = $status_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." where crm_cust_interest_status_active=:active";								
		}
		else
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." and crm_cust_interest_status_active=:active";				
		}
		
		// Data
		$get_cust_int_status_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." where crm_cust_interest_status_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." and crm_cust_interest_status_added_by=:added_by";				
		}
		
		// Data
		$get_cust_int_status_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." where crm_cust_interest_status_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." and crm_cust_interest_status_added_on >= :start_date";				
		}
		
		//Data
		$get_cust_int_status_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." where crm_cust_interest_status_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_cust_int_status_list_squery_where = $get_cust_int_status_list_squery_where." and crm_cust_interest_status_added_on <= :end_date";				
		}
		
		//Data
		$get_cust_int_status_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_cust_int_status_list_squery_order = ' order by crm_customer_interest_status_order desc';
	$get_cust_int_status_list_squery = $get_cust_int_status_list_squery_base.$get_cust_int_status_list_squery_where.$get_cust_int_status_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_cust_int_status_list_sstatement = $dbConnection->prepare($get_cust_int_status_list_squery);
		
		$get_cust_int_status_list_sstatement -> execute($get_cust_int_status_list_sdata);
		
		$get_cust_int_status_list_sdetails = $get_cust_int_status_list_sstatement -> fetchAll();
		
		if(FALSE === $get_cust_int_status_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_cust_int_status_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_cust_int_status_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new enquiry source
INPUT 	: Enquiry Source, Enquiry Source Type, Added By
OUTPUT 	: Enquiry Source id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_enquiry_source($source,$source_type,$added_by)
{
	// Query
    $enquiry_source_iquery = "insert into crm_enquiry_source_master (enquiry_source_master_name,enquiry_source_master_type,enquiry_source_master_active,enquiry_source_master_added_by,enquiry_source_master_added_on) values (:enquiry_source_name,:source_type,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $enquiry_source_istatement = $dbConnection->prepare($enquiry_source_iquery);
        
        // Data
        $enquiry_source_idata = array(':enquiry_source_name'=>$source,':source_type'=>$source_type,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $enquiry_source_istatement->execute($enquiry_source_idata);
		$enquiry_source_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_source_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get enquiry source list
INPUT 	: Source Name, Source Type, Active, Added By, Start Date(for added on), End Date(for added on), Source ID
OUTPUT 	: List of enquiry sources
BY 		: Nitin Kashyap
*/
function db_get_enquiry_source_list($source_name,$source_type,$active,$added_by,$start_date,$end_date,$source_id='')
{
	$get_enquiry_source_list_squery_base = "select * from crm_enquiry_source_master ESS inner join users U on U.user_id = ESS.enquiry_source_master_added_by left outer join crm_enquiry_source_type_master CESTM on CESTM.enquiry_source_type_id = ESS.enquiry_source_master_type";
	
	$get_enquiry_source_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_enquiry_source_list_sdata = array();
	
	if($source_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_name=:enquiry_source_status_name";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_name=:enquiry_source_status_name";				
		}
		
		// Data
		$get_enquiry_source_list_sdata[':enquiry_source_status_name']  = $source_name;
		
		$filter_count++;
	}
	
	if($source_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_type=:source_type";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_type=:source_type";				
		}
		
		// Data
		$get_enquiry_source_list_sdata[':source_type']  = $source_type;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_active=:active";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_active=:active";				
		}
		
		// Data
		$get_enquiry_source_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_added_by=:added_by";				
		}
		
		// Data
		$get_enquiry_source_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_source_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_source_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($source_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." where enquiry_source_master_id=:source_id";								
		}
		else
		{
			// Query
			$get_enquiry_source_list_squery_where = $get_enquiry_source_list_squery_where." and enquiry_source_master_id=:source_id";				
		}
		
		// Data
		$get_enquiry_source_list_sdata[':source_id']  = $source_id;
		
		$filter_count++;
	}
	
	$get_enquiry_source_list_squery = $get_enquiry_source_list_squery_base.$get_enquiry_source_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_source_list_sstatement = $dbConnection->prepare($get_enquiry_source_list_squery);
		
		$get_enquiry_source_list_sstatement -> execute($get_enquiry_source_list_sdata);
		
		$get_enquiry_source_list_sdetails = $get_enquiry_source_list_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_source_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_source_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_source_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get enquiry source type list
INPUT 	: Source Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of enquiry source types
BY 		: Nitin Kashyap
*/
function db_get_enquiry_source_type_list($source_type,$active,$added_by,$start_date,$end_date)
{
	$get_enquiry_source_type_list_squery_base = "select * from crm_enquiry_source_type_master";
	
	$get_enquiry_source_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_enquiry_source_type_list_sdata = array();
	
	if($source_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." where enquiry_source_type_name=:source_type";								
		}
		else
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." and enquiry_source_type_name=:source_type";				
		}
		
		// Data
		$get_enquiry_source_type_list_sdata[':source_type']  = $source_type;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." where enquiry_source_type_active=:active";								
		}
		else
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." and enquiry_source_type_active=:active";				
		}
		
		// Data
		$get_enquiry_source_type_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." where enquiry_source_type_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." and enquiry_source_type_added_by=:added_by";				
		}
		
		// Data
		$get_enquiry_source_type_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." where enquiry_source_type_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." and enquiry_source_type_added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_source_type_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." where enquiry_source_type_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_source_type_list_squery_where = $get_enquiry_source_type_list_squery_where." and enquiry_source_type_added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_source_type_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_enquiry_source_type_list_squery = $get_enquiry_source_type_list_squery_base.$get_enquiry_source_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_source_type_list_sstatement = $dbConnection->prepare($get_enquiry_source_type_list_squery);
		
		$get_enquiry_source_type_list_sstatement -> execute($get_enquiry_source_type_list_sdata);
		
		$get_enquiry_source_type_list_sdetails = $get_enquiry_source_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_source_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_source_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_source_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new reason
INPUT 	: Reason, Added By
OUTPUT 	: Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_reason($reason,$added_by)
{
	// Query
    $reason_iquery = "insert into crm_reason_master (reason_name,reason_active,reason_added_by,reason_added_on) values (:reason_name,:active,:added_by,:now)";  	
    try
    {
        $dbConnection = get_conn_handle();
        
        $reason_istatement = $dbConnection->prepare($reason_iquery);
        
        // Data
        $reason_idata = array(':reason_name'=>$reason,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $reason_istatement->execute($reason_idata);
		$reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $reason_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get reason list
INPUT 	: Reason Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of reasons
BY 		: Nitin Kashyap
*/
function db_get_reason_list($reason,$active,$added_by,$start_date,$end_date)
{
	$get_reason_list_squery_base = "select * from crm_reason_master RM inner join users U on U.user_id = RM.reason_added_by";
	
	$get_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_reason_list_sdata = array();
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_name=:reason_name";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_name=:reason_name";				
		}
		
		// Data
		$get_reason_list_sdata[':reason_name']  = $reason;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_active=:active";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_active=:active";				
		}
		
		// Data
		$get_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_by=:added_by";				
		}
		
		// Data
		$get_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_on >= :start_date";				
		}
		
		//Data
		$get_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_on <= :end_date";				
		}
		
		//Data
		$get_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reason_list_squery = $get_reason_list_squery_base.$get_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reason_list_sstatement = $dbConnection->prepare($get_reason_list_squery);
		
		$get_reason_list_sstatement -> execute($get_reason_list_sdata);
		
		$get_reason_list_sdetails = $get_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new release status
INPUT 	: Release Status, Added By
OUTPUT 	: Release Status id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_release_status($release_status,$added_by)
{
	// Query
    $release_status_iquery = "insert into crm_release_status_master (release_status,release_status_active,release_status_added_by,release_status_added_on) values (:release_status_name,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $release_status_istatement = $dbConnection->prepare($release_status_iquery);
        
        // Data
        $release_status_idata = array(':release_status_name'=>$release_status,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $release_status_istatement->execute($release_status_idata);
		$release_status_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $release_status_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get release status list
INPUT 	: Release Status Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of release status
BY 		: Nitin Kashyap
*/
function db_get_rel_status_list($rel_status,$active,$added_by,$start_date,$end_date)
{
	$get_release_status_list_squery_base = "select * from crm_release_status_master RSM inner join users U on U.user_id = RSM.release_status_added_by";
	
	$get_release_status_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_release_status_list_sdata = array();
	
	if($rel_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." where release_status=:release_status_name";								
		}
		else
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." and release_status=:release_status_name";				
		}
		
		// Data
		$get_release_status_list_sdata[':release_status_name']  = $rel_status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." where release_status_active=:active";								
		}
		else
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." and release_status_active=:active";				
		}
		
		// Data
		$get_release_status_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." where release_status_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." and release_status_added_by=:added_by";				
		}
		
		// Data
		$get_release_status_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." where release_status_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." and release_status_added_on >= :start_date";				
		}
		
		//Data
		$get_release_status_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." where release_status_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_release_status_list_squery_where = $get_release_status_list_squery_where." and release_status_added_on <= :end_date";				
		}
		
		//Data
		$get_release_status_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_release_status_list_squery = $get_release_status_list_squery_base.$get_release_status_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_release_status_list_sstatement = $dbConnection->prepare($get_release_status_list_squery);
		
		$get_release_status_list_sstatement -> execute($get_release_status_list_sdata);
		
		$get_release_status_list_sdetails = $get_release_status_list_sstatement -> fetchAll();
		
		if(FALSE === $get_release_status_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_release_status_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_release_status_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new site type
INPUT 	: Site Type, Added By
OUTPUT 	: Site Type id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_type($site_type,$added_by)
{
	// Query
    $site_type_iquery = "insert into crm_site_type (crm_site_type_name,crm_site_type_active,crm_site_type_added_by,crm_site_type_added_on) values (:site_type,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_type_istatement = $dbConnection->prepare($site_type_iquery);
        
        // Data
        $site_type_idata = array(':site_type'=>$site_type,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $site_type_istatement->execute($site_type_idata);
		$site_type_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_type_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site type list
INPUT 	: Site Type Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of site types
BY 		: Nitin Kashyap
*/
function db_get_site_type_list($site_type,$active,$added_by,$start_date,$end_date)
{
	$get_site_type_list_squery_base = "select * from crm_site_type ST inner join users U on U.user_id = ST.crm_site_type_added_by";
	
	$get_site_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_type_list_sdata = array();
	
	if($site_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." where crm_site_type_name=:site_type_name";								
		}
		else
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." and crm_site_type_name=:site_type_name";				
		}
		
		// Data
		$get_site_type_list_sdata[':site_type_name']  = $site_type;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." where crm_site_type_active=:active";								
		}
		else
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." and crm_site_type_active=:active";				
		}
		
		// Data
		$get_site_type_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." where crm_site_type_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." and crm_site_type_added_by=:added_by";				
		}
		
		// Data
		$get_site_type_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." where crm_site_type_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." and crm_site_type_added_on >= :start_date";				
		}
		
		//Data
		$get_site_type_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." where crm_site_type_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_type_list_squery_where = $get_site_type_list_squery_where." and crm_site_type_added_on <= :end_date";				
		}
		
		//Data
		$get_site_type_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_site_type_list_squery = $get_site_type_list_squery_base.$get_site_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_type_list_sstatement = $dbConnection->prepare($get_site_type_list_squery);
		
		$get_site_type_list_sstatement -> execute($get_site_type_list_sdata);
		
		$get_site_type_list_sdetails = $get_site_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new site status
INPUT 	: Site Status, Max number of days that can be pending in this status, Added By
OUTPUT 	: Site Status id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_status($site_status,$num_days,$added_by)
{
	// Query
    $site_status_iquery = "insert into crm_status_master (status_name,status_num_days,status_active,status_added_by,status_added_on) values (:site_status,:num_days,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_status_istatement = $dbConnection->prepare($site_status_iquery);
        
        // Data
        $site_status_idata = array(':site_status'=>$site_status,':num_days'=>$num_days,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $site_status_istatement->execute($site_status_idata);
		$site_status_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_status_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site status list
INPUT 	: Site Status Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of site status
BY 		: Nitin Kashyap
*/
function db_get_site_status_list($site_status,$active,$added_by,$start_date,$end_date)
{
	$get_site_status_list_squery_base = "select * from crm_status_master SM inner join users U on U.user_id = SM.status_added_by";
	
	$get_site_status_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_status_list_sdata = array();
	
	if($site_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." where status_name=:site_status_name";								
		}
		else
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." and status_name=:site_status_name";				
		}	
		
		// Data
		$get_site_status_list_sdata[':site_status_name']  = $site_status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." where status_active=:active";								
		}
		else
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." and status_active=:active";				
		}
		
		// Data
		$get_site_status_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." where status_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." and status_added_by=:added_by";				
		}
		
		// Data
		$get_site_status_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." where status_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." and status_added_on >= :start_date";				
		}
		
		//Data
		$get_site_status_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." where status_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_status_list_squery_where = $get_site_status_list_squery_where." and status_added_on <= :end_date";				
		}
		
		//Data
		$get_site_status_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_site_status_list_squery = $get_site_status_list_squery_base.$get_site_status_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_status_list_sstatement = $dbConnection->prepare($get_site_status_list_squery);
		
		$get_site_status_list_sstatement -> execute($get_site_status_list_sdata);
		
		$get_site_status_list_sdetails = $get_site_status_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_status_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_status_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_status_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new cab
INPUT 	: Cab travels, Cab Number, Added By
OUTPUT 	: Cab id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_cab($travels,$number,$added_by)
{
	// Query
    $cab_iquery = "insert into crm_cab_master (crm_cab_travels,crm_cab_number,crm_cab_added_by,crm_cab_added_on) values (:travels,:cab_no,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $cab_istatement = $dbConnection->prepare($cab_iquery);
        
        // Data
        $cab_idata = array(':travels'=>$travels,':cab_no'=>$number,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $cab_istatement->execute($cab_idata);
		$cab_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $cab_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get cab
INPUT 	: Cab ID, Cab Number, Added By, Start Date, End Date
OUTPUT 	: List of cabs
BY 		: Nitin Kashyap
*/
function db_get_cab_list($cab_id,$cab_number,$added_by,$start_date,$end_date)
{
	$get_cab_squery_base = "select * from crm_cab_master CM inner join users U on U.user_id = CM.crm_cab_added_by";
	
	$get_cab_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_cab_sdata = array();
	
	if($cab_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." where crm_cab_id=:cab_id";								
		}
		else
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." and crm_cab_id=:cab_id";				
		}
		
		// Data
		$get_cab_sdata[':cab_id']  = $cab_id;
		
		$filter_count++;
	}
	
	if($cab_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." where crm_cab_number=:cab_number";								
		}
		else
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." and crm_cab_number=:cab_number";				
		}
		
		// Data
		$get_cab_sdata[':cab_number']  = $cab_number;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." where crm_cab_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." and crm_cab_added_by=:added_by";				
		}
		
		// Data
		$get_cab_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." where crm_cab_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." and crm_cab_added_on >= :start_date";				
		}
		
		//Data
		$get_cab_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." where added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_cab_squery_where = $get_cab_squery_where." and added_on <= :end_date";				
		}
		
		//Data
		$get_cab_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_cab_squery = $get_cab_squery_base.$get_cab_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_cab_sstatement = $dbConnection->prepare($get_cab_squery);
		
		$get_cab_sstatement -> execute($get_cab_sdata);
		
		$get_cab_sdetails = $get_cab_sstatement -> fetchAll();
		
		if(FALSE === $get_cab_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_cab_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_cab_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new reason
INPUT 	: Name, Length, Breadth, Non-Standard, Added By
OUTPUT 	: Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_dimension($dimension_name,$dimension_length,$dimension_breadth,$dimension_non_standard,$added_by)
{
	// Query
    $dimension_iquery = "insert into crm_dimension_master (crm_dimension_name,crm_dimension_length,crm_dimension_breadth,crm_dimension_non_standard,crm_dimension_active,crm_dimension_added_by,crm_dimension_added_on) values (:name,:length,:breadth,:non_standard,:active,:added_by,:now)";  	
    try
    {
        $dbConnection = get_conn_handle();
        
        $dimension_istatement = $dbConnection->prepare($dimension_iquery);
        
        // Data
        $dimension_idata = array(':name'=>$dimension_name,':length'=>$dimension_length,':breadth'=>$dimension_breadth,':non_standard'=>$dimension_non_standard,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $dimension_istatement->execute($dimension_idata);
		$dimension_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $dimension_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get dimension list
INPUT 	: Name, Length, Breadth, Non Standard, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of dimensions
BY 		: Nitin Kashyap
*/
function db_get_dimension_list($name,$length,$breadth,$non_standard,$active,$added_by,$start_date,$end_date)
{
	$get_dimension_list_squery_base = "select * from crm_dimension_master DM inner join users U on U.user_id = DM.crm_dimension_added_by";
	
	$get_dimension_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_dimension_list_sdata = array();
	
	if($name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_name=:name";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_name=:name";				
		}
		
		// Data
		$get_dimension_list_sdata[':name']  = $name;
		
		$filter_count++;
	}
	
	if($length != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_length=:length";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_length=:length";				
		}
		
		// Data
		$get_dimension_list_sdata[':length']  = $length;
		
		$filter_count++;
	}
	
	if($breadth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_breadth=:breadth";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_breadth=:breadth";				
		}
		
		// Data
		$get_dimension_list_sdata[':breadth']  = $breadth;
		
		$filter_count++;
	}
	
	if($non_standard != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_non_standard=:non_standard";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_non_standard=:non_standard";				
		}
		
		// Data
		$get_dimension_list_sdata[':non_standard']  = $non_standard;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_active=:active";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_active=:active";				
		}
		
		// Data
		$get_dimension_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_added_by=:added_by";				
		}
		
		// Data
		$get_dimension_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_added_on >= :start_date";				
		}
		
		//Data
		$get_dimension_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." where crm_dimension_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_dimension_list_squery_where = $get_dimension_list_squery_where." and crm_dimension_added_on <= :end_date";				
		}
		
		//Data
		$get_dimension_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_dimension_list_squery = $get_dimension_list_squery_base.$get_dimension_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_dimension_list_sstatement = $dbConnection->prepare($get_dimension_list_squery);
		
		$get_dimension_list_sstatement -> execute($get_dimension_list_sdata);
		
		$get_dimension_list_sdetails = $get_dimension_list_sstatement -> fetchAll();
		
		if(FALSE === $get_dimension_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_dimension_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_dimension_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new management block reason
INPUT 	: Reason, Added By
OUTPUT 	: Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_mgmt_block_reason($reason_name,$added_by)
{
	// Query
    $mgmt_block_reason_iquery = "insert into crm_management_block_reason_master (crm_management_block_reason_name,crm_management_block_reason_active,crm_management_block_reason_added_by,crm_management_block_reason_added_on) values (:name,:active,:added_by,:now)";  	
    try
    {
        $dbConnection = get_conn_handle();
        
        $mgmt_block_reason_istatement = $dbConnection->prepare($mgmt_block_reason_iquery);
        
        // Data
        $mgmt_block_reason_idata = array(':name'=>$reason_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $mgmt_block_reason_istatement->execute($mgmt_block_reason_idata);
		$mgmt_block_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $mgmt_block_reason_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get management block reason list
INPUT 	: Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of mgmt block reasons
BY 		: Nitin Kashyap
*/
function db_get_mgmt_block_reason_list($name,$active,$added_by,$start_date,$end_date)
{
	$get_mgmt_blk_reason_list_squery_base = "select * from crm_management_block_reason_master MBRM inner join users U on U.user_id = MBRM.crm_management_block_reason_added_by";
	
	$get_mgmt_blk_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_mgmt_blk_reason_list_sdata = array();
	
	if($name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." where crm_management_block_reason_name=:name";				
		}
		else
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." and crm_management_block_reason_name=:name";				
		}
		
		// Data
		$get_mgmt_blk_reason_list_sdata[':name']  = $name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." where crm_management_block_reason_active=:active";			
		}
		else
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." and crm_management_block_reason_active=:active";				
		}
		
		// Data
		$get_mgmt_blk_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." where crm_management_block_reason_added_by=:added_by";		
		}
		else
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." and crm_management_block_reason_added_by=:added_by";			
		}
		
		// Data
		$get_mgmt_blk_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." where crm_management_block_reason_added_on >= :start_date";	
		}
		else
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." and crm_management_block_reason_added_on >= :start_date";		
		}
		
		//Data
		$get_mgmt_blk_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." where crm_management_block_reason_added_on <= :end_date";		
		}
		else
		{
			// Query
			$get_mgmt_blk_reason_list_squery_where = $get_mgmt_blk_reason_list_squery_where." and crm_management_block_reason_added_on <= :end_date";		
		}
		
		//Data
		$get_mgmt_blk_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_mgmt_blk_reason_list_squery = $get_mgmt_blk_reason_list_squery_base.$get_mgmt_blk_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_mgmt_blk_reason_list_sstatement = $dbConnection->prepare($get_mgmt_blk_reason_list_squery);
		
		$get_mgmt_blk_reason_list_sstatement -> execute($get_mgmt_blk_reason_list_sdata);
		
		$get_mgmt_blk_reason_list_sdetails = $get_mgmt_blk_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_mgmt_blk_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_mgmt_blk_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_mgmt_blk_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get payment mode list
INPUT 	: Payment Mode, Active, Added By, Start Date, End Date
OUTPUT 	: Payment Mode List
BY 		: Nitin Kashyap
*/
function db_get_payment_mode_list($payment_mode,$payment_mode_active,$added_by,$start_date,$end_date)
{
	$get_payment_mode_list_squery_base = "select * from payment_mode_master";
	
	$get_payment_mode_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_payment_mode_list_sdata = array();
	
	if($payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_name=:payment_mode";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_name=:payment_mode";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':payment_mode']  = $payment_mode;
		
		$filter_count++;
	}
	
	if($payment_mode_active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_active=:active";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_active=:active";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':active']  = $payment_mode_active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_by=:added_by";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_on >= :start_date";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_on <= :end_date";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_payment_mode_list_squery = $get_payment_mode_list_squery_base.$get_payment_mode_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_payment_mode_list_sstatement = $dbConnection->prepare($get_payment_mode_list_squery);
		
		$get_payment_mode_list_sstatement -> execute($get_payment_mode_list_sdata);
		
		$get_payment_mode_list_sdetails = $get_payment_mode_list_sstatement -> fetchAll();
		
		if(FALSE === $get_payment_mode_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_payment_mode_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_payment_mode_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new cancellation reason
INPUT 	: Cancellation Reason, Added By
OUTPUT 	: Cancellation Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_cancellation_reason($cancel_reason,$added_by)
{
	// Query
    $cancellation_reason_iquery = "insert into crm_cancellation_reason_master (crm_cancellation_reason,crm_cancellation_reason_active,crm_cancellation_reason_added_by,crm_cancellation_reason_added_on) values (:reason,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $cancellation_reason_istatement = $dbConnection->prepare($cancellation_reason_iquery);
        
        // Data
        $cancellation_reason_idata = array(':reason'=>$cancel_reason,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $cancellation_reason_istatement->execute($cancellation_reason_idata);
		$cancel_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $cancel_reason_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get cancellation reason list
INPUT 	: Reason Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of cancellation reasons
BY 		: Nitin Kashyap
*/
function db_get_cancel_reason_list($reason_name,$active,$added_by,$start_date,$end_date)
{
	$get_cancel_reason_list_squery_base = "select * from crm_cancellation_reason_master CRM inner join users U on U.user_id = CRM.crm_cancellation_reason_added_by";
	
	$get_cancel_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_cancel_reason_list_sdata = array();
	
	if($reason_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." where crm_cancellation_reason=:reason_name";						
		}
		else
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." and crm_cancellation_reason=:reason_name";				
		}
		
		// Data
		$get_cancel_reason_list_sdata[':reason_name']  = $reason_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." where crm_cancellation_reason_active=:active";					
		}
		else
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." and crm_cancellation_reason_active=:active";				
		}
		
		// Data
		$get_cancel_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." where crm_cancellation_reason_added_by=:added_by";				
		}
		else
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." and crm_cancellation_reason_added_by=:added_by";				
		}
		
		// Data
		$get_cancel_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." where crm_cancellation_reason_added_on >= :start_date";			
		}
		else
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." and crm_cancellation_reason_added_on >= :start_date";				
		}
		
		//Data
		$get_cancel_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." where crm_cancellation_reason_added_on <= :end_date";				
		}
		else
		{
			// Query
			$get_cancel_reason_list_squery_where = $get_cancel_reason_list_squery_where." and crm_cancellation_reason_added_on <= :end_date";				
		}
		
		//Data
		$get_cancel_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_cancel_reason_list_squery = $get_cancel_reason_list_squery_base.$get_cancel_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_cancel_reason_list_sstatement = $dbConnection->prepare($get_cancel_reason_list_squery);
		
		$get_cancel_reason_list_sstatement -> execute($get_cancel_reason_list_sdata);
		
		$get_cancel_reason_list_sdetails = $get_cancel_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_cancel_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_cancel_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_cancel_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get CRM delay reason list
INPUT 	: Delay Reason, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of delay reasons
BY 		: Nitin Kashyap
*/
function db_get_crm_delay_reason_master_list($reason_name,$active,$added_by,$start_date,$end_date)
{
	$get_reason_list_squery_base = "select * from crm_delay_reason_master";
	
	$get_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_reason_list_sdata = array();
	
	if($reason_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where crm_delay_reason_master_name = :reason_name";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and crm_delay_reason_master_name = :reason_name";				
		}
		
		// Data
		$get_reason_list_sdata[':reason_name']  = $reason_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where crm_delay_reason_master_active = :active";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and crm_delay_reason_master_active = :active";				
		}
		
		// Data
		$get_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where crm_delay_reason_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and crm_delay_reason_master_added_by = :added_by";				
		}
		
		// Data
		$get_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where crm_delay_reason_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and crm_delay_reason_master_added_on >= :start_date";				
		}
		
		//Data
		$get_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where crm_delay_reason_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and crm_delay_reason_master_added_on <= :end_date";				
		}
		
		//Data
		$get_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reason_list_squery = $get_reason_list_squery_base.$get_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reason_list_sstatement = $dbConnection->prepare($get_reason_list_squery);
		
		$get_reason_list_sstatement -> execute($get_reason_list_sdata);
		
		$get_reason_list_sdetails = $get_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
?>