<?php
/*
FILE		: task_listdownload.php
CREATED ON	: 19-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Download all files for a particular process plan
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

// Query String Data
$process_plan_id = $_GET["process"];

// Get the documents
$legal_task_plan_list = i_get_task_plan_list('','','',$process_plan_id,'','','1');
if($legal_task_plan_list["status"] == SUCCESS)
{
	$legal_task_plan_list_data = $legal_task_plan_list["data"];
	$file_array = "[";
	$files_exist = 0;
	for($count = 0; $count < count($legal_task_plan_list_data); $count++)
	{
		if($legal_task_plan_list_data[$count]["task_plan_document_path"] != "")
		{
			$file_url = 'http://demo.iweavesolutions.com'.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'documents'.DIRECTORY_SEPARATOR.$legal_task_plan_list_data[$count]["task_plan_document_path"];
			$file_array = $file_array."'".$file_url."'".','.' ';
			$files_exist = 1;
		}	
	}
	
	$file_array = trim($file_array,' ');
	$file_array = trim($file_array,',');
	$file_array = $file_array.']';
	
	if($files_exist == 1)
	{
		echo "<script>
		var files = ".$file_array.";
		for (var i = files.length - 1; i >= 0; i--) {
			var a = document.createElement(\"a\");
			a.target = \"_blank\";
			a.download = \"download\";
			a.href = files[i];
			a.click();
		};
	    </script>";
	}
	else
	{
		echo "No files uploaded yet for this project!";
	}
}
else
{
	$alert = $alert."No tasks at all";
}	
?>