<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management_masters.php');

/*
PURPOSE : To add new Project Management Master
INPUT 	: Name, Project Start Date, Location, Remarks, Added by
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_management_master($name, $project_start_date, $location, $remarks, $added_by)
{
    $project_management_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_management_master_sresult = db_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_sresult["status"] == DB_NO_RECORD) {
        $project_management_master_iresult = db_add_project_management_master($name, $project_start_date, $location, $remarks, $added_by);

        if ($project_management_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This project already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Management Master List
INPUT 	: Project IS, Name, Project Start Date, Location, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Projects or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_management_master_list($project_management_master_search_data)
{
    $project_management_master_sresult = db_get_project_management_master_list($project_management_master_search_data);

    if ($project_management_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_management_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Management Master
INPUT 	: Project ID, Project Management Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_management_master($project_id, $name, $project_management_master_update_data)
{
    $project_management_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_management_master_sresult = db_get_project_management_master_list($project_management_master_search_data);

    $allow_update = false;
    if ($project_management_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_management_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_management_master_sresult['data'][0]['project_management_master_id'] == $project_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_management_master_sresult = db_update_project_management_master($project_id, $project_management_master_update_data);

        if ($project_management_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Management Project Master Successfully added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Management Project Master is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Management Master
INPUT 	: Project ID, Project Management Master Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_management_master($project_id, $project_management_master_update_data)
{
    $project_management_master_sresult = db_update_project_management_master($project_id, $project_management_master_update_data);

    if ($project_management_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_process_master($name, $road, $method, $remarks, $added_by)
{
    $project_process_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_process_master_sresult = db_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_sresult["status"] == DB_NO_RECORD) {
        $project_process_master_iresult =  db_add_project_process_master($name, $road, $method, $remarks, $added_by);

        if ($project_process_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Process Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This process already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Process Master list
INPUT 	: Process ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Process Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_process_master($project_process_master_search_data)
{
    $project_process_master_sresult = db_get_project_process_master($project_process_master_search_data);

    if ($project_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_process_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No process added yet. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Process Master
INPUT 	: Process ID, Project Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_process_master($process_id, $name, $project_process_master_update_data)
{
    $project_process_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_process_master_sresult = db_get_project_process_master($project_process_master_search_data);

    $allow_update = false;
    if ($project_process_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_process_master_sresult['data'][0]['project_process_master_id'] == $process_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_process_master_sresult = db_update_project_process_master($process_id, $project_process_master_update_data);

        if ($project_process_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Process Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Process Master is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}

/*
PURPOSE : To Delete Project Process Master
INPUT 	: Process ID, Project Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_process_master($process_id, $project_process_master_update_data)
{
    $project_process_master_sresult = db_update_project_process_master($process_id, $project_process_master_update_data);

    if ($project_process_master_sresult['status'] == SUCCESS) {
        // Delete all user mappings
        $project_process_user_mapping_update_data['active'] = '0';
        $project_user_dresult = i_delete_project_process_user_mapping('', $project_process_user_mapping_update_data, $process_id);

        // Delete all tasks of this process
        $project_task_master_update_data['active'] = '0';
        $tasks_dresult = i_delete_project_task_master('', $project_task_master_update_data, $process_id);

        $return["data"]   = "Process Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Task Master
INPUT 	: Name, Process, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_master($name, $uom, $process, $remarks, $added_by)
{
    $project_task_master_search_data = array("name"=>$name,"process"=>$process);
    $project_task_master_sresult = db_get_project_task_master($project_task_master_search_data);
    if ($project_task_master_sresult["status"] == DB_NO_RECORD) {
        $project_task_master_iresult =  db_add_project_task_master($name, $uom, $process, $remarks, $added_by);

        if ($project_task_master_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_task_master_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This task already exists for this process";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Task Master list
INPUT 	: Task Master ID, Name, Process, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Task Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_task_master($project_task_master_search_data)
{
    $project_task_master_sresult = db_get_project_task_master($project_task_master_search_data);

    if ($project_task_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Task Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Master
INPUT 	: Task Master ID, Project Task Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_task_master($task_master_id, $name, $process, $project_task_master_update_data)
{
    $project_task_master_search_data = array("name"=>$project_task_master_update_data['name'],"task_name_check"=>'1',"process"=>$project_task_master_update_data['process'],
    "active"=>'1');
    $project_task_master_sresult = db_get_project_task_master($project_task_master_search_data);

    $allow_update = false;
    if ($project_task_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_task_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_task_master_sresult['data'][0]['project_task_master_id'] == $task_master_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_task_master_sresult = db_update_project_task_master($task_master_id, $project_task_master_update_data);

        if ($project_task_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Task Master Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Project Task Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To Delete Project Task Master
INPUT 	: Task Master ID, Project Task Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_task_master($task_master_id, $project_task_master_update_data, $process_id='')
{
    $project_task_master_sresult = db_update_project_task_master($task_master_id, $project_task_master_update_data, $process_id);

    if ($project_task_master_sresult['status'] == SUCCESS) {
        if ($process_id != '') {
            // Get all tasks
            $project_task_master_search_data['process'] = $process_id;
            $tasks_sresult = i_get_project_task_master($project_task_master_search_data);

            if ($tasks_sresult['status'] == SUCCESS) {
                for ($count = 0; $count < count($tasks_sresult['data']); $count++) {
                    $project_task_user_mapping_update_data['active'] = '0';
                    $task_user_dresult = i_delete_project_task_user_mapping('', $project_task_user_mapping_update_data, $tasks_sresult['data'][$count]['project_task_master_id']);
                }
            }
        } else {
            $project_task_user_mapping_update_data['active'] = '0';
            $task_user_dresult = i_delete_project_task_user_mapping('', $project_task_user_mapping_update_data, $task_master_id);
        }

        $return["data"]   = "Project Task Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Man Power Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_man_power_master($name, $remarks, $added_by)
{
    $project_man_power_master_search_data = array("name"=>$name);
    $project_man_power_master_sresult = db_get_project_man_power_master($project_man_power_master_search_data);
    if ($project_man_power_master_sresult["status"] == DB_NO_RECORD) {
        $project_man_power_master_iresult =  db_add_project_man_power_master($name, $remarks, $added_by);

        if ($project_man_power_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Man Power Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Man Power Master already exists for this Project";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get project Man Power Type Master list
INPUT 	: Power Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Power Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_man_power_master($project_man_power_master_search_data)
{
    $project_man_power_master_sresult = db_get_project_man_power_master($project_man_power_master_search_data);

    if ($project_man_power_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_man_power_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Man Power Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Type Master
INPUT 	: Power Type ID, Project Man Power Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_man_power_master($power_type_id, $name, $project_man_power_master_update_data)
{
    $project_man_power_master_search_data = array("name"=>$name);
    $project_man_power_master_sresult = db_get_project_man_power_master($project_man_power_master_search_data);
    if ($project_man_power_master_sresult["status"] == DB_NO_RECORD) {
        $project_man_power_master_sresult = db_update_project_man_power_master($power_type_id, $project_man_power_master_update_data);

        if ($project_man_power_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Man Power Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Man Power Master is already exist for this Project";
        $return["status"] = FAILURE;
    }
    return $return;
}


/*
PURPOSE : To Delete Project Man Power Type Master
INPUT 	: Power Type ID, Project Man Power Master Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_man_power_master($power_type_id, $project_man_power_master_update_data)
{
    $project_man_power_master_sresult = db_update_project_man_power_master($power_type_id, $project_man_power_master_update_data);

    if ($project_man_power_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_reason_master($name, $remarks, $added_by)
{
    $project_reason_master_search_data = array("name"=>$name);
    $project_reason_master_sresult = db_get_project_reason_master($project_reason_master_search_data);
    if ($project_reason_master_sresult["status"] == DB_NO_RECORD) {
        $project_reason_master_iresult =  db_add_project_reason_master($name, $remarks, $added_by);

        if ($project_reason_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Reason Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Reason already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Reason Master list
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Reason Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_reason_master($project_reason_master_search_data)
{
    $project_reason_master_sresult = db_get_project_reason_master($project_reason_master_search_data);

    if ($project_reason_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_reason_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Reason Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Reason Master
INPUT 	: Reason ID, Project Reason Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_reason_master($reason_id, $name, $project_reason_master_update_data)
{
    $project_reason_master_search_data = array("name"=>$name);
    $project_reason_master_sresult = db_get_project_reason_master($project_reason_master_search_data);
    if ($project_reason_master_sresult["status"] == DB_NO_RECORD) {
        $project_reason_master_sresult = db_update_project_reason_master($reason_id, $project_reason_master_update_data);

        if ($project_reason_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Reason Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Reason Master is already exist for this Project";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To Delete Project Reason Master
INPUT 	: Reason ID, Project Reason Master Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_reason_master($reason_id, $project_reason_master_update_data)
{
    $project_reason_master_sresult = db_update_project_reason_master($reason_id, $project_reason_master_update_data);

    if ($project_reason_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Reason Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Master
INPUT 	: Name, Number, Use, Machine Type, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_master($name, $machine_no, $vendor, $type, $use, $machine_type, $remarks, $added_by)
{
    $project_machine_master_search_data = array("name"=>$name,"machine_name_check"=>'1',"number"=>$machine_no,"vendor"=>$vendor,"active"=>'1');
    $project_machine_master_sresult = db_get_project_machine_master($project_machine_master_search_data);
    if ($project_machine_master_sresult["status"] == DB_NO_RECORD) {
        $project_machine_master_iresult = db_add_project_machine_master($name, $machine_no, $vendor, $type, $use, $machine_type, $remarks, $added_by);

        if ($project_machine_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Machine Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To get project Machine Master list
INPUT 	: machine ID, Name, Number, Use, Machine Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Machine Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_master($project_machine_master_search_data)
{
    $project_machine_master_sresult = db_get_project_machine_master($project_machine_master_search_data);

    if ($project_machine_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Machine Master Added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Master
INPUT 	: Machine ID, Project Machine Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_master($machine_id, $vendor, $project_machine_master_update_data)
{
    $project_machine_master_search_data = array("name"=>$project_machine_master_update_data['name'],"machine_name_check"=>'1',"vendor"=>$vendor,"active"=>'1');
    $project_machine_master_sresult = db_get_project_machine_master($project_machine_master_search_data);

    $allow_update = false;
    if ($project_machine_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_machine_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_machine_master_sresult['data'][0]['project_machine_master_id'] == $machine_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_machine_master_sresult = db_update_project_machine_master($machine_id, $project_machine_master_update_data);

        if ($project_machine_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Machine Master Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Machine Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To delete Project Machine Master
INPUT 	: Machine ID, Project Machine Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_machine_master($machine_id, $project_machine_master_update_data)
{
    $project_machine_master_sresult = db_update_project_machine_master($machine_id, $project_machine_master_update_data);

    if ($project_machine_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Machine Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_type_master($name, $remarks, $added_by)
{
    $project_machine_type_master_search_data = array("name"=>$name);
    $project_machine_type_master_sresult = db_get_project_machine_type_master($project_machine_type_master_search_data);
    if ($project_machine_type_master_sresult["status"] == DB_NO_RECORD) {
        $project_machine_type_master_iresult = db_add_project_machine_type_master($name, $remarks, $added_by);

        if ($project_machine_type_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Type Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Type already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To get project Machine Type Master list
INPUT 	: Machine Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Machine Type Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_type_master($project_machine_type_master_search_data)
{
    $project_machine_type_master_sresult = db_get_project_machine_type_master($project_machine_type_master_search_data);

    if ($project_machine_type_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_type_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Machine Type Master Added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Type Master
INPUT 	: Machine Type ID, Project Machine Type Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_type_master($machine_type_id, $name, $project_machine_type_master_update_data)
{
    $project_machine_type_master_search_data = array("name"=>$name);
    $project_machine_type_master_sresult = db_get_project_machine_type_master($project_machine_type_master_search_data);
    if ($project_machine_type_master_sresult["status"] == DB_NO_RECORD) {
        $project_machine_type_master_sresult =  db_update_project_machine_type_master($machine_type_id, $project_machine_type_master_update_data);

        if ($project_machine_type_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Machine Type Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Type Master is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Machine Type Master
INPUT 	: Machine Type ID, Project Machine Type Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_machine_type_master($machine_type_id, $project_machine_type_master_update_data)
{
    $project_machine_type_master_sresult =  db_update_project_machine_type_master($machine_type_id, $project_machine_type_master_update_data);

    if ($project_machine_type_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Type Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Man Power Rate Master
INPUT 	: Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_man_power_rate_master($power_type_id, $vendor_id, $cost_per_hours, $applicable_date, $remarks, $added_by)
{
    $project_man_power_rate_iresult =  db_add_project_man_power_rate_master($power_type_id, $vendor_id, $cost_per_hours, $applicable_date, $remarks, $added_by);

    if ($project_man_power_rate_iresult['status'] == SUCCESS) {
        $return["data"]   = "Man Power Rate Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Man Power Rate Master list
INPUT 	: Rate ID, Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate Master, success or failure message
BY 		: Lakshmi
*/
function i_get_project_man_power_rate_master($project_man_power_rate_search_data)
{
    $project_man_power_rate_sresult = db_get_project_man_power_rate_master($project_man_power_rate_search_data);

    if ($project_man_power_rate_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_man_power_rate_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Man Power Rate Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To get Project Man Power Rate Master list
INPUT 	: Rate ID, Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate Master, success or failure message
BY 		: Lakshmi
*/
function i_get_project_man_power_max_rate_master($project_man_power_rate_search_data)
{
    $project_man_power_rate_sresult = db_get_project_man_power_max_rate_master($project_man_power_rate_search_data);

    if ($project_man_power_rate_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_man_power_rate_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Man Power Rate Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Rate Master
INPUT 	: Rate ID, Project Man Power Rate Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_man_power_rate_master($rate_id, $project_man_power_rate_update_data)
{
    $project_man_power_rate_sresult = db_update_project_man_power_rate_master($rate_id, $project_man_power_rate_update_data);

    if ($project_man_power_rate_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Rate Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Process User Mapping
INPUT 	: Process ID, User ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_process_user_mapping($process_id, $user_id, $remarks, $added_by)
{
    $project_process_user_mapping_search_data = array("process_id"=>$process_id,"user_id"=>$user_id);
    $project_process_user_mapping_sresult = db_get_project_process_user_mapping($project_process_user_mapping_search_data);
    if ($project_process_user_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_process_user_mapping_iresult = db_add_project_process_user_mapping($process_id, $user_id, $remarks, $added_by);

        if ($project_process_user_mapping_iresult['status'] == SUCCESS) {
            $return["data"]   = "Process User Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This user is already mapped to this process";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Process User Mapping list
INPUT 	: Mapping Id, Process ID, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Process User Mapping List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_process_user_mapping($project_process_user_mapping_search_data)
{
    $project_process_user_mapping_sresult = db_get_project_process_user_mapping($project_process_user_mapping_search_data);

    if ($project_process_user_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_process_user_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No user is mapped to the process. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Process User Mapping
INPUT 	: Mapping ID,Project Process User Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_process_user_mapping($mapping_id, $process_id, $user_id, $project_process_user_mapping_update_data)
{
    $project_process_user_mapping_sresult = db_update_project_process_user_mapping($mapping_id, $project_process_user_mapping_update_data);

    if ($project_process_user_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Process User Mapping Successfully updated";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Process User Mapping
INPUT 	: Mapping ID,Project Process User Mapping Delete Array,Process ID
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_process_user_mapping($mapping_id, $project_process_user_mapping_update_data, $process_id='')
{
    $project_process_user_mapping_sresult = db_update_project_process_user_mapping($mapping_id, $project_process_user_mapping_update_data, $process_id);

    if ($project_process_user_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Process User Mapping Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Project Task User Mapping
INPUT 	: Task ID, User ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_user_mapping($task_id, $user_id, $remarks, $added_by)
{
    $project_task_user_mapping_search_data = array("task_id"=>$task_id,"user_id"=>$user_id);
    $project_task_user_mapping_sresult = db_get_project_task_user_mapping($project_task_user_mapping_search_data);
    if ($project_task_user_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_task_user_mapping_iresult =  db_add_project_task_user_mapping($task_id, $user_id, $remarks, $added_by);

        if ($project_task_user_mapping_iresult['status'] == SUCCESS) {
            $return["data"]   = "Task User Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This user is already mapped to this task!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project task User Mapping list
INPUT 	: Mapping Id, Task ID, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project task User Mapping List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_task_user_mapping($project_task_user_mapping_search_data)
{
    $project_task_user_mapping_sresult = db_get_project_task_user_mapping($project_task_user_mapping_search_data);

    if ($project_task_user_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_user_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No user mapped yet. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Task User Mapping
INPUT 	: Mapping ID, Project Task User Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_task_user_mapping($mapping_id, $task_id, $user_id, $project_task_user_mapping_update_data)
{
    $project_task_user_mapping_sresult = db_update_project_task_user_mapping($mapping_id, $project_task_user_mapping_update_data);

    if ($project_task_user_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Task User Mapping Successfully Updated";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Task User Mapping
INPUT 	: Mapping ID, Project Task User Mapping Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_task_user_mapping($mapping_id, $project_task_user_mapping_update_data, $task_id='')
{
    $project_task_user_mapping_sresult = db_update_project_task_user_mapping($mapping_id, $project_task_user_mapping_update_data, $task_id);

    if ($project_task_user_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Task User Mapping Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add Project Machine Rate Master
INPUT 	: Machine ID, Machine Rate, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_rate_master($machine_id, $machine_rate, $kns_fuel, $vendor_fuel, $kns_bata, $vendor_bata, $remarks, $added_by)
{
    $project_machine_rate_master_search_data = array("machine_id"=>$machine_id,"machine_rate"=>$machine_rate,"active"=>'1');
    $project_machine_rate_master_sresult = db_get_project_machine_rate_master($project_machine_rate_master_search_data);
    if ($project_machine_rate_master_sresult["status"] == DB_NO_RECORD) {
        $project_machine_rate_master_iresult =  db_add_project_machine_rate_master($machine_id, $machine_rate, $kns_fuel, $vendor_fuel, $kns_bata, $vendor_bata, $remarks, $added_by);

        if ($project_machine_rate_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Rate Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This rate already exists for this Master";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Machine Rate Master list
INPUT 	: Machine Rate ID, Machine ID, Machine Rate, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Machine Rate Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_rate_master($project_machine_rate_master_search_data)
{
    $project_machine_rate_master_sresult = db_get_project_machine_rate_master($project_machine_rate_master_search_data);

    if ($project_machine_rate_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_rate_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Machine Rate Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Rate Master
INPUT 	: Master Rate ID, Project Machine Rate Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_rate_master($machine_rate_id, $machine_id, $machine_rate, $project_machine_rate_master_update_data)
{
    $project_machine_rate_master_search_data = array("machine_id"=>$machine_id,"machine_rate"=>$machine_rate,"active"=>'1');
    $project_machine_rate_master_sresult = db_get_project_machine_rate_master($project_machine_rate_master_search_data);

    $allow_update = false;
    if ($project_machine_rate_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_machine_rate_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_machine_rate_master_sresult['data'][0]['project_machine_rate_master_id'] == $machine_rate_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_machine_rate_master_sresult = db_update_project_machine_rate_master($machine_rate_id, $project_machine_rate_master_update_data);

        if ($project_machine_rate_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Rate Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Rate Master is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}

/*
PURPOSE : To Delete Project Machine Rate Master
INPUT 	: Task Master ID, Project Task Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_machine_rate_master($machine_rate_id, $project_machine_rate_master_update_data)
{
    $project_machine_rate_master_sresult = db_update_project_machine_rate_master($machine_rate_id, $project_machine_rate_master_update_data);

    if ($project_machine_rate_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Rate Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_delay_reason($task_id, $road_id, $name, $start_date, $end_date, $remarks, $added_by)
{
    $delay_reason_iresult =  db_add_project_delay_reason($task_id, $road_id, $name, $start_date, $end_date, $remarks, $added_by);

    if ($delay_reason_iresult['status'] == SUCCESS) {
        $return["data"]   = "Delay Reason Master Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Delay Reason Master, success or failure message
BY 		: Lakshmi
*/
function i_get_project_delay_reason($delay_reason_search_data)
{
    $delay_reason_sresult = db_get_project_delay_reason($delay_reason_search_data);

    if ($delay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$delay_reason_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Delay Reason Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Delay Reason Master
INPUT 	: Reason ID, Delay Reason Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_delay_reason($reason_id, $delay_reason_update_data)
{
    $delay_reason_sresult = db_update_project_delay_reason($reason_id, $delay_reason_update_data);

    if ($delay_reason_sresult['status'] == SUCCESS) {
        $return["data"]   = "Delay Reason Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}/*
PURPOSE : To add new Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_delay_reason_master($name, $remarks, $added_by)
{
    $delay_reason_master_iresult =  db_add_delay_reason_master($name, $remarks, $added_by);

    if ($delay_reason_master_iresult['status'] == SUCCESS) {
        $return["data"]   = "Delay Reason Master Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Delay Reason Master, success or failure message
BY 		: Lakshmi
*/
function i_get_delay_reason_master($delay_reason_master_search_data)
{
    $delay_reason_master_sresult = db_get_delay_reason_master($delay_reason_master_search_data);

    if ($delay_reason_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$delay_reason_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Delay Reason Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Delay Reason Master
INPUT 	: Reason ID, Delay Reason Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_delay_reason_master($reason_id, $delay_reason_master_update_data)
{
    $delay_reason_master_sresult = db_update_delay_reason_master($reason_id, $delay_reason_master_update_data);

    if ($delay_reason_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Delay Reason Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Manpower Agency
INPUT 	: Agency ID, Name, Remarks, Added By
OUTPUT 	: Agency ID, success or failure message
BY 		: Ashwini
*/

function i_add_project_manpower_agency($name,$contact_name,$contact_number,$address,$email,$pan_number,$tin_number,$gst_number,$acc_name,$acc_bank,$acc_branch,$acc_no,$ifsc_code,$sec_acc_name,$sec_acc_bank,$sec_acc_branch,$sec_acc_no,$sec_ifsc_code,$remarks,$added_by)
{
    $project_manpower_agency_search_data = array("name"=>$name,"agency_name_check"=>'1',"active"=>'1');
    $project_manpower_agency_sresult = db_get_project_manpower_agency($project_manpower_agency_search_data);

    if ($project_manpower_agency_sresult["status"] == DB_NO_RECORD) {
      $project_manpower_agency_search_data = array("order"=>'desc',"active"=>'1');
      $project_manpower_agency_sresult = db_get_project_manpower_agency($project_manpower_agency_search_data);
  		if($project_manpower_agency_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
  		{
  			$agency_latest_id = $project_manpower_agency_sresult["data"][0]["project_manpower_agency_id"] + 1;
  		}
  		else
  		{
  			$agency_latest_id = 1;
  		}
      $vendor_code = strtoupper("NMR/".$agency_latest_id);

        $project_manpower_agency_iresult = db_add_project_manpower_agency($name,$vendor_code,$contact_name,$contact_number,$address,$email,$pan_number,$tin_number,$gst_number,$acc_name,$acc_bank,$acc_branch,$acc_no,$ifsc_code,$sec_acc_name,$sec_acc_bank,$sec_acc_branch,$sec_acc_no,$sec_ifsc_code,$remarks,$added_by);

        if ($project_manpower_agency_iresult['status'] == SUCCESS) {
            $return["data"]   = "Manpower Agency Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Manpower Agency name already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Manpower Agency List
INPUT 	: Manpower Agency ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Manpower Agency
BY 		: Ashwini
*/
function i_get_project_manpower_agency($project_manpower_agency_search_data)
{
    $project_manpower_agency_sresult = db_get_project_manpower_agency($project_manpower_agency_search_data);

    if ($project_manpower_agency_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_manpower_agency_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Manpower Agency Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Manpower Agency
INPUT 	: Manpower Agency ID, Project Manpower Agency Update Array
OUTPUT 	: Manpower Agency ID; Message of success or failure
BY 		: Ashwini
*/

function i_update_project_manpower_agency($agency_id, $project_manpower_agency_update_data)
{
    $project_manpower_agency_search_data = array("name"=>$project_manpower_agency_update_data['name'],"agency_name_check"=>'1',"active"=>'1');
    $project_manpower_agency_sresult = db_get_project_manpower_agency($project_manpower_agency_search_data);

    $allow_update = false;
    if ($project_manpower_agency_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_manpower_agency_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_manpower_agency_sresult['data'][0]['project_manpower_agency_id'] == $agency_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_manpower_agency_sresult = db_update_project_manpower_agency($agency_id, $project_manpower_agency_update_data);

        if ($project_manpower_agency_sresult['status'] == SUCCESS) {
            $return["data"]   = "Manpower Agency Details Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Agency Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete APF Bank Master
INPUT 	: Bank ID, APF Bank Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_manpower_agency($agency_id, $project_manpower_agency_update_data)
{
    $project_manpower_agency_update_data = array('active'=>'0');
    $project_manpower_agency_sresult = db_update_project_manpower_agency($agency_id, $project_manpower_agency_update_data);

    if ($project_manpower_agency_sresult['status'] == SUCCESS) {
        $return["data"]   = "Agency Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Machine Vendor Master
INPUT 	: Name, Remarks, Added by
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_add_project_machine_vendor_master($name, $contact_name,$contact_number,$address,$email,$pan_number,$tin_number,$gst_number,$acc_name,$acc_bank,$acc_branch,$acc_no,$ifsc_code,$sec_acc_name,$sec_acc_bank,$sec_acc_branch,$sec_acc_no,$sec_ifsc_code,$remarks, $added_by)
{
    $project_machine_vendor_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_machine_vendor_master_sresult = db_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
    if ($project_machine_vendor_master_sresult["status"] == DB_NO_RECORD) {
      $project_machine_master_search_data = array("order"=>'desc',"active"=>'1');
      $project_machine_master_sresult = db_get_project_machine_vendor_master_list($project_machine_master_search_data);
      if($project_machine_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
      {
        $vendor_latest_id = $project_machine_master_sresult["data"][0]["project_machine_vendor_master_id"] + 1;
      }
      else
      {
        $vendor_latest_id = 1;
      }
      $vendor_code = strtoupper("MC/".$vendor_latest_id);
      $project_machine_vendor_master_iresult = db_add_project_machine_vendor_master($name,$vendor_code,$contact_name,$contact_number,$address,$email,$pan_number,$tin_number,$gst_number,$acc_name,$acc_bank,$acc_branch,$acc_no,$ifsc_code,$sec_acc_name,$sec_acc_bank,$sec_acc_branch,$sec_acc_no,$sec_ifsc_code, $remarks,$added_by);

        if ($project_machine_vendor_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Machine Vendor Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Vendor already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To get Project Machine Vendor Master List
INPUT 	: Master ID, Name, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Projects or Error Details, success or failure message
BY 		: Ashwini
*/

function i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data)
{
    $project_machine_vendor_master_sresult = db_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);

    if ($project_machine_vendor_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_vendor_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Vendor Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Vendor Master
INPUT 	: Master ID, Project Machine Vendor Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_update_project_machine_vendor_master($master_id, $name, $project_machine_vendor_master_update_data)
{
    $project_machine_vendor_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
    $project_machine_vendor_master_sresult = db_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);

    $allow_update = false;
    if ($project_machine_vendor_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_machine_vendor_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_machine_vendor_master_sresult['data'][0]['project_machine_vendor_master_id'] == $master_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_machine_vendor_master_sresult = db_update_project_machine_vendor_master($master_id, $project_machine_vendor_master_update_data);

        if ($project_machine_vendor_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Vendor Master Successfully added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Vendor Master is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Machine vendor Master
INPUT 	: Master ID, Project Machine Vendor Master Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_delete_project_machine_vendor_master($master_id, $project_machine_vendor_master_update_data)
{
    $project_machine_vendor_master_sresult = db_update_project_machine_vendor_master($master_id, $project_machine_vendor_master_update_data);

    if ($project_machine_vendor_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Vendor Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Contract Rate Master
INPUT 	: Process, UOM, rate, Remarks, Added By
OUTPUT 	: Contract Rate ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_contract_rate_master($vendor_id, $process, $work_task, $aplicable_date, $uom, $rate, $remarks, $added_by)
{
    $project_contract_rate_master_search_data = array("process"=>$process,"vendor_id"=>$vendor_id,"work_task"=>$work_task,"active"=>'1');
    $project_contract_rate_master_sresult = db_get_project_contract_rate_master($project_contract_rate_master_search_data);
    if ($project_contract_rate_master_sresult["status"] == DB_NO_RECORD) {
        $project_contract_rate_master_iresult =  db_add_project_contract_rate_master($vendor_id, $process, $work_task, $aplicable_date, $uom, $rate, $remarks, $added_by);

        if ($project_contract_rate_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Contract Rate Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Rate already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Rate Master List
INPUT 	: Contract Rate ID, Process, UOM, Rate, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rate Master
BY 		: Lakshmi
*/
function i_get_project_contract_rate_master($project_contract_rate_master_search_data)
{
    $project_contract_rate_master_sresult = db_get_project_contract_rate_master($project_contract_rate_master_search_data);

    if ($project_contract_rate_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_contract_rate_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Rate added yet. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Rate Master List
INPUT 	: Contract Rate ID, Process, UOM, Rate, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rate Master
BY 		: Lakshmi
*/
function i_get_project_contract_rate_max_master($project_contract_rate_master_search_data)
{
    $project_contract_rate_master_sresult = db_get_project_contract_max_rate_master($project_contract_rate_master_search_data);

    if ($project_contract_rate_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_contract_rate_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Rate added yet. Please Contact The System Admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Contract Rate Master
INPUT 	: Contract Rate ID, Project Contract Rate Master Update Array
OUTPUT 	: Contract Rate ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_contract_rate_master($contract_rate_id, $process, $uom, $rate, $project_contract_rate_master_update_data)
{
    /*$project_contract_rate_master_search_data = array("process"=>$process,"uom"=>$uom,"rate"=>$rate,"active"=>'1');
    $project_contract_rate_master_sresult = db_get_project_contract_rate_master($project_contract_rate_master_search_data);*/

    $allow_update = true;
    /*if($project_contract_rate_master_sresult["status"] == DB_NO_RECORD)
    {
        $allow_update = true;
    }
    else if($project_contract_rate_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
    {
        if($project_contract_rate_master_sresult['data'][0]['project_contract_rate_master_id'] == $contract_rate_id)
        {
            $allow_update = true;
        }
    }*/

    if ($allow_update == true) {
        $project_contract_rate_master_sresult = db_update_project_contract_rate_master($contract_rate_id, $project_contract_rate_master_update_data);

        if ($project_contract_rate_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Contract Rate Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Contract Rate Master is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}

/*
PURPOSE : To Delete Project Contract Rate Master
INPUT 	: Contract Rate ID, Project Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_contract_rate_master($contract_rate_id, $project_contract_rate_master_update_data)
{
    $project_contract_rate_master_sresult = db_update_project_contract_rate_master($contract_rate_id, $project_contract_rate_master_update_data);

    if ($project_contract_rate_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Contract Rate Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Contract Process Master
INPUT 	: Contract Process ID, Name, Remarks, Added By
OUTPUT 	: Contract Process ID, success or failure message
BY 		: Ashwini
*/

function i_add_project_contract_process($name, $remarks, $added_by)
{
    $project_contract_process_search_data = array("name"=>$name,"contract_process_name_check"=>'1',"active"=>'1');
    $project_contract_process_sresult = db_get_project_contract_process($project_contract_process_search_data);

    if ($project_contract_process_sresult["status"] == DB_NO_RECORD) {
        $project_contract_process_iresult = db_add_project_contract_process($name, $remarks, $added_by);

        if ($project_contract_process_iresult['status'] == SUCCESS) {
            $return["data"]   = "Contract Process Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Contract Process Name already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Process Master List
INPUT 	: Contract Process ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Process Master
BY 		: Ashwini
*/
function i_get_project_contract_process($project_contract_process_search_data)
{
    $project_contract_process_sresult = db_get_project_contract_process($project_contract_process_search_data);

    if ($project_contract_process_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_contract_process_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Contract Process Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Contract Process Master
INPUT 	: Contract Process ID,Project Contract Process Master Update Array
OUTPUT 	: Contract Process ID; Message of success or failure
BY 		: Ashwini
*/

function i_update_project_contract_process($contract_process_id, $project_contract_process_update_data)
{
    $project_contract_process_search_data = array("name"=>$project_contract_process_update_data['name'],"contract_process_name_check"=>'1',"active"=>'1');
    $project_contract_process_sresult = db_get_project_contract_process($project_contract_process_search_data);

    $allow_update = false;
    if ($project_contract_process_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_contract_process_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_contract_process_sresult['data'][0]['project_contract_process_id'] == $contract_process_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_contract_process_sresult = db_update_project_contract_process($contract_process_id, $project_contract_process_update_data);

        if ($project_contract_process_sresult['status'] == SUCCESS) {
            $return["data"]   = "Contract Process Details Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Contract Process Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Contract Process Master
INPUT 	: Contract Process ID, Project Contract Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_delete_project_contract_process($contract_process_id, $project_contract_process_update_data)
{
    $project_contract_process_update_data = array('active'=>'0');
    $project_contract_process_sresult = db_update_project_contract_process($contract_process_id, $project_contract_process_update_data);

    if ($project_contract_process_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Contract Process Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Site Location Mapping Master
INPUT 	: Name, Project ID, Is Road, Remarks, Added by
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_add_project_site_location_mapping_master($name, $project_id, $is_road, $remarks, $added_by)
{
    $project_site_location_mapping_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1','project_id'=>$project_id);
    $project_site_location_mapping_master_sresult = db_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
    if ($project_site_location_mapping_master_sresult["status"] == DB_NO_RECORD) {
        $project_site_location_mapping_master_iresult = db_add_project_site_location_mapping_master($name, $project_id, $is_road, $remarks, $added_by);

        if ($project_site_location_mapping_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Location Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This location already exists in this project";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To get Project Site Location Mapping Master List
INPUT 	: Mapping ID, Name, Project ID, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Projects or Error Details, success or failure message
BY 		: Ashwini
*/
function i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data)
{
    $project_site_location_mapping_master_sresult = db_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);

    if ($project_site_location_mapping_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_site_location_mapping_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Location Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Site Location Mapping Master
INPUT 	: Mapping ID, Project Site Location Mapping Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_update_project_site_location_mapping_master($mapping_id, $name, $project_id, $project_site_location_mapping_master_update_data)
{
    $project_site_location_mapping_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1','project_id'=>$project_id);
    $project_site_location_mapping_master_sresult = db_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);

    $allow_update = false;
    if ($project_site_location_mapping_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_site_location_mapping_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_site_location_mapping_master_sresult['data'][0]['project_site_location_mapping_master_id'] == $mapping_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_site_location_mapping_master_sresult = db_update_project_site_location_mapping_master($mapping_id, $project_site_location_mapping_master_update_data);

        if ($project_site_location_mapping_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Location Mapping Successfully updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This  location already exists for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Site Location Mapping Master
INPUT 	: Mapping ID, Project Site Location Mapping Master Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_delete_project_site_location_mapping_master($mapping_id, $project_site_location_mapping_master_update_data)
{
    $project_site_location_mapping_master_sresult = db_update_project_site_location_mapping_master($mapping_id, $project_site_location_mapping_master_update_data);

    if ($project_site_location_mapping_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Location Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Man Power Rate History
INPUT 	: Type ID, Vendor ID, Cost Per Hour, Added By
OUTPUT 	: History ID, success or failure message
BY 		: Lakshmi
*/
function i_add_man_power_rate_history($type_id, $vendor_id, $cost_per_hour, $added_by)
{
    $project_man_power_rate_history_iresult = db_add_man_power_rate_history($type_id, $vendor_id, $cost_per_hour, $added_by);
    if ($project_man_power_rate_history_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_man_power_rate_history_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Man Power Rate History List
INPUT 	: History ID, Type ID, Vendor ID, Cost Per Hour, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate History
BY 		: Lakshmi
*/
function i_get_man_power_rate_history($man_power_rate_history_search_data)
{
    $project_man_power_rate_history_sresult = db_get_man_power_rate_history($man_power_rate_history_search_data);
    if ($project_man_power_rate_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_man_power_rate_history_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No ManPower Rate History added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Rate History
INPUT 	: History ID, Project Man Power Rate History  Update Array
OUTPUT 	: History ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_man_power_rate_history($history_id, $man_power_rate_history_update_data)
{
    $project_man_power_rate_history_iresult = db_update_man_power_rate_history($history_id, $man_power_rate_history_update_data);

    if ($project_man_power_rate_history_iresult['status'] == SUCCESS) {
        $return["data"]   = "Man Power Rate History Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}
/*
PURPOSE : To add new Project TDS Deduction
INPUT 	: Master Type, Deduction, Effective Date, Remarks, Added By
OUTPUT 	: Master ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_tds_deduction_master($master_type, $deduction, $vendor_id,$effective_date, $remarks, $added_by)
{
    $project_tds_deduction_iresult =  db_add_project_tds_deduction_master($master_type, $deduction, $vendor_id,$effective_date, $remarks, $added_by);

    if ($project_tds_deduction_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project TDS Deduction Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project TDS Deduction List
INPUT 	: Master ID, Master Type, Deduction, Effective Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project TDS Deduction
BY 		: Lakshmi
*/
function i_get_project_tds_deduction_master($project_tds_deduction_master_search_data)
{
    $project_tds_deduction_sresult = db_get_project_tds_deduction_master($project_tds_deduction_master_search_data);

    if ($project_tds_deduction_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_tds_deduction_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No  Project TDS Deduction Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project TDS Deduction
INPUT 	: Master ID,Project TDS Deduction Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_tds_deduction_master($master_id, $project_tds_deduction_master_update_data)
{
    $project_tds_deduction_sresult = db_update_project_tds_deduction_master($master_id, $project_tds_deduction_master_update_data);

    if ($project_tds_deduction_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project TDS Deduction Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add project object output
INPUT 	: Process ID, Task ID,UOM,Object Type,Object Per Hour, Added By, Added On
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_project_object_output($process_id, $task_id, $uom, $object_type, $reference_id, $object_per_hr, $remarks, $added_by)
{
    $project_object_output_search_data = array("process_id"=>$process_id,"task_id"=>$task_id,"object_type"=>$object_type,"reference_id"=>$reference_id);
    $project_object_output_sresult = db_get_project_object_output($project_object_output_search_data);
    if ($project_object_output_sresult["status"] == DB_NO_RECORD) {
        $project_object_output_iresult =  db_add_project_object_output($process_id, $task_id, $uom, $object_type, $reference_id, $object_per_hr, $remarks, $added_by);

        if ($project_object_output_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_object_output_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Object Product is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Object Output list
INPUT 	: Task ID, Process ID, Object Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Process Task List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_project_object_output_task($project_object_output_search_data)
{
    $project_object_output_sresult = db_get_project_object_output($project_object_output_search_data);
    if ($project_object_output_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_object_output_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Object Output Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Object OUTPUT
INPUT 	: Output ID, Project Object Output Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_update_project_object_output($output_id, $project_object_output_update_data)
{
    $project_object_output_sresult = db_update_project_object_output($output_id, $project_object_output_update_data);

    if ($project_object_output_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Object Output Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project CW Master
INPUT 	: Name, Set No, Rate, Remarks, Added By
OUTPUT 	: CW Master ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_cw_master($name, $set_no, $rate, $remarks, $added_by)
{
    $project_cw_master_search_data = array("name"=>$name,"cw_name_check"=>'1',"active"=>'1');
    $project_cw_master_sresult = db_get_project_cw_master($project_cw_master_search_data);

    if ($project_cw_master_sresult["status"] == DB_NO_RECORD) {
        $project_cw_master_iresult = db_add_project_cw_master($name, $set_no, $rate, $remarks, $added_by);

        if ($project_cw_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project CW Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "CW Name already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project CW Master list
INPUT 	: CW Master ID, Name, Set No, Rate, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project CW Master
BY 		: Lakshmi
*/
function i_get_project_cw_master($project_cw_master_search_data)
{
    $project_cw_master_sresult = db_get_project_cw_master($project_cw_master_search_data);

    if ($project_cw_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_cw_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project CW Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project CW Master
INPUT 	: CW Master ID, Project CW Master Update Array
OUTPUT 	: CW Master ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_project_cw_master($cw_master_id, $project_cw_master_update_data)
{
    $project_cw_master_search_data = array("name"=>$project_cw_master_update_data['name'],"cw_name_check"=>'1',"active"=>'1');
    $project_cw_master_sresult = db_get_project_cw_master($project_cw_master_search_data);

    $allow_update = false;
    if ($project_cw_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_cw_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_cw_master_sresult['data'][0]['project_cw_master_id'] == $cw_master_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_cw_master_sresult = db_update_project_cw_master($cw_master_id, $project_cw_master_update_data);

        if ($project_cw_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project CW Master Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "CW Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project CW Master
INPUT 	: CW Master ID, Project CW Master Update Array
OUTPUT 	: CW Master ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_cw_master($cw_master_id, $project_cw_master_update_data)
{
    $project_cw_master_update_data = array('active'=>'0');
    $project_cw_master_sresult = db_update_project_cw_master($cw_master_id, $project_cw_master_update_data);

    if ($project_cw_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project CW Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project  UOM Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Unit ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_uom_master($name, $remarks, $added_by)
{
    $project_uom_master_search_data = array("name"=>$name,"unit_name_check"=>'1',"active"=>'1');
    $project_uom_master_sresult = db_get_project_uom_master($project_uom_master_search_data);

    if ($project_uom_master_sresult["status"] == DB_NO_RECORD) {
        $project_uom_master_iresult = db_add_project_uom_master($name, $remarks, $added_by);

        if ($project_uom_master_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project UOM Master Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Project Unit Name already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project UOM Master List
INPUT 	: Unit ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project UOM Master
BY 		: Lakshmi
*/
function i_get_project_uom_master($project_uom_master_search_data)
{
    $project_uom_master_sresult = db_get_project_uom_master($project_uom_master_search_data);

    if ($project_uom_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_uom_master_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Unit Master Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project UOM Master
INPUT 	: Unit ID, Project UOM Master Update Array
OUTPUT 	: Unit ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_uom_master($unit_id, $project_uom_master_update_data)
{
    $project_uom_master_search_data = array("name"=>$project_uom_master_update_data['name'],"unit_name_check"=>'1',"active"=>'1');
    $project_uom_master_sresult = db_get_project_uom_master($project_uom_master_search_data);

    $allow_update = false;
    if ($project_uom_master_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($project_uom_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($project_uom_master_sresult['data'][0]['project_uom_id'] == $unit_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $project_uom_master_sresult = db_update_project_uom_master($unit_id, $project_uom_master_update_data);

        if ($project_uom_master_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Unit Master Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Unit Name Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To Delete Project Stock UOM Master
INPUT 	: Unit ID, Project Stock UOM Master Update Array
OUTPUT 	: Unit ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_uom_master($unit_id, $project_uom_master_update_data)
{
    $project_uom_master_update_data = array('active'=>'0');
    $project_uom_master_sresult = db_update_project_uom_master($unit_id, $project_uom_master_update_data);

    if ($project_uom_master_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Unit Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
