<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_reg_buy.php');

/*
PURPOSE : To add new Reg Buy Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_reg_buy_process_master($name,$remarks,$added_by)
{
	$reg_buy_process_master_search_data = array("name"=>$name,"process_name_check"=>'1',"active"=>'1');
	$reg_buy_process_master_sresult = db_get_reg_buy_process_master($reg_buy_process_master_search_data);
	
	if($reg_buy_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$reg_buy_process_master_iresult = db_add_reg_buy_process_master($name,$remarks,$added_by);
		
		if($reg_buy_process_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reg Buy Process Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Process Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Reg Buy Process Master List
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Process Master, success or failure message
BY 		: Lakshmi
*/
function i_get_reg_Buy_process_master($reg_buy_process_master_search_data)
{
	$reg_buy_process_master_sresult = db_get_reg_buy_process_master($reg_buy_process_master_search_data);
	
	if($reg_buy_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $reg_buy_process_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Reg Buy Process Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Reg Buy Process Master
INPUT 	: Process Master ID, Reg Buy Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_reg_buy_process_master($process_master_id,$reg_buy_process_master_update_data)
{
	$reg_buy_process_master_search_data = array("name"=>$reg_buy_process_master_update_data['name'],"process_name_check"=>'1',"active"=>'1');
	$reg_buy_process_master_sresult = db_get_reg_buy_process_master($reg_buy_process_master_search_data);
	
	$allow_update = false;
	if($reg_buy_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($reg_buy_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($reg_buy_process_master_sresult['data'][0]['reg_buy_process_master_id'] == $process_master_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$reg_buy_process_master_sresult = db_update_reg_buy_process_master($process_master_id,$reg_buy_process_master_update_data);
		
		if($reg_buy_process_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reg Buy Process Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


/*
PURPOSE : To Delete Reg Buy Process Master 
INPUT 	: Process Master ID, Survey Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_reg_buy_process_master($process_master_id,$reg_buy_process_master_update_data)
{   
    $reg_buy_process_master_update_data = array('active'=>'0');
	$reg_buy_process_master_sresult = db_update_reg_buy_process_master($process_master_id,$reg_buy_process_master_update_data);
	
	if($reg_buy_process_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Process Master  Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Request
INPUT 	: File ID, Booking ID, Request For, Request Date, Requested By, Requested On, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_reg_buy_request($village,$survey_no,$extent,$land_lard,$request_date,$request_for,$remarks,$added_by)
{
	$reg_buy_request_iresult = db_add_reg_buy_request($village,$survey_no,$extent,$land_lard,$request_date,$request_for,$remarks,$added_by);
	
	if($reg_buy_request_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Request Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Reg Buy Request List
INPUT 	: Request ID, File ID, Request For, Request Date, Requested By, Requested On, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Request, success or failure message
BY 		: Lakshmi
*/
function i_get_reg_buy_request($reg_buy_request_search_data)
{
	$reg_buy_request_sresult = db_get_reg_buy_request($reg_buy_request_search_data);
	
	if($reg_buy_request_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$reg_buy_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Reg Buy Request Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Reg Buy Request
INPUT 	: Request ID, To update Reg Buy Request Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_reg_buy_request($request_id,$reg_buy_request_update_data)
{   
	$reg_buy_request_sresult = db_update_reg_buy_request($request_id,$reg_buy_request_update_data);
	
	if($reg_buy_request_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Request Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Reg Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_reg_buy_reg_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	$reg_buy_reg_process_iresult = db_add_reg_buy_reg_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($reg_buy_reg_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Reg Process Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Reg Buy Reg Process List
INPUT 	: Process ID, buy ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Reg Process, success or failure message
BY 		: Lakshmi
*/
function i_get_reg_buy_reg_process($reg_buy_reg_process_search_data)
{
	$reg_buy_reg_process_sresult = db_get_reg_buy_reg_process($reg_buy_reg_process_search_data);
	
	if($reg_buy_reg_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$reg_buy_reg_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Reg Buy Reg Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Reg Buy Reg Process
INPUT 	: Process ID, To update Reg Buy Reg Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_reg_buy_reg_process($process_id,$reg_buy_reg_process_update_data)
{   
	$reg_buy_reg_process_sresult = db_update_reg_buy_reg_process($process_id,$reg_buy_reg_process_update_data);
	
	if($reg_buy_reg_process_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Reg Process Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function i_add_reg_buy_document($process_id,$file_path_id,$remarks,$added_by)
{   
	$reg_buy_document_iresult =  db_add_reg_buy_document($process_id,$file_path_id,$remarks,$added_by);
	
	if($reg_buy_document_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $reg_buy_document_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Reg Buy Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Document
BY 		: Lakshmi
*/
function i_get_reg_buy_document($reg_buy_document_search_data)
{
	$reg_buy_document_sresult = db_get_reg_buy_document($reg_buy_document_search_data);
	
	if($reg_buy_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$reg_buy_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  Reg Buy Document Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

 /*
PURPOSE : To update Reg Buy Document 
INPUT 	: Document ID, Reg Buy Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_reg_buy_document($document_id,$reg_buy_document_update_data)
{       
		$reg_buy_document_sresult = db_update_reg_buy_document($document_id,$reg_buy_document_update_data);
		
		if($reg_buy_document_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reg Buy Document Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Village Master 
INPUT 	: Master ID, Name, Remarks, Added By
OUTPUT 	: Master ID, success or failure message
BY 		: Ashwini
*/
function i_add_reg_buy_village_master($name,$remarks,$added_by)
{
	$reg_buy_village_master_search_data = array("name"=>$name,"village_name_check"=>'1',"active"=>'1');
	$reg_buy_village_master_sresult = db_get_reg_buy_village_master($reg_buy_village_master_search_data);
	
	if($reg_buy_village_master_sresult["status"] == DB_NO_RECORD)
	{
		$reg_buy_village_master_iresult = db_add_reg_buy_village_master($name,$remarks,$added_by);
		
		if($reg_buy_village_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reg Buy village Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey village Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}



/*
PURPOSE : To get new Reg Buy Village Master  
INPUT 	: Reg Buy Village Master Search Data Array
OUTPUT 	: Reg Buy Village Master list or Error Details, success or failure message
BY 		: Ashwini
*/
function i_get_reg_buy_village_master($reg_buy_village_master_search_data)
{
	$reg_buy_village_master_sresult = db_get_reg_buy_village_master($reg_buy_village_master_search_data);
	
	if($reg_buy_village_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $reg_buy_village_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Reg Buy Village Master added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Reg Buy Village Master  
INPUT 	: Master ID,Reg Buy Village Master Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Ashwini
*/
function i_update_reg_buy_village_master($village_id,$reg_buy_village_master_update_data)
{
	$reg_buy_village_master_search_data = array("name"=>$reg_buy_village_master_update_data['name'],"village_name_check"=>'1',"active"=>'1');
	$reg_buy_village_master_sresult = db_get_reg_buy_village_master($reg_buy_village_master_search_data);
	
	$allow_update = false;
	if($reg_buy_village_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($reg_buy_village_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($reg_buy_village_master_sresult['data'][0]['reg_buy_village_master_id'] == $village_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$reg_buy_village_master_sresult = db_update_reg_buy_village_master($village_id,$reg_buy_village_master_update_data);
		
		if($reg_buy_village_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reg Buy village Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


/*
PURPOSE : To delete Reg Buy Village Master 
INPUT 	: Master ID,Reg Buy Village Master  Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Ashwini
*/

function i_delete_reg_buy_village_master($village_id,$reg_buy_village_master_update_data)
{
	$reg_buy_village_master_update_data = array('active'=>'0');
	$reg_buy_village_master_sresult = db_update_reg_buy_village_master($village_id,$reg_buy_village_master_update_data);
	
	if($reg_buy_village_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Reg Buy Village Master Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	return $return;
}
?>