<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';

  /* DEFINES - START */
  define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
  /* DEFINES - END */

  // Includes
  $base = $_SERVER["DOCUMENT_ROOT"];
  require dirname(__FILE__) . '/utilities/PHPExcel-1.8/Classes/PHPExcel.php';
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['hd_project_id'])) {
      $project_id = $_REQUEST['hd_project_id'];
    } else {
      $project_id = "";
    }

    // Get Already added Object Output
    $project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id);

    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
      $project_task_planning_list_data = $project_task_planning_list["data"];
    } else {
      $alert = $alert."Alert: ".$project_task_planning_list["data"];
    }

    $project_name = $project_task_planning_list["data"][0]['project_master_name'];
    $filename = $project_name."-planning-data_".date("m-d-Y-h-m-s").".xls";

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // PRINT HEADERS
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Project')
                ->setCellValue('B1', 'planned_project_id')
                ->setCellValue('C1', 'Process_name')
                ->setCellValue('D1', 'planned_process_id')
                ->setCellValue('E1', 'Task')
                ->setCellValue('F1', 'task_id')
                ->setCellValue('G1', 'Road Number')
                ->setCellValue('H1', 'planned_road_id')
                ->setCellValue('I1', 'UOM')
                ->setCellValue('J1', 'Uom_name')
                ->setCellValue('K1', 'Qty')
                ->setCellValue('L1', 'Resource_type')
                ->setCellValue('M1', 'Resource_name')
                ->setCellValue('N1', 'Resource_id')
                ->setCellValue('O1', 'Resource_mc_id')
                ->setCellValue('P1', 'No_of_objects')
                ->setCellValue('Q1', 'Per_day_output')
                ->setCellValue('R1', 'Total_days')
                ->setCellValue('S1', 'Start_date')
                ->setCellValue('T1', 'End_date')
                ->setCellValue('U1', 'task_planning_id')
                ->setCellValue('V1', 'Planning ID');

      for ($count = 0; $count < count($project_task_planning_list_data); $count++) {
        if (($project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] != "No Roads")) {
            $road_name = $project_task_planning_list_data[$count]["project_site_location_mapping_master_name"];
        } else {
            $road_name = "No Roads";
        }
        if ($project_task_planning_list_data[$count]['project_task_planning_object_type'] !== '') {
          $object_type= $project_task_planning_list_data[$count]['project_task_planning_object_type'];
        }
        else{
          $object_type=" ";
        }

        if ($project_task_planning_list_data[$count]['project_task_planning_object_type'] == 'CW') {
          $resource_name= $project_task_planning_list_data[$count]['project_cw_master_name'];
        }
        else if($project_task_planning_list_data[$count]['project_task_planning_object_type'] == 'MC'){
          $resource_name= $project_task_planning_list_data[$count]['project_machine_master_name'];
        }
        else{
          $resource_name=" ";
        }

        $start_date=date("d-m-Y",strtotime($project_task_planning_list_data[$count]["project_task_planning_start_date"]));
        $end_date=date("d-m-Y",strtotime($project_task_planning_list_data[$count]["project_task_planning_end_date"]));

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($count+2), $project_task_planning_list_data[$count]["project_master_name"])
                    ->setCellValue('B'.($count+2), $project_task_planning_list_data[$count]["project_management_master_id"])
                    ->setCellValue('C'.($count+2), $project_task_planning_list_data[$count]["project_process_master_name"])
                    ->setCellValue('D'.($count+2), $project_task_planning_list_data[$count]["project_plan_process_id"])
                    ->setCellValue('E'.($count+2), $project_task_planning_list_data[$count]["project_task_master_name"])
                    ->setCellValue('F'.($count+2), $project_task_planning_list_data[$count]["project_task_master_id"])
                    ->setCellValue('G'.($count+2), $road_name)
                    ->setCellValue('H'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"])
                    ->setCellValue('I'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_uom"])
                    ->setCellValue('J'.($count+2), $project_task_planning_list_data[$count]["project_uom_name"])
                    ->setCellValue('K'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_measurment"])
                    ->setCellValue('L'.($count+2), $object_type)
                    ->setCellValue('M'.($count+2), $resource_name)
                    ->setCellValue('N'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_machine_type"])
                    ->setCellValue('O'.($count+2), $project_task_planning_list_data[$count]["project_machine_master_id"])
                    ->setCellValue('P'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_no_of_object"])
                    ->setCellValue('Q'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_per_day_out"])
                    ->setCellValue('R'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_total_days"])
                    ->setCellValue('S'.($count+2), $start_date)
                    ->setCellValue('T'.($count+2), $end_date)
                    ->setCellValue('U'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_task_id"])
                    ->setCellValue('V'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_id"]);
    }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($project_name);

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  } else {
    header("location:login.php");
  }
