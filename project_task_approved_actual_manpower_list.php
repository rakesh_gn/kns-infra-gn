<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';
  define('PROJECT_MASTER_FUNC_ID', '180');

  $base = $_SERVER["DOCUMENT_ROOT"];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")){
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_FUNC_ID, '1', '1');

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_vendor = "";
    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor = $_REQUEST["search_vendor"];
    }

    $start_date = "";
    if (isset($_REQUEST["dt_start_date"])) {
        $start_date = $_REQUEST["dt_start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["dt_end_date"])) {
        $end_date = $_REQUEST["dt_end_date"];
    }

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_vendor_master_list["status"] == SUCCESS) {
        $project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project,"user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    //Get Company List
  	$company_list = i_get_company_list(array());
  } else {
  	header("location:login.php");
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
    <script type="text/javascript" src="./datatable/project_task_approved_actual_mp_list_datatable.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
  </head>
  <body>
    <?php
      include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          	<h4 class="modal-title">Add Bill No</h4>
          </div>
          <div class="modal-body">
          	<form id="add-bill-form">
          			<!-- <input type="hidden" id="manpower_id" name="manpower_id" value="<?php echo $manpower_id; ?>" />
          			<div class="form-group">
          				<label for="bill_no" class="control-label">Bill No</label>
          				<input type="text" class="form-control" name="bill_no" id="bill_no" value="<?php echo $bill_no ;?>" disabled>
          			</div> -->

          			<div class="form-group">
          				<label for="BillAddress" class="control-label">Bill Address</label>
          				<select name="billing_address" required id="billing_address" class="form-control">
          				<option value="">- - -Select Billing Address- - -</option>
          				<?php foreach($company_list['data'] as $item){  ?>
          				<option value="<?php echo $item['stock_company_master_id']; ?>">
          					<?php echo $item['stock_company_master_name']; ?>
          				</option>
          				<?php } ?>
          				</select>
          			</div>

          			<div class="form-group">
          				<label for="remarks" class="control-label">Remarks</label>
          				<input type="text" class="form-control" id="remarks" name="remarks" class="form-control">
          			</div>
          		</form>
          </div>
          <div class="modal-footer">
          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          	<button type="button" class="btn btn-primary" onclick="generateManPowerId()">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <!-- MODAL -->

    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6" style="width:100%;">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Task Actual Man Power List</h3>
                    <button type="button" id="generateBillBtn"
                            onclick="generateBill()" name="items_submit"
                            class="btn btn-success pull-right btn-sm"
                            style="margin: 5px 5px 0px 0px;">Generate Bill</button>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">

                      <select id="search_vendor" name="search_vendor" class="form-control">
                      <option value="">- - Select Vendor - -</option>
                      <?php
                        for ($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++) {
                      ?>
  								          <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) {
                                            ?> selected="selected" <?php
                                        } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
    								  <?php } ?>
                      </select>
                      <select id="search_project" name="search_project" class="form-control">
                        <option value="">- - Select Project - -</option>
      								  <?php
                                        for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                            ?>
      								  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                                ?> selected="selected" <?php
                                            } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
      								  <?php
                                        } ?>
      								  </select>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo $start_date; ?>"  class="form-control"/>
                      <input type="date" id="end_date" name="dt_end_date" value="<?php echo $end_date; ?>"  class="form-control"/>
                      <button type="button" class="btn btn-primary" onclick="drawTable()">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>

                  <!-- /widget-header -->
                  <div class="widget-content">
                    <table id="example" class="table table-striped table-bordered display nowrap">
                      <thead>
                          <tr>
                            <th colspan="9"></th>
                            <th colspan="3" class="center">Men</th>
                            <th colspan="3" class="center">Women</th>
                            <th colspan="3" class="center">Mason</th>
                            <th colspan="8" id="lastColSpanGroup"></th>
                          </tr>
                          <tr>
                              <th>#</th>
                              <th>Agency</th>
                              <th>Project</th>
                              <th>Process</th>
                              <th>Task Name</th>
                              <th>Road Name</th>
                              <th>Work Type</th>
                              <th>%</th>
                              <th>Msrmt</th>
                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Men</th>

                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Women</th>

                              <th>Hour</th>
                              <th>Rate</th>
                              <th>Mason</th>

                              <th>No of People</th>
                              <th>Amount</th>
                              <th>Remarks</th>
                              <th>Aproved By</th>
                              <th>Aproved On</th>
                              <th>Billed Date</th>
                              <th>Print</th>
                              <th><input type="checkbox" id="selectAllCheckbox"/ onchange="toggleSelection(this)"></th>
                          </tr>
                      </thead>
                  </table>
                </div>
                <!-- /widget-content -->

                </div>
                <!-- /widget -->

                </div>
                <!-- /widget -->
              </div>
              <!-- /span6 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /container -->
        </div>

  </body>
</html>
