<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 27th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING DATA - START */
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	/* QUERY STRING DATA - END */
	
	// Capture the form data
	if(isset($_POST["edit_enquiry_submit"]))
	{
		$enquiry_id      = $_POST["hd_enquiry_id"];
		$name            = $_POST["stxt_cust_name"];
		$cell            = $_POST["stxt_cust_cell"];
		$email           = $_POST["stxt_cust_email"];
		$company         = $_POST["stxt_cust_company"];
		$location        = $_POST["stxt_cust_location"];
		$source          = $_POST["ddl_enq_src"];

		if(isset($_POST["cb_walk_in"]))
		{
			$walk_in = 1;
		}
		else
		{
			$walk_in = 0;
		}		
		
		// Check for mandatory fields
		if(($enquiry_id !="") && ($name !="") && ($cell !="") && ($source !=""))
		{
			$enquiry_uresult = i_edit_enquiry($enquiry_id,$name,$cell,$email,$company,$location,$source,$walk_in,$user);
			
			if($enquiry_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Enquiry Details successfully edited!";
				
				header("location:crm_enquiry_list.php");
			}
			else
			{
				$alert_type = 0;
				$alert      = $enquiry_uresult["data"];
			}			
		}
		else
		{
			$alert = "Please fill all the mandatory fields. Also, please check if it is a valid enquiry being edited";
			$alert_type = 0;
		}
	}
	
	// Get details of this enquiry ID
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
		
		$d_cust_name = $enquiry_list_data[0]["name"];
		$d_cell      = $enquiry_list_data[0]["cell"];
		$d_email     = $enquiry_list_data[0]["email"];
		$d_company   = $enquiry_list_data[0]["company"];
		$d_location  = $enquiry_list_data[0]["location"];
		$d_source    = $enquiry_list_data[0]["source"];
		$d_walk_in   = $enquiry_list_data[0]["walk_in"];
	}
	else
	{
		$alert_type = 1;
		$alert      = $enquiry_list["data"];
		
		$d_cust_name = "";
		$d_cell      = "";
		$d_email     = "";
		$d_company   = "";
		$d_location  = "";
		$d_source    = "";
		$d_walk_in   = "";
	}	
	
	// Source
	$enq_source_list = i_get_enquiry_source_list('','1');
	if($enq_source_list["status"] == SUCCESS)
	{
		$enq_source_list_data = $enq_source_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enq_source_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Enquiry</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Enquiry</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_enquiry" class="form-horizontal" method="post" action="crm_edit_enquiry.php">
								<input type="hidden" name="hd_enquiry_id" value="<?php echo $enquiry_id; ?>" />
									<fieldset>
									
										<div class="control-group">											
											<label class="control-label" for="stxt_cust_name">Customer Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cust_name" placeholder="Customer Name" required="required" value="<?php echo $d_cust_name; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                           										    <div class="control-group">											
											<label class="control-label" for="stxt_cust_cell">Mobile*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cust_cell" placeholder="10 digit mobile no." required="required" value="<?php echo $d_cell; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	 
										
										<div class="control-group">											
											<label class="control-label" for="stxt_cust_email">Email</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cust_email" placeholder="Valid Customer Email ID" value="<?php echo $d_email; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_cust_company">Company</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cust_company" placeholder="Company of the Customer" value="<?php echo $d_company; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_cust_location">Location</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cust_location" placeholder="Location of the customer" value="<?php echo $d_location; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_enq_src">Enquiry Source*</label>
											<div class="controls">
												<select name="ddl_enq_src" required>
												<?php
												for($count = 0; $count < count($enq_source_list_data); $count++)
												{
												?>
												<option value="<?php echo $enq_source_list_data[$count]["enquiry_source_master_id"]; ?>" <?php if($enq_source_list_data[$count]["enquiry_source_master_id"] == $d_source){ ?>selected="selected"<?php } ?>><?php echo $enq_source_list_data[$count]["enquiry_source_master_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_walk_in">Walk In</label>
											<div class="controls">
												<input type="checkbox" name="cb_walk_in" <?php if($d_walk_in == "1"){?> checked <?php } ?> />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
																		
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_enquiry_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
