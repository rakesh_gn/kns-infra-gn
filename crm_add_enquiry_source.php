<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* DEFINES - START */
define('CRM_ADD_ENQUIRY_SOURCE_FUNC_ID','65');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'CRM Masters';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$add_perms_list    = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_SOURCE_FUNC_ID,'1','1');	
	$view_perms_list   = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_SOURCE_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_enq_source_submit"]))
	{
		$enquiry_source      = $_POST["enquiry_source"];
		$enquiry_source_type = $_POST["ddl_enq_src_type"];
		
		// Check for mandatory fields
		if(($enquiry_source !="") && ($enquiry_source_type != ""))
		{
			$enquiry_source_iresult = i_add_enquiry_source($enquiry_source,$enquiry_source_type,$user);
			
			if($enquiry_source_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $enquiry_source_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	$enquiry_source_type_list = i_get_enquiry_source_type_list('','1');
	if($enquiry_source_type_list["status"] == SUCCESS)
	{
		$enquiry_source_type_list_data = $enquiry_source_type_list["data"];
	}
	else
	{
		$alert = $enquiry_source_type_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Enquiry Source</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Enquiry Source</h3><?php if($view_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="crm_enquiry_source_list.php">Enquiry Source List</a></span><?php } ?>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Interest Status</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_enq_src_form" class="form-horizontal" method="post" action="crm_add_enquiry_source.php">
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="enquiry_source">Enquiry Source</label>
											<div class="controls">
												<input type="text" class="span6" name="enquiry_source" placeholder="Enquiry Source" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="ddl_enq_src_type">Enquiry Source Type</label>
											<div class="controls">
												<select name="ddl_enq_src_type" required>
												<?php
												for($count = 0; $count < count($enquiry_source_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $enquiry_source_type_list_data[$count]["enquiry_source_type_id"]; ?>"><?php echo $enquiry_source_type_list_data[$count]["enquiry_source_type_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
										<?php
										if($add_perms_list['status'] == SUCCESS)
										{
										?>
											<input type="submit" class="btn btn-primary" name="add_enq_source_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php
										}
										else
										{
											echo 'You are not authorized to view this page';
										}
										?>	
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
