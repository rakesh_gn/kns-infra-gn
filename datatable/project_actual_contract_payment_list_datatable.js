var table;
var columnsMapping = {
  '2': 'project'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function setBataAmount(row_id, amount, payment_id, bill_no, security_deposit) {
  bill_no = bill_no + "/S"
  if (amount > 0 && security_deposit > 0) {
    var ok = confirm("Are you sure you want to Accept?");
    if (ok) {
      // amount -= security_deposit;
      $.ajax({
        url: 'ajax/update_contract_payment_details.php',
        data: "total_amount=" + amount + "&payment_id=" + payment_id + "&bill_no=" + bill_no,
        success: function(response) {
          response = JSON.parse(response);
          console.log(response);
          if (response.status == 'SUCCESS') {
            tableDraw();
            alert("SUCCESS");
          } else {
            alert(response.status);
          }
        }
      });
    }
  } else {
    alert("security amount is nil");
  }
}

function project_approve_task_actual_contract_payment(contract_payment_id, contract_vendor_id) {
  var ok = confirm("Are you sure you want to Approve?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          project = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          tableDraw();
        }
      }
    }

    xmlhttp.open("POST", "project_approve_contract_payment.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("contract_payment_id=" + contract_payment_id + "&contract_vendor_id=" + contract_vendor_id + "&action=Approved");
  }
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_actual_contract_payment_bill_no;
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_to_date).
        format('DD-MM-YYYY');
      }
    },
    {
      orderable: false,
      data: `project_actual_contract_payment_total_measurement`
    },
    {
      orderable: false,
      data: `stock_unit_name`
    },
    {
      orderable: false,
      data: `project_actual_contract_payment_rate`
    },
    {
      orderable: false,
      data: `project_actual_contract_payment_amount`
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_actual_contract_deposit_amount;
        }
        return `₹ ${data.project_actual_contract_deposit_amount}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_actual_contract_payment_remarks;
        }
        return createToolTip(data.project_actual_contract_payment_remarks);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.added_by;
        }
        return createToolTip(data.added_by);
      }
    },
    {
      "orderable": true,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_added_on).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.aprove) {
          return `<a href="#" onclick=project_approve_task_actual_contract_payment(${data.project_actual_contract_payment_id},${data.project_manpower_agency_id})>
          <span class="glyphicon glyphicon-ok"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_contract_weekly_print.php?contract_payment_id=${data.project_actual_contract_payment_id}&deposit_status=>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (!window.permissions.edit) {
          return '***';
        } else if (data.project_actual_contract_payment_deposit_status != 'Bill Generated') {
          return `<button type="button" class="btn btn-primary btn-xs"
          onclick=setBataAmount(${meta.row},${data.project_actual_contract_payment_amount},${data.project_actual_contract_payment_id},'${data.project_actual_contract_payment_bill_no}',${data.project_actual_contract_deposit_amount})>
          Issue Deposit</button>`;
        } else {
          return 'Bill Generated';
        }
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit && data.project_actual_contract_payment_deposit_status == 'Bill Generated') {
          return `<a target = _blank href=project_contract_weekly_print.php?contract_payment_id=${data.project_actual_contract_payment_id}&deposit_status=${data.project_actual_contract_payment_deposit_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_contract_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_contract_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_contract_details.php',
        data: "payment_id=" + full.project_actual_contract_payment_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 4
    },
    scrollX: true,
    "columns": columns,
  });
});