var table;

function toggleSelection(ele) {
  $("input.input-checkbox").prop("checked", ele.checked);
}

function redrawTable() {
  table.fnDraw(false);
}

function exportData() {
  var project_id = $("#project_id").val();
  var status = $("#status").val();
  if (project_id == "" || status == "") {
    alert("select project and status to continue");
  } else {
    $('#hd_project_id').val(project_id);
    $('#hd_status').val(status);
    $('#formImport')[0].action = 'stock_indent_items_approval_excel.php';
    $('#formImport')[0].submit();
  }
}

$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_indent_item_for_approval.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      // get the stock qty
      $.ajax({
        url: 'ajax/stock_get_material_stock.php',
        data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
        dataType: 'json',
        success: function(stock_response) {
          $('span#stock_qty_' + index).html(stock_response);
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.search_status = $('#status').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 5
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'indent_no'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.stock_indent_item_added_on).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: 'project_name'
      },
      {
        orderable: false,
        data: 'material_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.material_code, 20);
        }
      },
      {
        orderable: false,
        data: 'uom'
      },
      {
        orderable: false,
        data: 'material_qty'
      },
      {
        orderable: false,
        data: function() {
          return '<span id="stock_qty_' + arguments[3].row + '">loading..</span>';
        },
      },
      {
        orderable: false,
        data: 'indent_item_status'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_indent_item_remarks, 20);
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          return moment(data.added_on).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: 'indent_by'
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#status").val() == 'Approved' || !window.permissions.edit) {
            return "***";
          }
          return `<a href="#"><span id="approve" class="glyphicon glyphicon-ok"></span></a>`;
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (!window.permissions.delete) {
            return "***";
          }
          return `<a style=color:red href=# ><span id="delete" class="glyphicon glyphicon-trash"></span></a>`;
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#status").val() == 'Rejected' || !window.permissions.edit) {
            return "***";
          }
          return `<a style=color:red href="#"><span id=reject class="glyphicon glyphicon-remove-circle"></span></a>`;
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data) {
          if (!window.permissions.delete) {
            return "***";
          }
          return '<input type="checkbox" name="cb_multiple_delete" class="input-checkbox" value="' + data.indent_item_id + '" >';
        }
      }
    ]
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'approve') {
      approve_indent(rowData.indent_item_id);
    } else if (event.target.tagName == 'SPAN' && event.target.id == 'delete') {
      delete_indent_item(rowData.indent_item_id, rowData.stock_indent_id);
    } else {
      if (event.target.tagName == 'SPAN' && event.target.id == 'reject')
        reject_indent(rowData.indent_item_id);
    }
  });
});

function approve_indent(indent_item_id) {
  var ok = confirm("Are you sure you want to Approve?");
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          redrawTable();
        }
      }
    }
    xmlhttp.open("POST", "ajax/stock_approve_indent_item.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("indent_item_id=" + indent_item_id + "&action=Approved");
  }
}

function delete_indent_item(indent_item_id, indent_id) {
  var ok = confirm("Are you sure you want to Delete?");
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          redrawTable();
        }
      }
    }
    xmlhttp.open("POST", "ajax/delete_indent_item.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("indent_item_id=" + indent_item_id + "&indent_id=" + indent_id + "&action=0");
  }
}

function delete_indent_item_multiple() {
  var indent_items = document.getElementsByName('cb_multiple_delete');
  var selected_items = '';
  for (var count = 0; count < indent_items.length; count++) {
    if (indent_items[count].checked) {
      selected_items = selected_items + indent_items[count].value + ',';
    }
  }
  if (selected_items == '') {
    alert("select atleast one item to delete");
    return false;
  }
  var ok = confirm("Are you sure you want to delete all the selected indent items?");
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "1") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          redrawTable();
        }
      }
    }
    xmlhttp.open("POST", "ajax/delete_indent_item_multiple.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("indent_items=" + selected_items + "&action=0");
  }
}

function reject_indent(indent_item_id) {
  var ok = confirm("Are you sure you want to Reject?");
  if (ok) {
    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          redrawTable();
        }
      }
    }
    xmlhttp.open("POST", "ajax/stock_reject_indent_item.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("indent_item_id=" + indent_item_id + "&action=Rejected");
  }
}