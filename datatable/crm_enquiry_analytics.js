var table;
var timeArray = [
  '06:00:00-10:00:00',
  '10:00:00-11:00:00',
  '11:00:00-12:00:00',
  '12:00:00-13:00:00',
  '13:00:00-14:00:00',
  '14:00:00-15:00:00',
  '15:00:00-16:00:00',
  '16:00:00-17:00:00',
  '17:00:00-22:00:00'
];

var columnsMapping = {
  '2': 'answered_06_10',
  '3': 'not_answered_06_10',
  '4': 'answered_10_11',
  '5': 'not_answered_10_11',
  '6': 'answered_11_12',
  '7': 'not_answered_11_12',
  '8': 'answered_12_13',
  '9': 'not_answered_12_13',
  '10': 'answered_13_14',
  '11': 'not_answered_13_14',
  '12': 'answered_14_15',
  '13': 'not_answered_14_15',
  '14': 'answered_15_16',
  '15': 'not_answered_15_16',
  '16': 'answered_16_17',
  '17': 'not_answered_16_17',
  '18': 'answered_17_22',
  '19': 'not_answered_17_22',
};

function tableDraw() {
  table.draw();
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span id="' + classname + '">Loading..</span>';
}

function viewFollowUpData(user_id, user_name) {
  var data = {
    start_date: $('#start_date').val() + " 06:00:00",
    end_date: $('#end_date').val() + " 21:59:59",
    user_id: user_id,
    user_name: user_name
  };

  $('#ajax_loading').show();
  $.ajax({
    url: "ajax/view_follow_up_details.php",
    data: data,
    success: function(result) {
      $('#ajax_loading').hide();
      $("#enquiry_details").html(result);
      $("#myModal").modal('show');
      $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'csvHtml5',
        ]
      });
      $('#example1').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'csvHtml5',
        ]
      });
    }
  });
}

$(document).ready(function() {
  $("td span").on("click", function(ele) {
    console.log(ele);
  })
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }

  var columns = [{
      className: 'noVis',
      width: "1%",
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "user_name"
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      orderable: false,
      "data": function(data) {
        return `<a href="#"><span id="enquiry" class="glyphicon glyphicon-eye-open"></span></a>`;
      }
    }
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_enquiry_analytics.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      for (i = 0; i < timeArray.length; i++) {
        var splitArray = timeArray[i].split("-");
        var data = {
          start_date: `${$('#start_date').val()} ${splitArray[0]}`,
          end_date: `${$('#end_date').val()} ${splitArray[1]}`,
          user_id: full.user_id
        };

        $.ajax({
          url: "ajax/get_crm_analytics_data.php",
          data: data,
          success: function(response) {
            response = JSON.parse(response);
            $.each(response, function(key, value) {
              $('span#' + key + '_' + index).html(value);
            });
          }
        });
      }
    },
    "language": {
      "infoFiltered": " "
    },
    fnServerParams: function(aoData) {
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "crm_users";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 1
    },
    scrollX: true,
    "columns": columns,
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'enquiry') {
      viewFollowUpData(rowData['user_id'], rowData['user_name']);
    }
  });
});