var table;

function redrawTable() {
  table.fnDraw(false);
}

$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_material_percentage_datatable.php',
    fnServerParams: function(aoData) {
      aoData.hd_material_id = $('#hd_material_id').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'stock_percentage_material_name'
      },
      {
        orderable: false,
        data: 'stock_percentage_material_code'
      },
      {
        orderable: false,
        data: 'stock_percentage_material_uom'
      },
      {
        orderable: false,
        data: 'stock_percentage'
      },
      {
        orderable: false,
        data: 'project_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_percentage_remarks, 10);
        },
      },
      {
        orderable: false,
        data: 'user_name'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.stock_percentage_added_on).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: function() {
          if ($('#edit_perm').val() == 0) {
            return '<button  onclick="showModal(' + arguments[3].row + ')"> <span class="glyphicon glyphicon-pencil"></span></button>';
          }
          return '***';
        }
      }
    ]
  });
});
function showModal(row_id) {
  $('#alert_show').addClass('hide');
  var rowData = table.api().data()[row_id];
  $('#myModal').modal();
  $('#selected_row_id').val(row_id);
  $('#myModal').on('shown.bs.modal', function() {
    $('span#header_material_name').html(rowData.stock_percentage_material_name);
    $('span#header_material_code').html(rowData.stock_percentage_material_code);
    $('span#header_material_uom').html(rowData.stock_percentage_material_uom);
    $('span#percentage_id').html(rowData.stock_percentage_id);
    // $('#percentage_id').html(rowData.stock_percentage_id);
    $('#percentage').val(rowData.stock_percentage);
    $('#remarks').val(rowData.stock_percentage_remarks);
  });
  // remove the Modal content from DOM
  $('#myModal').on('hide.bs.modal', function() {
    document.forms[0].reset();
  });
}
