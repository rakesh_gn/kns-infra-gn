var table;
var columnsMapping = {
  '2': 'project',
  '7': 'machine',
  '8': 'machine_name',
  '14': 'issued_amount',
  '15': 'balance_amount'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function machine_payment(vendor_id) {
  $('#ajax_loading').show();
  $.ajax({
    url: "tools/machine_payment_list/getVenderPaymentDetails.php?search_vendor=" + vendor_id,
    success: function(result) {
      $('#ajax_loading').hide();
      $("#details").html(result);
      $("#myModal").modal('show');
      $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
        ]
      });
    }
  });
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project' || columnsMapping[meta.col] == 'machine' || columnsMapping[meta.col] == 'machine_name') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_vendor_master_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_payment_machine_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.stock_company_master_name;
        }
        if (data.stock_company_master_name == null) {
          return '';
        }
        return createToolTip(data.stock_company_master_name);
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_to_date).
        format('DD-MM-YYYY');
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_bata;
        }
        return `₹ ${data.project_payment_machine_bata}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_amount;
        }
        return `₹ ${data.project_payment_machine_amount}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        return Math.round(data.project_payment_machine_tds) + ' % ';
      }
    },

    {
      "orderable": false,
      "data": function(data, type) {
        var amount = data.project_payment_machine_amount - data.project_payment_machine_bata;
        if (type === 'export') {
          return Math.round((data.project_payment_machine_tds / 100) * amount).toFixed(2);
        }
        return `₹ ${Math.round((data.project_payment_machine_tds / 100) * amount).toFixed(2)}`;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        var amount = data.project_payment_machine_amount - data.project_payment_machine_bata;
        var tds_amount = (data.project_payment_machine_tds / 100) * amount;
        if (type === 'export') {
          return Math.round((data.project_payment_machine_amount - data.project_payment_machine_bata) - tds_amount).toFixed(2);
        }
        return `₹ ${Math.round((data.project_payment_machine_amount - data.project_payment_machine_bata)-tds_amount).toFixed(2)}`;
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_remarks;
        }
        return createToolTip(data.project_payment_machine_remarks);
      }
    },
    {
      "orderable": false,
      "data": "accepted_by"
    },
    {
      "orderable": false,
      data: function(data, type) {
        if (data.project_payment_machine_accepted_on == "0000-00-00") {
          return '00-00-0000';
        }
        return moment(data.project_payment_machine_accepted_on).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        var end_date = moment().format('x');
        var diff_time = Math.round(end_date - moment(data.project_payment_machine_accepted_on).format('x'));
        return parseInt((diff_time / (3600 * 1000 * 24)))
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_machine_weekly_print.php?machine_payment_id=${data.project_payment_machine_id}&bata_status=${data.project_bata_payment_machine_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a href=# onclick=machine_payment(${data.project_payment_machine_vendor_id})>
          <span style=font-size:16px>₹</span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_accept_machine_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_machine_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, data, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_machine_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'machine' + '_' + index).html(createToolTip(response.machine));
          $('span.' + 'machine_name' + '_' + index).html(createToolTip(response.machine_name));
        }
      });

      $.ajax({
        url: 'ajax/get_payment_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'issued_amount' + '_' + index).html(Math.round(response.issued_amount).toFixed(2));
          var amount = data.project_payment_machine_amount - data.project_payment_machine_bata;
          var tds_amount = Math.round((data.project_payment_machine_tds / 100) * amount).toFixed(2);
          var payable = Math.round(amount - tds_amount).toFixed(2);
          $('span.' + 'balance_amount' + '_' + index).html(`${((payable - Math.round(response.issued_amount).toFixed(2)).toFixed(2))}`);
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });
});