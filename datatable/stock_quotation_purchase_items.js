var table;
var columnsMapping = {
  '23': 'po_qty',
  '24': 'grn_qty',
  '25': 'balance_qty',
}
var commonCellDefinition = {
  orderable: false,
  data: getCellValue,
  // createdCell: colorizeCell,
}
function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}
function redrawTable() {
  table.fnDraw(false);
}

$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 5) ? str : str.substr(0, 10) + '...';
    return '<abbr data-toggle="tooltip" id="abbr" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    dom: 'lBfrtip',
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    ajax: 'datatable/stock_quotation_purchase_items_datatable.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      // get the stock qty
   
    },
    fnServerParams: function(aoData) {
      aoData.hd_material_id = $('#hd_material_id').val();
      aoData.project_id = $('#project_id').val();
      aoData.hd_status = $('#hd_status').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 6
    },

    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data:'indent_no'
      },

      {
        orderable: false,
        data: function(data,type) {
          return moment(data.stock_indent_item_added_on).format('DD/MM/YYYY');
        },
      },
      {
        orderable: false,
        data: function(data,type) {
          if(type == 'export')
          {
            return data.indent_project_name ;
          }
          return createToolTip(data.indent_project_name, 10);
        },
      },
      {
        orderable: false,
        data: 'indent_added_by'
      },
      {
        orderable: false,
        data: function(data,type) {
          console.log(data.stock_indent_item_approved_on);
          if(data.stock_indent_item_approved_on != "0000-00-00 00:00:00")
          {
            return moment(data.stock_indent_item_approved_on).format('DD/MM/YYYY');
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: function(data,type) {
          if(type == 'export')
          {
            return data.indent_approved_by ;
          }
          if(data.indent_approved_by != null)
          {
            return createToolTip(data.indent_approved_by, 10);
          }
          return "" ;
        },
      },

      {
        orderable: false,
        data: function(data) {
          var a = moment(data.stock_indent_item_approved_on).format('x');
          var b = moment(data.stock_indent_item_added_on).format('x');
          var days = Math.round((a-b)/(1000*60*60*24))+1;
          return days ;
        }
      },
      {
        orderable: false,
        data: 'stock_indent_item_quantity'
      },
      {
        orderable: false,
        data: function(data,type) {
          if(type == 'export')
          {
            return data.stock_material_name ;
          }
          return createToolTip(data.stock_material_name, 10);
        },
      },
      {
        orderable: false,
        data: function(data,type) {
          if(type == 'export')
          {
            return data.stock_material_code;
          }
          return createToolTip(data.stock_material_code, 10);
        },
      },
      {
        orderable: false,
        data: 'stock_unit_name'
      },
      {
        orderable: false,
        data: function(data,type) {
          if(type == 'export')
          {
            return data.stock_quote_no ;
          }
          if(data.stock_quote_no != null)
          {
            return createToolTip(data.stock_quote_no, 10);
          }
          return "" ;
        },
      },
      {
        orderable: false,
        data: function(data,type) {
          if(data.stock_quotation_added_on != null)
          {
          return moment(data.stock_quotation_added_on).format('DD/MM/YYYY');
        }
        return " ";
        }
      },
      {
        orderable: false,
        data: function(data) {
          if(data.stock_quotation_approved_on != null)
          {
            return moment(data.stock_quotation_approved_on).format('DD/MM/YYYY');
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: function(data) {
          if((data.stock_quotation_approved_on) != null && (data.stock_indent_item_approved_on != null))
          {
            var a = moment(data.stock_quotation_approved_on).format('x');
            var b = moment(data.stock_indent_item_approved_on).format('x');
            var days = Math.round((a-b)/(1000*60*60*24))+1;
            return days ;
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: 'po_number'
      },
      {
        orderable: false,
        data: function(data) {
          if(data.po_date != null)
          {
            return moment(data.po_date).format('DD/MM/YYYY');
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: function(data,type) {
          if(data.vendor_name != null)
          {
            if(type == 'export')
            {
              return data.vendor_name ;
            }
            return createToolTip(data.vendor_name, 5);
          }
          return " ";

        },
      },

      
      {
        orderable: false,
        data: function(data) {
          if((data.po_date)!= null && (data.stock_quotation_approved_on != null))
          {
            var a = moment(data.po_date).format('x');
            var b = moment(data.stock_quotation_approved_on).format('x');
            var days = Math.round((a-b)/(1000*60*60*24))+1;
            return days ;
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: function(data) {
          if((data.po_date)!= null && (data.stock_indent_item_added_on != null))
          {
          var a = moment(data.po_date).format('x');
          var b = moment(data.stock_indent_item_added_on).format('x');
          var days = Math.round((a-b)/(1000*60*60*24))+1;
          return days ;
        }
        return " ";
        }
      },
      {
        orderable: false,
        data: function(data) {
          if((data.po_date)!= null && (data.stock_indent_item_added_on != null))
          {
          var a = moment(data.po_date).format('x');
          var b = moment(data.stock_indent_item_added_on).format('x');
          var days = Math.round((a-b)/(1000*60*60*24))+1;
          return days ;
        }
        return " ";
        }
      },
      
     
       {
        orderable: false,
        data: 'stock_quotation_po_qty'
      },
      {
        orderable: false,
        data: 'stock_grn_qty'
      },
      {
        orderable: false,
        data: 'stock_grn_remaning_qty'
      },
      {
        orderable: false,
        data: 'po_total_value'
      },
      {
         orderable: false,
        data: 'item_status'
      },
      {
        orderable: false,
        data: function(data) {
          return '<a href="#"><span id="update_status'+ arguments[3].row +'"  class="update_status glyphicon glyphicon-check"></span></a>';
        }
      }
    ]
  });

   $('#example tbody').on('click', 'tr', function(event) {
    
    var rowData = table.api().row(this).data();
    var po_id = rowData.po_id;
    var material_id = rowData.stock_material_id;
    

    /*console.log(rowData);
    console.log(rowData.indent_no);*/

    var material_name = rowData.stock_material_name;
    var project_name = rowData.indent_project_name;
    var indent_no = rowData.indent_no;
    var po_no =  rowData.po_number;
   
    var vendor = rowData.vendor_name;
    var id = rowData.po_items_id;

    var user_name= rowData.indent_added_by;
    var tr = $(this).closest("tr");
    var rowindex = tr.index();
    if ( event.target.id == 'update_status'+rowindex) {
     // get the stock qty
     
          
              
              var total_qty =  rowData.stock_quotation_po_qty;
              var pending_qty =  rowData.stock_grn_remaning_qty;
              var accepted_qty = rowData.stock_grn_qty;
              var closing_qty = total_qty - accepted_qty;


              if(pending_qty > 0 ){

                  $('#status_modal').modal('show');
                  
                  $('span#po_material_name').html(material_name);
                  $('span#po_project_name').html(project_name);
                  $('span#vendor').html(vendor);

                  $('span#total_qty').html(total_qty);
                  $('span#accepted_qty').html(accepted_qty);

                  $('span#po_indent_no').html(indent_no);
                  $('span#po_no').html(po_no);
                  $('#closing_qty').val(closing_qty);
                  $('#po_item_id').val(id);
                  $('#po_id').val(po_id);
                  $('#user_name').val(user_name);
                 
                }

            
          



    }



  });
});




