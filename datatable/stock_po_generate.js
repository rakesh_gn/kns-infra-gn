
function check_qty(){

  var po_qty = $('#po_qty').val();
  var quote_qty = $('#quote_qty').val();
  var rate = $('#quote_rate').val(); 
  if(parseInt(po_qty,10) > parseInt(quote_qty,10)){

    alert("Purchase Order quantity cannot be greater than Indent quantity.");
    $('#po_qty').val('');
    $('#po_value').val('');


  } else {
    
     var value = rate * po_qty; 
     $('#po_value').val(Math.round(value));

  }
  
}

var table;
// function toggleSelection(ele) {
//   $("input.input-checkbox").prop("checked", ele.checked);
// }

function redrawTable() {
  table.fnDraw(false);
  if ($('#project_id').val() != '' &&  $('#hd_vendor_id').val() != '') {
    document.getElementById("po_button").style.display = "block";
  }else{
    document.getElementById("po_button").style.display = "none";
  }
  

}

var PoData = [];
function add_purchase_order() {
  var po_total = 0;

  var user = $('#user').val();
  var table = $('#example').DataTable();
  var indent_items = [];
  var tableData = table.data();
  // get all the 'checked' input:checkbox(s)
  $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele) {
    indent_items.push(tableData[parseInt(ele.value)]);
  });
  if ($('input#selectAllCheckbox').is(':checked')) {
    indent_items = tableData;
  } else if (!indent_items.length) {
    indent_items = [];
  }

  if ($('#hd_vendor_id').val() == '') {
    alert('Error: Select Project and Vendor');
    return false;
  }

  if ($('#project_id').val() == '') {
    alert('Error: Select Project');
    return false;
  }

  if (!indent_items.length) {
    alert('Error: Select atleast one row to Bill');
    return false;
  }
  $.each(indent_items, function(i, value) {
    var keys = [ "system_indent_no","system_quote_no",
      "stock_material_name", "stock_material_code",
      "system_project_name", "quote_po_qty","quote_amount",
      "indent_id","quote_project","quote_vendor_id","material_id","stock_unit_name",
      "quote_po_value","quote_id"
    ];
    var data = {};
    for (var key in keys) {
      data[keys[key]] = value[keys[key]];
    }
    PoData.push(data);
  });

  var i;
 
  for (i = 0; i < PoData.length; ++i) {
     po_total = parseInt(po_total,10) + parseInt( PoData[i].quote_po_value,10);
  }
  
  var add_po_check = 1;
  var project_id = $('#project_id').val();

    $.ajax({

          url: '/kns/Legal/datatable/stock_po_calculation.php',
          type: "GET",
          data: {project_id: project_id ,add_po_check:add_po_check},
          success: function(result) {
            result = JSON.parse(result);
            total_including_this_po = parseInt(result.calculated_amt,10) + parseInt(po_total,10);


            if(parseInt(result.planned_amt,10) < parseInt(total_including_this_po,10) ){
              var difference = parseInt(total_including_this_po,10) - parseInt(result.planned_amt,10);
              alert("Current Budget: "+result.planned_amt+" , Current PO Value: "+po_total+", The total budget for processing this PO is less by amount "+difference   );
              location.reload();

            }else{

               var vendor_id = $('#hd_vendor_id').val();
                $.ajax({
                    url: 'stock_masters/stock_purchase_functions.php',
                    type: 'POST',
                    data: {
                      vendor: vendor_id,
                      user: user,
                      function2call: 'i_add_stock_purchase_order',
                      PoData : PoData
                    },
                    dataType: 'json',
                    success: function(response) {
                      //if(response){
                      if(response.status_code == 200){

                        window.location.href ='/kns/Legal/stock_add_po_details.php?order_id='+response.data.data+'?quotation_id='+response.data.quotation_id;
                        console.log(response);
                        //console.log('/kns/Legal/stock_add_po_details.php?order_id='+response.data.data+'?quotation_id='+response.data.quotation_id);
                        

                      }
                    //$('#exampleModal').modal('hide');
                    //drawTable();
                    }
                 });

            }
            
          },
          error: function(response) { alert('Failed!'); },
  });
  

}


$(document).ready(function() {

 

  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_po_generate.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      // get the stock qty
      $.ajax({
        url: 'ajax/stock_get_material_stock.php',
        data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
        dataType: 'json',
        success: function(stock_response) {
          $('span#stock_qty_' + index).html(stock_response);
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.search_status = $('#status').val();
      aoData.project_id = $('#project_id').val();
      aoData.vendor_id = $('#hd_vendor_id').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 5
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'system_indent_no'
      },
      
      {
        orderable: false,
        data: 'stock_project_name'
      },
      /*{
        orderable: false,
        data: 'indent_requested_by'
      },
      {
        orderable: false,
        data: 'indent_requested_on'
      },*/
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_material_name, 10);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_material_code, 10);
        },
      },

      {
        orderable: false,
        data: 'stock_unit_name'
      },
      {
        orderable: false,
        data: 'system_quote_no'
      },
      {
        orderable: false,
        data: 'added_on'
      },
      {
        orderable: false,
        data: 'approved_on'
      },
      {
        orderable: false,
        data: 'user_name'
      },
      {
        orderable: false,
        data: 'stock_vendor_name'
      },
      {
        orderable: false,
        data: 'quote_qty'
      },

      {
        orderable: true,
        data:  function(data) {
          var project_name = encodeURIComponent(data.stock_project_name);
          var material_name = encodeURIComponent(data.stock_material_name);
          var material_code = encodeURIComponent(data.stock_material_code);
          return `${data.quote_po_qty} <a href="#"><span onclick=update_poqty(${data.quote_amount},${data.quote_qty},'${data.system_indent_no}','${data.system_quote_no}','${project_name}','${material_name}',${data.quote_id},${data.quote_po_value},'${material_code}') class="glyphicon glyphicon-pencil"></span></a>`;
        }
        
      },
      {
        orderable: false,
        data: 'quote_amount'
      },

      {
        orderable: false,
        data: 'quote_po_value'
      },
       
      {
      "orderable": false,
      "className": 'noVis',
      "data": function() {
        if (!window.permissions.edit) {
          return "***";
        }

        return '<input type="checkbox" class="input-checkbox checkbox' + arguments[3].row + '"  name="input-checkbox"  id="input-checkbox' + arguments[3].row + '" value="' + arguments[3].row + '" >';


      }
    }
    ]
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    var value = rowData.quote_po_value;

    var tr = $(this).closest("tr");
    var rowindex = tr.index();

    if ( event.target.id == 'input-checkbox'+rowindex) {
        if(value){
      //
     var cond_passed = check_conditions(rowData.project_material_min_qty,rowData.project_material_stock_qty,rowData.material_id,
      rowData.quote_project,rowData.stock_material_name,rowData.stock_project_name,rowindex,rowData.stock_material_code);


    }else{
      alert("Please update PO Quantity");
      $(".checkbox"+rowindex).prop("checked", false);
    }
    }

   

    
  
  });
 

});

  

function updateCheckbox(Data,rowNo){

  if(Data == 0){

    $(".checkbox"+rowNo).prop("checked", false);

  }
  

}

function update_poqty(quote_rate,quote_qty,indent_no,quote_no,project_name,material_name,quote_id,quote_po_value,material_code) {
  
  if(quote_po_value){
    alert("Quotation value already added");
  }else{
    $('#exampleModal').modal('show');

    $('#exampleModal').on('shown.bs.modal', function(){

      $('#quote_qty').val(quote_qty);
      $('#quote_rate').val(quote_rate);
      $('#quotation_id').val(quote_id);
      $('#indent_id').val(indent_no);
      var material_name_full = decodeURIComponent(material_name) +' - '+decodeURIComponent(material_code);
      $('span#material_name').html(material_name_full);
      $('span#stock_quote_qty').html(quote_qty);
      $('span#project_name').html(decodeURIComponent(project_name));
      $('span#indent_no').html(indent_no);
      $('span#quote_no').html(quote_no);

    });

    // flush the previously loaded modal content
    $('#exampleModal').on('hidden.bs.modal', function(e) {
      $('form#update-poqty-form')[0].reset();
    });
  }
 
}




function generatePOQuantity(){
   var po_qty = $('#po_qty').val();
   var value = $('#po_value').val();
   var quotation_id = $('#quotation_id').val();
   var indent_id = $('#indent_id').val();
   var update_status = 1;
          $.ajax({

          url: '/kns/Legal/stock_approve_quotation.php',
          type: "POST",
          data: {po_qty: po_qty,value:value,quotation_id:quotation_id,update_status:update_status,indent_id:indent_id},
          success: function(data) {
             switch(true){
                case (data.status === 200): {

                 

                  alert("Updated successfully");
                  location.reload();
                    
                }
                                 
                 break;
                 case (data.status === 400): {
                  alert(data.data);
                }
                 break;
                 case (data.status === 450): {
                  alert(data.data);
                  console.log(data);
                }
                break;
                break;
                default: 
                
               // window.location.href ='{{env('APP_URL')}}/users';
              } 
          },
          error: function(response) { alert('Failed!'); },
          });
   
 

}

function showQuoteItems(quote_id,vendor_name) {
  var temp = {
    id: quote_id,
    name: vendor_name
  };
  console.log('temp ', temp);
  $('#myModal1').modal({
    remote: 'stock_quotation_items_list.php?'+ $.param(temp)
  });

  $('#myModal1').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}



function get_vendor_list()

{ 
  //update by Rakesh
  //clear the previous selected vendor
  $('#hd_vendor_id').val('');

  var searchstring = document.getElementById('stxt_vendor').value;
  if(searchstring.length >= 3)

  { 
    //update by Rakesh
    active = 1;
    //end of update
    
    if (window.XMLHttpRequest)

    {// code for IE7+, Firefox, Chrome, Opera, Safari

      xmlhttp = new XMLHttpRequest();

    }

    else

    {// code for IE6, IE5

      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    }



    xmlhttp.onreadystatechange = function()

    {       

      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

      { 

        if(xmlhttp.responseText != 'FAILURE')

        {

          document.getElementById('search_results').style.display = 'block';

          document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

        }

      }

    }


    xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written

    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //update by Rakesh
    xmlhttp.send("search=" + searchstring+"&active="+active);
    //xmlhttp.send("search=" + searchstring);       
    //end of update
  }

  else  

  {

    document.getElementById('search_results').style.display = 'none';

  }

}

function select_vendor(vendor_id,search_vendor)

{

  document.getElementById('hd_vendor_id').value = vendor_id;

  document.getElementById('stxt_vendor').value = search_vendor;

  document.getElementById('search_results').style.display = 'none';

}


function check_conditions(project_material_min_qty,project_material_stock_qty,material_id,project_id,material_name,project_name,rowNo,material_code){
  //alert(row_no);
$('#input-checkbox1').prop('checked', false);
  var cond1 = false;
  var cond2 = false;
  var cond3 = false;
  var material_full = material_name + '-' + material_code;
  
  $('span#po_material_name').html(decodeURIComponent(material_full));
  $('span#po_project_name').html(decodeURIComponent(project_name));
  if(parseInt(project_material_min_qty,10) > parseInt(project_material_stock_qty,10)){
    $('span#min_stock').html("<p style='color:green;'>Min Quantity Stock Check Passed</p>");
    $('span#min_stock_reason').html("Mtrl min qty :"+project_material_min_qty+" >  Stock qty :"+project_material_stock_qty);
    cond1 = true;
  }else{

    $('span#min_stock').html("<p style='color:red;'>Min Quantity Stock Check Failed</p>");
    $('span#min_stock_reason').html("Mtrl min qty :"+project_material_min_qty+" <  Stock qty :"+project_material_stock_qty);
    //$('span#min_stock_action').html("<a href='stock_add_material_percentage.php' target='_blank'>Add Percentage Min</a>");
  }
 

  $.ajax({

          url: '/kns/Legal/datatable/stock_po_calculation.php',
          type: "GET",
          data: {project_id: project_id ,material_id:material_id},
          success: function(result) {
            result = JSON.parse(result) ;

            var stock = result.data.stock;
            var po = result.data.po_amount;
            var issue = result.data.issue;
            var planned_amt = result.data.planned_amt;
            var total_amount = result.data.total_amount;
            //console.log(project_id);
             switch(true){


                case (result.status == 200): {

                 console.log(result);
                  if(result.data.planned_amt > result.data.total_amount){

                    $('span#budget').html("<p style='color:green;'>Budget for Project Material Check Passed</p>");
                    $('span#budget_reason').html("s : "+stock+"<br/>p : "+po+"<br/> i : "+issue+"<br/> T = "+total_amount);
                    $('span#budget_action').html("Project Material Budget:<br/>"+planned_amt);
                    var cond2=true;

                  }else{

                    $('span#budget').html("<p style='color:red;'>Budget for Project Material Check Failed</p> ");
                    $('span#budget_reason').html("s : "+stock+"<br/>p : "+po+"<br/> i : "+issue+"<br/> T = "+total_amount);
                    $('span#budget_action').html("Project Material Budget:<br/>"+planned_amt);
                    var cond3=false;

                  }

                  //Opne PO
                  if(result.data.open_po.status == 450){

                    $('span#wip_po').html("<p style='color:green;'>WIP Open PO - Passed</p>");
                    var cond3=true;

                  }else{

                    $('span#wip_po').html("<p style='color:red;'>WIP Open PO - Failed</p>");
                    var cond3=false;

                  }

                  if(cond1 == true && cond2 == true && cond3 == true){
                  //  $('#pomodal').modal('show');
                    var Data = 1;
                    updateCheckbox(Data,rowNo);
                    

                  }else{

                    $('#pomodal').modal('show');
                    var Data = 0;
                    updateCheckbox(Data,rowNo);

                  }

                    
                }
                                 
                 break;
                 case (result.status_value == 400): {
                  alert(result.data);
                }
                break;
                break;
                default: 
                
               // window.location.href ='{{env('APP_URL')}}/users';
              } 
          },
          error: function(response) { alert('Failed!'); },
  });

 

}




