<?php
session_start();
  $base = $_SERVER['DOCUMENT_ROOT'];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
  $dbConnection = get_conn_handle();

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
   $user = $_SESSION["loggedin_user"];
    $aColumns = array(
      'added_by',
      'project_manpower_agency_id',
      'project_manpower_agency_name',
      'project_manpower_agency_contact_person',
      'project_manpower_agency_contact_number',
      'project_manpower_agency_active',
      'project_manpower_agency_added_by',
      'project_manpower_agency_added_on'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_manpower_agency_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}

	/*
	 * Ordering
	 */
   $sOrder = "Order By project_manpower_agency_added_on desc";

	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
if($sWhere==""){
  $sWhere .= "WHERE `project_manpower_agency_active`=1";
}
    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    {
      $sWhere = "WHERE (";
      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {
        $sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
      }
      $sWhere = substr_replace( $sWhere, "", -3 );
      $sWhere .= ") AND project_manpower_agency_active=1";
    }

    /* Individual column filtering */
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
      {
        if ( $sWhere == "" )
        {
          $sWhere = "WHERE ";
        }
        else
        {
          $sWhere .= " AND ";
        }
        $sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch_'.$i])."%' ";
      }
    }

    if(isset($_GET['search_vendor']) && $_GET['search_vendor'] != '') {
      $sWhere .= " AND `project_manpower_agency_id` = ". $_GET['search_vendor'];
    }


	/*
	 * SQL queries
	 * Get data to display
	 */
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
		FROM $sTable
		$sWhere
    $sOrder
		$sLimit
		";
  $statement = $dbConnection->prepare($sQuery);
  $statement -> execute();
  $rResult = $statement -> fetchAll();

// get 'iFilteredTotal'
  $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
  $statement -> execute();
  $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

// get 'iTotal' Total data set length
  $statement = $dbConnection->prepare("SELECT COUNT(`".$sIndexColumn."`) as iTotal FROM $sTable");
  $statement -> execute();
  $iTotal = $statement -> fetch()['iTotal'];

	/*
	 * Output
	 */
	$output = array(
		// "sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"sQuery" => $sQuery,
		"aaData" => array(),
	);

  foreach($rResult as $aRow){
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
			}
		}
    $row[0] = 'index';
		$output['aaData'][] = $row;
    $output['where'] = $sWhere;
	}
	echo json_encode( $output );
?>
