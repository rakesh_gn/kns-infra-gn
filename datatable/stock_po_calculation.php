<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
/*
LIST OF TBDs
1. Wherever user clicks on check PO conditions following events takes places
*/

/*
PURPOSE : To Calculate PO Conditions - Get The sum of material_qty * material_price for perticular project from these views stock_material_list , issue_material_list po_material_list
INPUT 	: Project Id
OUTPUT 	: Success status / Fail status along with total for 3 conditions
BY 		: Rakesh GN 
*/

 if(isset($_GET['project_id']) && isset($_GET['add_po_check'])){

   $project_id = $_GET['project_id'];

   //sum the amount in PO,Stock,Issue
   $po_amount_query = "select sum(material_price *  stock_grn_remaning_qty ) as po_amount from po_material_list where project_id=:project_id";

   $stock_amount_query = "select sum(material_price * material_stock_quantity) as stock_amount from stock_material_list where material_stock_project_id=:project_id";

   $issue_amount_query = "select sum(material_price * material_issue_quantity) as issue_amount from issue_material_list where material_issue_project_id=:project_id";
   try{
       $dbConnection = get_conn_handle();
       $po_amount = $dbConnection->prepare($po_amount_query);
       $stock_amount = $dbConnection->prepare($stock_amount_query);
       $issue_amount = $dbConnection->prepare($issue_amount_query);

       $search_data = array(':project_id'=>$project_id);
       $po_amount->execute($search_data);
       $stock_amount->execute($search_data);
       $issue_amount->execute($search_data);
       $po_amount_details = $po_amount -> fetchAll();
       $stock_amount_details = $stock_amount -> fetchAll();
       $issue_amount_details = $issue_amount -> fetchAll();

       if(FALSE === $po_amount_details || FALSE === $stock_amount_details ||FALSE === $issue_amount_details)
        {
          $return["status"] = FAILURE;
          $return["data"]   = "";
          echo json_encode($return);
        }
        else if(count($po_amount_details) <= 0 || count($stock_amount_details) <= 0 || count($issue_amount_details)<= 0)
        {
          $return["status"] = DB_NO_RECORD;
          $return["data"]   = "";
          echo json_encode($return);
        }

        $total_amt = @$po_amount_details[0]['po_amount'] + @$stock_amount_details[0]['stock_amount'] + @$issue_amount_details[0]['issue_amount'];

   }
   catch(PDOException $e){
        $total_amt = 0;
   }
   //end of sum



  //get the planned amt
   $project_stock_module_mapping_search_data = array("active"=>'1','stock_project_id' => $project_id);
   $project_stock_module_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);
   if($project_stock_module_mapping_list['status'] == SUCCESS){
      $mapping_data = $project_stock_module_mapping_list['data'];
      $mgmt_project_id = $mapping_data[0]['project_stock_module_mapping_mgmnt_project_id'];
    }

    else{

      $mgmt_project_id = 0;
    }

    if($mgmt_project_id != 0){

     $project_planned_data = array("project_id"=>$mgmt_project_id);
     $get_planned_data = db_get_planned_data($project_planned_data);

     $total_planned_material_cost = 0 ;
       if ($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS) {
          for ($plan_count = 0 ; $plan_count < count($get_planned_data["data"]) ; $plan_count++) {

              $total_planned_material_cost = $total_planned_material_cost + $get_planned_data["data"][$plan_count]["planned_material"];
                                                       

          }
          $return_data  = array('calculated_amt' => round($total_amt) , 'planned_amt' => $total_planned_material_cost);


          echo json_encode($return_data);exit();
      }

     }else{


         $total_planned_material_cost = 0;

     }

 }

 //this for indivudial check
 elseif (isset($_GET['project_id']) && isset($_GET['material_id']) ) {	

	$project_id = $_GET['project_id'];
	$material_id = $_GET['material_id'];

	// Query
   
   $project_stock_module_mapping_search_data = array("active"=>'1','stock_project_id' => $project_id);
   $project_stock_module_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);
   if($project_stock_module_mapping_list['status'] == SUCCESS)
    {
      $mapping_data = $project_stock_module_mapping_list['data'];
      $mgmt_project_id = $mapping_data[0]['project_stock_module_mapping_mgmnt_project_id'];
    }

    else{

      $mgmt_project_id = 0;
    }
  
   $po_amount_query = "select sum(material_price * po_material_qty) as po_amount from po_material_list where project_id=:project_id";

   $stock_amount_query = "select sum(material_price * material_stock_quantity) as stock_amount from stock_material_list where material_stock_project_id=:project_id";

   $issue_amount_query = "select sum(material_price * material_issue_quantity) as issue_amount from issue_material_list where material_issue_project_id=:project_id";

   //2nd condition
   //get the total planned amount for project
   if($mgmt_project_id != 0){

     $project_planned_data = array("project_id"=>$mgmt_project_id);
   $get_planned_data = db_get_planned_data($project_planned_data);
   $total_planned_material_cost = 0 ;
     if ($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS) {
        for ($plan_count = 0 ; $plan_count < count($get_planned_data["data"]) ; $plan_count++) {
            
            $total_planned_material_cost = $total_planned_material_cost + $get_planned_data["data"][$plan_count]["planned_material"];
        }
    }

   }else{


       $total_planned_material_cost = 0;

   }
  

    //3rd condition
    $open_po_query = "select stock_purchase_order_number from po_material_list where project_id=:project_id and material_id=:material_id";
    

    try
    {
        $dbConnection = get_conn_handle();
        $po_amount = $dbConnection->prepare($po_amount_query);
        $stock_amount = $dbConnection->prepare($stock_amount_query);
        $issue_amount = $dbConnection->prepare($issue_amount_query);
        $open_po = $dbConnection->prepare($open_po_query);


        // Data
        $search_data = array(':project_id'=>$project_id);
        $open_po_search_data = array(':project_id'=>$project_id,':material_id'=>$material_id);

        //Connection
		    $dbConnection->beginTransaction();

		    //execute
        $po_amount->execute($search_data);
        $stock_amount->execute($search_data);
        $issue_amount->execute($search_data);
        $open_po->execute($open_po_search_data);

       	//check db status for amounts . i.e 1st 3 connections
        $po_amount_details = $po_amount -> fetchAll();
        $stock_amount_details = $stock_amount -> fetchAll();
        $issue_amount_details = $issue_amount -> fetchAll();
        $open_po_details = $open_po -> fetchAll();
        


        if(FALSE === $po_amount_details || FALSE === $stock_amount_details ||FALSE === $issue_amount_details)
    		{
    			$return["status"] = FAILURE;
    			$return["data"]   = "";
    			echo json_encode($return);
    		}
    		else if(count($po_amount_details) <= 0 || count($stock_amount_details) <= 0 || count($issue_amount_details)<= 0)
    		{
    			$return["status"] = DB_NO_RECORD;
    			$return["data"]   = "";
    			echo json_encode($return);
    		}


    		//check db status for open po connection
    		$open_po  = array('status' =>200 ,'data' => 0 );
    		if(count($open_po_details) > 0){
    			$open_po["status"] = 200;
    			$open_po["data"]   =  @$open_po_details[0]['stock_purchase_order_number'];

    		}else{
    			$open_po["status"] = 450;
    			$open_po["data"]   = 'No material found';


    		}
		
        $total_amt = @$po_amount_details[0]['po_amount'] + @$stock_amount_details[0]['stock_amount'] + @$issue_amount_details[0]['issue_amount'];

		  $data = array(
			"po_amount" =>$po_amount_details[0]['po_amount'] ,
			'stock' =>$stock_amount_details[0]['stock_amount'],
			'issue'=> $issue_amount_details[0]['issue_amount'],
			"planned_amt" => round($total_planned_material_cost),
			'total_amount'=> round($total_amt),
			'open_po' =>$open_po );

		$status =  200;

		echo json_encode(array('data' => $data,'status' => $status ));
		
    }
    catch(PDOException $e)
    {
       //send error exception
    	$data = array("po_amount_details" =>$po_amount_details ,'stock' =>$stock_amount_details,'issue'=> $issue_amount_details);
    	$status =  400;
        $data = array('data' =>"Failed in fetching data" ,'db'=>$data, 'status' => $status);
		echo json_encode($data);
		
    }

    
}
?>