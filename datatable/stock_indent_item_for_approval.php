<?php
  session_start();
  define('INDENT_FUNC_ID', '165');
  $base = $_SERVER['DOCUMENT_ROOT'];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
  $dbConnection = get_conn_handle();

  $role = $_SESSION["loggedin_role"];
  $user = $_SESSION["loggedin_user"];
  $approve_perms_list = i_get_user_perms($user, '', INDENT_FUNC_ID, '6', '1');
  $user_list = i_get_user_list('','','','','','',$user);
  if($user_list["status"]==0){
    $user_list_data=$user_list["data"];
  }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
  $user = $_SESSION["loggedin_user"];
  $aColumns = array(
    'uom',
    'added_on',
    'stock_indent_item_added_on',
    'indent_id',
    'indent_no',
    'indent_by',
    'project_id',
    'material_id',
    'project_name',
    'material_qty',
    'material_code',
    'material_name',
    'material_price',
    'indent_item_id',
    'stock_indent_id',
    'indent_item_status',
    'indent_item_active',
    'stock_indent_item_remarks',
    'stock_indent_item_added_by'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_id";

   /* DB table to use */
   $sTable = 'stock_indent_items_list';

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * Local functions
     */
    function fatal_error($sErrorMessage = '')
    {
        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
        die($sErrorMessage);
    }

    /*
     * Paging
     */
    $sLimit = "";
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = "LIMIT ".intval($_GET['iDisplayStart']).", ".
            intval($_GET['iDisplayLength']);
    }
    /*
     * Ordering
     */
    $sOrder = "";
    if ( isset( $_GET['order'][0]["column"] ) )
    {
      $sOrder = "ORDER BY ";
      for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
      {
          $sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
          ($_GET['order'][$i]["dir"]=='asc' ? 'desc' : 'asc') .", ";
      }

      $sOrder = substr_replace( $sOrder, "", -2 );
      if ( $sOrder == "ORDER BY" )
      {
        $sOrder = "";
      }
    }
    /*
\     */
    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
     $sWhere = "Where (";
     if($approve_perms_list["status"]==0 || $role==1) {
       $sWhere .= "`indent_item_active` = 1";
      }
      else if(!empty($user_list_data)){
        for ($count=0; $count < count($user_list_data); $count++) {
          $sWhere .= "(`stock_indent_item_added_by`=". $user_list_data[$count]["user_id"].") OR ";
        }
        $sWhere = substr_replace($sWhere, "", -3);
      }
      else{
        $sWhere .= "`stock_indent_item_added_by` = $user AND `indent_item_active` = 1";
      }
      $sWhere.= ") AND `indent_item_active` = 1";

      if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
        if($sWhere==''){
          $sWhere = "Where (";
        }
        else{
          $sWhere.=" AND (";
        }
        for ($i=0 ; $i<count($aColumns) ; $i++) {
          $sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch'])."%' OR ";
        }
        $sWhere = substr_replace($sWhere, "", -3);
          $sWhere .= ")";
      }

    if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
        if ($sWhere == "") {
          $sWhere .= " WHERE `project_id` = ". $_GET['project_id'];
        }
        else{
          $sWhere .= " AND `project_id` = ". $_GET['project_id'];
        }
    }

    if (isset($_GET['search_status']) && $_GET['search_status'] != '') {
        if ($sWhere == "") {
            $sWhere .= " WHERE `indent_item_status` = '". $_GET['search_status']."'";
        } else {
            $sWhere .= " AND `indent_item_status` = '". $_GET['search_status']."'";
        }
    }
    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";

    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $rResult = $statement -> fetchAll();

   
    /* Data set length after filtering */

    $sQuery = "SELECT FOUND_ROWS() as iFilteredTotal";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

    /* Total data set length */
    $sQuery = "
		SELECT COUNT(".$sIndexColumn.") as iTotal
		FROM   $sTable
	";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iTotal = $statement -> fetch()['iTotal'];
    /*
     * Output
     */
     $output = array(
         "draw" => intval($_GET['draw']),
         "iTotalRecords" => $iTotal,
         "iTotalDisplayRecords" => $iFilteredTotal,
         "aaData" => $rResult,
         "where" => $sQuery,
     );
     echo json_encode($output);
