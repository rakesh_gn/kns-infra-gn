var table;

function tableDraw() {
  table.draw();
}

function openModal(enquiry_id) {
  $('#exampleModal').modal({
    remote: 'crm_assign_enquiry.php?enquiry=' + enquiry_id
  });

  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function openSiteVisitPlanModal(enquiry_id) {
  $('#exampleModal').modal({
    remote: 'crm_add_site_visit_plan.php?enquiry=' + enquiry_id
  });

  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function enquiry_submit() {
  $.ajax({
    url: 'crm_assign_enquiry.php',
    type: 'POST',
    data: {
      hd_enquiry: $('#hd_enquiry').val(),
      ddl_assigned_to: $('#ddl_assigned_to').val(),
      add_assign_enquiry_submit: 'yes'
    },
    success: function(response) {
      $('#exampleModal').modal('hide');
      tableDraw();
      alert('success');
    }
  })
}

function submitPlan() {
  $.ajax({
    url: 'crm_add_site_visit_plan.php',
    type: 'POST',
    data: {
      hd_enquiry: $('#hd_enquiry').val(),
      dt_site_visit: $('#dt_site_visit').val(),
      stxt_visit_time: $('#stxt_visit_time').val(),
      stxt_pickup_location: $('#stxt_pickup_location').val(),
      ddl_project: $('#ddl_project').val(),
      rd_confirm_status: $("input[id='rd_confirm_status']:checked").val(),
      rd_drive_type: $("input[id='rd_drive_type']:checked").val(),
      add_site_visit_plan_submit: 'yes'
    },
    success: function(response) {
      if (response == '') {
        $('#exampleModal').modal('hide');
        alert("succes");
        tableDraw();
      } else {
        alert(response);
      }
    }
  })
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "name"
    },
    {
      "orderable": false,
      "data": "cell"
    },
    {
      "orderable": false,
      "data": "enquiry_number"
    },
    {
      "orderable": false,
      "data": "enquiry_source_master_name"
    },
    {
      "orderable": false,
      "data": "project_name"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.walk_in == "1") {
          return 'Yes';
        }
        return 'No';
      }
    },
    {
      "orderable": false,
      "data": "crm_cust_interest_status_name"
    },
    {
      "orderable": true,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return moment(data.enquiry_follow_up_date).format('DD-MM-YYYY HH:mm a');
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        var end = moment(data.enquiry_follow_up_date).format('x');
        var now = moment().format('x');
        if (type !== 'display') {
          return '';
        }
        var diff_time = end - now;
        var cd = 24 * 60 * 60 * 1000;
        var diff_date = Math.ceil(diff_time / cd);
        meta.settings.aoData[meta.row]._aData.diff_time = diff_date;
        return diff_date;
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return createToolTip(data.enquiry_follow_up_remarks, 20);
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return data.enquiry_follow_up_call_status;
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return createToolTip(data.user_name, 20);
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return createToolTip(data.assigned_by, 20);
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return '<a target="_blank" href="crm_add_enquiry_fup.php?enquiry=' + data.enquiry_id_for_follow_up + '"><span class="glyphicon glyphicon-calendar"></span></a>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return '<a href="#"><span id="enquiry" class="fas fa-users"></span></a>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return '<a href="#"><span id="visit_plan" class="fas fa-car"></span></a>';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_enquiry_fup.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    "language": {
      "infoFiltered": " "
    },
    "order": [
      [8, "asc"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_sales_work";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();
      aoData.cell = $('#cell').val();
      aoData.enquiry_number = $('#enquiry_number').val();
      aoData.search_source = $('#ddl_search_source').val();
      aoData.search_status = $('#ddl_search_int_status').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      if (full.diff_time < 0) {
        $(row).css("color", "red");
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'enquiry') {
      openModal(rowData['enquiry_id_for_follow_up']);
    } else if (event.target.tagName == 'SPAN' && event.target.id == 'visit_plan') {
      openSiteVisitPlanModal(rowData['enquiry_id_for_follow_up']);
    }
  });
});