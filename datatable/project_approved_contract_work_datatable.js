var table;

function toggleSelection(ele) {
  if (!enableGenerateBilling()) {
    $(ele).prop("checked", false);
  }
  $("input.input-checkbox").prop("checked", ele.checked);
}

function drawTable() {
  table.draw();
}

function generateBill() {
  var contractData = [];
  var tableData = table.data();
  var tmpContractData = [];

  // get all the 'checked' input:checkbox(s)
  $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele) {
    contractData.push(tableData[parseInt(ele.value)]);
  });

  if ($('input#selectAllCheckbox').is(':checked')) {
    contractData = tableData;
  } else if (!contractData.length) {
    contractData = [];
  }

  if (!contractData.length) {
    alert('Error: Select atleast one row to Bill');
    return false;
  }

  $.each(contractData, function(i, value) {
    var keys = ["project_task_actual_boq_length", "project_task_actual_boq_breadth", "project_task_actual_boq_depth",
      "project_task_actual_boq_total_measurement", "project_task_actual_boq_amount", "project_task_actual_boq_uom", "project_task_boq_actual_number",
      "project_task_actual_boq_date", "project_task_actual_boq_lumpsum", "project_task_boq_actual_id"
    ];
    var data = {};
    for (var key in keys) {
      data[keys[key]] = value[keys[key]];
    }
    tmpContractData.push(data);
  });

  generateContractId(tmpContractData);
}

function generateContractId(tmpContractData) {
  $.ajax({
    url: 'ajax/project_select_contract_payment.php',
    type: 'POST',
    data: {
      val: tmpContractData,
      vendor_id: $('#search_vendor').val(),
    },
    dataType: 'json',
    success: function(response) {
      $('#exampleModal').modal({
        remote: 'project_add_contract_bill_no.php?payment_contract_id=' + response.id
      });

      $('#exampleModal').on('hidden.bs.modal', function(e) {
        $(this).find('.modal-content').empty();
        $(this).data('bs.modal', null);
      });

      $('#exampleModal').on('shown.bs.modal', function(e) {
        $('#num_sec_deposit').on('change', check_deposit_validity);
      });
    }
  });
}

function submitBilling() {
  if (($('#ddl_company option:selected').val().trim() == '') ||
    ($('#stxt_remarks').val().trim() == '')) {
    alert('Please input required field');
    return false;
  }
  var bill_substr = $("#ddl_company option:selected").text().trim().replace(/\./g, '').replace('KNS', '').trim().substr(0, 5);
  bill_substr = (bill_substr === "KN SU") ? "SUREN" : bill_substr;
  $.ajax({
    url: 'project_add_contract_bill_no.php',
    type: 'POST',
    data: {
      add_bill_no_submit: 'yes',
      hd_contract_payment_id: $('#hd_contract_payment_id').val(),
      ddl_company: $('#ddl_company').val(),
      num_sec_deposit: $('#num_sec_deposit').val(),
      stxt_remarks: $('#stxt_remarks').val(),
      bill_substr: bill_substr,
    },
    success: function(response) {
      alert(response);
      $('#exampleModal').modal('hide');
      drawTable();
    }
  });
}

function check_deposit_validity(payable) {
  var payable = parseInt($('#payable_limit').val());
  payable = payable || 0;
  var num_sec_deposit = $('#num_sec_deposit');
  var deposit = parseInt(num_sec_deposit.val());

  if (payable > 5000) {
    if (deposit > payable) {
      num_sec_deposit.val(0);
      alert('Deposit cannot be greater than total payable to vendor');
    }
  } else {
    num_sec_deposit.val(0);
  }
}

function hasValue(name) {
  var value = $(name).val();
  return (value !== '' && value.length);
}

function enableGenerateBilling() {
  return hasValue('#search_project') &&
    hasValue('#search_vendor') &&
    hasValue('#start_date') &&
    hasValue('#end_date') &&
    table.data().length;
}


$(document).ready(function() {
  var columnsMapping = {
    '17': 'planned_msmt',
    '18': 'actual_msmt',
    '20': 'pending_msmt'
  };
  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_approved_contract_work.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    drawCallback: function() {
      contractData = [];
      $("input#selectAllCheckbox").prop("checked", false);
      $("input.input-checkbox").prop("checked", false);
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_contract_work";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_contract = $('#search_contract').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      if (Math.round(full.project_task_actual_boq_lumpsum) !=
        Math.round(full.project_task_actual_boq_total_measurement * full.project_task_actual_boq_amount)) {
        $(row).css("color", "red");
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": [{
        className: 'noVis',
        "orderable": false,
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "orderable": false,
        "data": "project_manpower_agency_name"
      },
      {
        "orderable": false,
        "data": "project_master_name"
      },
      {
        "orderable": false,
        "data": "project_process_master_name"
      },
      {
        "orderable": false,
        "data": "project_task_master_name"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.road_name == "No Roads") {
            return 'No Roads';
          }
          return data.road_name;
        }
      },
      {
        "orderable": false,
        "data": "project_contract_process_name"
      },
      {
        "orderable": false,
        "data": "project_contract_rate_master_work_task"
      },
      {
        "orderable": false,
        "data": "stock_unit_name"
      },
      {
        "orderable": false,
        "data": "project_task_boq_actual_number"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_length"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_breadth"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_depth"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_total_measurement"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return Math.round(parseFloat(data.project_task_actual_boq_amount));
        }
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_lumpsum"
      },
      {
        "orderable": false,
        "data": "location_name"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_boq_work_type === 'Regular') {
            return data.project_task_actual_boq_completion;
          } else {
            return data.project_task_actual_boq_rework_completion;
          }
        }
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_work_type"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_remarks"
      },
      {
        "orderable": false,
        "data": "project_task_actual_boq_approved_by"
      },
      {
        "orderable": true,
        "orderData": [22, 23],
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_boq_approved_on).format('DD-MM-YYYY');
        }
      },
      {
        "orderable": true,
        "orderData": [23, 22],
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_boq_date).format('DD-MM-YYYY');
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a target="_blank" href="project_contract_work_print.php?boq_id=' + data.project_task_boq_actual_id + '"><span class="glyphicon glyphicon-print"></span></a>';
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function() {
          if ((arguments[1] !== 'display') || (!enableGenerateBilling())) {
            return '';
          }
          return '<input type="checkbox" class="input-checkbox" value="' + arguments[3].row + '" >';
        }
      }
    ],
  });
});