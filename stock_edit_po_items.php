<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 10-Oct-2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];



include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	if(isset($_POST["item_id"]) )

	{

		$item_id = $_POST["item_id"];
		

	}

	else

	{

		$item_id = "-1";

	}


	


	if(isset($_POST["order_id"]) || isset($_GET["order_id"]) )

	{

		$order_id = $_POST["order_id"];

	}

	else

	{

		$order_id = "-1";

	}

	//for get method
	if(isset($_GET["item_id"])){
			$item_id = $_GET["item_id"];
	}
	if(isset($_GET["order_id"])){
			$order_id = $_GET["order_id"];
	}

	// Capture the form data
	if(isset($_POST["purchase_edit_items_submit"]))

	{

		$order_id 			   = $_POST["hd_order_id"];

		$item_id 			   = $_POST["hd_item_id"];

		$quantity	           = $_POST["num_qty"];

		$tax_type	           = $_POST["sgst_tax_type"];

		$cost		           = $_POST["num_cost"];
		
		$excise_duty	       = $_POST["excise_duty"];
		
		$transport	           = $_POST["ddl_transport"];

		$delivery_schedule	   = $_POST["txt_delivery_schedule"];

		$discount	       	   = $_POST["item_discount"];

		$taxable_amount	       = $_POST["taxable_amount"];

		$cgst_amt	           = $_POST["cgst_amount"];

		$cgst_tax_type	       = $_POST["cgst_tax_type"];

		$sgst_amt	   		   = $_POST["sgst_amount"];

		$sgst_tax_type	       = $_POST["sgst_tax_type"];

		$total_amt	           = $_POST["total_amount"];

		$brand	               = $_POST["txt_brand"];


		// Check for mandatory fields

		if(($quantity != "") && ($tax_type != "") && ($cost != ""))

		{
			$purchase_order_items_update_data = array("item_id"=>$item_id,"quantity"=>$quantity,"uom"=>$uom,"excise_duty"=>$excise_duty,"tax_type"=>$tax_type,"cost"=>$cost,"transport"=>$transport,"delivery_schedule"=>$delivery_schedule,"discount" => $discount,"taxable_amount" => $taxable_amount,"cgst_amt" => $cgst_amt,
				"sgst_amt" => $sgst_amt ,"total_amt" => $total_amt,'brand' => $brand, 'cgst_tax_type' => $cgst_tax_type,
				'sgst_tax_type' => $sgst_tax_type);

			$po_items_iresult = i_update_purchase_order_items($order_id,$purchase_order_items_update_data);
			if($po_items_iresult["status"] == SUCCESS)

				

			{	

				header("location:stock_purchase_order_items.php?po_id=$order_id");

				

			}

			

			$alert = $po_items_iresult["data"];

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	

	// Get purchase modes already added

	$stock_purchase_order_search_data = array();

	$purchase_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);
	if($purchase_order_list['status'] == SUCCESS)

	{

		$purchase_order_list_data = $purchase_order_list['data'];

	}

	else

	{

		$alert = $purchase_order_list["data"];

		$alert_type = 0;

	}

	// Get Purchase Item Details

	$stock_purchase_order_items_search_data = array("item_id"=>$item_id);
	$purchase_order_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);

	if($purchase_order_items_list["status"] == SUCCESS)

	{

		$purchase_order_items_list_data = $purchase_order_items_list["data"];
		
	}

	else

	{

		$alert = $alert."Alert: ".$purchase_order_items_list["data"];

	}

	

	//Get Uom List

	$stock_unit_search_data = array();

	$unit_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($unit_list["status"] == SUCCESS)

	{

		$unit_list_data = $unit_list["data"];

	}

	else

	{

		$alert = $unit_list["data"];

		$alert_type = 0;

	}

	//Get Tax Type List

	$stock_tax_type_master_search_data = array();

	$tax_type_list = i_get_stock_tax_type_master_list($stock_tax_type_master_search_data);

	if($tax_type_list["status"] == SUCCESS)

	{

		$tax_type_list_data = $tax_type_list["data"];

	}

	else

	{

		$alert = $tax_type_list["data"];

		$alert_type = 0;

	}

	

	//Get Transporation List

	$transportation_search_data = array();

	$transportation_list = i_get_transportation_list($transportation_search_data);

	if($transportation_list["status"] == SUCCESS)

	{

		$transportation_list_data = $transportation_list["data"];

	}

	else

	{

		$alert = $transportation_list["data"];

		$alert_type = 0;

	}

	

	// Get Material Details

	$stock_material_search_data = array();

	$material_list = i_get_stock_material_master_list($stock_material_search_data);

	if($material_list["status"] == SUCCESS)

	{

		$material_list_data = $material_list["data"];

	}

	else

	{

		$alert = $material_list["data"];

		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Edit GRN</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">
   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->
    <script src="js/jquery-1.7.2.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/base.js"></script>
	<script src="datatable/tax_calculation.js"></script>

	

  </head>



<body>

    

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Edit Purchase Order Items</h3>
	      				
	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Edit Purchase Order Items</a>

						  </li>	
						 

						</ul>
						<p>PO Number : <?php echo $purchase_order_items_list_data[0]["stock_purchase_order_number"] ;?></p>
      					<p>Project : <?php echo $purchase_order_items_list_data[0]["stock_project_name"] ;?></p>
      					<p>Material : <?php echo $purchase_order_items_list_data[0]["stock_material_name"] ;?> - <?php echo $purchase_order_items_list_data[0]["stock_material_code"] ;?></p>

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                         

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

							<div class="tab-pane active" id="formcontrols">

							<form method="post" class="form-row" action="stock_edit_po_items.php">

							<input type="hidden" name="hd_item_id" value="<?php echo $item_id; ?>" />

							<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />

	

							            

										<div class="control-group col-sm-6">											

											<label class="control-label" for="num_qty">Quantity*</label>

											<div class="controls">

												<input type="number" id="qty" readonly name="num_qty" placeholder="Quantity" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_item_quantity"] ;?>" min="0.01" step="0.01" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										
									  <div class="control-group col-sm-6">											

											<label class="control-label" for="num_cost">cost</label>

											<div class="controls">

												<input type="number" name="num_cost" placeholder="Cost" min="0.01" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_item_cost"] ;?>" id="cost">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										
										<div class="control-group col-sm-6">											

											<label class="control-label" for="item_discount">Discount</label>

											<div class="controls">

												<input type="number" id="discount" name="item_discount" placeholder="Discount" min="0" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_item_discount"] ;?>" >

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->


								

										

										<!-- <div class="control-group">		
										
											<label class="control-label" for="excise_duty">Excise Duty*</label>
											
											<div class="controls">
											
												<input type="number" name="excise_duty" step="0.01" max="100" value="<?php echo $purchase_order_items_list_data
												
												[0]["stock_purchase_order_item_excise_duty"] ;?>">
												
											</div> 				
											
										</div> --> <!-- /control-group -->
										
										<div class="control-group col-sm-6">											

											<label class="control-label" for="taxable_amount">Taxable Amount</label>

											<div class="controls">

												<input type="number" id="taxable_amt" readonly name="taxable_amount" placeholder="Taxable Amount" min="0.01" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_taxable_amount"] ;?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										<div class="control-group col-sm-6">											

											<label class="control-label" for="cgst_tax_type">CGST*</label>

											<div class="controls">

												<select class="cgst" name="cgst_tax_type" required id="cgst">

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option 
												 data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_purchase_order_cgst_tax_type"]){ ?> selected="selected" <?php } ?>>

												 <?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?>

												 

												

												 </option>	

											

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->


										<div class="control-group col-sm-6">											

											<label class="control-label" for="sgst_tax_type">SGST*</label>

											<div class="controls ">

												<select name="sgst_tax_type" required id="sgst" >

												<option>- - -Select Tax Type- - -</option>

												<?php

												for($count = 0; $count < count($tax_type_list_data); $count++)

												{

												?>

												<option data-taxvalue="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"] ?>" value="<?php echo $tax_type_list_data[$count]["stock_tax_type_master_value"]; ?>" <?php if($tax_type_list_data[$count]["stock_tax_type_master_value"] == $purchase_order_items_list_data[0]["stock_purchase_order_sgst_tax_type"]){ ?> selected="selected" <?php } ?>><?php echo $tax_type_list_data[$count]["stock_tax_type_master_name"]; ?></option>	

											

												<?php

												}

												?>

												</select>

												

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->


										<div class="control-group col-sm-6">											

											<label class="control-label" for="taxable_amount">CGST Amount</label>

											<div class="controls">

												<input type="number" readonly id="cgst_amt" readonly name="cgst_amount" placeholder="CGST Amount" min="0.01" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_cgst_amt"] ;?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										<div class="control-group col-sm-6">											

											<label class="control-label" for="taxable_amount">SGST Amount</label>

											<div class="controls">

												
												<input type="number" readonly id="sgst_amt" readonly name="sgst_amount" placeholder="SGST Amount" min="0.01" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_sgst_amt"] ;?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										<div class="control-group col-sm-6">											

											<label class="control-label" for="total_amount">Total Amount</label>

											<div class="controls">

												<input type="number" id="total_amt" readonly name="total_amount" placeholder="Total Amount" min="0.01" step="0.01" value="<?php echo $purchase_order_items_list_data

												[0]["stock_purchase_order_total_amount"] ;?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										<!-- <div class="control-group col-sm-6">	
										
											<label class="control-label" for="ddl_transport">Transport*</label>

											<div class="controls">

												<select name="ddl_transport">

												<option value="">- - Select Transportation - -</option>

												<?php

												for($count = 0; $count < count($transportation_list_data); $count++)

												{

												?>

												<option value="<?php echo $transportation_list_data[$count]["stock_transportation_id"]; ?>" <?php if($transportation_list_data[$count]["stock_transportation_id"] == $purchase_order_items_list_data[0]["stock_purchase_order_item_transport"]){ ?> selected="selected" <?php } ?>>

												<?php echo $transportation_list_data[$count]["stock_transportation_name"]; ?></option>

											

												<?php

												}

												?>

												</select>

											</div>					

										</div> -->

										

										<!-- <div class="control-group col-sm-6">											

											<label class="control-label" for="txt_delivery_schedule">Delivery Schedule*</label>

											<div class="controls">

												<input type="name" name="txt_delivery_schedule" placeholder="Delivery Schedule" value="<?php echo $purchase_order_items_list_data[0]["stock_purchase_order_item_delivery_schedule"] ;?>">

											</div> 					

										</div> --> <!-- /control-group -->


										<div class="control-group col-sm-6">											

											<label class="control-label" for="txt_brand">Description</label>

											<div class="controls">

											<textarea  name="txt_brand" placeholder="Description" maxlength="250"><?php echo $purchase_order_items_list_data[0]["stock_purchase_order_brand"] ;?></textarea>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

                           <div class="modal-footer">                              

                            <button type="submit" name="purchase_edit_items_submit" class="btn btn-primary">Save changes</button>					  

                           </div>

                           </form>

								</div>

								

							</div> 

							

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      	

	      	

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

</body>
</html>

