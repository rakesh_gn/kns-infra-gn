
<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');	



// Session Data

$user 		   = $_SESSION["loggedin_user"];

$role 		   = $_SESSION["loggedin_role"];

$loggedin_name = $_SESSION["loggedin_user_name"];



// Temp Data

$alert_type = -1;

$alert = "";



$po_id = $_REQUEST["po_id"];
$stock_purchase_order_search_data = array("order_id"=>$po_id,"active"=>'1');
$purchase_order = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

if($purchase_order["status"] == SUCCESS)

{

	$purchase_order_list_data = $purchase_order["data"];
	//get the total value
	$stock_purchase_order_items_search_data = array("active"=>'1',"order_id"=>$po_id);


	$purchase_order_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);
	if($purchase_order_items_list["status"] == SUCCESS)

	{

		$purchase_order_items_list_data = $purchase_order_items_list["data"];

		for($item_count = 0; $item_count < count($purchase_order_items_list_data); $item_count++)

		{


			$new_po_items_value[]   =  $purchase_order_items_list_data[$item_count]["stock_purchase_order_total_amount"];

			$item_value[]   =  $purchase_order_items_list_data[$item_count]["stock_purchase_order_total_amount"];

		}
		$items_value = array_sum($new_po_items_value);
		$po_total =  $items_value + 
						   $purchase_order['data'][0]['stock_transportation_total'] + 
						   $purchase_order['data'][0]['stock_other_total'] +
			 			   $purchase_order['data'][0]['stock_fright_total'];
		$po_total =	 number_format((float)$po_total, 2, '.', '');
		$item_total = array_sum($item_value);


	}
	

	$stock_purchase_order_items_search_data = array("order_id"=>$po_id,"active"=>'1');

	$po_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data); 

	if($po_items_list['status'] == SUCCESS)
	{

		$po_items_list_data = $po_items_list['data'];

		$po_no 			    = $po_items_list_data[0]["stock_purchase_order_number"];

		$po_date 		    = $po_items_list_data[0]["stock_purchase_order_added_on"];

		$credit_duration    = $po_items_list_data[0]["stock_purchase_order_credit_duration"];

		$delivery_date      = date("d-M-Y",strtotime($po_items_list_data[0]["stock_purchase_order_due_date"]));
		
		$project		    = $po_items_list_data[0]["stock_project_name"];
		
		$trans_charges		= $po_items_list_data[0]["stock_purchase_order_transportation_charges"];
		
		$vendor 		    = $po_items_list_data[0]["stock_vendor_name"];
		
		$vendor_id 		    = $po_items_list_data[0]["stock_purchase_order_vendor"];
		
		$vendor 		    = $po_items_list_data[0]["stock_vendor_name"];

		$vendor_address     = $po_items_list_data[0]["stock_vendor_address"];

		$vendor_contact_no  = $po_items_list_data[0]["stock_vendor_contact_number"];

		$quotation_no 	    = $po_items_list_data[0]["stock_quotation_no"];	

		$po_terms    	    = $po_items_list_data[0]["stock_purchase_order_terms"];	

		$company_name       = $po_items_list_data[0]["stock_company_master_name"];

		$company_contact    = $po_items_list_data[0]["stock_company_master_contact_person"];	

		$company_address    = $po_items_list_data[0]["stock_company_master_address"];

		$company_tin        = $po_items_list_data[0]["stock_company_master_tin_no"];	

		$location_name      = $po_items_list_data[0]["stock_location_name"];	

		$location_address   = $po_items_list_data[0]["stock_location_address"];

		

		

	}	

	else

	{

		// Do nothing

	}

}

else

{

	$alert = $purchase_order["data"];

	$alert_type = 0;

}





$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Purchase Order</title>


</head>



<body>

<table width="100%" border="1" cellpadding="3" style="border-collapse:collapse; border:1px solid #333;">

  <tr>

    <td width="50%" rowspan="3" valign="top"><img src="kns-logo.png" width="186" height="46" /><br /></td>

    <td colspan="2" align="right" valign="middle"><h3>Purchase Order</h3></td>

  </tr>

  <tr>

    <td width="25%" align="right" valign="middle">PO NO.</td>

    <td width="25%" align="left" valign="middle">'.$po_no.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">Date</td>

    <td align="left" valign="middle">'.date("d-M-Y",strtotime($po_date)).'</td>

  </tr>

  <tr>

    <td style="border-top:1px solid #fff;" width="50%" rowspan="3" valign="top">'.$company_name.',<br />

'.$company_contact.',<br />

'.$company_address.'</td>

    <td align="right" valign="middle">Quotation Ref</td>

    <td align="left" valign="middle">'.$quotation_no.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">Delivery Date</td>

    <td align="left" valign="middle">'.$delivery_date.'</td>

  </tr>

  <tr>

    <td align="right" valign="middle">GSTIN</td>

    <td align="left" valign="middle">'.$company_tin.'</td>

  </tr>

  <tr>

    <td valign="top">Vendor<br />

      '.$vendor.'<br />

   Address: '.$vendor_address.'<br/>

	Contact No:'.$vendor_contact_no.'</td>

    <td colspan="2">Delivery Address:<br />
      '.$location_name.' (for '.$project.')<br />
      '.$location_address.'</td>

  </tr>

</table><br/>

<table width="100%" border="1" cellpadding="3" style="border-collapse:collapse; border:1px solid #333;">

  <tr>

    <th width="4%" align="center">Sl.No</th>

    <th width="35%" align="center">Material Description</th>
	
	
	<th width="8%" align="center">UOM</th>

    <th width="7%" align="center">Qty</th>

    <th width="9%" align="center">Rate</th>
	


    <th width="9%" align="center"><small>Taxable (INR)</small></th>
	
	<th width="9%" align="center">CGST</th>

	<th width="9%" align="center">SGST</th>
	
	
    <th colspan="2" width="10%" align="right"><small>Total amount INR</small></th>

  </tr>';

		

$grand_total =  $po_total;
$igst = 0;
$igst_value = 0;


for($count = 0; $count < count($po_items_list["data"]); $count++)

{

	$qty   = $po_items_list_data[$count]["stock_purchase_order_item_quantity"];

	$value = $po_items_list_data[$count]["stock_purchase_order_item_cost"];
	
	$cgst = $po_items_list_data[$count]["stock_purchase_order_cgst_tax_type"];

	$sgst = $po_items_list_data[$count]["stock_purchase_order_sgst_tax_type"];
	
	$cgst_value = $po_items_list_data[$count]["stock_purchase_order_cgst_amt"];

	$sgst_value = $po_items_list_data[$count]["stock_purchase_order_sgst_amt"];

	$po_t_cgst_value = 
	(($purchase_order['data'][0]['stock_transportation_taxable'] * $purchase_order['data'][0]['stock_t_cgst'])/100)/2;

	$po_t_sgst_value = (($purchase_order['data'][0]['stock_transportation_taxable'] * $purchase_order['data'][0]['stock_t_sgst'])/100)/2;


	$po_f_cgst_value = 
	(($purchase_order['data'][0]['stock_fright_taxable'] * $purchase_order['data'][0]['stock_f_cgst'])/100)/2;

	$po_f_sgst_value = 
	(($purchase_order['data'][0]['stock_fright_taxable'] * $purchase_order['data'][0]['stock_f_sgst'])/100)/2;


	$po_o_cgst_value = (
	($purchase_order['data'][0]['stock_other_taxable'] * $purchase_order['data'][0]['stock_o_cgst'])/100)/2;

	$po_o_sgst_value = (($purchase_order['data'][0]['stock_other_taxable'] * $purchase_order['data'][0]['stock_o_sgst'])/100)/2;

	$po_igst_value = 0;

	$grand_total_words = convert_num_to_words($grand_total);

	$total_without_tax = $po_items_list_data[$count]["stock_purchase_order_item_cost"] * $po_items_list_data[$count]["stock_purchase_order_item_quantity"];


	$po_print_format = $po_print_format.'<tr>

              <td style="height:30px;">'.($count + 1).'</td>

              <td >'.$po_items_list_data[$count]["stock_material_name"].''.'-'.''.$po_items_list_data[$count]["stock_material_code"].' <br>
              '.$po_items_list_data[$count]["stock_purchase_order_brand"].'</td>
			  
			  
			  <td align="right"><small>'.$po_items_list_data[$count]["stock_unit_name"].'</small></td>

			  <td align="right"><small>'. $qty.'</small></td>

              <td align="right"><small>'. get_formatted_amount(round($value,2),INDIA).'</small></td>
			  
			   

			  <td align="right"><small>'.$po_items_list_data[$count]["stock_purchase_order_taxable_amount"].'</small></td>

			  <td align="right"><small>'.round($cgst_value,2).''.'<br>('.' '.$cgst.''.'%'.''.')'.'</small></td>

			  <td align="right"><small>'.round($sgst_value,2).''.'<br>('.' '.$sgst.''.'%'.''.')'.'</small></td>
			  

              <td align="right" colspan="3"><small>'.$po_items_list_data[$count]["stock_purchase_order_total_amount"].'</	small></td>

            </tr>';


}



$po_print_format = $po_print_format.'<tr>
	
	<td align="right" colspan="5">Transport Charges</td>
	<td align="right"><small>'.$purchase_order['data'][0]['stock_transportation_taxable'].'</small></td>
    <td align="right"><small>'.$po_t_cgst_value.'<br>('.$purchase_order['data'][0]['stock_t_cgst'].'%)'.'</small></td>
    <td align="right"><small>'.$po_t_sgst_value.'<br>('.$purchase_order['data'][0]['stock_t_sgst'].'%)'.'</small></td>
   
    <td align="right" colspan="3"><small>'.$purchase_order['data'][0]['stock_transportation_total'].'</small></td>
	</tr>';

$po_print_format = $po_print_format.'<tr>
	
	<td align="right" colspan="5">Fright/Packing</td>
	<td align="right"><small>'.$purchase_order['data'][0]['stock_fright_taxable'].'</small></td>
    <td align="right"><small>'.$po_f_cgst_value.'<br>('.$purchase_order['data'][0]['stock_f_cgst'].'%)'.'</small></td>
    <td align="right"><small>'.$po_f_sgst_value.'<br>('.$purchase_order['data'][0]['stock_f_sgst'].'%)'.'</small></td>
    
    <td align="right" colspan="3"><small>'.$purchase_order['data'][0]['stock_fright_total'].'</small></td>
	</tr>';

$po_print_format = $po_print_format.'<tr>
	
	<td align="right" colspan="5">Other Charges </td>
	<td align="right"><small>'.$purchase_order['data'][0]['stock_other_taxable'].'</small></td>
    <td align="right"><small>'.$po_o_cgst_value.'<br>('.$purchase_order['data'][0]['stock_o_cgst'].'%)'.'</small></td>
    <td align="right"><small>'.$po_o_sgst_value.'<br>('.$purchase_order['data'][0]['stock_o_sgst'].'%)'.'</small></td>
   
    <td align="right" colspan="3"><small>'.$purchase_order['data'][0]['stock_other_total'].'</small></td>
	</tr>';

	$po_print_format = $po_print_format.'<tr>
	<td colspan="6" align="right" rowspan="1" style="height:30px;border-top:none;"><strong>Grand Total</strong></td>
	  	  
	  <td colspan="5" align="right"><small>'.'Rs.'.''.$grand_total.'</small></td>
	  
	</tr>';
	$po_print_format = $po_print_format.'<tr>
	<td  colspan="13"  rowspan="1" style="height:30px;border-top:none;"><strong> Amount In words:- Indian Rupees -</strong> &nbsp;&nbsp;
	'.$grand_total_words.'</td>
	</tr>';


	$po_print_format = $po_print_format.'<tr>
	<td colspan="13" rowspan="1" style="height:30px;border-top:none;"><strong>Payment Terms:-</strong>'.$purchase_order['data'][0]['stock_purchase_order_credit_duration'].'days of the receipt of the invoice from the vendor</td>
	</tr>';


	
	$po_print_format = $po_print_format.
		'<tr>
		<td colspan="10" rowspan="4"  style="height:30px;border-top:none;"><strong>Remarks : </strong> '.$po_terms.' </td>
		
		</tr>';

	
$po_print_format = $po_print_format.'



</table>

<br />

<table width="100%" border="0">

  <tr>

    <td colspan="3" align="left"><strong>Terms and Conditions</strong></td>

  </tr>

  <tr>

    <td colspan="3" align="left"><ol>

    	<li> Please send two copies of your invoice.</li>

       <li> Delivery date/ Schedule : As per site requirement</li>

       <li> The quality of material will be checked by our QC/Engineers</li>

       <li> Quote our PO no in all future correspondence and documents</li>

        <li> Specify on every package - 1. Our PO No. 2. Gross, Net &amp; Tare weights 3. If any other (as per UOM)</li>

      

	  </ol></td>

  </tr>

  <tr>

    <td width="4%" align="left">&nbsp;</td>

    <td width="47%" align="left">CC<br />

Finance<br />

Project<br />

Site/Store</td>

    <td width="49%" align="center"><br />

    <br />

    <br />

    (Purchase Manager)</td>

  </tr>



</table>

</body>

</html>';
//echo $po_print_format;
$mpdf = new mPDF();
$mpdf->WriteHTML($po_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');

?>