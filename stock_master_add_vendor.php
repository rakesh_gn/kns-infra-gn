<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 28th Sep 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
//include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_ind//ent_functions.php'//);
/*include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');*/
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_vendor_master_submit"]))
	{
		$address                 = $_POST["stxt_address"];
		$vendor_name             = $_POST["stxt_vendor_name"];
		$contact_person          = $_POST["stxt_contact_person"];
		$contact_number	         = $_POST["num_contact_number"];
		$email_id                = $_POST["email_id"];
		$type_of_service         = $_POST["ddl_type_of_service"];
		$country                 = $_POST["stxt_country"];
		$pan_no                  = $_POST["stxt_pan_no"];
		$service_contact_no      = $_POST["num_service_contact_no"];
		$approved_list_no        = $_POST["num_approved_list_no"];
		$remarks 	             = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($address != "") && ($contact_person != "") && ($contact_number != "")
	    && ($type_of_service != ""))
		{
			$vendor_master_iresult = i_add_stock_vendor_master($address,$vendor_name,$contact_person,$contact_number,$email_id,$type_of_service,$country,$pan_no,$service_contact_no,$approved_list_no,$remarks,$user);
			
			if($vendor_master_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;				
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $vendor_master_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Vendor Type of Service List
	$stock_type_of_service_search_data = array();
	$type_of_service_list = i_get_type_of_service_list($stock_type_of_service_search_data);
	if($type_of_service_list["status"] == SUCCESS)
	{
		$type_of_service_list_data = $type_of_service_list["data"];
	}
	else
	{
		$alert = $type_of_service_list["data"];
		$alert_type = 0;
	}
	
	// Get already added vendors
	$stock_vendor_master_search_data = array();
	$vendor_master_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);
	if($vendor_master_list['status'] == SUCCESS)
	{
		$vendor_master_list_data = $vendor_master_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Vendor - stock Masters</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Vendor Details</h3><span style="float:right; padding-right:20px;"><a href="stock_master_vendor_list.php">Vendor List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Vendor</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								else if($alert_type == 1)
								{
								?>
									<div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_vendor_form" class="form-horizontal" method="post" action="stock_master_add_vendor.php">
									<fieldset>										
														
										<div class="control-group">											
											<label class="control-label" for="stxt_vendor_name">Vendor Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_vendor_name" placeholder="Vendor Name"
												required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_address">Address*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_address" placeholder="Address"
												required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_contact_person">Contact Person*</label>
											<div class="controls">
												<input type="name" class="span6" name="stxt_contact_person" placeholder="Contact Person" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_contact_number">Contact Number*</label>
											<div class="controls">
												<input type="text" class="span6" name="num_contact_number"  required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="email_id">Email ID*</label>
											<div class="controls">
												<input type="text" class="span6" name="email_id" placeholder="Email ID">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
											<div class="control-group">											
											<label class="control-label" for="ddl_type_of_service">Type Of Service*</label>
											<div class="controls">
												<select name="ddl_type_of_service" required>
												<option>- - -Select service- - -</option>
												<?php
												for($count = 0; $count < count($type_of_service_list_data); $count++)
												{
												?>
												<option value="<?php echo $type_of_service_list_data[$count]["stock_type_of_service_id"]; ?>"><?php echo $type_of_service_list_data[$count]["stock_type_of_service_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_country">Country*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_country" placeholder="Country">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_pan_no">PAN NO</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_pan_no">
											</div> <!-- /controls -->	
											</div> <!-- /control-group -->
											
										<div class="control-group">											
											<label class="control-label" for="num_service_contact_no">Tin No</label>
											<div class="controls">
												<input type="number" class="span6" name="num_service_contact_no">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_approved_list_no">Approved List No</label>
											<div class="controls">
												<input type="number" class="span6" name="num_approved_list_no">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_vendor_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      		
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
