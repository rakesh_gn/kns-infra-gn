<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Collection Report
CREATED ON	: 14-Nov-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Collection Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];


	// Temp data
	$alert      = "";
	$alert_type = "";		
	
	// Query String
	if(isset($_GET["status"]))
	{
		$status = $_GET["status"];
	}
	else
	{
		$status = "";
	}	
	
	// Delay Reason List
	$delay_reason_value = array();
	$delay_reason_list = i_get_crm_delay_reason_list('','1');
	if($delay_reason_list["status"] == SUCCESS)
	{
		$delay_reason_list_data = $delay_reason_list["data"];
		
		for($rcount = 0; $rcount < count($delay_reason_list_data); $rcount++)
		{
			$delay_reason_value[$delay_reason_list_data[$rcount]['crm_delay_reason_master_id']] = 0;
		}
	}
	
	// Get Pending Payment list
	$pending_delayed_list     = i_get_delayed_payment_list('',$status,'','');		
	$pending_non_delayed_list = i_get_pending_nondelayed_payment_list('',$status,'');
	if($pending_delayed_list['status'] == SUCCESS)
	{
		if($pending_non_delayed_list['status'] == SUCCESS)
		{
			$pending_payment_list['data'] = array_merge($pending_non_delayed_list['data'],$pending_delayed_list['data']);
		}
		else
		{
			$pending_payment_list['data'] = $pending_delayed_list['data'];
		}
		
		$pending_payment_list['status'] = SUCCESS;
	}
	else
	{
		$pending_payment_list = $pending_non_delayed_list;
	}
	
	// Collectibles
	$pending_collection_value = array();
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Create second sheet
	$objWorkSheet = $objPHPExcel->createSheet();
	// Set the active Excel worksheet to sheet 1
	$objPHPExcel->setActiveSheetIndex(1); 
	// Set sheet title
	$objPHPExcel->getActiveSheet()->setTitle("Details");
	// Initialise the Excel row number
	$row_count = 1; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R');
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SL NO");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "PROJECT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "SITE NO"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "AREA (IN SQ. FT)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "RATE PER SQ. FT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "CLIENT NAME"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "CLIENT NO"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "BOOKING DATE"); 	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "TOTAL COST"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "AMOUNT RECEIVED"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "LEAD TIME"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "LATEST STATUS"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "TENTATIVE REGISTERED DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "DELAY REASON"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, "FOLLOW UP"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, "FOLLOW UP DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, "PENDING AMOUNT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, "LOAN"); 
	
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[17].$row_count)->applyFromArray($style_array);
	$row_count++;
	
	if($pending_payment_list["status"] == SUCCESS)
	{
		$pending_payment_list_data = $pending_payment_list['data'];
		$sl_no                     = 0;
		$no_schedule_value         = 0;
		
		for($pcount = 0 ; $pcount < count($pending_payment_list_data); $pcount++)
		{
			$sl_no++;
			
			// Get customer profile details, if entered
			$cust_details = i_get_cust_profile_list('',$pending_payment_list_data[$pcount]["crm_booking_id"],'','','','','','','','');
			if($cust_details["status"] == SUCCESS)
			{
				if($cust_details["data"][0]["crm_customer_funding_type"] == '2')
				{
					$bank_loan = $cust_details["data"][0]["crm_bank_name"];
				}
				else if($cust_details["data"][0]["crm_customer_funding_type"] == '3')
				{
					$bank_loan = "Waiting for info";
				}
				else
				{
					$bank_loan = "Self Funding";
				}
			}
			else
			{
				$bank_loan = "";
			}
			
			// Total cost of the site
			if($pending_payment_list_data[$pcount]["crm_booking_consideration_area"] != "0")
			{
				$consideration_area = $pending_payment_list_data[$pcount]["crm_booking_consideration_area"];
			}
			else
			{
				$consideration_area = $pending_payment_list_data[$pcount]["crm_site_area"];
			}
			$total_cost = ($pending_payment_list_data[$pcount]["crm_booking_rate_per_sq_ft"] * $consideration_area);			
			
			// Customer Follow Up Data
			$customer_fup = i_get_payment_fup_list('',$pending_payment_list_data[$pcount]["crm_booking_id"],'','','','','');
			if($customer_fup['status'] == SUCCESS)
			{
				if($customer_fup['data'][0]['crm_payment_follow_up_cust_remarks'] != '')
				{
					$fup_remarks = $customer_fup['data'][0]['crm_payment_follow_up_cust_remarks'];
				}
				else
				{
					if(count($customer_fup['data']) > 1)
					{
						$fup_remarks = $customer_fup['data'][1]['crm_payment_follow_up_cust_remarks'];
					}
					else
					{
						$fup_remarks = '';
					}
				}
				$fup_date    = date('d-M-Y',strtotime($customer_fup['data'][0]['crm_payment_follow_up_date_time']));
			}
			else
			{
				$fup_remarks = '';
				$fup_date    = '';
			}
		
			$current_status = 'Booking';
			// Current Status Data										
			if($pending_payment_list_data[$pcount]["crm_booking_date"] != "0000-00-00")
			{
				$booking_date = date("d-M-Y",strtotime($pending_payment_list_data[$pcount]["crm_booking_date"])); 
				$current_status = 'Booked';
				$milestone = 'REGISTRATION';
				
				$lead_data = get_date_diff($pending_payment_list_data[$pcount]["crm_booking_date"],date('Y-m-d'));
			}
			else
			{
				$booking_date = "NA. Approved Date: ".date("d-M-Y",strtotime($pending_payment_list_data[$pcount]["crm_booking_approved_on"])); 
				$milestone = 'REGISTRATION';
				
				$lead_data = get_date_diff($pending_payment_list_data[$pcount]["crm_booking_approved_on"],date('Y-m-d'));
			}												
									
			$agreement_list = i_get_agreement_list('',$pending_payment_list_data[$pcount]["crm_booking_id"],'','','','','','','','','');
			if($agreement_list["status"] == SUCCESS)
			{							
				$current_status = 'Agreement';
				$milestone = 'REGISTRATION';
			}
			else
			{							
				// Do nothing
			}											
									
			$registration_list = i_get_registration_list('',$pending_payment_list_data[$pcount]["crm_booking_id"],'','','','','','','','','');
			if($registration_list["status"] == SUCCESS)
			{							
				$current_status = 'Registration';
				$milestone = 'KHATHA TRANSFER';
			}
			else
			{														
				// Do nothing
			}				
			
			// Payment Data
			$total_payment_done = 0;
			$payment_details = i_get_payment('',$pending_payment_list_data[$pcount]["crm_booking_id"],'','','');
			if($payment_details["status"] == SUCCESS)
			{
				for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)
				{
					$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
				}
			}
			else						
			{
				$total_payment_done = 0;
			}
			
			// Delay Reason
			$delay_reason_list = i_get_delay_reason_list($pending_payment_list_data[$pcount]["crm_booking_id"],'','1');
			if($delay_reason_list['status'] == SUCCESS)
			{
				$delay_reason    = $delay_reason_list['data'][0]['crm_delay_reason_master_name'];
				$delay_remarks   = $delay_reason_list['data'][0]['crm_delay_reason_remarks'];
				$delay_reason_id = $delay_reason_list['data'][0]['crm_delay_reason'];
			}
			else
			{
				$delay_reason    = 'NO REASON';
				$delay_remarks   = '';
				$delay_reason_id = '-1';
			}
			
			// Pending Payment Value
			$pending_payment_value = round(($total_cost - $total_payment_done));
			
			// Get payment schedule
			$pay_schedule_list = i_get_pay_schedule($pending_payment_list_data[$pcount]["crm_booking_id"],'','','','','date_desc');
			if($pay_schedule_list['status'] == SUCCESS)
			{
				$next_stage_date = date('d-M-Y',strtotime($pay_schedule_list['data'][0]['crm_payment_schedule_date']));
				if((strtotime($next_stage_date)) >= (strtotime(date('Y-m-d'))))
				{
					$color = 'black';
				}
				else
				{
					$color = 'red';
				}								
				
				if($delay_reason_id == '-1')
				{
					$schedule_check_amount = $total_payment_done;
					for($pscount = 0; $pscount < count($pay_schedule_list['data']); $pscount++)
					{						
						if($schedule_check_amount < $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'])
						{
							if($schedule_check_amount >= 0)
							{
								if(array_key_exists($pay_schedule_list['data'][$pscount]['crm_payment_schedule_date'],$pending_collection_value))
								{
									$pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] = $pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] + $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'] - $schedule_check_amount;												
								}
								else
								{
									$pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] = $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'] - $schedule_check_amount;		
								}
							}
							else
							{
								if(array_key_exists($pay_schedule_list['data'][$pscount]['crm_payment_schedule_date'],$pending_collection_value))
								{
									$pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] = $pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] + $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'];
								}
								else
								{
									$pending_collection_value[$pay_schedule_list['data'][$pscount]['crm_payment_schedule_date']] = $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'];
								}
							}
						}
						
						$schedule_check_amount = $schedule_check_amount - $pay_schedule_list['data'][$pscount]['crm_payment_schedule_amount'];
					}
				}
			}						
			else
			{
				$next_stage_date = 'NO SCHEDULE';
				$color = 'red';
				
				$no_schedule_value = $no_schedule_value + $pending_payment_value;
			}						
			
			// Delay Reason Value
			if($delay_reason_id != '-1')
			{
				$delay_reason_value[$delay_reason_id] = $delay_reason_value[$delay_reason_id] + $pending_payment_value;
			}			
			
			if($pending_payment_list_data[$pcount]["crm_booking_status"] == "1")
			{
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $sl_no); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $pending_payment_list_data[$pcount]["project_name"]); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $pending_payment_list_data[$pcount]['crm_site_no']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $pending_payment_list_data[$pcount]['crm_site_area']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $pending_payment_list_data[$pcount]['crm_booking_rate_per_sq_ft']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $pending_payment_list_data[$pcount]['name']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $pending_payment_list_data[$pcount]['cell']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $booking_date); 			
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $total_cost);
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $total_payment_done); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $lead_data['data']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $current_status); 	
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $next_stage_date); 			
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, $delay_reason); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, $fup_remarks);
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, $fup_date);
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, $pending_payment_value);
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, $bank_loan);
				
				$row_count++;
			}
		}
	}
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[16].$row_count)->getAlignment()->setWrapText(true); 
	
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Set sheet title
	$objPHPExcel->getActiveSheet()->setTitle("Summary");
	// Initialise the Excel row number
	$row_count = 2;
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		
	/* - - - - - - - - - - - Pending Collection Section Start - - - - - - - - - - - */
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "DATE WISE COLLECTION");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "AMOUNT CONFIRMED");
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[16].$row_count)->applyFromArray($style_array);
	$row_count++;
	$grand_total_pending = 0;
	$style_array = array('font' => array('bold' => true));	
	
	// No Schedule Values
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "NO SCHEDULE");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $no_schedule_value);
	$grand_total_pending = $grand_total_pending + $no_schedule_value;
	$row_count++;

	while(current($pending_collection_value))	
	{
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, date('d-M-Y',strtotime(key($pending_collection_value)))); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $pending_collection_value[key($pending_collection_value)]); 
		$grand_total_pending = $grand_total_pending + $pending_collection_value[key($pending_collection_value)];
		$row_count++;
		next($pending_collection_value);
	}
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, 'GRAND TOTAL'); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $grand_total_pending);
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[2].$row_count)->applyFromArray($style_array);
	$row_count++;
	$row_count++;
	/* - - - - - - - - - - - Pending Collection Section End - - - - - - - - - - - */
	
	/* - - - - - - - - - - - Delayed With Reason Receivables Section Start - - - - - - - - - - - */
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "DELAY REASON");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "SUM OF TOTAL BALANCE");
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[16].$row_count)->applyFromArray($style_array);
	$row_count++;
	$grand_total_delayed = 0;
	for($rcount = 0; $rcount < count($delay_reason_list_data); $rcount++)
	{
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $delay_reason_list_data[$rcount]['crm_delay_reason_master_name']); 
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $delay_reason_value[$delay_reason_list_data[$rcount]['crm_delay_reason_master_id']]); 
		$grand_total_delayed = $grand_total_delayed + $delay_reason_value[$delay_reason_list_data[$rcount]['crm_delay_reason_master_id']];
		$row_count++;
	}
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, 'GRAND TOTAL'); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $grand_total_delayed);
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[2].$row_count)->applyFromArray($style_array);
	/* - - - - - - - - - - - Delayed With Reason Receivables Section End - - - - - - - - - - - */

	$row_count++;
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[13].$row_count)->getAlignment()->setWrapText(true); 	
	/* Create excel sheet and write the column headers - END */
	
	
	header('Content-Type: application/vnd.ms-excel'); 
	header('Content-Disposition: attachment;filename="Receivables.xls"'); 
	header('Cache-Control: max-age=0'); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output');
}		
else
{
	header("location:login.php");
}	
?>