<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_list.php
CREATED ON	: 11-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD files
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'post_legal_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["msg"]))
	{
		$msg = $_GET["msg"];
	}
	else
	{
		$msg = "";
	}
	// Nothing here

	// Temp data
	$alert = "";

	$project_id     = "";
	$survey_no      = "";
	
	// Search parameters
	if(isset($_POST["file_search_submit"]))
	{		
		$project_id   = $_POST["ddl_project"];
		$survey_no    = $_POST["stxt_survey_no"];		
	}
	
	// Get list of files
	$file_list = i_get_file_list('','','',$survey_no,'','','','','','','',$project_id,'','');

	if($file_list["status"] == SUCCESS)
	{
		$file_list_data = $file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$file_list["data"];
	}			
	
	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list('','','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>File List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:60px;"> <i class="icon-th-list"></i>
              <h3>File List&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;</h3>
			</div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="completed_file_list.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php for($count = 0; $count < count($bd_project_list_data); $count++)
			  {?>
			  <option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>" <?php if($bd_project_list_data[$count]["bd_project_id"] == $project_id){ ?> selected="selected" <?php } ?>><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>
			  <?php
			  }?>
			  </select>
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="stxt_survey_no" value="<?php echo $survey_no; ?>" placeholder="Search by survey number" />
			  </span>			  
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php echo $msg; ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    
				    <th style="word-wrap:break-word;">File ID</th>
					<th style="word-wrap:break-word;">Survey No</th>
					<th style="word-wrap:break-word;">Project</th>
					<th style="word-wrap:break-word;">Land Owner</th>							
					<th style="word-wrap:break-word;">Village</th>						
					<th style="word-wrap:break-word;">Extent</th>
					<th style="word-wrap:break-word;">Land Status</th>					
					<th style="word-wrap:break-word;">Land Cost</th>										
					<th style="word-wrap:break-word;">Process Status</th>	
					<th style="word-wrap:break-word;">Paid Amount</th>	
					<th style="word-wrap:break-word;">Account</th>							
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$total_extent = 0;
				if($file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($file_list_data); $count++)
					{													
						$file_handover_data = i_get_file_handover_details($file_list_data[$count]['file_id']);
						if($file_handover_data['status'] == SUCCESS)
						{
							// Check if this file is already mapped
							$project_file_list = i_get_site_project_list($file_list_data[$count]['file_id'],'','','1','','','','','');
							
							if($project_file_list['status'] != SUCCESS)
							{
								// Get payment done for this file
								$total_payment = 0;
								$file_payment_list = i_get_file_payment_list($file_list_data[$count]["file_id"],'1','');
								if($file_payment_list['status'] == SUCCESS)
								{
									for($fp_count = 0; $fp_count < count($file_payment_list['data']); $fp_count++)
									$total_payment = $total_payment + $file_payment_list['data'][$fp_count]['file_payment_amount'];
								}
								else
								{
									$total_payment = 0;
								}
								?>
								<tr>
								<?php
								$total_extent = $total_extent + $file_list_data[$count]["bd_file_extent"];
																							
								?>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["file_number"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_file_survey_no"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_project_name"]; ?></td>						
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_file_owner"]; ?></td>						
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["village_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_file_extent"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_file_owner_status_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_file_land_cost"]; ?></td>						
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["process_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $total_payment; ?></td>
									<td style="word-wrap:break-word;"><?php echo $file_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
									<td style="word-wrap:break-word;"><a href="file_details.php?file=<?php echo $file_list_data[$count]["file_id"]; ?>">Details</a></td>	
									<td style="word-wrap:break-word;"><a href="file_payment_list.php?file=<?php echo $file_list_data[$count]['file_id']; ?>" target="_blank">Payment Details</a></td>																	
									<td style="word-wrap:break-word;"><a href="print_lawyer_report.php?file=<?php echo $file_list_data[$count]["file_bd_file_id"]; ?>" target="_blank">Lawyer Report</a></td>
									<td style="word-wrap:break-word;"><a href="add_legal_file_to_project.php?file=<?php echo $file_list_data[$count]["file_id"]; ?>">Assign to Project</a></td>
									<td style="word-wrap:break-word;"><a href="bd_edit_file.php?file=<?php echo $file_list_data[$count]["file_bd_file_id"]; ?>&src=completed">Edit File</a></td>
								</tr>
								<?php 	
							}
						}
					}
				}
				else
				{
					?>
					<td colspan="18">No Files added yet!</td>
					<?php					
				}
				 ?>	
				 <script>
				 document.getElementById('total_extent_span').innerHTML = <?php echo $total_extent; ?>;
				 </script>
				 <script>
				 <?php
				 for($count = 0; $count < count($owner_status_list_data); $count++)
				 {
					if($owner_status_list_data[$count]['bd_file_owner_status_id'] == '8')
					 {
						?>
						document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').style.color = "red";
						<?php
					 }
				 ?>				 
				 document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').innerHTML = <?php echo $status_totals[$owner_status_list_data[$count]['bd_file_owner_status_id']]; ?>;				 				 
				 <?php
				 }
				 ?>
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function confirm_deletion(bd_file)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "bd_file_list.php";
				}
			}

			xmlhttp.open("GET", "bd_file_delete.php?file=" + bd_file);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}
</script>

</body>

</html>
