<?php
session_start();
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{

  $user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

  $project_id = 0;
  if (isset($_GET["project_id"])) {
    $project_id   = $_GET["project_id"];
  }

  $picker_date = "";
  if (isset($_GET["picker_date"])) {
    $picker_date   = $_GET["picker_date"];
  }
  // Get Project Management Master modes already added
  $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
  $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
  $project_management_master_list_data = array();
  if ($project_management_master_list['status'] == SUCCESS) {
      $project_management_master_list_data = $project_management_master_list['data'];
  }
}
else {
	header("location:login.php");
}
?>
  <!DOCTYPE html>

  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Gantt Chart - KNS Infra</title>
    <script src="codebase/dhtmlxgantt.js"></script>
    <script src="codebase/ext/dhtmlxgantt_tooltip.js" type="text/javascript" charset="utf-8"></script>
    <script src="codebase/ext/dhtmlxgantt_marker.js" type="text/javascript" charset="utf-8"></script>
    <script src="codebase/ext/dhtmlxgantt_quick_info.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0-RC3/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0-RC3/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="codebase/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

    <style type="text/css" media="screen">
      html,
      body {
        margin: 0px;
        padding: 0px;
        height: 100%;
        overflow: hidden;
      }

      b {
        color: black;
        z-index: 1;
        padding: 10px;
        margin-left: 25px;
      }

      .btn {
        z-index: 10006;
      }

      .btn-icon {
        padding: 0px !important;
        margin-bottom: 17px;
        width: 43px;
        margin-left: 50px;
      }

      .baseline {
        position: absolute;
        border-radius: 2px;
        opacity: 0.6;
        margin-top: -11px;
        height: 28px;
        background: #3760e2;
        border: 1px solid #3760e2;
      }

      .updated_planned {
        position: absolute;
        z-index: 2;
        border-radius: 2px;
        opacity: 0.9;
        margin-top: -11px;
        height: 28px;
        background: #292677;
        border: 1px solid #292677;
      }
      .actual_on_updated_planned {
        position: absolute;
        z-index: 2;
        border-radius: 2px;
        opacity: 0.9;
        margin-top: -11px;
        height: 28px;
        background: #f9e6a3;
        border: 1px solid #f9e6a3;
      }

      .actual_on_updated_planned {
        position: absolute;
        z-index: 2;
        border-radius: 2px;
        opacity: 0.9;
        margin-top: -11px;
        height: 28px;
        background: #f9e6a3;
        border: 1px solid #f9e6a3;
      }

      .rescheduled {
        position: absolute;
        z-index: 1;
        border-radius: 2px;
        margin-top: -11px;
        height: 28px;
        background-color: red !important;
        border: 1px solid red;
      }

      .invalid-dates {
        background: #eae98ad9 !important;
      }

      /* move task lines upper */

      .gantt_grid_scale .gantt_grid_head_cell {
        color: #000 !important;
        font-weight: bold;
        background: #efefef;
        border-top: 1px solid #fff !important;
        border-left: 1px solid #fff !important;
        border-right: 1px solid #ececec !important;
        border-bottom: 1px solid #cecccc !important;
      }

      .gantt_task .gantt_task_scale .gantt_scale_cell {
        color: #000 !important;
        font-weight: bold;
        background: #efefef;
        border-top: 1px solid #fff !important;
        border-left: 1px solid #fff !important;
        border-right: 1px solid #cecccc !important;
        border-bottom: 1px solid #cecccc !important;
      }

      .gantt_side_content {
        margin-bottom: 7px;
      }

      .gantt_task_link .gantt_link_arrow {
        margin-top: -10px
      }

      .gantt_side_content.gantt_right {
        bottom: 0;
      }

      .gantt_link_point {
        z-index: 4;
      }

      .red {
        background-color: #e25757 !important;
      }

      .data_container {
        display: flex;
        flex-wrap: wrap;
      }

      .header-label {
        margin: 10px;
        color: #00ba8b;
        font-weight: bold;
      }

      .legend {
        width: 18px;
        height: 18px;
        margin: 10px;
      }

      .legend-title {
        margin-top: 10px;
        font-size: 90%;
      }

      .data_container.widget-header {
        background-color: #f9e6a3;
      }

      .btn-position {
        z-index: 10;
        position: relative;
        margin: 2px;
        height: 27px;
        top: -1px;
      }

      .gantt_cal_qi_title,
      .gantt_cal_qi_controls {
        display: none;
      }

      .date_border {
        display: block;
        border-bottom: 1px solid #cecece;
        font-weight: bold;
        font-size: 14px;
      }

      img {
        width: 26px;
        height: 22px;
      }

      .gantt_marker {
        width: 17px;
      }

      span.glyphicon.task_indication {
        color: #efefef;
        padding: 2px !important;
        margin-bottom: 2px;
        background-color: #4777bd;
        font-size: 14px;
        border: 1px solid transparent;
        border-radius: 4px;
      }

      .select2 {
        width: 135px !important;
      }

      .active {
        background-color: #558adc !important;
      }

      .select2-selection__arrow {
        display: none !important;
      }

      .loading-indicator {
        position: absolute;
        display: inline-block;
        z-index: 1000;
        background-color: white;
        width: 300px;
        text-align: center;
        line-height: 50px;
        margin: auto 50%;
        left: -150px;
        top: 100px;
        border-radius: 10px;
        font-size: large;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
      }

      .anchor_link {
        margin: 10px;
        font-size: 20px;
      }

      td,
      th {
        text-align: center;
        padding: 5px !important;
      }

      .rupee-value:before {
        content: '₹';
        display: inline-block;
        margin-right: 2px;
      }

      .glyphicon.glyphicon-eye-open.task_indication {
        margin-right: 287px;
      }

      .btn-eye-open {
        margin-right: 260px !important;
        right: 300px !important;
      }
    </style>
  </head>

  <div class="loading-indicator">
    <img src="img/ajax-loader.gif"> Data loading, please wait
  </div>

  <body onresize="modSampleHeight()">
    <div class="data_container widget-header">
      <button style="margin:15px;" type="button" class="btn btn-primary" onclick="hideColumns()"><span class="glyphicon glyphicon-eye-close"></span></button>
      <select name="scale" class="form-control" id="scale" style="max-width:120px;margin:15px;">
    <option value="year" selected>Year</option>
    <option value="month">Month</option>
    <option value="week">Week</option>
    <option value="quarter">Quarterly</option>
    </select>
      <form class="form-inline" style="margin:7px" action="gantchart.php">
        <!-- <button id="datepicker" name="picker_date" style="margin:8px;" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-calendar"></button> -->
        <input id="datepicker" name="picker_date" style="margin:8px; width:96px;" class="form-control">
        <select name="project_id" id="project" class="form-control" required>
        <option value=0>Select Project</option>
        <?php
          for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) { ?>
           <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>"
             <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
              ?> selected="selected" <?php } ?>>
              <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
           </option>
        <?php } ?>
  </select>
        <!-- <button type="submit" class="btn btn-danger">Go</button> -->
        <button id="submit_button" type="submit" class="btn btn-primary">Go</button>
      </form>
      <span class="header-label">Budget : </span><span style="margin-top:10px;" id="budget_amount" class="rupee-value"></span>
      <span class="header-label">Actual : </span><span style="margin-top:10px;" id="actual_amount" class="rupee-value"></span>
      <span class="header-label">Diff: </span><span style="margin-top:10px;" id="difference_amount" class="rupee-value"></span>
      <span style="margin-left:auto;"><a target="_blank" href="./dashboard_overview.php"><span class="glyphicon glyphicon-home anchor_link"></span></a>
      </span>
    </div>
    <div class="data_container">
      <!-- <div class="legend" style="background-color:red;"></div><span class="legend-title">Overdue</span> -->
      <div class="legend" style="background-color:#292677"></div><span class="legend-title">Changed Plan End Dates</span>
      <div class="legend" style="background-color:#3760e2;opacity:0.6;"></div><span class="legend-title">Original Planned Dates</span>
      <div class="legend" style="background-color:#65c16f"></div><span class="legend-title">Progress</span>
      <!-- <div class="legend" style="background-color:#3db9d3"></div><span class="legend-title">Task Progress</span> -->
      <div class="legend" style="background-color:#f9e6a3 !important"></div><span class="legend-title">Progress On Changed Plan End Dates</span>
      <div class="legend" style="background-color:#e25757 !important"></div><span class="legend-title">No Planning Dates</span>
      <div class="legend"></div><span class="legend-title"><span class="glyphicon glyphicon-eye-open"></span> Task Scheduling History</span>
      <div class="legend"></div><span class="legend-title"><span><img src="./img/rework.png"></span>Rework</span>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Show/Hide Columns</h4>
          </div>
          <div class="modal-body">
            <form id="hide_columns">
              <div class="row">
                <div class="form-group col-md-4">
                  <div class="radio">
                    <label><input type="checkbox" id="text" value="text">&nbsp;Project</label>
                    <label><input type="checkbox" id="planned_start_date" value="planned_start_date" checked>&nbsp;Planned Start</label>
                    <label><input type="checkbox" id="planned_end_date" name="planned_end_date" value="planned_end_date" checked>&nbsp;Planned End </label>
                  </div>
                </div>

                <div class="form-group col-md-4">
                  <div class="radio">
                    <label><input type="checkbox" id="duration" value="duration" checked>&nbsp;Duration</label>
                    <label><input type="checkbox" id="start_date" value="start_date" checked>&nbsp;Actual Start</label>
                    <label><input type="checkbox" id="end_date" name="planned_end_date" value="end_date" checked>&nbsp;Actual End</label>
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <div class="radio">
                    <label><input type="checkbox" id="actual_duration" value="actual_duration" checked>&nbsp;Actual Duration</label>
                    <label><input type="checkbox" id="budget_amount" value="budget_amount" checked>&nbsp;Budget Amount </label>
                    <label><input type="checkbox" id="completed_percent" value="completed_percent">&nbsp;Completed % </label>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="hideColumnsHandler()">Apply</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="exmplModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="width:80%; padding-left: 25%;">
      <div class="modal-dialog" role="document" style="width:100%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Planned Dates History</h4>
          </div>
          <div class="modal-body">
            <table id="modal-table" class="table table-bordered" style="table-layout: auto; width:100%">
              <thead>
                <tr id=row>
                  <th style="width:3%">Process</th>
                  <th style="width:20%">Task</th>
                  <th style="width:3%">Road</th>
                  <th style="width:3%">Planned Start</th>
                  <th style="width:3%">Planned End</th>
                  <th style="width:3%">Changed By</th>
                </tr>
              </thead>
              <tbody id="body">
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Rework Expenditure</h4>
          </div>
          <div class="modal-body">
            <table id="modal-table1" class="table table-striped table-bordered display nowrap">
              <thead>
                <tr id=row1>
                  <th>Process</th>
                  <th>Task</th>
                  <th id="road">Road</th>
                  <th>Rework Measurement</th>
                  <th>Rework Expenditure</th>
                </tr>
              </thead>
              <tbody id="body1">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="wish_dates_history" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Wish Date History</h4>
          </div>
          <div class="modal-body">
            <table id="modal-table1" class="table table-striped table-bordered display nowrap">
              <thead>
                <tr>
                  <th>Wish Date</th>
                  <th>Changed By</th>
                </tr>
              </thead>
              <tbody id="wish_dates">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div id="modalContent" style="color:red;font-weight: bold;font-size: large;">
              <span>Rework Measurement: <span id="road_measurement"></span></span>
              <span>Rework Expenditure: <span class="rupee-value" id="road_rework_budget"></span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Alert Message -->
    <div class="modal fade bs-example-modal-sm" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div id="modalContent" style="display:none;color:red;font-weight: bold;font-size: large;"></div>
          </div>
        </div>
      </div>
    </div>
    <script>
      function modSampleHeight() {
        var headHeight = 100;
        var sch = document.getElementById("gantt_here");
        sch.style.height = (parseInt(document.body.offsetHeight) - headHeight) + "px";
        var contbox = document.getElementById("contbox");

        gantt.setSizes();
      }
    </script>
    <div id="gantt_here" style='width:100%; height:100%;'></div>
    <script data-jsfiddle="common" src="./js/handsontable-master/demo/js/moment/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
    <script type="text/javascript">
      gantt.config.task_height = 28;
      gantt.config.row_width = 10;
      gantt.config.row_height = 40;
      gantt.locale.labels.baseline_enable_button = 'Set';
      gantt.locale.labels.baseline_disable_button = 'Remove';
      gantt.config.auto_scheduling_strict = false;
      gantt.config.date_grid = "%d-%m-%Y"
      gantt.config.show_unscheduled = true;
      gantt.config.min_column_width = 120;
      var date_history = [];
      var rework_data = [];
      var marker;
      $(document).ready(function() {
        $('#project').select2();
      });
      $('.loading-indicator').hide();
      var project_id = <?php echo $project_id ?>;
      getData();
      $('#datepicker').on('changeDate', function() {
        // getData();
        $("#datepicker").datepicker('hide');
      })
      scale.onchange = function() {
        date_scale($('#scale').val());
      }

      $("#datepicker").datepicker();
      gantt.templates.tooltip_text = function(start, end, task) {
        return getToolTipData(task, start, end);
      };

      //defines the style of task bars
      gantt.templates.grid_row_class = gantt.templates.task_row_class = function(start, end, task) {
        if (getDates(task.planned_start_date) == true) {
          task.planned_start_date = new Date();
          task.planned_end_date = new Date();
          task["plan_unscheduled"] = true;
        }
        if (task.unscheduled) {
          return "red";
        }
        if (task.plan_unscheduled) {
          return "invalid-dates";
        }
      };

      gantt.templates.quick_info_content = function(start, end, task) {
        return getToolTipData(task, start, end);
      };

      function hideColumns() {
        $('#exampleModal').modal('show');
      }

      function hideColumnsHandler() {
        var hiddenColumns = [];
        $("input:checkbox").each(function(i, ele) {
          hiddenColumns.push(ele);
        });
        toggleView(hiddenColumns);
      }

      function getDates(task_date) {
        var date = new Date(2099, 10, 30, 12);
        var date1 = new Date(1970, 00, 01, 01);
        var date2 = new Date(1901, 10, 30, 12);
        var date3 = new Date(1970, 00, 01, 00);
        if (_.isEqual(task_date, date) ||
          _.isEqual(task_date, date1) || _.isEqual(task_date, date2) || _.isEqual(task_date, date3)) {
          return true;
        }
        return false;
      }

      gantt.config.columns = [{
          name: "text",
          label: "Project",
          width: 260,
          tree: true,
          resize: true
        },
        {
          name: "planned_start_date",
          label: "Planned Start",
          align: "center",
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "planned_end_date",
          label: "Planned End",
          align: "center",
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "duration",
          label: "Duration",
          align: "center",
          template: getPlanDuration,
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "budget_amount",
          label: "Budget Amount",
          align: "center",
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "completed_percent",
          label: "Comp %",
          align: "center",
          template: getCompleted,
          width: 89,
          resize: true
        },
        {
          name: "start_date",
          label: "Actual Start",
          align: "center",
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "end_date",
          label: "Actual End",
          align: "center",
          width: 89,
          resize: true,
          hide: true
        },
        {
          name: "actual_duration",
          label: "Actual Duration",
          align: "center",
          template: getActualDuration,
          width: 89,
          resize: true,
          hide: true
        }
      ];

      function getActualDuration(obj) {
        var date_diff;
        if (getDates(obj.start_date) == true) {
          obj.actual_duration = 1;
        } else {
          obj.actual_duration = findDateDiff(obj.start_date, obj.end_date);
        }
        return parseInt(obj.actual_duration);
      }

      function getPlanDuration(obj) {
        var date_diff;
        if (getDates(obj.planned_end_date) == true) {
          obj.duration = findDateDiff(obj.planned_start_date, new Date());
        } else {
          obj.duration = findDateDiff(obj.planned_start_date, obj.planned_end_date);
        }
        return parseInt(obj.duration);
      }

      function getCompleted(obj) {
        return Math.round(obj.progress * 100) + '%';
      }

      gantt.addTaskLayer(function draw_updated_plan(task) {
        if (task.$level > 2) {
          if (task.road_dates.length) {
            for (road_dates = 0; road_dates < task.road_dates.length; road_dates++) {
              road_end_date = task.road_dates[road_dates].end_date;
            }
            var sizes = gantt.getTaskPosition(task, task.road_dates[0].old_end_date, road_end_date);
            var el = document.createElement('div');
            el.className = 'updated_planned';
            el.style.left = sizes.left + 'px';
            el.style.width = sizes.width + 'px';
            el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
            $(el).attr("task_id", task.id);
            return el;
          }
        }
        if (task.$level == 0) {
          if (task.project_dates.length) {
            var sizes = gantt.getTaskPosition(task, dateFormat(task.project_dates[0].old_end_date), dateFormat(task.project_dates[0].end_date));
            var el = document.createElement('div');
            el.className = 'updated_planned';
            el.style.left = sizes.left + 'px';
            el.style.width = sizes.width + 'px';
            el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
            $(el).attr("task_id", task.id);
            return el;
          }
        }
        if (task.$level == 1) {
          if (task.process_dates.length) {
            if ((dateFormat(task.process_dates[0].old_end_date) != "0000-00-00") && (dateFormat(task.process_dates[0].end_date) != "0000-00-00")) {
              var sizes = gantt.getTaskPosition(task, dateFormat(task.process_dates[0].old_end_date), dateFormat(task.process_dates[0].end_date));
              var el = document.createElement('div');
              el.className = 'updated_planned';
              el.style.left = sizes.left + 'px';
              el.style.width = sizes.width + 'px';
              el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
              $(el).attr("task_id", task.id);
              return el;
            }
            return false;
          }
        }
        if (task.$level == 2) {
          if (task.task_dates.length) {
            if ((dateFormat(task.task_dates[0].old_end_date) != "0000-00-00") && (dateFormat(task.task_dates[0].end_date) != "0000-00-00")) {
              var sizes = gantt.getTaskPosition(task, dateFormat(task.task_dates[0].old_end_date), dateFormat(task.task_dates[0].end_date));
              var el = document.createElement('div');
              el.className = 'updated_planned';
              el.style.left = sizes.left + 'px';
              el.style.width = sizes.width + 'px';
              el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
              $(el).attr("task_id", task.id);
              return el;
            }
          }
        }
      });
      gantt.addTaskLayer(function draw_actual_on_updated_plan(task) {
        if (task.$level > 2) {
          if (task.road_dates.length) {
            if (task.end_date > task.road_dates[0].old_end_date) {
              var sizes = gantt.getTaskPosition(task, task.road_dates[0].old_end_date, task.end_date);
              var el = document.createElement('div');
              el.className = 'actual_on_updated_planned';
              el.style.left = sizes.left + 'px';
              el.style.width = sizes.width + 'px';
              el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
              $(el).attr("task_id", task.id);
              return el;
            }
          }
        }
        if (task.$level == 0) {
          if (task.project_dates.length) {
            if (dateFormat(task.project_dates[0].old_end_date) != "0000-00-00") {
              if (task.end_date > dateFormat(task.project_dates[0].old_end_date)) {
                var sizes = gantt.getTaskPosition(task, dateFormat(task.project_dates[0].old_end_date), task.end_date);
                var el = document.createElement('div');
                el.className = 'actual_on_updated_planned';
                el.style.left = sizes.left + 'px';
                el.style.width = sizes.width + 'px';
                el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
                $(el).attr("task_id", task.id);
                return el;
              }
            }
          }
        }
        if (task.$level == 1) {
          if (task.process_dates.length) {
            if (dateFormat(task.process_dates[0].old_end_date) != "0000-00-00") {
              if (task.end_date > dateFormat(task.process_dates[0].old_end_date)) {
                var sizes = gantt.getTaskPosition(task, dateFormat(task.process_dates[0].old_end_date), task.end_date);
                var el = document.createElement('div');
                el.className = 'actual_on_updated_planned';
                el.style.left = sizes.left + 'px';
                el.style.width = sizes.width + 'px';
                el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
                $(el).attr("task_id", task.id);
                return el;
              }
            }
          }
        }
        if (task.$level == 2) {
          if (task.task_dates.length) {
            if (dateFormat(task.task_dates[0].old_end_date) != "0000-00-00") {
              if (task.end_date > dateFormat(task.task_dates[0].old_end_date)) {
                var sizes = gantt.getTaskPosition(task, dateFormat(task.task_dates[0].old_end_date), task.end_date);
                var el = document.createElement('div');
                el.className = 'actual_on_updated_planned';
                el.style.left = sizes.left + 'px';
                el.style.width = sizes.width + 'px';
                el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
                $(el).attr("task_id", task.id);
                return el;
              }
            }
          }
        }
      });

      // gantt.addTaskLayer(function draw_deadline(task) {
      //   var sizes;
      //   if (task.planned_end_date && task.$level > 2) {
      //     if (task.road_dates.length) {
      //           for (road_dates = 0; road_dates < task.road_dates.length; road_dates++) {
      //                 road_end_date = task.road_dates[road_dates].end_date ;
      //           }
      //           if (task.planned_end_date.getTime() < road_end_date.getTime()) {
      //             sizes = gantt.getTaskPosition(task, road_end_date, road_end_date);
      //           }
      //         }
      //     }
      //     if (task.planned_end_date && task.$level == 0) {
      //       if (task.project_dates.length) {
      //           if (task.planned_end_date.getTime() < dateFormat(task.project_dates[0].end_date)) {
      //             sizes = gantt.getTaskPosition(task, dateFormat(task.project_dates[0].end_date), dateFormat(task.project_dates[0].end_date));
      //             console.log(sizes);
      //           }
      //         }
      //       }
      //       if (task.planned_end_date && task.$level == 1) {
      //         if (task.process_dates.length) {
      //             if (task.planned_end_date.getTime() < dateFormat(task.process_dates[0].end_date)) {
      //               sizes = gantt.getTaskPosition(task, dateFormat(task.process_dates[0].end_date), dateFormat(task.process_dates[0].end_date));
      //             }
      //           }
      //         }
      //         if (task.planned_end_date && task.$level == 2) {
      //           if (task.task_dates.length) {
      //               if (task.planned_end_date.getTime() < dateFormat(task.task_dates[0].end_date)) {
      //                 sizes = gantt.getTaskPosition(task, dateFormat(task.task_dates[0].end_date), dateFormat(task.task_dates[0].end_date));
      //               }
      //             }
      //           }
      //
      //   var el = document.createElement('div');
      //   if (sizes !== undefined) {
      //     el.className = 'rescheduled';
      //     el.style.left = sizes.left + 'px';
      //     el.style.width = 30 + 'px';
      //     el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
      //     $(el).attr("task_id", task.id);
      //     return el;
      //   }
      //   return false;
      // });

      gantt.attachEvent("onBeforeTaskDisplay", function(id, task) {
        if (task.project_id == $("#project").val() || task.id == $("#project").val()) {
          return true;
        }
        return false;
      })

      //Grid date formatting Tue Dec 11 1917 00:00:00 GMT+0530 (IST)
      function dateFormat(date) {
        if (date == "0000-00-00") {
          formatted_date = "0000-00-00";
        } else {
          formatted_date = gantt.date.parseDate(new Date(date), "%d-%m-%Y");
        }
        return formatted_date;
      }

      function findDateDiff(start_date, end_date) {
        var iWeeks, iDateDiff, iAdjust = 0;
        // if (start_date < end_date) return -1; // error code if dates transposed
        var iWeekday1 = start_date.getDay(); // day of week
        var iWeekday2 = end_date.getDay();
        iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
        iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
        if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1; // adjustment if both days on weekend
        iWeekday1 = (iWeekday1 > 6) ? 6 : iWeekday1; // only count weekdays
        iWeekday2 = (iWeekday2 > 6) ? 6 : iWeekday2;

        // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
        iWeeks = Math.floor((end_date.getTime() - start_date.getTime()) / 604800000)

        if (iWeekday1 <= iWeekday2) {
          iDateDiff = (iWeeks * 6) + (iWeekday2 - iWeekday1)
        } else {
          iDateDiff = ((iWeeks + 1) * 6) - (iWeekday1 - iWeekday2)
        }
        iDateDiff -= iAdjust // take into account both days on weekend
        iDateDiff = iDateDiff + 1
        return iDateDiff;
      }

      function toggleView(hiddenColumns) {
        $('#exampleModal').modal('hide');
        var width = 0;
        $.each(hiddenColumns, function(i, ele) {
          if (ele.checked == true) {
            gantt.getGridColumn(ele.value).hide = true;
          } else {
            gantt.getGridColumn(ele.value).hide = false;
            width = width + gantt.getGridColumn(ele.value).width;
          }
        });
        gantt.config.grid_width = width;
        gantt.render();
      };


      gantt.locale.labels.section_baseline = "Planned";

      function date_scale(format) {
        if (format == 'year') {
          gantt.config.scale_unit = "year";
          gantt.config.date_scale = "%M, %Y";
        } else if (format == 'month') {
          gantt.config.scale_unit = "month";
          gantt.config.date_scale = "%M, %Y";
        } else if (format == 'quarter') {
          gantt.config.scale_unit = "quarter";
          gantt.config.date_scale = "%M, %Y";
        } else {
          gantt.config.scale_unit = "week";
          gantt.config.date_scale = "%M %Y, #W %w";
        }
        gantt.render();
      }

      gantt.addTaskLayer(function draw_planned(task) {
        if (task.planned_start_date && task.planned_end_date) {
          var sizes = gantt.getTaskPosition(task, task.planned_start_date, task.planned_end_date);
          el = document.createElement('div');
          el.className = 'baseline';
          el.style.left = sizes.left + 'px';
          el.style.width = sizes.width + 'px';
          el.style.top = sizes.top + gantt.config.task_height - 11 + 'px';
          $(el).attr("task_id", task.id);
          if (task.$level == 3) {
            if (task.road_dates.length > 0) {
              var baseline_road_button = document.createElement('button');
              baseline_road_button.innerHTML = '<span class="glyphicon glyphicon-eye-open"></span>';
              el.appendChild(baseline_road_button);
              baseline_road_button.className = 'btn btn-primary btn-eye-open';
              baseline_road_button.style.right = sizes.width + 10 + 'px';
              baseline_road_button.style.position = 'absolute';
              $(baseline_road_button).on("click", function() {
                showHistory(task.id);
              });
            }
          }
          if (task.$level >= 1 && task.$level !== 3) {
            _.each(Object.keys(date_history), (data) => {
              if (gantt.getTask(data).parent == task.id) {
                var baseline_indication = document.createElement('span');
                baseline_indication.className = 'glyphicon glyphicon-eye-open task_indication';
                el.appendChild(baseline_indication);
                baseline_indication.style.right = sizes.width + 10 + 'px';
                baseline_indication.style.position = 'absolute';
              }
              if (task.id == data) {
                var baseline_rightside = document.createElement('button');
                baseline_rightside.innerHTML = '<span class="glyphicon glyphicon-eye-open"></span>';
                el.appendChild(baseline_rightside);
                baseline_rightside.className = 'btn btn-primary btn-eye-open';
                baseline_rightside.style.right = sizes.width + 10 + 'px';
                baseline_rightside.style.position = 'absolute';
                $(baseline_rightside).on("click", function() {
                  showHistory(task.id);
                });
              }
            })
          }
        }
        return el;
      });

      function getModalContent(ele, data, task, val) {
        task = task || data;
        val = val || data;
        $(ele).append($('<td></td>').text(getCellValue(data, "process")));
        $(ele).append($('<td></td>').text(getCellValue(data, "task")));
        $(ele).append($('<td></td>').text(task.text));
        $(ele).append($('<td></td>').text(moment(val.start_date).format("DD/MM/YYYY")));
        $(ele).append($('<td></td>').text(moment(val.end_date).format("DD/MM/YYYY")));
        $(ele).append($('<td></td>').text(val.changed_by));
        return ele;
      }

      function showHistory(task_id) {
        var data = gantt.getTask(task_id);
        if (data.$level == 3) {
          $.each(data.road_dates, function(i, val) {
            tr = document.createElement("tr");
            tr = getModalContent(tr, data, "", val);
            $("#body").append(tr);
          })
        } else {
          var task_data = date_history[task_id];
          _.each(task_data, (task) => {
            $.each(task.road_dates, function(i, val) {
              tr = document.createElement("tr");
              tr = getModalContent(tr, data, task, val);
              $("#body").append(tr);
            })
          })
        }
        $("#exmplModal").modal('show');

        $('#exmplModal').on('hidden.bs.modal', function(e) {
          $("#body").empty();
        })
      }

      function getCellValue(value, text) {
        if (value.$level == 2) {
          if (text == "process") {
            $("#road").hide();
            return gantt.getTask(value.parent).text;
          } else {
            return value.text
          }
        } else {
          if (value.$level == 3) {
            $("#road").show();
            if (text == "task") {
              return gantt.getTask(value.parent).text;
            } else {
              return gantt.getTask(gantt.getParent(value.parent)).text;
            }
          }
        }
      }

      function reworkData(task_id) {
        var task_object = gantt.getTask(task_id);
        if (task_object.$level === 3) {
          $("#road_measurement").text(task_object.rw_msmrt);
          $("#road_rework_budget").text(getRupeeVal(task_object.rw_budget_amount));
          $("#myModal2").modal('show');
        } else {
          var task_data = rework_data[task_id];
          var tr;
          $.each(task_data, function(i, val) {
            tr = document.createElement("tr");
            $(tr).append($('<td></td>').text(getCellValue(val, "process")));
            $(tr).append($('<td></td>').text(getCellValue(val, "task")));
            if (val.$level != 2) {
              $(tr).append($('<td></td>').text(val.text));
            }
            $(tr).append($('<td></td>').text(val.rw_msmrt));
            $(tr).append($('<td class="rupee-value"></td>').text(getRupeeVal(val.rw_budget_amount)));
            $("#body1").append(tr);
          })
          $("#myModal1").modal('show');

          $('#myModal1').on('hidden.bs.modal', function(e) {
            $("#body1").empty();
          })
        }
      }
      gantt.templates.rightside_text = function(start,end, task) {
        var ele = "";
        var overdue;
        if (task.rw_budget_amount > 0 && task.type !== 'project') {
          ele = ele + `<button class="btn btn-danger btn-icon"  onclick="reworkData('${task.id}')"> <span><img src="./img/rework.png"></span></button>`;
        }

        if (task.planned_end_date && task.$level > 2) {
          if (task.road_dates.length) {
            for (road_dates = 0; road_dates < task.road_dates.length; road_dates++) {
              road_end_date = task.road_dates[road_dates].end_date;
            }
            if (task.planned_end_date.getTime() < road_end_date.getTime()) {
              if (task.progress == 1) {
                overdue = Math.ceil((task.end_date.getTime() - road_end_date.getTime()) / (24 * 60 * 60 * 1000));

              } else {
                overdue = Math.ceil(Math.abs((road_end_date.getTime() - task.planned_end_date.getTime()) / (24 * 60 * 60 * 1000)));
              }
              // ele = ele + "<b>Overdue: " + (overdue - task.pause_days) + " days</b>"+ "<b>Pause Days: " + (task.pause_days) + " days</b>";
              ele = ele + "<b>Overdue: " + overdue + " days</b>";
            }
          }
        }
        if (task.planned_end_date && task.$level == 0) {
          if (task.project_dates.length) {
            if (task.planned_end_date.getTime() < dateFormat(task.project_dates[0].end_date)) {
              if (task.progress == 1) {
                overdue = Math.ceil((task.end_date.getTime() - (dateFormat(task.project_dates[0].end_date))) / (24 * 60 * 60 * 1000));

              } else {
                overdue = Math.ceil(Math.abs((dateFormat(task.project_dates[0].end_date) - task.planned_end_date.getTime()) / (24 * 60 * 60 * 1000)));

              }
              // ele = ele + "<b>Overdue: " + (overdue - task.pause_days) + " days</b>"+ "<b>Pause Days: " + (task.pause_days) + " days</b>";
              ele = ele + "<b>Overdue: " + overdue + " days</b>";
            }
          }
        }
        if (task.planned_end_date && task.$level == 1) {
          if (task.process_dates.length) {
            if (task.planned_end_date.getTime() < dateFormat(task.process_dates[0].end_date)) {
              if (task.progress == 1) {
                overdue = Math.ceil((task.end_date.getTime() - (dateFormat(task.process_dates[0].end_date))) / (24 * 60 * 60 * 1000));
              } else {
                overdue = Math.ceil(Math.abs((dateFormat(task.process_dates[0].end_date) - task.planned_end_date.getTime()) / (24 * 60 * 60 * 1000)));
              }
              // ele = ele + "<b>Overdue: " + (overdue - task.pause_days) + " days</b>"+ "<b>Pause Days: " + (task.pause_days) + " days</b>";
              ele = ele + "<b>Overdue: " + overdue + " days</b>";
            }
          }
        }
        if (task.planned_end_date && task.$level == 2) {
          if (task.task_dates.length) {
            if (task.planned_end_date.getTime() < dateFormat(task.task_dates[0].end_date)) {
              if (task.progress == 1) {
                overdue = Math.ceil((task.end_date.getTime() - (dateFormat(task.task_dates[0].end_date))) / (24 * 60 * 60 * 1000));
              } else {
                overdue = Math.ceil(Math.abs((dateFormat(task.task_dates[0].end_date) - task.planned_end_date.getTime()) / (24 * 60 * 60 * 1000)));
              }
              // ele = ele + "<b>Overdue: " + (overdue - task.pause_days) + " days</b>"+ "<br>Pause Days: " + (task.pause_days) + " days</b>";

              ele = ele + "<b>Overdue: " + overdue + " days</b>";
            }
          }
        }
        return ele;
      };

      gantt.templates.task_text = function(start, end, task) {
        return "";
      };

      function getToolTipData(task, start, end) {
        var progress = Math.round(task.progress * 100);
        if (task.unscheduled == true && task.process_master_id !== "33" && task.process_master_id !== "32") {
          return "<b>Task not started yet</b><br>" +
            "<b>Task:</b>" + task.text + "<br/>" +
            "<b>Plan start:</b>" + gantt.templates.date_grid(task.planned_start_date) +
            "<br/><b>Plan end:</b>" + gantt.templates.date_grid(task.planned_end_date);
        } else {
          var status;
          if (task.work_to_be_completed == 0) {
            status = "Not Planned Yet!!";
          }
          if (progress > task.work_to_be_completed && (progress - task.work_to_be_completed > 10)) {
            status = "Excellent";
          } else if (progress > task.work_to_be_completed && (progress - task.work_to_be_completed > 5)) {
            status = "Very Good";
          } else if (progress > task.work_to_be_completed) {
            status = "Good";
          } else {
            status = "Poor";
          }

          var date_data = {};

          if (task.$level < 3) {
            if (task.$level == 0 && task.project_dates.length) {
              date_data[task.$level] = task.project_dates[0].end_date;
            } else if (task.$level == 1 && task.process_dates.length) {
              date_data[task.$level] = task.process_dates[0].end_date;
            }
            if (task.$level == 2 && task.task_dates.length) {
              date_data[task.$level] = task.task_dates[0].end_date;
            }
            changed_end_date = date_data[task.$level] || '0000-00-00';
            return "<div class='gantt_cal_qi_tcontent'>" + task.text + "</div>" +
              "<div class='date_border'>" + moment(task.start_date).format('LL') +
              " - " + moment(task.end_date).format('LL') +
              "</div></br><b>Plan Start date:</b> " +
              gantt.templates.date_grid(task.planned_start_date) +
              "<br/><b>Plan End date:</b>" + gantt.templates.date_grid(task.planned_end_date) +
              "<br/><b>Plan Duration:</b>" + getPlanDuration(task) +
              "<br/><b>Changed Plan End Date:</b>" + changed_end_date +
              "<br/><b>Completion %:</b>" + Math.round(task.progress * 100) +
              "<br/><b>Actual Duration:</b>" + getActualDuration(task) +
              "<br/><b>Planned Budget:</b><span class='rupee-value'>" + getRupeeVal(task.planned_budget_amount) +
              "<br/><b>Spent:</b><span class='rupee-value'>" + getRupeeVal(task.budget_amount) + "</span>" +
              "<br/><b>Suppose to complete %:</b>" + Math.round(task.work_to_be_completed) +
              "<br/><b>Note:</b>" + status;
          }
          var road_end_date;
          if (task.road_dates.length) {
            for (road_dates = 0; road_dates < task.road_dates.length; road_dates++) {
              road_end_date = moment(task.road_dates[road_dates].end_date).format('DD-MM-YYYY');
            }
          }
          return "<div class='gantt_cal_qi_tcontent'>" + task.text + "</div>" +
            "<div class='date_border'>" + moment(task.start_date).format('LL') +
            " - " + moment(task.end_date).format('LL') +
            "</div></br><b>Plan Start date:</b> " +
            gantt.templates.date_grid(task.planned_start_date) +
            "<br/><b>Plan End date:</b>" + gantt.templates.date_grid(task.planned_end_date) +
            "<br/><b>Plan Duration:</b>" + getPlanDuration(task) +
            "<br/><b>Changed Plan End dates:</b>" + road_end_date +
            "<br/><b>Actual Msrmt:</b>" + task.reg_msmrt +
            "<br/><b>Plan Msrmt:</b>" + task.planned_msmrt +
            "<br/><b>Completion %:</b>" + Math.round(task.progress * 100) +
            "<br/><b>Actual Duration:</b>" + getActualDuration(task) +
            "<br/><b>Planned Budget:</b>" + getRupeeVal(task.planned_budget_amount) +
            "<br/><b>Spent:</b>" + getRupeeVal(task.budget_amount) +
            "<br/><b>Suppose to complete %:</b>" + Math.round(task.work_to_be_completed) +
            "<br/><b>Note:</b>" + status;
        }
      }

      gantt.templates.progress_text = function(start, end, task) {
        return "<b>" + Math.round(task.progress * 100) + "% </b>";
      };



      gantt.config.grid_width = 590;
      gantt.config.scale_height = 40;
      gantt.templates.date_grid = function(date, task) {
        if (getDates(date) == true) {
          return "";
        }
        return gantt.templates.grid_date_format(date);
      };

      gantt.attachEvent("onTaskLoading", function(task) {
        task.planned_start_date = gantt.date.parseDate(task.planned_start_date, "%d-%m-%Y");
        task.planned_end_date = gantt.date.parseDate(task.planned_end_date, "%d-%m-%Y");
        return true;
      });

      gantt.init("gantt_here");
      modSampleHeight();

      gantt.config.scale_unit = "year";
      gantt.config.step = 1;
      gantt.config.date_scale = "%M, %Y";
      gantt.config.drag_progress = false;
      gantt.config.drag_resize = false;
      gantt.config.drag_move = false;
      gantt.config.show_task_cells = false;
      gantt.config.static_background = true;
      gantt.config.branch_loading = true;


      function getRupeeVal(number) {
        number = Math.round(number) || 0;
        return number.toLocaleString();
      }

      function showMessage(alert) {
        $('#myModal2').modal('show');
        $('#modalContent').show().html(alert);
      }

      function getData() {
        rework_data = [];
        date_history = [];
        if (project_id == 0 ) {
          alert('Choose the project');
          $("#datepicker").datepicker('setDate', new Date());
          $("#datepicker").datepicker('hide');
          return false;
        }
        gantt.deleteMarker(marker);
        <?php $picker_date = date("d-m-Y",strtotime($picker_date)); ?>
        var date = '<?php echo date("m-d-Y",strtotime($picker_date)); ?>';
        if (date!== '01-01-1970'||date!== "") {
          $("#datepicker").datepicker('setDate',new Date(date));
        }
        $("#datepicker").datepicker('hide');
        <?php
        $directoryName = "./gantchart_json/".$picker_date."/project_".$project_id.".json";
        $status="success";
        //Check if the directory already exists.
        if(!file_exists($directoryName)){
          $status="failure";
          $project_management_master_search_data = array('project_id' => $project_id,"user_id"=>$user);
          $project_management_master_list = db_get_project_management_master_list($project_management_master_search_data);
          if ($project_management_master_list['status'] == DB_RECORD_ALREADY_EXISTS) {
              $project_management_master_list_data = $project_management_master_list['data'];
              $directoryName = "./gantchart_json/".date("d-m-Y",strtotime($project_management_master_list_data[0]["project_master_cronjob_completed_date"]))."/project_".$project_management_master_list_data[0]["project_management_master_id"].".json";
          }
        }
          ?>
        var directory_path = '<?php echo $directoryName ?>';
        var status = '<?php echo $status ?>';
        var completed_date = '<?php echo date("m-d-Y",strtotime($project_management_master_list_data[0]["project_master_cronjob_completed_date"])) ?>'
        if (status == "failure") {
          var message = `Could not fetch data for the date ${moment(date).format("DD/MM/YYYY")} Showing results for the date
            ${moment(completed_date).format("DD/MM/YYYY")}`;
          completed_date = new Date(completed_date);
          $("#datepicker").datepicker('setDate', completed_date);
          showMessage(message);
        }
        $.ajax({
          type: "GET",
          contentType: "application/json;",
          url: directory_path + "?" + moment().format('x'),
          success: function(result) {
            $('.loading-indicator').hide();
            gantt.parse(temp = {
              "data": JSON.parse(result)
            }, "json");
            var min_date, max_date;
            gantt.eachTask(function(task) {
              var task_data = gantt.getTask($("#project").val());
              $("#budget_amount").text(getRupeeVal(task_data.planned_budget_amount));
              $("#actual_amount").text(getRupeeVal(task_data.budget_amount));
              $("#difference_amount").text(getRupeeVal(task_data.planned_budget_amount - task_data.budget_amount));
              if (task.type !== 'project') {
                var parent = gantt.getParent(task);
                var parent_data = gantt.getTask(parent);
              }
              if (task.rw_budget_amount > 0) {
                if (!rework_data[parent]) {
                  rework_data[parent] = []
                  rework_data[parent].push(task);
                } else {
                  rework_data[parent].push(task);
                }
              }
              if (task.$level === 3) {
                if (task.road_dates.length) {
                  $.each(task.road_dates, function(i, val) {
                    val.start_date = gantt.date.parseDate(val.start_date, "%d-%m-%Y");
                    val.end_date = gantt.date.parseDate(val.end_date, "%d-%m-%Y");
                    val.old_start_date = gantt.date.parseDate(val.old_start_date, "%d-%m-%Y");
                    val.old_end_date = gantt.date.parseDate(val.old_end_date, "%d-%m-%Y");
                  })
                  if (!date_history[parent]) {
                    date_history[parent] = []
                    date_history[parent].push(task);
                  } else {
                    date_history[parent].push(task);
                  }
                }
              }
              if (task.type == "project") {
                if (task.wish_date.length > 0) {
                  $.each(task.wish_date, function(i, val) {
                    val.wish_end_date = gantt.date.parseDate(val.wish_end_date, "%d-%m-%Y");
                  })
                  marker = gantt.addMarker({
                    start_date: task.wish_date[task.wish_date.length - 1].wish_end_date,
                    text: "End date"
                  });
                }
                min_date = gantt.config.start_date = gantt.getState().min_date
                max_date = gantt.config.end_date = gantt.getState().max_date
                if (task.planned_start_date && task.planned_start_date < min_date) min_date = task.planned_start_date;

                if (dateFormat(task.project_dates[0].end_date) && dateFormat(task.project_dates[0].end_date) > max_date) max_date = dateFormat(task.project_dates[0].end_date);
                gantt.config.start_date = min_date;
                gantt.config.end_date = max_date;
                gantt.render();
              }
            })
          },
          error: function(result) {
            $('.loading-indicator').hide();
            if (project_id != 0) {
              alert("Error");
            }
          }
        });
      }
      if (project_id !== 0) {
        gantt.attachEvent("onMouseMove", function(id, e) {
          var marker1 = document.getElementsByClassName("gantt_marker")[0];
          if (marker1) {
            var task = gantt.getTask(project_id);
            marker1.onclick = function() {
              $.each(task.wish_date, function(i, val) {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var td1 = document.createElement("td");
                $("#wish_dates").append(tr);
                $(tr).append(td);
                $(tr).append(td1);
                $(td).attr("id", "date" + i).text(moment(val.wish_end_date).format("DD/MM/YYYY"));
                $(td1).attr("id", "changed_by" + i).text(val.created_by);
              })
              $("#wish_dates_history").modal('show');

              $('#wish_dates_history').on('hidden.bs.modal', function(e) {
                $("#wish_dates").empty();
              })
            }
          }
        });
      }
    </script>
  </body>
