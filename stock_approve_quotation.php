<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$quotation_id = $_POST["quotation_id"];
	
	// $indent_item_id = $_POST["indent_item_id"];
	$status       = $_POST["action"];

	//ajax to update value
	//Author: Rakesh
	//Purpose : To update purchse order value after being calculated 
	//Input : quotation_id , quote_status, po_value
	//Output: data: "success" , status : 200
	if(isset($_POST['update_status']) && $_POST['update_status'] == 1){

		$stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id,'status'=>'Approved');
		$quotation_item_list_results =  i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);
		

		if($quotation_item_list_results['status'] == SUCCESS){
			$quotation_compare_update_data = array("quotation_id"=> $_POST["quotation_id"],"po_qty"=>$_POST["po_qty"],"po_value"=>$_POST["value"],'status'=>'Approved');

			$quotation_compare_update_data_result = i_update_quotation_compare($quotation_id,$indent_id,$quotation_compare_update_data);
			if($quotation_compare_update_data_result['status'] == SUCCESS){

				$data = ["data" =>$quotation_compare_update_data_result,'status' =>200];
				header('Content-Type: application/json');
				echo json_encode($data);
				exit();

			}
			$data = ["data" =>"Update Failed",'status' =>450];
			header('Content-Type: application/json');
			echo json_encode($data);
			exit();
			

		}
		$data = ["data" =>"Quotation Item Not Found For Status",'status' =>450,'db'=>$quotation_item_list_results,'search'=>$stock_quotation_compare_search_data];
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
		
	}
	

	$approved_by  = $user;
	$approved_on  = date("Y-m-d H:i:s");
	$stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id);
	$quotation_list_results =  i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);	


	$quotation_list_results_data = $quotation_list_results["data"];
	$item_id  = $quotation_list_results_data[0]["stock_quotation_indent_id"];
	$project = $quotation_list_results_data[0]["stock_quotation_project"];
	$price = $quotation_list_results_data[0]["stock_quotation_amount"];
	//Reject all waiting quotation of same material and project
	$stock_quotation_compare_search_data = array("indent_id"=>$item_id,'status'=>'Approved','project'=>$project);

	


	/*$quotation_item_list_results =  i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	


	if($quotation_item_list_results["status"] == SUCCESS)
	{
		for($count = 0 ; $count < count($quotation_item_list_results["data"]) ; $count++)
		{
			$quotation_compare_update_data = array("approved_by"=>$approved_by,"approved_on"=>$approved_on,"status"=>'Rejected');
			$approve_quotation_result = i_update_quotation_compare($quotation_item_list_results['data'][$count]['stock_quotation_id'],'',$quotation_compare_update_data);
		}
	}
	else
	{

	}*/
	//Update Material Master price
	if($status == "Approved")
	{
		$material_master_update_data = array("material_price"=>$price);
		$materail_master_list = i_delete_material_list($item_id,$material_master_update_data);
	}
	$quotation_compare_update_data = array("approved_by"=>$approved_by,"approved_on"=>$approved_on,"status"=>$status);
	$approve_quotation_result = i_update_quotation_compare($quotation_id,'',$quotation_compare_update_data);
	if($approve_quotation_result["status"] == FAILURE)
	{
		echo $approve_quotation_result["data"];
	}
	else
	{
		if($status == "Approved")
		{
			$quote_status = "Quote Approved";
		}
		else {
			$quote_status = "Quote Rejected";
		}

		//Update each ident item status which are selceted during quotation
		$stock_items_quotation_search_data = array("quote_id"=>$quotation_id);
		$quotation_item_list = db_get_stock_items_quotations($stock_items_quotation_search_data);
		if($quotation_item_list["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$quotation_item_list_data = $quotation_item_list["data"];
			for($indent_item_count =0 ; $indent_item_count < count($quotation_item_list_data) ; $indent_item_count++)
			{
				$indent_item_id = $quotation_item_list_data[$indent_item_count]["quote_indent_item_id"] ;
				$indent_items_update_data = array("quote_status"=>$quote_status);
				$approve_files_result = i_update_indent_items($indent_item_id,'',$indent_items_update_data);
			}
		}
		else
		{
			//
		}
		// Reset the quote process for this item
		$update_quote_reset_data = array('reset_date_time'=>date('Y-m-d H:i:s'),'updated_by'=>$user);
		$quote_reset_uresult = i_update_stock_quote_reset($item_id,$project,$update_quote_reset_data);
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}

?>