<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 28-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Capture the form data
	if(isset($_POST["add_man_power_rate_master_submit"]))
	{
		
		$power_type_id          = $_POST["ddl_power_type_id"];
		$man_power_vendor        = $_POST["ddl_vendor"];
		$cost_per_hours         = $_POST["cost_per_hours"];
		$applicable_date        = $_POST["applicable_date"];
		$remarks 	            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($power_type_id != "") && ($cost_per_hours != "") && ($applicable_date != ""))
		{
			$project_man_power_rate_iresult = i_add_project_man_power_rate_master($power_type_id,$man_power_vendor,$cost_per_hours,$applicable_date,$remarks,$user);
			
			if($project_man_power_rate_iresult["status"] == SUCCESS)
				
			{	
				$alert_type = 1;
			}
			
			$alert = $project_man_power_rate_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Project Man Power Type Master modes already added
	$project_man_power_master_search_data = array("active"=>'1');
	$project_man_power_type_list = i_get_project_man_power_master($project_man_power_master_search_data);
	if($project_man_power_type_list['status'] == SUCCESS)
	{
		$project_man_power_type_list_data = $project_man_power_type_list['data'];
		
    }
     else
    {
		$alert = $project_man_power_type_list["data"];
		$alert_type = 0;
	}
	
	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}	
	 else
	{
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Man Power Rate modes already added
	$project_man_power_rate_search_data = array("active"=>'1');
	$project_man_power_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
	if($project_man_power_rate_list['status'] == SUCCESS)
	{
		$project_man_power_rate_list_data = $project_man_power_rate_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master Add Man Power Rate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
	<link href="css/style1.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master Add Man Power Rate</h3><span style="float:right; padding-right:20px;"><a href="project_master_man_power_rate_list.php">Project Master Man Power Rate List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Add Man Power Rate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_man_power_rate_master_form" class="form-horizontal" method="post" action="project_master_add_man_power_rate.php">
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_power_type_id">Power Type*</label>
											<div class="controls">
												<select name="ddl_power_type_id" required>
												<option value="">- - Select Man Power Type - -</option>
												<option value="1">- - Male - -</option>
												<option value="2">- - Female- -</option>
												<option value="3">- - Mason - -</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor">Man Power Vendor*</label>
											<div class="controls">
												<select name="ddl_vendor" required>
												<option value="">- - Select Agency - -</option>
												<?php
												for($count = 0; $count < count($project_manpower_agency_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>"><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cost_per_hours">Cost Per Hours</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="0.001" name="cost_per_hours" placeholder="Cost Per Hours">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="applicable_date">Applicable Date</label>
											<div class="controls">
												<input type="date" class="span6" name="applicable_date" placeholder="Applicable Date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_man_power_rate_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
