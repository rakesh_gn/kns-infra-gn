	<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 28-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Detailed Report of files
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');


if(isset($_GET["file"]))
{
	$bd_file_id = $_GET["file"];
	
}
else
{
	echo "Please choose a file";
}

// Get file details
$bd_file_list = i_get_bd_files_list($bd_file_id,'','','','','','','');
if($bd_file_list['status'] == SUCCESS)
{
	$bd_file_list_data = $bd_file_list['data'];
}

// Get Legal File Details from BD file ID
$file_sresult = i_get_file_list('',$bd_file_id,'','','','','','','','','','','','');
if($file_sresult['status'] == SUCCESS)
{
	$file = $file_sresult['data'][0]['file_id'];
	
	// Get Payment Data
	$payment_list = i_get_file_payment_list($file,'1');	
	if($payment_list["status"] == SUCCESS)
	{
		$payment_list_data = $payment_list["data"];
	}	

	// Get Chnge of Land Payment Data
	$change_of_land_list = i_get_file_payment_list($file,'2');
	if($change_of_land_list["status"] == SUCCESS)
	{
		$change_of_land_list_data = $change_of_land_list["data"];
	}
	 
	// Get Conversion Payment Data
	$conversion_list = i_get_file_payment_list($file,'3');
	if($conversion_list["status"] == SUCCESS)
	{
		$conversion_list_data = $conversion_list["data"];
	}

	// Get Brokerage Payment Data
	$brokerage_list = i_get_file_payment_list($file,'4');
	if($brokerage_list["status"] == SUCCESS)
	{
		$brokerage_list_data = $brokerage_list["data"];
	}

	// Get Stamp Duty Payment Data
	$stamp_duty_list = i_get_file_payment_list($file,'5');
	if($stamp_duty_list["status"] == SUCCESS)
	{
		$stamp_duty_list_data = $stamp_duty_list["data"];
	}

	// Get Registration Payment Data
	$registration_list = i_get_file_payment_list($file,'6');
	if($registration_list["status"] == SUCCESS)
	{
		$registration_list_data = $registration_list["data"];
	}

	// Get Other Expension Payment Data
	$other_expension_list = i_get_file_payment_list($file,'7');
	if($other_expension_list["status"] == SUCCESS)
	{
		$other_expension_list_data = $other_expension_list["data"];
	}
}
else
{
	echo '<script>alert("Legal FIle Not Created");
	window.location.replace("http://localhost/kns/Legal/bd_file_list.php");</script>';	
}
	
$total  = 0;
$total1 = 0;
$total2 = 0;
$total3 = 0;	
$total4 = 0;
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Report</title>
	<script type="text/javascript">
        function PrintWindow()
        {                     
           window.print();                       
        }         
	</script>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>
<body>
<a href="#" onclick="return PrintWindow();">Click to Print</a>
<table width="100%" cellspacing="0" border="1" style="border-collapse:collapse; font-family:Arial, Helvetica, sans-serif" cellpadding="2">
  <col width="14" />
  <col width="69" />
  <col width="6" />
  <col width="63" />
  <col width="41" />
  <col width="6" />
  <col width="120" />
  <col width="6" />
  <col width="90" />
  <col width="6" />
  <col width="87" />
  <col width="6" />
  <col width="102" />
  <col width="6" />
  <col width="156" />
  <col width="6" />
  <col width="50" />
  <col width="14" />
  <tr>
    <td width="14" rowspan="92">&nbsp;</td>
    <td colspan="16">&nbsp;</td>
    <td width="14" rowspan="92">&nbsp;</td>
  </tr>
  <tr style="background-color:#062453; color:#fff; border: none">
    <td colspan="16"><strong>KNS Infrastructure Pvt   Ltd</strong></td>
  </tr>
  <tr style="background-color:#b3b3b3;">
    <td colspan="16">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Payment Details -   Landlordwise:</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd" >File No.</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_project_file_id']; ?></td>
    <td colspan="4">&nbsp;</td>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Status</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_file_owner_status_name']; ?></td>
  </tr>  
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Survey No.</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_file_survey_no']; ?></td>
    <td colspan="4">&nbsp;</td>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Date</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo date('d-M-Y'); ?></td>    
  </tr>
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Village</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['village_name']; ?></td>
    <td colspan="4">&nbsp;</td>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Account</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_own_account_master_account_name']; ?></td>    
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
    <td colspan="4"></td>
    <td colspan="6"></td>
  </tr>
  <tr>
    <td colspan="6" bgcolor="#848484" style="color:#bdfcfd">Party Name &amp; Address</td>
    <td colspan="4">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"  bgcolor="#c1fac1" style="color:#0c157d">Name: <?php echo $bd_file_list_data[0]['bd_file_owner']; ?>&nbsp;&nbsp;&nbsp;Mob: <?php echo $bd_file_list_data[0]['bd_file_owner_phone_no']; ?></td>
    <td colspan="4">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_file_owner_address']; ?></td>
    <td colspan="4">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
    <td colspan="4">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Land Details:</td>    
  </tr>
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Extent</td>
    <td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo ((($bd_file_list_data[0]['bd_file_extent'])/GUNTAS_PER_ACRE) * SQUARE_FOOT_PER_ACRE); ?></td>
    <td colspan="2">(in Sq ft)</td>    
    <td bgcolor="#c1fac1" colspan="2" style="color:#0c157d;"><?php echo (($bd_file_list_data[0]['bd_file_extent'])/GUNTAS_PER_ACRE); ?></td>
    <td colspan="2">(in Acres)</td>    
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Tkn Adv Date</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php
	if($payment_list["status"] == SUCCESS)
	{
		echo date("d-M-Y",strtotime($payment_list_data[0]["file_payment_date"]));
	}
	else
	{
		echo '';
	}
	?></td>    
  </tr>  
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Rate</td>
    <td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo ($bd_file_list_data[0]['bd_file_land_cost'])/($bd_file_list_data[0]['bd_file_extent']); ?></td>
    <td colspan="2">per gunta</td>    
    <td bgcolor="#c1fac1" colspan="2" style="color:#0c157d;"><?php echo ($bd_file_list_data[0]['bd_file_land_cost'])/($bd_file_list_data[0]['bd_file_extent']) * GUNTAS_PER_ACRE; ?></td>
    <td colspan="2">per acre</td>    
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Agreement Date</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Total Value</td>
    <td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_file_land_cost']; ?></td>
    <td colspan="2">&nbsp;</td>    
    <td bgcolor="#c1fac1" colspan="2" style="color:#fff;">&nbsp;</td>
    <td colspan="2">&nbsp;</td>    
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Registration Date</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php
	if($registration_list["status"] == SUCCESS)
	{
		echo date("d-M-Y",strtotime($registration_list_data[0]["file_payment_date"])); 
	}
	else
	{
		echo '';
	}
	?></td>    
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Payment Details:</td>    
  </tr>  
  <tr style="color:#fff;">
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Type</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Amount</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Payment Mode</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Details</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Paid Date</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Account</td>    
    <td colspan="4" bgcolor="#535186" style="color:#fff;">Remarks</td>
  </tr>  
  <?php				
	if($payment_list["status"] == SUCCESS)
	{
		$total = 0;
		$total4 = 0;
		$sl_no = 0;
		for($count = 0; $count < count($payment_list_data); $count++)
		{
			if($payment_list_data[$count]["file_payment_type"] == 1)
			{
				$total = $total + $payment_list_data[$count]["file_payment_amount"];
				$sl_no = $sl_no + 1;
		?>
				<tr>
				<td colspan="2" bgcolor="#7c7a7a" style="color:#0c157d"><?php echo get_ordinal_number($sl_no); ?> Adv</td>				
				<td colspan="2"  bgcolor="#c1fac1" style="color:#0c157d">  <?php echo get_formatted_amount($payment_list_data[$count]["file_payment_amount"],"INDIA"); ?> </td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $payment_list_data[$count]["payment_mode_name"]; ?></td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $payment_list_data[$count]["file_payment_cheque_dd_no"]; ?></td>
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo date("d-M-Y",strtotime($payment_list_data[$count]["file_payment_date"])); ?></td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $payment_list_data[$count]["crm_bank_name"]; ?></td>				
				<td colspan="4"  bgcolor="#c1fac1" style="color:#0c157d"><?php echo $payment_list_data[$count]["file_payment_done_to"]; ?></td>
				</tr>
		    <?php
			}
		}
	}
	else
	{
		?>
		<tr>
		<td colspan="16">No payments done yet</td>
		</tr>
		<?php
	}
	?>
			
  <tr height="30px;">
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="4"></td>    
  </tr>
  <tr>
    <td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Total Amount Paid</td>
    <td colspan="2" bgcolor="#535186" style="color:#fff;"> <?php echo get_formatted_amount($total,"INDIA"); ?> </td>
    <td colspan="2"></td>
    <td colspan="2"></td>
	<td colspan="2"></td>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td colspan="4"  bgcolor="#848484" style="color:#bdfcfd">Balance Amount</td>
    <td colspan="2" bgcolor="#535186" style="color:#fff;"> <?php echo get_formatted_amount(($file_sresult["data"][0]["file_deal_amount"] - $total),"INDIA"); ?></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
	<td colspan="2"></td>
    <td colspan="4"></td>    
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Brokerage Details:</td>
  </tr>  
  <tr>
    <td colspan="6"  bgcolor="#848484" style="color:#bdfcfd">Broker Name &amp; Address</td>
    <td colspan="2"></td>    
    <td colspan="4"  bgcolor="#848484" style="color:#bdfcfd">Total Brokerage</td>    
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo get_formatted_amount(($bd_file_list_data[0]["bd_file_brokerage_amount"]),"INDIA"); ?></td>    
  </tr>
  <tr>
    <td colspan="6" bgcolor="#c1fac1" style="color:#0c157d"><?php echo($bd_file_list_data[0]["bd_file_broker_name"]); ?></td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
	<td colspan="2">&nbsp;</td>
    <td colspan="4">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="6" bgcolor="#c1fac1" style="color:#0c157d"><?php echo($bd_file_list_data[0]["bd_file_broker_address"]); ?></td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
	<td colspan="2">&nbsp;</td>
    <td colspan="4">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Brokerage Payment  Details:</td>    
  </tr>
  <tr style="color:#fff;">
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Type</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Amount</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Payment Mode</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Details</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Paid Date</td>    
    <td colspan="2" bgcolor="#535186" style="color:#fff;">Account</td>    
    <td colspan="4" bgcolor="#535186" style="color:#fff;">Remarks</td>
  </tr>  
    <?php				
	if($brokerage_list["status"] == SUCCESS)
	{
		$total1 = 0;
		$sl_no = 0;
		for($count = 0; $count < count($brokerage_list_data); $count++)
		{
			if($brokerage_list_data[$count]["file_payment_type"] == 4)
			{
				$total1= $total1 + $brokerage_list_data[$count]["file_payment_amount"];
				$sl_no = $sl_no + 1;
		?>
				<tr>
				<td colspan="2" bgcolor="#7c7a7a" style="color:#0c157d"><?php echo get_ordinal_number($sl_no); ?> Adv</td>				
				<td colspan="2"  bgcolor="#c1fac1" style="color:#0c157d"><?php echo get_formatted_amount($brokerage_list_data[$count]["file_payment_amount"],"INDIA"); ?></td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $brokerage_list_data[$count]["payment_mode_name"]; ?></td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $brokerage_list_data[$count]["file_payment_cheque_dd_no"]; ?></td>
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo date("d-M-Y",strtotime($brokerage_list_data[$count]["file_payment_date"])); ?></td>				
				<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $brokerage_list_data[$count]["crm_bank_name"]; ?></td>				
				<td colspan="4"  bgcolor="#c1fac1" style="color:#0c157d"><?php echo $brokerage_list_data[$count]["file_payment_done_to"]; ?></td>
				</tr>
			<?php
			}
		}
	}
	else
	{
		?>
		<tr>
		<td colspan="16">No brokerage paid yet</td>
		</tr>
		<?php
	}
	?>
  <tr height="30px;">
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="4"></td>    
  </tr>
  <tr>
    <td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Total Brokerage Paid</td>    
    <td align="right" colspan="2" bgcolor="#535186" style="color:#fff;"><?php echo get_formatted_amount($total1,"INDIA"); ?></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="4"></td>    
  </tr>  
  <tr>
    <td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Balance Brokerage</td>    
    <td align="right" colspan="2" bgcolor="#535186" style="color:#fff;"><?php echo get_formatted_amount((($bd_file_list_data[0]["bd_file_brokerage_amount"]) - $total1),"INDIA"); ?></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="4"></td>     
  </tr>
  <tr>
    <td colspan="16" height="30px;"></td>
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Registration Details:</td>    
  </tr>  
  <tr>
    <td colspan="2"  bgcolor="#848484" style="color:#bdfcfd">Registration Date</td>
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php if($registration_list['status'] == SUCCESS)
	{
		echo date("d-M-Y",strtotime($registration_list_data[0]["file_payment_date"])); 
	} ?></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="2"></td>
    <td colspan="4"></td>    
  </tr>  
  <tr>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Sy nos.</td>
    <td colspan="4" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $bd_file_list_data[0]['bd_file_survey_no']; ?></td>    
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Extent</td>
    <td colspan="2" bgcolor="#c1fac1" style="color:#0c157d" align="right"><?php echo $bd_file_list_data[0]['bd_file_extent']; ?></td>    
    <td colspan="2">Guntas</td>    
    <td colspan="4"></td>    
  </tr>
  <tr height="30px;">
    <td colspan="16"></td>
  </tr>
  <tr>
    <td colspan="16" bgcolor="#0c157d" style="color:#fff;">Registration Expenses:</td>    
  </tr>
    <?php				
	if($stamp_duty_list["status"] == SUCCESS)
	{
		$total2 = 0;
		$sl_no = 0;
		for($count = 0; $count < count($stamp_duty_list_data); $count++)
		{
			$total2 = $total2 + $stamp_duty_list_data[$count]["file_payment_amount"];
			$sl_no = $sl_no + 1;
			?>
		  <tr>
			<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Stamp Duty</td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo get_formatted_amount($stamp_duty_list_data[$count]["file_payment_amount"],"INDIA"); ?></td>			
			<td colspan="2"></td>
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $stamp_duty_list_data[$count]["payment_mode_name"]; ?></td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $stamp_duty_list_data[$count]["file_payment_cheque_dd_no"]; ?></td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo date("d-M-Y",strtotime($stamp_duty_list_data[$count]["file_payment_date"])); ?></td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $stamp_duty_list_data[$count]["file_payment_done_to"]; ?></td>
		  </tr>
		  <?php			
		}
	}
	else
	{
		?>
		<tr>
			<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Stamp Duty</td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d">0.00</td>
			<td colspan="2">&nbsp;</td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>				
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>
		</tr>
		<?php
	}
	?>
	
    <?php				
	if($registration_list["status"] == SUCCESS)
	{
		$total3 = 0;
		$sl_no = 0;
		for($count = 0; $count < count($registration_list_data); $count++)
		{			
			$total3 = $total3 + $registration_list_data[$count]["file_payment_amount"];
			$sl_no = $sl_no + 1;
			?>
			<tr>
			<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Reg Fee</td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo get_formatted_amount($registration_list_data[$count]["file_payment_amount"],"INDIA"); ?></td>
			<td colspan="2">&nbsp;</td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $registration_list_data[$count]["payment_mode_name"]; ?></td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $registration_list_data[$count]["file_payment_cheque_dd_no"]; ?></td>
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo date("d-M-Y",strtotime($registration_list_data[$count]["file_payment_date"])); ?></td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $registration_list_data[$count]["file_payment_done_to"]; ?></td>
			</tr>
			<?php			
		}
	}
	else
	{
		?>
		<tr>
		<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Reg Fee</td>			
		<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d">0.00</td>
		<td colspan="2">&nbsp;</td>			
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
		<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d">&nbsp;</td>
		</tr>
		<?php
	}
	?>  
    <?php				
	if($other_expension_list["status"] == SUCCESS)
	{
		$total4 = 0;
		$sl_no = 0;
		for($count = 0; $count < count($other_expension_list); $count++)
		{			
			$total4 = $total4 + $other_expension_list[$count]["file_payment_amount"];
			$sl_no = $sl_no + 1;
			?>
			<tr>
			<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Other Exp.</td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo get_formatted_amount($other_expension_list[$count]["file_payment_amount"],"INDIA"); ?></td>
			<td colspan="2">&nbsp;</td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $other_expension_list[$count]["payment_mode_name"]; ?></td>			
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo $other_expension_list[$count]["file_payment_cheque_dd_no"]; ?></td>
			<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d"><?php echo date("d-M-Y",strtotime($other_expension_list[$count]["file_payment_date"])); ?></td>			
			<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d"><?php echo $other_expension_list[$count]["file_payment_done_to"]; ?></td>
			</tr>
			<?php			
		}
	}
	else
	{
		?>
		<tr>
		<td colspan="4" bgcolor="#848484" style="color:#bdfcfd">Other Exp.</td>			
		<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d">0.00</td>
		<td colspan="2">&nbsp;</td>			
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>
		<td bgcolor="#c1fac1" colspan="2" style="color:#0c157d">&nbsp;</td>			
		<td colspan="2" bgcolor="#c1fac1" style="color:#0c157d">&nbsp;</td>
		</tr>
		<?php
	}
	?>
  <tr>
    <td colspan="8"></td>
    <td colspan="2"></td>    
	<?php
	$total5 = 0;
	$total5 = $total + $total1 + $total2 + $total3 + $total4; ?>
    <td colspan="2" bgcolor="#848484" style="color:#bdfcfd">Total Value</td>    
    <td colspan="2" align="right" bgcolor="#535186" style="color:#fff;"><?php echo get_formatted_amount($total5,"INDIA"); ?></td>
    <td colspan="2"></td>    
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>
  </tr>
  <tr height="50px;">
    <td colspan="16">Note:</td>    
  </tr>
</table>
</body>

</html>
