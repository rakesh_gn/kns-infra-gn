<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */

	if(isset($_REQUEST["tds_master_id"]))
	{
		$tds_master_id   = $_REQUEST["tds_master_id"];
	}
	else
	{
		$tds_master_id   = "";
	}

	if(isset($_REQUEST["tds_master_type"]))
	{
		$tds_master_type   = $_REQUEST["tds_master_type"];
	}
	else
	{
		$tds_master_type   = "";
	}

	if(isset($_REQUEST["vendor"]))
	{
		$vendor  = $_REQUEST["vendor"];
	}
	else
	{
		$vendor   = "";
	}
	//Get TDS Master List
	$project_tds_deduction_master_search_data = array("master_id"=>$tds_master_id,"master_type"=>$tds_master_type);
	$tds_master_list = i_get_project_tds_deduction_master($project_tds_deduction_master_search_data);
	if($tds_master_list["status"] == SUCCESS)
	{
		$tds_master_list_data = $tds_master_list["data"];
		// print_r($tds_master_list_data[0]["project_tds_deduction_master_remarks"]); exit;
	}

	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	// Capture the form data
	if(isset($_POST["add_tds_deduction_submit"]))
	{
		$deduction_percentage     = $_POST["num_deduction"];
		$remarks      		  	  = $_POST["stxt_remarks"];
		$master_id      		  	  = $_POST["master_id"];
		if($deduction_percentage!="" && $deduction_percentage>0  && $remarks!=""){
			$tds_update_data=array("deduction"=>$deduction_percentage,"remarks"=>$remarks);
			$tds_deduction_result = i_update_project_tds_deduction_master($master_id,$tds_update_data);
			if($tds_deduction_result["status"] == SUCCESS)
			{
				echo "<script type='text/javascript'>alert('Tds Deduction added successfully');
				window.location.href='project_tds_master_list.php'
				</script>";
				// header("location:project_tds_master_list.php");
			}
	}
	else
	{
		$alert_type=0;
		$alert="Kindly Fill all the fields";
	}
}
}
else
{
	header("location:login.php");
}
?>
<style>
  .input-sms{
	margin: 10px !important;
}
.table-top{
	margin-top: 28px;
  border-top: 1px solid #D5D5D5 !important;
}
</style>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>TDS Deduction Master</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
  <link href="./css/style.css" rel="stylesheet">
  <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
  <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="./bootstrap_aku.min.css" rel="stylesheet">

</head>

<body>

  <?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
?>

  <div class="main">

    <div class="main-inner">

      <div class="container">

        <div class="row">

          <div class="span12">

            <div class="widget ">

              <div class="widget-header">
                <i class="icon-user"></i>
              </div> <!-- /widget-header -->

              <div class="widget-content table-top">

                <ul class="nav nav-tabs">
                  <li>
                    <a href="#formcontrols" data-toggle="tab">TDS Deduction Master</a>
                  </li>
                </ul>
                <br>
                <div class="control-group">
                  <div class="controls">
                    <?php
								if($alert_type == 0) // Failure
								{
								?>
                    <div class="alert alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>
                        <?php echo $alert; ?></strong>
                    </div>
                    <?php
								}
								?>

                    <?php
								if($alert_type == 1) // Success
								{
								?>
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <?php echo $alert; ?>
                      <?php
								}
								?>
                    </div> <!-- /controls -->
                  </div> <!-- /control-group -->
                  <div>
                    <form id="add_quotation_compare_form" class="form-horizontal" method="post" action="project_edit_tds_master.php">
                      <input type="hidden" name="master_id" value="<?php echo $tds_master_id; ?>" />
                      <div class="form-group input-sms">
                        <label class="control-label" for="ddl_tds_type">Deduction Type*</label>
                        <select class="form-control" name="ddl_tds_type" id="ddl_tds_type" required  disabled >
                          <option selected value=<?php echo $tds_master_type;?>><?php echo $tds_master_type ?></option>
                        </select>
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
                        <label class="control-label" for="ddl_tds_type">Vendor List*</label>
                        <select class="form-control" id="ddl_vendor_id" name="ddl_vendor_id" disabled required>
													<option selected value=<?php echo $tds_master_list_data[0]["project_tds_deduction_master_type_vendor_id"];?>><?php echo $vendor ?></option>
                        </select>
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
                        <label class="control-label" for="num_deduction">Deduction Percentage*</label>
                        <input class="form-control" type="number" name="num_deduction" value=<?php echo $tds_master_list_data[0]["project_tds_deduction_master_deduction"]?>>
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
													<label class="control-label" for="stxt_remarks">Remarks*</label>
                          <input class="form-control" type="text" name="stxt_remarks" value="<?php echo $tds_master_list_data[0]["project_tds_deduction_master_remarks"]?>">
                        </div> <!-- /control-group -->

                        <div class="form-actions input-sms">
                          <input type="submit" class="btn btn-primary" name="add_tds_deduction_submit" value="Submit" />
                          <button type="reset" class="btn">Cancel</button>
                        </div> <!-- /form-actions -->

                    </form>
                  </div>
                </div>

              </div> <!-- /widget-content -->

            </div> <!-- /widget -->

          </div> <!-- /span8 -->




        </div> <!-- /row -->

      </div> <!-- /container -->

    </div> <!-- /main-inner -->

  </div> <!-- /main -->

</body>

</html>
