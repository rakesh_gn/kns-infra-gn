<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';
/* DEFINES - START */
define('INDENT_FUNC_ID', '165');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

  

    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];
    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', INDENT_FUNC_ID, '2', '1');
	  $edit_perms_list   = i_get_user_perms($user, '', INDENT_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', INDENT_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', INDENT_FUNC_ID, '1', '1');
    


  if(isset($_POST["vendor_search_submit"]))

  {

    $search_vendor      = $_POST["hd_vendor_id"];

    $search_vendor_name = $_POST["stxt_vendor"];

  }

  else

  {

    $search_vendor      = "";

    $search_vendor_name = "";

  }


    //Get Project List

	$stock_project_search_data = array();

	$project_list = i_get_project_list($stock_project_search_data);
	if($project_list["status"] == SUCCESS)

	{

		$project_list_data = $project_list["data"];

	}

	else

	{

		$alert = $project_list["data"];

		$alert_type = 0;

	}


    ?>


    <script>
      window.permissions = {
          view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

      <?php
    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
      print_r($project_id);exit();
    }

    if (isset($_GET["search_status"])) {
      $search_status  = $_GET["search_status"];
    }
    else {
      $search_status = '';
    }

    // Get Project Management Master modes already added
		$stock_project_search_data = array('active'=>'1');
		$project_list = i_get_project_list($stock_project_search_data);
		if($project_list["status"] == SUCCESS)
		{   

			$project_list_data = $project_list["data"];
		}
} else {
    header("location:login.php");
}
?>

    <html>

    <head>
      <meta charset="utf-8">
      <title>List of Indent Items for Approval</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
      <script src="datatable/stock_po_generate.js?<?php echo time();?>"></script>
      <link href="./css/style.css?<?php echo time();?>" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
      <link href="./bootstrap_aku.min.css" rel="stylesheet">
      <link href="css/style1.css" rel="stylesheet">
    </head>

    <body>
      <?php
      include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'menu_header.php');?>
        <div class="main margin-top">
          <div class="main-inner">
            <div class="container">
              <div class="row">
                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3 style="padding-right:3px;">PO - New - Work In Progress </h3>
                    <input type="hidden" id="user" value="<?php echo $user?>" name="user">
                     <button type="button"
                      onclick="add_purchase_order()" name="items_submit"
                      class="btn btn-success pull-right btn-sm"
                      style="margin: 5px 5px 0px 0px;display: none;" id="po_button">Add Purchase Order</button>
                    <!-- <button  style="float:right;margin:3px;" class="btn btn-danger" onclick="delete_indent_item_multiple()">
                      <span class="glyphicon glyphicon-trash"></span> Delete Multiple
                    </button> -->
                    <?php if($edit_perms_list['status'] == SUCCESS){ ?>
                    <!-- <iframe src="about:blank" id="iframe_file_upload" name="iframe_file_upload" style="width:0px; height:0px;"></iframe>
                    <form id="formImport" style="float:right;margin-top: 5px; margin-right: 5px;" class="form-horizontal" target="iframe_file_upload" method="post" enctype="multipart/form-data">
                      <input type="hidden" id="hd_project_id" name="hd_project_id" class="form-control" />
                      <input type="hidden" id="hd_status" name="hd_status" class="form-control" />
                      <div class="btn-group btn-group-sm pull-right" role="group">
                        <button class="btn btn-primary" onclick="exportData()">
                         <span class="glyphicon glyphicon-download"></span> Export
                        </button>
                      </div>
                      
                    </form> -->
                    <?php } ?>




                     <!-- Example Model -->
                   <div class="modal fade" id="pomodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">Conditions for PO </h4><br>
                          <span class="header-label">Material Name: </span><span id="po_material_name"></span><br>
                          <span class="header-label">Project Name: </span><span id="po_project_name"></span>
                        <br>
                         <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Sl No</th>
                                  <th>Condition</th>
                                  <th colspan="2">Reason</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td><span id="min_stock"></span></td>
                                  <td><span id="min_stock_reason"></span></td>
                                  <td><span id="min_stock_action"></span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td><span id="budget"></span></td>
                                  <td><span id="budget_reason"></span></td>
                                  <td><span id="budget_action"></span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td><span id="wip_po"></span></td>
                                  <td><span id="po_reason"></span></td>
                                  <td><span id="po_action"></span></td>
                                </tr>
                              </tbody>
                            </table>
                       

                        </div>
                          <div class="modal-body">
                            <form id="update-poqty-form">
                            
                              </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>


                  <!-- Example Model -->
                   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">Update PO Quantity</h4>
                        <br>
                        <span class="header-label">Indent No : </span><span id="indent_no"></span>
                        <span class="header-label">Quotation No: </span><span id="quote_no"></span>
                        <span class="header-label">Material Name: </span><span id="material_name"></span>
                        <span class="header-label">Quotation QTY: </span><span id="stock_quote_qty"></span><br>
                        <span class="header-label">Project Name: </span><span id="project_name"></span>
                        </div>
                        <div class="modal-body">
                          <form id="update-poqty-form">
                            <div class="form-group">
                              <label for="quote_rate" class="control-label">Rate</label>
                              <input type="number" class="form-control" id="quote_rate" name="quote_rate" class="form-control" readonly="">
                            </div>

                            <div class="form-group">
                              <label for="po_qty" class="control-label">Quantity</label>
                              <input type="number" class="form-control"  id="po_qty" required name="po_qty" class="form-control" onkeyup="return check_qty()" required>
                            </div>

                            <div class="form-group">
                              <label for="date_received_date" class="control-label">Value</label>
                              <input type="text" class="form-control" id="po_value" name="po_value" class="form-control" readonly>
                            </div>
                            <input type="hidden" name="stock_quote_qty" id="quote_qty" value="">
                            <input type="hidden" name="quotation_id" id="quotation_id" value="">
                            <input type="hidden" name="indent_id" id="indent_id" value="">
                            <input type="hidden" id="row_no" name="row_no" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" onclick="generatePOQuantity()">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>

                    <!-- Export & Import buttons -->
                  </div>
                  <div class="widget-header" style="height:50px; padding-top:10px;"> 


                     <form class="form-inline" method="post" >
                      <span class="form-control">

                      <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />
                      <input type="text" name="stxt_vendor" placeholder="Search by Vendor Name" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" value="<?php echo $search_vendor_name; ?>" / style="border: 0px;">

                      <div id="search_results" class="dropdown-content"></div>

                      </span>
                      
                      <select name="project_id" id="project_id" style="max-width:250px" class="form-control input-sm">
                                 <option value="">- - Select Project - -</option>
                              <?php
                                for ($project_count = 0; $project_count < count($project_list_data); $project_count++) { ?>
                                 <option value="<?php echo $project_list_data[$project_count]["stock_project_id"]; ?>"
                                   <?php if ($project_id == $project_list_data[$project_count]["stock_project_id"]) {
                                    ?> selected="selected" <?php } ?>>
                                    <?php echo $project_list_data[$project_count]["stock_project_name"]; ?>
                                 </option>
                              <?php } ?>
                      </select>
                      <input type="hidden" name="status" value="Approved">
                       
                      <button id="submit_button" type="button" class="btn btn-primary" onclick="redrawTable()">Submit</button>
                    </form>
                  </div>
                  <?php if($view_perms_list['status'] == SUCCESS){ ?>
                  <div class="widget-content">
                    <table class="table table-striped table-bordered display nowrap" id="example" style="width:100%">
                       <thead>
                         <tr>
                            <th colspan="15">&nbsp;</th>
                            <th colspan="2" class="center blue">Actions</th>
                         </tr>
                         <tr>

                           <th>#</th>
                           <th>Indent No</th>
                           <th>Project</th>
                          

                           <th>Material Name</th>
                           <th>Material Code</th>
                           
                           <th>UOM</th>
                           <th>Quotation No</th>
                           <th>Quotation Added On</th>
                           <th>Quotation Approved On</th>
                           <th>Quotation Approved By</th>
                           <th>Vendor Name</th>
                           <th>Indent Qty</th>
                           <th>PO Qty</th>
                           <th>Rate</th>
                           <th>Value</th>
                           <th>A</th>

                        </tr>
                     </thead>
                      <tbody>
                      </tbody>
                     </table>
                  </div>
                  <?php }
                else{ ?>
                  <div class="widget-content">
                    <h1>Access Denied</h1>
                    <h3>You dont have permissions to view the Document</h3>
                  </div>
                  <?php } ?>
                  <!-- widget-content -->
                </div>
              </div>
            </div>
          </div>

    </body>

    </html>
