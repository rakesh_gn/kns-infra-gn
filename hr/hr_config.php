<?php
/* LEAVE STATUS - START */
define('LEAVE_STATUS_PENDING','0');
define('LEAVE_STATUS_APPROVED','1');
define('LEAVE_STATUS_REJECTED','2');
define('LEAVE_STATUS_CANCELLED','3');
/* LEAVE STATUS - END */

/* LEAVE TYPE - START */
define('ATTENDANCE_TYPE_PRESENT','1');
define('ATTENDANCE_TYPE_HALFDAY','2');
define('ATTENDANCE_TYPE_ABSENT','3');
define('LEAVE_TYPE_EARNED','5');
define('LEAVE_TYPE_SICK','6');
define('LEAVE_TYPE_CASUAL','7');
define('LEAVE_TYPE_COMP_OFF','8');
/* LEAVE TYPE - END */
/** NOTE: Earned Leave is treated as CL from a user's perspective because, in KNS, there is no concept of EL. EL is called CL **/

/* MAX LEAVE SETTINGS - START */
define('SICK_LEAVE_PER_MONTH','0.5');
define('CASUAL_LEAVE_PER_YEAR','0');
/* MAX LEAVE SETTINGS - END */

/* COMP OFF RELATED - START */
define('COMP_OFF_MAX_DURATION','365');
/* COMP OFF RELATED - END */

/* EARNED LEAVE RELATED - START */
define('EL_TO_BE_ADDED',1);
define('EL_MIN_WORKING_DAYS',21);
/* EARNED LEAVE RELATED - END */

/* SICK LEAVE RELATED - START */
define('SL_MIN_WORKING_DAYS',15);
/* SICK LEAVE RELATED - END */

/* OUT PASS STATUS - START */
define('OUT_PASS_STATUS_PENDING','0');
define('OUT_PASS_STATUS_APPROVED','1');
define('OUT_PASS_STATUS_REJECTED','2');
define('OUT_PASS_STATUS_CANCELLED','3');
/* OUT PASS STATUS - END *//* ATTENDANCE BUFFER RELATED - START */define('ATTENDANCE_RECTIFY_BUFFER_DAYS',20);/* ATTENDANCE BUFFER RELATED - END */
?>