<?php

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');

$employee_filter_data = array('user_status'=>'1');
$employee_list = i_get_employee_list($employee_filter_data);

for($count = 0; $count < count($employee_list['data']); $count++)
{
	$employee = $employee_list['data'][$count]['hr_employee_id'];
	$year     = '2017';
	$month    = '01';
	// Check for number of days in the month worked by the employee
	$fd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_PRESENT,"attendance_status"=>'1');
	$fd_attendance_sresult = i_get_attendance_list($fd_attendance_filter_data);
	
	if($fd_attendance_sresult["status"] == SUCCESS)
	{
		$fd_count = count($fd_attendance_sresult["data"]);
	}
	else
	{
		$fd_count = 0;
	}
	
	$hd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_HALFDAY,"attendance_status"=>'1');
	$hd_attendance_sresult = i_get_attendance_list($hd_attendance_filter_data);
	
	if($hd_attendance_sresult["status"] == SUCCESS)
	{
		$hd_count = count($hd_attendance_sresult["data"]);
	}
	else
	{
		$hd_count = 0;
	}
	
	// Get public holiday list
	$number_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
	$holiday_filter_data = array("date_start"=>$year.'-'.$month.'-01',"date_end"=>$year.'-'.$month.'-'.$number_of_days,"status"=>'1');
	$holiday_list = i_get_holiday($holiday_filter_data);
	if($holiday_list['status'] == SUCCESS)
	{
		$pholidays = count($holiday_list['data']);
	}
	else
	{
		$pholidays = 0;
	}
	
	// If no. of working days >= 21, add appropriate no of leaves
	if(($fd_count + $hd_count + $pholidays) >= EL_MIN_WORKING_DAYS)
	{
		var_dump($employee_list['data'][$count]['hr_employee_id']);
		$employee_el_filter_data = array("employee_id"=>$employee);
		$employee_el_sresult = db_get_el_list($employee_el_filter_data);
		var_dump($employee_el_sresult['status']);
		if($employee_el_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
		{
			$el_details = array("count"=>'-3');
			$el_uresult = db_update_el_details($employee,$year,$el_details);
			var_dump($employee_el_sresult["data"][0]["hr_earned_leave_count"]);
		}
	}
}
?>