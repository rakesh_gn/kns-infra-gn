<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_BUDGET_LIST_FUNC_ID', '365');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '6', '1');
    ?>


    <?php
    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
    }
    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Project Wish Date List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/project_wish_date_list.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <!-- // modal -->
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Project Wish Date List</h3>
            </div>
            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                <select name="project_id" id="project_id" class="form-control">
                 <option value="">- - Select Project - -</option>
                 <?php
                   for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) { ?>
                    <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>"
                      <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                       ?> selected="selected" <?php } ?>>
                       <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
                    </option>
                 <?php } ?>
                 </select>
              <input type="submit" class="btn btn-primary" />
              </form>
            </div>
            <br>
          </div>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example" style="width:100%">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Wish date</th>
                   <th>Added By</th>
                   <th>Added on</th>
                   <th>Remarks</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
