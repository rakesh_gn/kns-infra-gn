<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "-1";
	}		
	/* QUERY STRING - END */
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get payment schedule
	$pay_schedule_details = i_get_pay_schedule($booking_id,'','','');
	if($pay_schedule_details["status"] == SUCCESS)
	{
		$pay_schedule_details_list = $pay_schedule_details["data"];
	}
	else
	{
		$alert = $pay_schedule_details["data"];
		$alert_type = 0;
	}
	
	// Get payment terms
	$payment_terms_details = i_get_payment_terms($booking_id);
	if($payment_terms_details["status"] == SUCCESS)
	{
		$payment_terms_details_list = $payment_terms_details["data"];
	}
	else
	{
		$alert = $payment_terms_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>View Payment Schedule</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3><?php if($cust_details["status"] == SUCCESS){ ?>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; }
						else
						{
							echo "<b>".$cust_details["data"]."</b>";
						}?></h3>
						
						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_id; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
									<?php
									if($cust_details["status"] == SUCCESS)
									{
										$total_site_cost = $pay_schedule_details_list[0]["crm_booking_rate_per_sq_ft"] * $pay_schedule_details_list[0]["crm_booking_consideration_area"];
									?>
										<table class="table">										  
										  <tbody>
										  <tr>
										  <td>Customer Name</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_customer_name_one"]; ?></td>
										  </tr>
										  <tr>
										  <td>Address</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_customer_agreement_address"]; ?></td>
										  </tr>
										  <tr>
										  <td>Phone</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_customer_contact_no_one"]; ?></td>
										  </tr>
										  <tr>
										  <td>Company Name</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_customer_company"]; ?></td>
										  </tr>
										  <tr>
										  <td>Office Address</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_customer_office_address"]; ?></td>
										  </tr>
										  <tr>
										  <td>Project Name</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["project_name"]; ?></td>
										  </tr>
										  <tr>
										  <td>Plot No</td>
										  <td>:</td>
										  <td><?php echo $cust_details_list[0]["crm_site_no"]; ?></td>
										  </tr>
										  <tr>
										  <td>Plot Area</td>
										  <td>:</td>
										  <td><?php echo $pay_schedule_details_list[0]["crm_booking_consideration_area"]; ?> sq. ft</td>
										  </tr>
										  <tr>
										  <td>Rate per sq.ft</td>
										  <td>:</td>
										  <td><?php echo $pay_schedule_details_list[0]["crm_booking_rate_per_sq_ft"]; ?></td>
										  </tr>
										  </tbody>
										</table>
									<?php
									}
									else
									{
									?>
									<table class="table">										  										
										<tbody>
										<tr>
										<td>No Customer Details added yet</td>
										</tr>
										</tbody>
									</table>
									<?php
									}
									?>
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Payment Schedule Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Schedule Description</th>
										  <th>Scheduled Date</th>
										  <th>Amount (Rs.)</th>
										  <th>Received (Rs.)</th>
										  <th>Balance (Rs.)</th>
										  <th>&nbsp;</th>
										  </tr>
										  </thead>
										  <tbody>
										  <?php 
										  $scheduled_amount = 0;
										  $received_amount  = 0;
										  $balance          = 0;
										  $agreement_amount = 0;
										  if($pay_schedule_details["status"] == SUCCESS)
										  {											  											  
											  $payment_data = i_get_payment_total('',$booking_id,'','','');
											  if($payment_data["status"] == SUCCESS)
											  {
												  $received_amount = $payment_data["data"][0]["paid"];
												  $final_received_amount = $payment_data["data"][0]["paid"];
											  }
											  else
											  {
											      $received_amount = 0;
											  }
											  
											  for($count = 0; $count < count($pay_schedule_details_list); $count++)
											  {
											  if($pay_schedule_details_list[$count]["crm_payment_schedule_milestone"] == 'AGREEMENT')
											  {
												  $agreement_amount = $agreement_amount + $pay_schedule_details_list[$count]["crm_payment_schedule_amount"];
											  }?>
											  <tr>
											  <td><?php echo $pay_schedule_details_list[$count]["crm_payment_schedule_milestone"]; ?></td>
											  <td><?php echo date('d-M-Y',strtotime($pay_schedule_details_list[$count]["crm_payment_schedule_date"])); ?></td>
											  <td><?php echo $pay_schedule_details_list[$count]["crm_payment_schedule_amount"]; ?></td>
											  <td><?php if($received_amount > $pay_schedule_details_list[$count]["crm_payment_schedule_amount"])
											  {
											      echo $pay_schedule_details_list[$count]["crm_payment_schedule_amount"];
												  $this_balance = 0;
											  }
											  else
											  {
												  echo $received_amount; 
												  $this_balance = $pay_schedule_details_list[$count]["crm_payment_schedule_amount"] - $received_amount;
											  }?></td>
											  <td><?php echo $this_balance;?></td>
											  <td><a href="#" onclick="return go_to_edit_pay_schedule('<?php echo $pay_schedule_details_list[$count]["crm_payment_schedule_id"]; ?>','<?php echo $booking_id; ?>');">Edit</a></td>
											  </tr>
											  <?php
											  $scheduled_amount = $scheduled_amount + $pay_schedule_details_list[$count]["crm_payment_schedule_amount"];
											  if($received_amount > $pay_schedule_details_list[$count]["crm_payment_schedule_amount"])
											  {
												  $received_amount = $received_amount - $pay_schedule_details_list[$count]["crm_payment_schedule_amount"];
											  }
											  else
											  {
												  $received_amount  = 0;
											  }	
											  $balance          = $balance + $this_balance;
											  }
											  ?>
											  <tr>
											  <td><strong>Total Sale Consideration (Rs.)</strong></td>
											  <td>&nbsp;</td>
											  <td><strong><?php echo $scheduled_amount; ?></strong></td>
											  <td><strong><?php echo $final_received_amount; ?></strong></td>
											  <td><strong><?php echo $balance; ?></strong></td>
											  <td>&nbsp;</td>
											  </tr>
											  <?php
										  }
										  else
										  {
										  $agreement_amount = 0;
										  ?>
										  <tr>
										  <td colspan="4">No Payment Schedule added yet</td>
										  </tr>
										  <?php
										  }?>
										  </tbody>
										</table>
										<br />
										<table class="table table-bordered" style="table-layout: fixed;">
										<?php $other_total = 0;
										?>
										  <thead>			  
										  <tr>
										  <th>Other Charges Description</th>
										  <th>Amount (Rs.)</th>
										  <th>Received (Rs.)</th>
										  <th>Balance (Rs.)</th>
										  </tr>
										  </thead>
										  <tbody>
										  <?php if($payment_terms_details["status"] == SUCCESS)
										  {?>
										  <tr>
										  <td>Layout Maintenance Charges</td>
										  <td><?php echo $payment_terms_details_list[0]["crm_pt_layout_maintenance_charges"]; 
										  $other_total = $other_total + $payment_terms_details_list[0]["crm_pt_layout_maintenance_charges"];?></td>
										  <td>0</td>
										  <td>0</td>
										  </tr>
										  <tr>
										  <td>Club House Charges</td>
										  <td><?php echo $payment_terms_details_list[0]["crm_pt_club_house_charges"]; 
										  $other_total = $other_total + $payment_terms_details_list[0]["crm_pt_club_house_charges"]; ?></td>
										  <td>0</td>
										  <td>0</td>
										  </tr>
										  <tr>
										  <td>Water Charges</td>
										  <td><?php echo $payment_terms_details_list[0]["crm_pt_water_charges"]; 
										  $other_total = $other_total + $payment_terms_details_list[0]["crm_pt_water_charges"]; ?></td>
										  <td>0</td>
										  <td>0</td>
										  </tr>
										  <tr>
										  <td>Documentation Charges</td>
										  <td><?php echo $payment_terms_details_list[0]["crm_pt_documentation_charges"]; 
										  $other_total = $other_total + $payment_terms_details_list[0]["crm_pt_documentation_charges"]; ?></td>
										  <td>0</td>
										  <td>0</td>
										  </tr>
										  <tr>
										  <td><strong>Total Other Charges</strong></td>
										  <td><strong><?php echo $other_total; ?></strong></td>
										  <td>0</td>
										  <td>0</td>
										  </tr>
										  <tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  </tr>
										  <tr>
										  <td><strong>Total Cost Excluding Registration (Rs.)</strong></td>
										  <td><strong><?php echo $scheduled_amount + $other_total; ?></strong></td>
										  <td><strong><?php echo $final_received_amount + 0; ?></strong></td>
										  <td><strong><?php echo $balance + 0; ?></strong></td>
										  </tr>
										  <tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  </tr>
										  <tr>
										  <td colspan="4"><strong>* Cheque/ DD in favor "KNS INFRASTRUCTURE PVT LTD _________ ESCROW ACCOUNT”</strong></td>										  
										  </tr>
										  <?php
										  }
										  else
										  {
										  ?>
										  <tr>
										  <td colspan="4">No Payment Terms added</td>										  
										  </tr>
										  <?php
										  }?>
										  </tbody>
										</table>										
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Registration Charges Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  
										<tr>
										  <td colspan="4">Three separate DD in favor of " Sub Registrar _______________" payable at Bangalore</td>					
										</tr>
										<tr>
										<td>Stamp Duty Charges (5.15% of Agreement Value)</td>
										<td><?php echo ($total_site_cost * 5.15/100); ?></td>
										<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
										<td>Registration Charges (1% of Agreement Value)</td>
										<td><?php echo ($total_site_cost * 1/100); ?></td>
										<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
										<td>Cess Charges (0.5% of Agreement Value)</td>
										<td><?php echo ($total_site_cost * 0.5/100); ?></td>
										<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
										<td>&nbsp</td>
										<td>&nbsp;</td>
										<td colspan="2">&nbsp;</td>								
										</tr>
										<tr>
										<td colspan="4">&nbsp;</td>										
										</tr>
										<tr>										
										<td colspan="4"><span style="float:right;">Date & Timings</span></td>
										</tr>
										</table>
																												
									</fieldset>																								
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_edit_pay_schedule(schedule_id,booking_id)
{		
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "crm_edit_payment_schedule.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","schedule_id");
	hiddenField1.setAttribute("value",schedule_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","booking_id");
	hiddenField2.setAttribute("value",booking_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
	form.submit();
}
</script>

  </body>

</html>
