<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_reason_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of reasons for customer withdrawals
*/

/* DEFINES - START */
define('CRM_REASON_LIST_FUNC_ID','68');
/* DEFINES - END */

/*
TBD: 
*/
$_SESSION['module'] = 'CRM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',CRM_REASON_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_REASON_LIST_FUNC_ID,'3','1');

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";

	$reason_list = i_get_reason_list('','1');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reason List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
               <h3>Reason List</h3><?php if($view_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="crm_add_reason.php">Add Reason</a>
			  </span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Reason</th>					
					<th>Added By</th>
					<th>Added On</th>
					<th>Status</th>
					<th>&nbsp;</th>										
				</tr>
				</thead>
				<tbody>							
				<?php
				if($reason_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($reason_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $reason_list_data[$count]["reason_name"]; ?></td>					
					<td><?php echo $reason_list_data[$count]["user_name"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($reason_list_data[$count]["reason_added_on"])); ?></td>	
					<td><?php echo i_get_active_name($reason_list_data[$count]["reason_active"]); ?></td>
					<td><?php if($edit_perms_list['status'] == SUCCESS){?><a href="">Enable/Disable</a><?php } ?></td>					
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="6">No reason added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>