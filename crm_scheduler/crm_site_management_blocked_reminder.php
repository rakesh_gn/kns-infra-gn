<?php
/**
 * @author Nitin Kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

// What is the date today
$today = date("Y-m-d");

// Get list of blocked sites
$blocked_site_sresult = i_get_mgmt_block_list('','','','','','1','','','','','blocked_date');

if($blocked_site_sresult['status'] == SUCCESS)
{
	$blocked_site_data = $blocked_site_sresult['data'];

	$subject = 'Management Blocked Sites';
	$message = 'Dear Team,<br><br>Below sites are temporally blocked by the management:<br><br>';
	$message = $message.'<table border="1" style="border-collapse:collapse; border-width:2px;">';
	// Header row - start
	$message = $message.'<tr style="border-width:2px;">';
	$message = $message.'<td style="border-width:2px;"><strong>SL No.</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Project</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Site No</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Dimension</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Master Rate</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Blocked By</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Blocked Date</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>No. of days</strong></td>';
	$message = $message.'</tr>';
	// Header row - end

	$sl_no = 0;

	for($count = 0; $count < count($blocked_site_data); $count++)
	{
		$sl_no++;

		// Get Master rate for this project
		$project_cost_details = i_get_site_cost($blocked_site_data[$count]['crm_project_id'],'',date("Y-m-d",strtotime($blocked_site_data[$count]['crm_management_block_added_on'])),'','','');
		if($project_cost_details['status'] == SUCCESS)
		{
			$project_cost = $project_cost_details['data'][0]['crm_cost_value'];
		}
		else
		{
			$project_cost = 'N.A';
		}

		// Get booking approved date
		$blocked_date	  = date('d-M-Y',strtotime($blocked_site_data[$count]['crm_management_block_added_on']));

		// Get the number of days expired from booking approved date till today
		$date_diff_result = get_date_diff(date("Y-m-d",strtotime($blocked_site_data[$count]['crm_management_block_added_on'])),$today);
		$no_of_days = $date_diff_result['data'];

		// Compose the message
		$project_name 	  = $blocked_site_data[$count]['project_name'];
		$site_no	  	  = $blocked_site_data[$count]['crm_site_no'];
		$enquiry_no	  	  = $blocked_site_data[$count]['enquiry_number'];
		$dimension	  	  = $blocked_site_data[$count]['crm_dimension_name'].' ('.$blocked_site_data[$count]['crm_site_area'].' sq. ft)';	$blocked_by	  	  = $blocked_site_data[$count]['user_name'];

		$message = $message.'<tr>';
		$message = $message.'<td style="border-width:2px;">'.$sl_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$project_name.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$site_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$dimension.'</td>'; // Dimension
		$message = $message.'<td style="border-width:2px;">'.$project_cost.'</td>'; // Master Rate
		$message = $message.'<td style="border-width:2px;">'.$blocked_by.'</td>'; // Blocked By
		$message = $message.'<td style="border-width:2px;">'.$blocked_date.'</td>'; // Blocked Date
		$message = $message.'<td style="border-width:2px;">'.$no_of_days.'</td>'; // No. of days
		$message = $message.'</tr>';
	}
	$message = $message.'</table>';
	$message = $message.'<br>Regards,<br>KNS ERP';

	// Identify recipients
	$cc = array();
	$cc_count = 0;
	$crm_perms = i_get_user_email_perms('','','9','2','1');
	$cc_string = '';

	if($crm_perms['status'] == SUCCESS)
	{
		for($ucount = 0; $ucount < count($crm_perms['data']); $ucount++)
		{
			$crm_sresult = i_get_user_list($crm_perms['data'][$ucount]['permission_user'],'','','','1','');
			if($ucount == 0)
			{
				$to = $crm_sresult['data'][0]['user_email_id'];
				$name = $crm_sresult['data'][0]['user_name'];
			}
			else
			{
				$cc[$cc_count] = $crm_sresult['data'][0]['user_email_id'];
				if(strpos($cc[$cc_count], '@')){
					$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
				}
				$cc_count++;
			}
		}
	}

	$manager_perms = i_get_user_email_perms('','','9','3','1');
	if($manager_perms['status'] == SUCCESS)
	{
		$manager_sresult = i_get_user_list($user_sresult['data'][0]['permission_user'],'','','','1','');
		$cc[$cc_count] = $manager_sresult['data'][0]['user_email_id'];
		if(strpos($cc[$cc_count], '@')){
			$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
		}
		$cc_count++;
	}

	$admin_perms = i_get_user_email_perms('','','9','4','1');
	if($admin_perms['status'] == SUCCESS)
	{
		for($ucount = 0; $ucount < count($admin_perms['data']); $ucount++)
		{
			$admin_sresult = i_get_user_list($admin_perms['data'][$ucount]['permission_user'],'','','','1','');
			$cc[$cc_count] = $admin_sresult['data'][0]['user_email_id'];
			if(strpos($cc[$cc_count], '@')){
				$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
			}
			$cc_count++;
		}
	}
	$cc_string = trim($cc_string,',');

	// Send email
	$res = sendBulkMail($to,$cc_string,'venkataramanaiah@knsgroup.in',$subject,$message);

	var_dump($res);
}
?>
