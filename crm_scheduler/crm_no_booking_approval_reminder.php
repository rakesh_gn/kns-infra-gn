<?php
/**
 * @author Nitin Kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

// What is the date today
$today = date("Y-m-d");

// Get list of approved bookings with no booking date
$no_booking_approval_sresult = i_get_site_booking('','','','','','0','','','','','','','','','','');

if($no_booking_approval_sresult['status'] == SUCCESS)
{
	$no_booking_approval_data = $no_booking_approval_sresult['data'];

	$message = 'Dear Sales HOD,<br><br>The following bookings have been done by the STM but not yet approved by you:<br><br>';
	$message = $message.'<table border="1" style="border-collapse:collapse; border-width:2px;">';
	// Header row - Start
	$message = $message.'<tr style="border-width:2px;">';
	$message = $message.'<td style="border-width:2px;"><strong>SL No.</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Project</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Site No</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Dimension</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Booking Rate</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>STM</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Booking Date</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>No. of days</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Enquiry No</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Source</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Customer Name</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Contact No</strong></td>';
	$message = $message.'</tr>';
	// Header row - End

	$sl_no = 0;
	for($count = 0; $count < count($no_booking_approval_data); $count++)
	{
		$sl_no++;
		// Get booking approved date
		$booked_date = date('Y-m-d',strtotime($no_booking_approval_data[$count]['crm_booking_added_on']));

		// Get the number of days expired from booking approved date till today
		$date_diff_result = get_date_diff($booked_date,$today);
		$no_of_days = $date_diff_result['data'];

		// Compose the message
		$project_name 	  = $no_booking_approval_data[$count]['project_name'];
		$site_no	  	  = $no_booking_approval_data[$count]['crm_site_no'];
		$enquiry_no	  	  = $no_booking_approval_data[$count]['enquiry_number'];
		$dimension	  	  = $no_booking_approval_data[$count]['crm_dimension_name'].' ('.$no_booking_approval_data[$count]['crm_site_area'].' sq. ft)';
		$booked_rate	  = $no_booking_approval_data[$count]['crm_booking_rate_per_sq_ft'];
		$name	  		  = $no_booking_approval_data[$count]['name'];
		$contact_no	  	  = $no_booking_approval_data[$count]['cell'];
		$source	  		  = $no_booking_approval_data[$count]['enquiry_source_master_name'];
		$booked_by	  	  = $no_booking_approval_data[$count]['user_name'];
		$added_date		  = date('d-M-Y',strtotime($no_booking_approval_data[$count]['crm_booking_added_on']));
		$subject = 'Bookings Not Yet Approved';

		$message = $message.'<tr style="border-width:2px;">';
		$message = $message.'<td style="border-width:2px;">'.$sl_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$project_name.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$site_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$dimension.'</td>'; // Dimension
		$message = $message.'<td style="border-width:2px;">'.$booked_rate.'</td>'; // Booked Rate
		$message = $message.'<td style="border-width:2px;">'.$booked_by.'</td>'; // Booked By
		$message = $message.'<td style="border-width:2px;">'.$added_date.'</td>'; // Booking Added Date
		$message = $message.'<td style="border-width:2px;">'.$no_of_days.'</td>'; // No. of days since booking entered
		$message = $message.'<td style="border-width:2px;">'.$enquiry_no.'</td>'; // Enquiry No
		$message = $message.'<td style="border-width:2px;">'.$source.'</td>'; // Source
		$message = $message.'<td style="border-width:2px;">'.$name.'</td>'; // Client Name
		$message = $message.'<td style="border-width:2px;">'.$contact_no.'</td>'; // Client Contact No
		$message = $message.'</tr>';
	}
	$message = $message.'</table>';
	$message = $message.'<br>Regards,<br>KNS ERP';

	if($sl_no > 0)
	{
		// Identify recipients
		$cc = array();
		$cc_count = 0;
		$cc_string = '';

		$stm_perms = i_get_user_email_perms('','','6','1','1');
		if($stm_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($stm_perms['data']); $ucount++)
			{
				$stm_sresult = i_get_user_list($stm_perms['data'][$ucount]['permission_user'],'','','','1','');
				if($ucount == 0)
				{
					$to    = $stm_sresult['data'][0]['user_email_id'];
					$uname = $stm_sresult['data'][0]['user_name'];
				}
				else
				{
					$cc[$cc_count] = $stm_sresult['data'][0]['user_email_id'];
					if(strpos($cc[$cc_count], '@')){
						$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
					}
					$cc_count++;
				}
			}
		}

		$crm_perms = i_get_user_email_perms('','','6','2','1');
		if($crm_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($crm_perms['data']); $ucount++)
			{
				$crm_sresult = i_get_user_list($crm_perms['data'][$ucount]['permission_user'],'','','','1','');
				$cc[$cc_count] = $crm_sresult['data'][0]['user_email_id'];
				if(strpos($cc[$cc_count], '@')){
					$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
				}
				$cc_count++;
			}
		}

		$manager_perms = i_get_user_email_perms('','','6','3','1');
		if($manager_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($manager_perms['data']); $ucount++)
			{
				$manager_sresult = i_get_user_list($manager_perms['data'][$ucount]['permission_user'],'','','','1','');
				$cc[$cc_count] = $manager_sresult['data'][0]['user_email_id'];
				if(strpos($cc[$cc_count], '@')){
					$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
				}
				$cc_count++;
			}
		}

		$admin_perms = i_get_user_email_perms('','','6','4','1');

		if($admin_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($admin_perms['data']); $ucount++)
			{
				$admin_sresult = i_get_user_list($admin_perms['data'][$ucount]['permission_user'],'','','','1','');
				$cc[$cc_count] = $admin_sresult['data'][0]['user_email_id'];
				if(strpos($cc[$cc_count], '@')){
					$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
				}
				$cc_count++;
			}
		}
		$cc_string = trim($cc_string,',');
		$res = sendBulkMail($to,$cc_string,'venkataramanaiah@knsgroup.in',$subject,$message);

		var_dump($res);
		exit;
	}
}
?>
