<?php
/**
 * @author Nitin Kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

// What is the date today
$today = date("Y-m-d");

// Get list of approved bookings with no booking date
$no_booking_date_sresult = i_get_site_booking("","","","","",'1',"","","","","","","","",'0000-00-00','0000-00-00',"","","","",'approval_date');

if($no_booking_date_sresult['status'] == SUCCESS)
{
	$no_booking_date_data = $no_booking_date_sresult['data'];

	$subject = 'Booking Date Not Updated';
	$message = 'Dear CRM Team,<br><br>Sales Considerable Booking Date not updated by you for the following bookings:<br><br>';
	$message = $message.'<table border="1" style="border-collapse:collapse; border-width:2px;">';
	// Header row - start
	$message = $message.'<tr style="border-width:2px;">';
	$message = $message.'<td style="border-width:2px;"><strong>SL No.</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Project</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Site No</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Dimension</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Approved Rate</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>STM</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>STM Booked Date</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Manager Approved Date</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>No. of days</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Enquiry No</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Enq. Source</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Cust. Name</strong></td>';
	$message = $message.'<td style="border-width:2px;"><strong>Cust. No</strong></td>';
	$message = $message.'</tr>';
	// Header row - end

	$sl_no = 0;

	for($count = 0; $count < count($no_booking_date_data); $count++)
	{
		$sl_no++;

		// Get booking approved date
		$approved_date = date('Y-m-d',strtotime($no_booking_date_data[$count]['crm_booking_approved_on']));

		// Get the number of days expired from booking approved date till today
		$date_diff_result = get_date_diff($approved_date,$today);
		$no_of_days = $date_diff_result['data'];

		// Compose the message
		$project_name 	  = $no_booking_date_data[$count]['project_name'];
		$site_no	  	  = $no_booking_date_data[$count]['crm_site_no'];
		$enquiry_no	  	  = $no_booking_date_data[$count]['enquiry_number'];
		$dimension	  	  = $no_booking_date_data[$count]['crm_dimension_name'].' ('.$no_booking_date_data[$count]['crm_site_area'].' sq. ft)';
		$approved_rate	  = $no_booking_date_data[$count]['crm_booking_rate_per_sq_ft'];
		$name	  		  = $no_booking_date_data[$count]['name'];
		$contact_no	  	  = $no_booking_date_data[$count]['cell'];
		$source	  		  = $no_booking_date_data[$count]['enquiry_source_master_name'];
		$booked_by	  	  = $no_booking_date_data[$count]['user_name'];
		$stm_booked_date  = date('d-M-Y',strtotime($no_booking_date_data[$count]['crm_booking_added_on']));
		$approved_date	  = date('d-M-Y',strtotime($no_booking_date_data[$count]['crm_booking_approved_on']));

		$message = $message.'<tr style="border-width:2px;">';
		$message = $message.'<td style="border-width:2px;">'.$sl_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$project_name.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$site_no.'</td>';
		$message = $message.'<td style="border-width:2px;">'.$dimension.'</td>'; // Dimension
		$message = $message.'<td style="border-width:2px;">'.$approved_rate.'</td>'; // Approved Rate
		$message = $message.'<td style="border-width:2px;">'.$booked_by.'</td>'; // Booked By
		$message = $message.'<td style="border-width:2px;"><strong>'.$stm_booked_date.'</strong></td>'; // Booked Date
		$message = $message.'<td style="border-width:2px;"><strong>'.$approved_date.'</stron></td>'; // Manager Approved Date
		$message = $message.'<td style="border-width:2px;"><strong>'.$no_of_days.'</stron></td>'; // No. of days expired
		$message = $message.'<td style="border-width:2px;">'.$enquiry_no.'</td>'; // Enquiry No
		$message = $message.'<td style="border-width:2px;">'.$source.'</td>'; // Source
		$message = $message.'<td style="border-width:2px;">'.$name.'</td>'; // Client Name
		$message = $message.'<td style="border-width:2px;">'.$contact_no.'</td>'; // Client Contact No
		$message = $message.'</tr>';
	}
	$message = $message.'</table>';
	$message = $message.'<br>Regards,<br>KNS ERP';

	if($sl_no > 0)
	{
		// Identify recipients
		$cc = array();
		$cc_count = 0;
		$cc_string = '';

		$crm_perms = i_get_user_email_perms('','','3','2','1');

		if($crm_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($crm_perms['data']); $ucount++)
			{
				$crm_sresult = i_get_user_list($crm_perms['data'][$ucount]['permission_user'],'','','','1','');
				if($ucount == 0)
				{
					$to = $crm_sresult['data'][0]['user_email_id'];
					$name = $crm_sresult['data'][0]['user_name'];
				}
				else
				{
					$cc[$cc_count] = $crm_sresult['data'][0]['user_email_id'];
					if(strpos($cc[$cc_count], '@')){
						$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
					}
					$cc_count++;
				}
			}
		}

		$manager_perms = i_get_user_email_perms('','','3','3','1');
		if($manager_perms['status'] == SUCCESS)
		{
			$manager_sresult = i_get_user_list($manager_perms['data'][0]['permission_user'],'','','','1','');
			$cc[$cc_count] = $manager_sresult['data'][0]['user_email_id'];
			if(strpos($cc[$cc_count], '@')){
				$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
			}
			$cc_count++;
		}

		$admin_perms = i_get_user_email_perms('','','3','4','1');
		if($admin_perms['status'] == SUCCESS)
		{
			for($ucount = 0; $ucount < count($admin_perms['data']); $ucount++)
			{
				$admin_sresult = i_get_user_list($admin_perms['data'][$ucount]['permission_user'],'','','','1','');
				$cc[$cc_count] = $admin_sresult['data'][0]['user_email_id'];
				if(strpos($cc[$cc_count], '@')){
					$cc_string = $cc_string.'"'.$cc[$cc_count].'",';
				}
				$cc_count++;
			}
		}
		$cc_string = trim($cc_string,',');

		$res = sendBulkMail($to,$cc_string,'venkataramanaiah.y@gmail.com,firozss31@gmail.com',$subject,$message);

		var_dump($res);
		exit;
	}
}
?>
