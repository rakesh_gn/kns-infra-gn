<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th April 2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_DASHBOARD_PENDING_FUNC_ID','332');
/* DEFINES - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');


/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$view_perms_list   = i_get_user_perms($user,'',APF_DASHBOARD_PENDING_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	$search_project    = "";
	$search_bank       = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_project    = $_POST["search_project"];
		$search_bank       = $_POST["search_bank"];
	}
	
	// Get Apf Details already added
	$apf_dashboard_pending_search_data = array("is_pending"=>'1',"project_name"=>$search_project,"bank_name"=>$search_bank);
	$apf_dashboard_pending_list = i_get_apf_dashboard_pending($apf_dashboard_pending_search_data);
	if($apf_dashboard_pending_list['status'] == SUCCESS)
	{
		$apf_dashboard_pending_list_data = $apf_dashboard_pending_list['data'];
	}
	else
	{
		$alert = $apf_dashboard_pending_list["data"];
		$alert_type = 0;
	}
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF Pending Dashboard</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
		 
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3> Pending Dashboard List</h3>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">
			<form method="post" id="file_search_form" action="apf_dashboard_pending.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_project_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_id"]; ?>" <?php if($search_project == $apf_project_master_list_data[$apf_count]["apf_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php
			  for($bank_count = 0; $bank_count < count($bank_master_list_data); $bank_count++)
			  {
			  ?>
			  <option value="<?php echo $bank_master_list_data[$bank_count]["apf_bank_master_id"]; ?>" <?php if($search_bank == $bank_master_list_data[$bank_count]["apf_bank_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$bank_count]["apf_bank_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
			
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>	
				    <th>Project</th>
				    <th>Bank</th>
				    <th>Planned Days</th>
				    <th>Process Start Date</th>
				    <th>Process End Date</th>
				    <th>Leadtime</th>
					<th>Details</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($apf_dashboard_pending_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_dashboard_pending_list_data); $count++)
					{
						$sl_no++;

						// Get APF Dashboard modes already added
						$apf_process_search_data = array("apf_id"=>$apf_dashboard_pending_list_data[$count]["apf_details_id"],"is_process_started"=>'1');
						$apf_process_list = i_get_apf_process($apf_process_search_data);
						if($apf_process_list['status'] == SUCCESS)
						{	
						    $actual_start_date = $apf_process_list['data'][0]['apf_process_start_date'];							
							$actual_end_date   = date('Y-m-d');
							
							$days			   = get_date_diff($actual_start_date,$actual_end_date);	 							
							$leadtime          = $days["data"];
							
							$actual_start_date_display = get_formatted_date($actual_start_date,"d-M-Y");
							$actual_end_date_display   = '';
						}
						else
						{
							$actual_start_date_display = 'NOT STARTED';
							$actual_end_date_display   = 'NOT STARTED';
							
							$leadtime		   = 'NA';
						}	
						
						// Get Planned Days
						$planned_start_date = $apf_dashboard_pending_list_data[$count]["apf_details_planned_start_date"];
						$planned_end_date   = $apf_dashboard_pending_list_data[$count]["apf_details_planned_end_date"];
						$planned_days = get_date_diff($planned_start_date,$planned_end_date);						
							 
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $apf_dashboard_pending_list_data[$count]["apf_project_master_name"]; ?></td>
					<td><?php echo $apf_dashboard_pending_list_data[$count]["apf_bank_master_name"]; ?></td>
					<td><?php echo $planned_days['data']; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_start_date_display; ?></td>
					<td style="word-wrap:break-word;"></td>
					<td><?php echo $leadtime; ?></td>
					<td><?php if($view_perms_list['status'] == SUCCESS){?><a href="apf_process_list.php?apf_id=<?php echo $apf_dashboard_pending_list_data[$count]["apf_details_id"]; ?>">DETAILS</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No APF Dashboard Pending Added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
