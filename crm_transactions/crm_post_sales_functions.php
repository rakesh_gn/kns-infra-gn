<?php

/**

 * @author Nitin kashyap

 * @copyright 2015

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_transactions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_post_sales.php');



/*

PURPOSE : To add site blocking

INPUT 	: Client, Site ID, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_blocking($client,$site_id,$remarks,$added_by)

{	

	$site_blocking_result = db_add_site_blocking($client,$site_id,$remarks,$added_by);

	

	if($site_blocking_result["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Site Successfully Blocked";

		

		$site_block_history_iresult = db_add_site_blocking_history($site_blocking_result["data"],BLOCK_ACTION,$remarks,$added_by);

		

		if($site_block_history_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Site Successfully Blocked";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Site Successfully Blocked; but there seems to be a minor issue. Please inform the admin!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get blocked site list

INPUT 	: Blocking ID, Profile ID, Project ID, Site ID, Status, Added By, Start Date, End Date

OUTPUT 	: Blocked Site List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_site_blocking($blocking_id,$profile_id,$project_id,$site_id,$status,$added_by,$start_date,$end_date,$project_user="")

{

	$site_blocking_sresult = db_get_site_blocking($blocking_id,$profile_id,$project_id,$site_id,$status,$added_by,$start_date,$end_date,$project_user="");

	

	if($site_blocking_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $site_blocking_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No blocked sites!"; 

    }

	

	return $return;

}



/*

PURPOSE : To unblock a site

INPUT 	: Blocking ID, Remarks, Added By

OUTPUT 	: success or failure message

BY 		: Nitin Kashyap

*/

function i_unblock_site($blocking_id,$remarks,$added_by)

{

	$site_block_uresult = db_update_blocking_id($blocking_id,'0');



	if($site_block_uresult["status"] == SUCCESS)

	{		

		// Add blocking history

		$site_block_history_iresult = db_add_site_blocking_history($blocking_id,UNBLOCK_ACTION,$remarks,$added_by);

			

		if($site_block_history_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Blocking Successfully Extended!";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Blocking Successfully Extended; but there seems to be a minor issue. Please inform the admin!";

		}

	}

	else

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "There seems to be an internal error. Please try again later!";

	}

}



/*

PURPOSE : To add site booking

INPUT 	: Client, Site ID, Initial Amount, Initial Payment Details, Consideration Area, Sq. ft rate, Premium Type, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_booking($client,$site_id,$initial_amount,$initial_pay_details,$consideration_area,$rate_sqft,$premium_type,$remarks,$added_by)

{	

	$site_booking_sresult = db_get_site_booking('','',$site_id,'','','1','','','','','','','','');



	if($site_booking_sresult["status"] != DB_RECORD_ALREADY_EXISTS)

	{

		$site_booking_iresult = db_add_site_booking($client,$site_id,$initial_amount,$initial_pay_details,$consideration_area,$rate_sqft,$premium_type,$remarks,$added_by);

		

		if($site_booking_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Site Successfully Sent for Booking Approval";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Sorry. A booking has already been generated for this site!";

	}

		

	return $return;

}



/*

PURPOSE : To get site booking list

INPUT 	: Booking ID, Project ID, Client, Site ID, Is Premium, Status, Added By, Start Date, End Date, Approved By, Start Date, End Date, Enquiry ID, Is the site still in booking state, Booking Date Filter Start, Booking Date Filter End, Site No, Enquiry Date Filter (Start), Enquiry Date Filter (End), Enquiry Source, Order of Sorting, Source Type

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_site_booking($booking_id,$project_id,$site_id,$profile_id,$premium,$status,$added_by,$start_date,$end_date,$approved_by,$approved_start_date,$approved_end_date,$enquiry_id="",$is_active="",$booking_date_start="",$booking_date_end="",$site_no="",$enquiry_start="",$enquiry_end="",$source="",$order="",$source_type="",$project_user="")
{	
	$site_booking_sresult = db_get_site_booking($booking_id,$project_id,$site_id,$profile_id,$premium,$status,$added_by,$start_date,$end_date,$approved_by,$approved_start_date,$approved_end_date,$enquiry_id,$is_active,$booking_date_start,$booking_date_end,$site_no,$enquiry_start,$enquiry_end,$source,$order,$source_type,$project_user);	

	

	if($site_booking_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $site_booking_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No site booked!";

	}

		

	return $return;

}



/*

PURPOSE : To add site cost

INPUT 	: Project, Cost, Effective From Date, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_cost($project,$value,$effective_date,$added_by)

{	

	$site_cost_iresult = db_add_site_cost($project,$value,$effective_date,$added_by);

	

	if($site_cost_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Cost Details Added!";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add site discount

INPUT 	: Role, Project, Cost, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_discount_perms($role,$project,$value,$added_by)

{	

	$site_discount_iresult = db_add_site_discount_perms($role,$project,$value,$added_by);

	

	if($site_discount_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Discount Details Added!";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add site premium

INPUT 	: Project, Premium Type, Cost, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_premium($project,$premium,$value,$added_by)

{	

	$site_premium_iresult = db_add_site_premium_value($project,$premium,$value,$added_by);

	

	if($site_premium_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Premium Details Added!";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get site cost

INPUT 	: Project ID, Effective Date Start, Effective Date End, Added By, Start Date, End Date

OUTPUT 	: Cost list for this project

BY 		: Nitin Kashyap

*/

function i_get_site_cost($project_id,$effective_start_date,$effective_end_date,$added_by,$start_date,$end_date)

{

	$site_cost_sresult = db_get_site_cost($project_id,$effective_start_date,$effective_end_date,$added_by,$start_date,$end_date);

	

	if($site_cost_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $site_cost_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No cost added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get site discount thresholds

INPUT 	: Project ID, Role, Added By, Start Date, End Date

OUTPUT 	: Discount Threshold List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_site_discount_perms($project_id,$role,$added_by,$start_date,$end_date)

{

	$site_discount_sresult = db_get_site_discount($project_id,$role,$added_by,$start_date,$end_date);

	

	if($site_discount_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $site_discount_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No discount details added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get site cost

INPUT 	: Project ID, Premium Type, Added By, Start Date, End Date

OUTPUT 	: Cost list for this project

BY 		: Nitin Kashyap

*/

function i_get_site_premium($project_id,$premium_type,$added_by,$start_date,$end_date)

{

	$site_premium_sresult = db_get_site_premium($project_id,$premium_type,$added_by,$start_date,$end_date);

	

	if($site_premium_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $site_premium_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No premium added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To update approval or rejection of booking

INPUT 	: Booking ID, Approval Status, Approved By, Booking Date, Booking User, Consideration Area, Booking Rate

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_booking($booking_id,$approval,$approved_by,$booking_date="",$booking_user="",$consideration_area="",$booking_rate="")

{	

	$site_booking_uresult = db_update_booking_status($booking_id,$approval,$approved_by,$booking_date,$booking_user,$consideration_area,$booking_rate);

	

	if($site_booking_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $site_booking_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add site blocking from management

INPUT 	: Site ID, Mgmt User, Reason, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_mgmt_blocking($site_id,$mgmt_user,$reason,$remarks,$added_by)

{	

	$site_mgmt_blocking_result = db_add_site_mgmt_blocking($site_id,$mgmt_user,$reason,$remarks,$added_by);

	

	if($site_mgmt_blocking_result["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Site Successfully Blocked";

		

		$site_block_history_iresult = db_add_site_blocking_history($site_mgmt_blocking_result["data"],MGMNT_BLOCK_ACTION,$remarks,$added_by);

		

		if($site_block_history_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Site Successfully Blocked";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Site Successfully Blocked; but there seems to be a minor issue. Please inform the admin!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get mgmt block list

INPUT 	: Mgmt Block ID, Project ID, Site ID, Mgmnt User, Reason, Status, Added By, Start Date, End Date, Project User, Sort Order

OUTPUT 	: Mgmnt BLock list

BY 		: Nitin Kashyap

*/

function i_get_mgmt_block_list($mgmt_block_id,$project_id,$site_id,$mgmt_user,$reason,$status,$added_by,$start_date,$end_date,$project_user="",$order="")

{

	$site_premium_sresult = db_get_site_mgmt_block_list($mgmt_block_id,$project_id,$site_id,$mgmt_user,$reason,$status,$added_by,$start_date,$end_date,$project_user,$order);	

	

	if($site_premium_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $site_premium_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No management block details added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To unblock a management blocked site

INPUT 	: Management Blocking ID, Remarks, Added By

OUTPUT 	: success or failure message

BY 		: Nitin Kashyap

*/

function i_mgmnt_unblock_site($mgmnt_blocking_id,$remarks,$added_by)

{

	$site_mgmt_block_uresult = db_update_mgmnt_blocking_id($mgmnt_blocking_id,'0');



	if($site_mgmt_block_uresult["status"] == SUCCESS)

	{		

		// Add blocking history

		$site_block_history_iresult = db_add_site_blocking_history($mgmnt_blocking_id,MGMNT_UNBLOCK_ACTION,$remarks,$added_by);

			

		if($site_block_history_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Site Successfully Unblocked!";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Site Successfully Unblocked; but there seems to be a minor issue. Please inform the admin!";

		}

	}

	else

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "There seems to be an internal error. Please try again later!";

	}

}



/*

PURPOSE : To add site agreement

INPUT 	: Booking ID, Registering Person, Remarks, Agreement Date, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_agreement($booking_id,$registerer,$remarks,$agreement_date,$added_by)

{	

	$site_agreement_sresult = db_get_agreement_list('',$booking_id,'','','1','','','','','','');



	if($site_agreement_sresult["status"] != DB_RECORD_ALREADY_EXISTS)

	{

		$site_agreement_iresult = db_add_agreement($booking_id,$registerer,$remarks,$agreement_date,$added_by);

		

		if($site_agreement_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Agreement Successfully Created";						

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Sorry. An agreement has already been generated for this booking!";

	}

		

	return $return;

}



/*

PURPOSE : To get agreement list

INPUT 	: Agreement ID, Booking ID, Project ID, Site ID, Status, Agreement Start Date, Agreement End Date, Added By, Start Date, End Date, Is the site still in agreement status, Site Number

OUTPUT 	: Agreement list

BY 		: Nitin Kashyap

*/

function i_get_agreement_list($agreement_id,$booking_id,$project_id,$site_id,$status,$agreement_date_start,$agreement_date_end,$added_by,$start_date,$end_date,$is_agreement,$site_no="",$project_user="")

{

	$agreement_list_sresult = db_get_agreement_list($agreement_id,$booking_id,$project_id,$site_id,$status,$agreement_date_start,$agreement_date_end,$added_by,$start_date,$end_date,$is_agreement,$site_no,$project_user);

	

	if($agreement_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $agreement_list_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No agreement details added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add site booking cancellation

INPUT 	: Booking ID, Cancellation Date, Reason, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_booking_cancellation($booking_id,$cancel_date,$reason,$remarks,$added_by)

{	

	$site_booking_cancel_result = db_add_site_booking_cancellation($booking_id,$cancel_date,$reason,$remarks,$added_by);

	

	if($site_booking_cancel_result["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Site Booking Cancelled!";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add site registration

INPUT 	: Booking ID, Remarks, Registration Date, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_registration($booking_id,$remarks,$registration_date,$added_by)

{	

	$site_registration_sresult = db_get_registration_list('',$booking_id,'','','','','','','','','');



	if($site_registration_sresult["status"] != DB_RECORD_ALREADY_EXISTS)

	{

		$site_registration_iresult = db_add_registration($booking_id,$remarks,$registration_date,$added_by);

		

		if($site_registration_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Registration Successfully Created";						

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Sorry. A registration has already been generated for this booking!";

	}

		

	return $return;

}



/*

PURPOSE : To get registration list

INPUT 	: Registration ID, Booking ID, Project ID, Site ID, Status, Registration Start Date, Registration End Date, Added By, Start Date, End Date, Is the site still in registration status, Site Number

OUTPUT 	: Registration list

BY 		: Nitin Kashyap

*/

function i_get_registration_list($registration_id,$booking_id,$project_id,$site_id,$status,$registration_date_start,$registration_date_end,$added_by,$start_date,$end_date,$is_registration,$site_no="",$project_user="")

{

	$registration_list_sresult = db_get_registration_list($registration_id,$booking_id,$project_id,$site_id,$status,$registration_date_start,$registration_date_end,$added_by,$start_date,$end_date,$is_registration,$site_no,$project_user);

	

	if($registration_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $registration_list_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No registration details added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add site Katha transfer

INPUT 	: Booking ID, Remarks, Katha Transfer Date, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_site_katha_transfer($booking_id,$remarks,$kt_date,$added_by)

{	

	$site_kt_sresult = db_get_katha_transfer_list('',$booking_id,'','','','','','','','','');



	if($site_kt_sresult["status"] != DB_RECORD_ALREADY_EXISTS)

	{

		$site_kt_iresult = db_add_katha_transfer($booking_id,$remarks,$kt_date,$added_by);

		

		if($site_kt_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Katha Transfer Successfully Created";						

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Sorry. A KathaTransfer has already been generated for this booking!";

	}

		

	return $return;

}



/*

PURPOSE : To get Katha Transfer list

INPUT 	: KT ID, Booking ID, Project ID, Site ID, Status, KT Start Date, KT End Date, Added By, Start Date, End Date, Is the site still in Katha Transfer status, Site Number

OUTPUT 	: Katha Transfer list

BY 		: Nitin Kashyap

*/

function i_get_katha_transfer_list($kt_id,$booking_id,$project_id,$site_id,$status,$kt_date_start,$kt_date_end,$added_by,$start_date,$end_date,$is_kt,$site_no="",$project_user="")

{

	$kt_list_sresult = db_get_katha_transfer_list($kt_id,$booking_id,$project_id,$site_id,$status,$kt_date_start,$kt_date_end,$added_by,$start_date,$end_date,$is_kt,$site_no,$project_user);

	

	if($kt_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $kt_list_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Katha Transfer details added!"; 

    }

	

	return $return;

}



/*

PURPOSE : To update agreement status and date

INPUT 	: Agreement ID, Status, Agreement Date

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_agreement($agreement_id,$status,$agreement_date)

{	

	$site_agreement_uresult = db_update_agreement_status($agreement_id,$status,$agreement_date);

	

	if($site_agreement_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $site_agreement_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add CRM delay reason

INPUT 	: Booking ID, Reason ID, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_payment_delay_reason($booking_id,$reason_id,$remarks,$added_by)

{	

	$payment_delay_reason_iresult =  db_add_crm_delay_reason($booking_id,$reason_id,$remarks,$added_by);

		

	if($payment_delay_reason_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Delay Reason Added!";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Internal error. Please inform the admin!";

	}

		

	return $return;

}



/*

PURPOSE : To get delay reason list

INPUT 	: Booking ID, Reason ID, Active

OUTPUT 	: Delay Reason List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_delay_reason_list($booking_id,$reason_id,$active='')

{

	$delay_reason_sresult = db_get_crm_delay_reason_list($booking_id,$reason_id,$active);

	

	if($delay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $delay_reason_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No delay reasons!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get delayed payment bookings

INPUT 	: Project, Status, Reason, Added By

OUTPUT 	: Delayed Payment Data or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_delayed_payment_list($project,$status,$reason,$added_by)

{

	$delayed_payment_sresult = db_get_delay_reason_latest($project,$status,$reason,$added_by);

	

	if($delayed_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $delayed_payment_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No collection data!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get delayed payment bookings

INPUT 	: Project, Status, Added By

OUTPUT 	: Non-Delayed Payment Data or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_pending_nondelayed_payment_list($project,$status,$added_by)

{

	$non_delayed_payment_sresult = db_get_pending_collection($project,$status,$added_by);

	

	if($non_delayed_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $non_delayed_payment_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No collection data!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get bookings with no dates

INPUT 	: Project, Status, Added By, Booking Date Start, Booking Date End, Site No

OUTPUT 	: No Date Data

BY 		: Nitin Kashyap

*/

function i_get_no_date_list($project_id,$status,$added_by,$booking_date_start,$booking_date_end,$site_no)

{

	$no_date_sresult = db_get_site_no_date_data($project_id,$status,$added_by,$booking_date_start,$booking_date_end,$site_no);

	

	if($no_date_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $no_date_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No bookings with this criteria!"; 

    }

	

	return $return;

}



/*

PURPOSE : To untag a delay reason

INPUT 	: Booking ID

OUTPUT 	: Status

BY 		: Nitin Kashyap

*/

function i_untag_delay_reason($booking_id)

{

	$untag_sresult = db_untag_delay_reason($booking_id);

	

	if($untag_sresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $untag_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "Internal Error. Please try later"; 

    }

	

	return $return;

}



/*

PURPOSE : To get cancelled booking details

INPUT 	: Project, Status, Added By, Booking Date Start, Booking Date End, Site No

OUTPUT 	: No Date Data

BY 		: Nitin Kashyap

*/

function i_get_cancelled_booking_list($cancel_filter_data)

{

	$booking_cancelled_sresult = db_get_booking_cancellation_list($cancel_filter_data);

	

	if($booking_cancelled_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $booking_cancelled_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No cancelled bookings with this criteria!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get booking summary data

INPUT 	: Booking Filters

OUTPUT 	: Sales Count and Sold area

BY 		: Nitin Kashyap

*/

function i_get_booking_summary_data($booking_filter_data)

{

	$booking_summary_sresult = db_get_site_booking_summary_data($booking_filter_data);

	

	if($booking_summary_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		if($booking_summary_sresult['data'][0]['booking_area'] == NULL)

		{

			$booking_summary_sresult['data'][0]['booking_area'] = 0;

		}

		

		$return["status"] = SUCCESS;

        $return["data"]   = array('salecount'=>$booking_summary_sresult['data'][0]['booking_qty'],'salearea'=>$booking_summary_sresult['data'][0]['booking_area']);

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No bookings"; 

    }

	

	return $return;
}

/*
PURPOSE : To get approved bookings which don't have customer profie
INPUT 	: Booking Filters
OUTPUT 	: List of approved bookings with no customer profile
BY 		: Nitin Kashyap
*/
function i_get_no_profile_list($booking_filter_data)
{
	$no_profile_sresult = db_get_approved_booking_no_cust_profile($booking_filter_data);
	
	if($no_profile_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $no_profile_sresult['data'];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bookings"; 
    }
	
	return $return;
}

/*
PURPOSE : To get approved bookings which don't have agreement
INPUT 	: Booking Filters
OUTPUT 	: List of approved bookings with no agreements
BY 		: Nitin Kashyap
*/
function i_get_no_agreement_list($booking_filter_data)
{
	$no_agreement_sresult = db_get_approved_booking_no_agreement($booking_filter_data);
	
	if($no_agreement_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $no_agreement_sresult['data'];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bookings"; 
    }
	
	return $return;
}

/*
PURPOSE : To get approved bookings which have agreement but no registration
INPUT 	: Booking Filters
OUTPUT 	: List of approved bookings with agreement but no registration
BY 		: Nitin Kashyap
*/
function i_get_no_registration_list($booking_filter_data)
{
	$no_registration_sresult = db_get_agreement_no_registration($booking_filter_data);
	
	if($no_registration_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $no_registration_sresult['data'];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bookings"; 
    }
	
	return $return;
}

/*
PURPOSE : To get approved bookings which have registration but no katha transfer
INPUT 	: Booking Filters
OUTPUT 	: List of approved bookings with registration but no katha transfer
BY 		: Nitin Kashyap
*/
function i_get_no_kt_list($booking_filter_data)
{
	$no_kt_sresult = db_get_registration_no_kt($booking_filter_data);
	
	if($no_kt_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $no_kt_sresult['data'];
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bookings"; 
    }
	
	return $return;
}
?>