<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	/* QUERY STRING - START */
	if(isset($_GET["enquiry"]))
	{
		$enquiry = $_GET["enquiry"];
	}
	else
	{
		$enquiry = "";
	}
	/* QUERY STRING - END */


	// Capture the form data
	if(isset($_POST["add_unq_enquiry_submit"]))
	{
		$enquiry = $_POST["hd_enquiry_id"];
		$reason  = $_POST["ddl_reason"];
		$remarks = $_POST["txt_remarks"];

		// Check for mandatory fields
		if(($enquiry !="") && ($reason !=""))
		{
			$unq_enquiry_iresult = i_add_unqualified_enquiry($enquiry,$remarks,$reason,$user);
			$enquiry_fup_edit_iresult = db_update_enquiry_status($enquiry,"4","YES");
			$enquiry_fup_iresult = i_add_enquiry_fup($enquiry,"Unqualified","4",date("Y-m-d H:i:s",strtotime('+1 hour')),"answered",$user);
			if($enquiry_fup_edit_iresult["status"]== SUCCESS && $unq_enquiry_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				echo 'success';
				exit;
			}
			else
			{
				$alert_type = 0;
				echo 'failure';
				exit;
			}
			$alert = $unq_enquiry_iresult["data"];
		}
		else
		{
			echo 'Failed due to insuffecient data'; exit;
		}
	}

	// Reason List
	$reason_list = i_get_reason_list('','1');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Mark Enquiry As Unqualified</h4>
</div>
<div class="modal-body">
  <form id="add_unq_enquiry" class="form-horizontal">
    <input type="hidden" id="enquiry" name="hd_enquiry_id" value="<?php echo $enquiry; ?>" />
    <div style="margin:10px" class="form-group">
      <label class="control-label" for="ddl_reason">Reason*</label>
      <select class="form-control" id="ddl_reason" name="ddl_reason">
        <option value="">- - Select a reason - -</option>
        <?php
				for($count = 0; $count < count($reason_list_data); $count++)
					{
				?>
        <option value="<?php echo $reason_list_data[$count]["reason_id"]; ?>">
          <?php echo $reason_list_data[$count]["reason_name"]; ?>
        </option>
        <?php
					}
				?>
      </select>
    </div>
    <div class="form-group" style="margin:10px">
      <label class="control-label" for="txt_remarks">Remarks</label>
      <textarea name="txt_remarks" id="txt_remarks" class="form-control"></textarea>
    </div>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary" onclick="update_status()">Submit</button>
</div>

<script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
<script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
