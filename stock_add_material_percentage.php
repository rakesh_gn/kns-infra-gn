<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_masters.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
   // Get Project Management Master modes already added
	$stock_project_search_data = array('active'=>'1');
	$project_list = i_get_project_list($stock_project_search_data);
	if($project_list["status"] == SUCCESS)
	{   

			$project_list_data = $project_list["data"];
	}


	// Capture the form data
	if(isset($_POST["material_id"]))
	{	

		//print_r($_POST);exit();
		$material_id       = $_POST["hd_material_id"];
		$material_name 	   = $_POST["material_id"];
		$material_code 	   = $_POST["material_code"];
		$material_uom 	   = $_POST["hd_uom_id"];
		$percentage 	     = $_POST["percentage"];
		$remarks		 	     = $_POST["txt_remarks"];
		$project_id 	     = $_POST["project_id"];
		// Check for mandatory fields
		if(($material_id != ""))
		{	
			$stock_material_percenatge_search_data = array('material_id' => $material_id, 'project_id' =>$project_id);
			$old_data = db_get_stock_material_percentage_list($stock_material_percenatge_search_data);
/*			print_r($old_data);exit();
*/			if($old_data['status'] == SUCCESS){

				$alert = "This material is already present in the selected project, please update same";
				$alert_type = 0;
				

				//header("location:stock_add_material_percentage.php");

			}else{

				$stock_material_percentage_iresult = db_add_stock_material_percentage($material_id,$material_code,$material_name,$material_uom,$percentage,$remarks,$user,$project_id );
				if($stock_material_percentage_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
				}
				//stock_material_percentage
				$alert = $stock_material_percentage_iresult["data"];
				header("location:stock_material_percentage.php");

			}

			


		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Material Percenatge</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
// include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Material Percentage</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master - Add Minimum Stock Quantity</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_reason_master_form" class="form-horizontal" method="post" action="stock_add_material_percentage.php" >
									<input type="hidden" name="hd_material_id" id="hd_material_id" value="" />
									<input type="hidden" name="hd_uom_id" id="hd_uom_id" value="" />
									<!-- <input type="hidden" id="hd_material_name" value="" name=""> -->
									<fieldset>

										<div class="control-group">
											<label class="control-label" for="material_id">Item</label>
											<div class="controls">
												<input type="text" name="material_id" class="span5" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" />
												<div id="search_results" class="dropdown-content"></div>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="material_code">Material Code</label>
											<div class="controls">
												<span id="span_material_code" style="display:block"></span>
												<input type="hidden" id="material_code" value="" name="material_code">
												
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="material_uom">Uom</label>
											<div class="controls">
												<span id="span_material_uom" style="display:inline"></span>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										 <div class="control-group">
											<label class="control-label" for="percentage">Project</label>
											 <select name="project_id" id="project_id" style="max-width:250px;margin-left: 25px;" class="controls">
					                              <option value="">- - Select Project - -</option>
					                              <?php
					                                for ($project_count = 0; $project_count < count($project_list_data); $project_count++) { ?>
					                                 <option value="<?php echo $project_list_data[$project_count]["stock_project_id"]; ?>"
					                                   <?php if ($project_id == $project_list_data[$project_count]["stock_project_id"]) {
					                                    ?> selected="selected" <?php } ?>>
					                                    <?php echo $project_list_data[$project_count]["stock_project_name"]; ?>
					                                 </option>
					                              <?php } ?>
					                      </select> 
										</div>


										<div class="control-group">
											<label class="control-label" for="percentage">Minimum Stock</label>
											<div class="controls">
												<input type="number" class="span6" name="percentage"  id="percentage" placeholder="Enter Stock">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" id="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_material_percentage_submit" value="Submit" />
											  <!-- <button id="submit_button" type="button" class="btn btn-primary" onclick="add_material_percentage()">Submit</button> -->
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function get_material_list()
{
	var searchstring = document.getElementById('stxt_material').value;
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}
		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);
	}
	else
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_material(material_id,material_name,material_code,unit_measure)
{
	document.getElementById('hd_material_id').value = material_id;

	document.getElementById('stxt_material').value = material_name;

	var name_code = material_name.concat('-',material_code);

	document.getElementById('span_material_code').innerHTML = name_code;

	document.getElementById('span_material_uom').innerHTML = unit_measure;

	document.getElementById('search_results').style.display = 'none';

	$('#name_code').val(name_code);

	$('#hd_uom_id').val(unit_measure);

	$('#material_code').val(name_code);

}
/*function add_material_percentage()
{
	var material_id = document.getElementById('hd_material_id').value;
	var material_code = document.getElementById('span_material_code').innerHTML;
	var material_name = document.getElementById('stxt_material').value;
	var material_uom = document.getElementById('span_material_uom').innerHTML;
	var remarks = document.getElementById('txt_remarks').value;
	var percentage = document.getElementById('percentage').value;
	var project_id = document.getElementById('project_id').value;

	

	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			 window.location = "stock_material_percentage.php";
		}
	}
	xmlhttp.open("POST", "stock_add_material_percentage.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("material_id=" + material_id + "&material_name=" +material_name + "&material_code=" +material_code + "&material_uom=" + material_uom + "&remarks="+ remarks + "&percentage="+ percentage+ "&project_id="+ project_id );
	// reset_form_data();
	return false;
}*/
</script>

  </body>

</html>
