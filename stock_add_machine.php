<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 28-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	
	if(isset($_POST["type"]))
	{
		$machine_type  = $_POST["type"];
	}
	else
	{
		$machine_type = "";
	}
	// Capture the form data
	if(isset($_POST["add_machine_master_submit"]))
	{		
		$name                = $_POST["name"];
		$machine_no          = $_POST["number"];
		$vendor         	 = $_POST["ddl_vendor"];
		$type         		 = $_POST["type"];
		$remarks 	         = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($name != "") && ($machine_no != ""))
		{
			$stock_machine_master_iresult = i_add_stock_machine_master($name,$machine_no,$vendor,$type,$remarks,$user);
			
			if($stock_machine_master_iresult["status"] == SUCCESS)
				
			{	
				$alert_type = 1;
			}
			
			$alert = $stock_machine_master_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get stock_machine_vendor_master modes already added
	$project_machine_vendor_master_search_data = array("active"=>'1');
	$project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
	if($project_machine_vendor_master_list['status'] == SUCCESS)
	{
		$project_machine_vendor_master_list_data = $project_machine_vendor_master_list['data'];
	}
	else
	{
		$alert = $project_machine_vendor_master_list["data"];
		$alert_type = 0;
	}
	
	// User List

	$user_list = i_get_user_list('','','','','1');

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

		$alert_type = 0; // Failure

	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Stock Master - Add Machine</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Stock Master - Add Machine</h3><span style="float:right; padding-right:20px;"><a href="stock_machine_master_list.php">Machine List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Machine</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="stock_master_add_machine_form" class="form-horizontal" method="post" action="stock_add_machine.php">
									<fieldset>	
									
										<div class="control-group">											
											<label class="control-label" for="type">Fuel Type*</label>
											<div class="controls">
												<select name="type" id="type" required onchange="this.form.submit();">
												<option value="">- - Select Type of Machine - -</option>
												<option value="Diesel" <?php if($machine_type == "Diesel") {?> selected="selected"<?php } ?>>Diesel</option>
												<option value="Petrol" <?php if($machine_type == "Petrol") { ?> selected="selected"<?php } ?>>Petrol</option>
												</select>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->					
																
																				
										<div class="control-group">											
											<label class="control-label" for="name">Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="name" placeholder="Name Ex:Concrete Mixer" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="number">Number*</label>
											<div class="controls">
												<input type="text" class="span6" name="number" placeholder="Name Ex:1,2,3.." required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<?php if($machine_type == "Diesel")
										{?>
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor">Vendor*</label>
											<div class="controls">
												<select name="ddl_vendor" required>
												<option value="">- - Select Machine Vendor - -</option>
												<?php
												for($count = 0; $count < count($project_machine_vendor_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"]; ?>"><?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<?php
										}
										?>
										
										<?php if($machine_type == "Petrol")
										{?>
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor">Users*</label>
											<div class="controls">
												<select name="ddl_vendor">

											  <option value="">- - Select Users - -</option>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
													?>

													<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 

													if($user_id == $user_list_data[$count]["user_id"])

													{
														
													?>	
													
													selected="selected"
													<?php
													}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>	
													<?php					

												}

											  ?>

											  </select>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										<?php
										}
										?>
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_machine_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>


<script>
function fuel_type()
{ 
	var type = document.getElementById("type").value; 
	if(type == "diesel")
	{
		document.getElementById("ddl_user").disabled = false;
	}
	else if(type == "petrol")
	{
		document.getElementById("ddl_vendor").disabled = false;
	}
}

</script>
