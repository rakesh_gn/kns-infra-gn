<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24-Aug-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'crm'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'crm'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'crm'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["mapping_id"]))
	{
		$mapping_id = $_GET["mapping_id"];
	}
	else
	{
		$mapping_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_user_project_mapping_submit"]))
	{
		$mapping_id      = $_POST["hd_mapping_id"];
		$user_id         = $_POST["ddl_user_id"];
		$project_id      = $_POST["ddl_project_id"];
		$remarks         = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($user_id != "") && ($project_id != ""))
		{
			$crm_user_project_mapping_update_data = array("user_id"=>$user_id,"project_id"=>$project_id,"remarks"=>$remarks);
			$user_project_mapping_iresult = i_update_crm_user_project_mapping($mapping_id,$user_id,$project_id,$crm_user_project_mapping_update_data);
			
			if($user_project_mapping_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:crm_user_project_mapping_list.php");
			}
			else
			{
				$alert 		= $user_project_mapping_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

    // Get CRM Project Master already added
	$crm_project_master_list = i_get_project_list('','1','');
	if($crm_project_master_list['status'] == SUCCESS)
	{
		$crm_project_master_list_data = $crm_project_master_list['data'];
	}
      else
	{
		$alert = $crm_project_master_list["data"];
		$alert_type = 0;
	}
		
	// Get User Project mapping already added
	$crm_user_project_mapping_search_data = array("active"=>'1',"mapping_id"=>$mapping_id);
	$user_project_mapping_list = i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($user_project_mapping_list['status'] == SUCCESS)
	{
		$user_project_mapping_list_data = $user_project_mapping_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>User - Edit Project Mapping</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'crm'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>User - Edit Project Mapping</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">project Mapping - Edit User</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="crm_edit_user_project_mapping_form" class="form-horizontal" method="post" action="crm_edit_user_project_mapping.php">
								<input type="hidden" name="hd_mapping_id" value="<?php echo $mapping_id; ?>" />
									<fieldset>

									        <div class="control-group">											
											<label class="control-label" for="ddl_user_id">User*</label>
											<div class="controls">
												<select name="ddl_user_id" required>
												<option>- - Select User - -</option>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $user_project_mapping_list_data[0]["crm_user_project_mapping_user_id"]){ ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										    </div> <!-- /control-group -->
																
											<div class="control-group">											
											<label class="control-label" for="ddl_project_id">project*</label>
											<div class="controls">
											<select name="ddl_project_id" required>
											<option>- - Select project - -</option>
											<?php
											for($count = 0; $count < count($crm_project_master_list_data); $count++)
											{
											?>
											<option value="<?php echo $crm_project_master_list_data[$count]["project_id"]; ?>" <?php if($crm_project_master_list_data[$count]["project_id"] == $user_project_mapping_list_data[0]["crm_user_project_mapping_project_id"]){ ?> selected="selected" <?php } ?>><?php echo $crm_project_master_list_data[$count]["project_name"]; ?></option>
											<?php
											}
											?>
											</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text"  name="txt_remarks" placeholder="Remarks" value="<?php echo $user_project_mapping_list_data[0]["crm_user_project_mapping_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_user_project_mapping_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 .
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
