<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID', '403');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '6', '1');

?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
    // Query String Data
    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Project Vendor Overall Payment List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
     <link href="./css/style.css?<?php echo time();?>" rel="stylesheet">
     <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
     <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
     <link href="./bootstrap_aku.min.css" rel="stylesheet">
		 <script src="datatable/project_master_overall_payment_list_datatable.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>

    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Vendor Overall Payment List</h3>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <select id="search_vendor" name="search_vendor" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Vendor - -</option>
                        <?php
                              for ($vendor_count = 0; $vendor_count < count($project_manpower_agency_list_data); $vendor_count++) {
                                  ?>
                        <option value="<?php echo $project_manpower_agency_list_data[$vendor_count]["project_manpower_agency_id"]; ?>">
                          <?php echo $project_manpower_agency_list_data[$vendor_count]["project_manpower_agency_name"]; ?></option>
                        <?php
                              } ?>
                        </select>
                      <button type="button" onclick="tableDraw()" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                    <tr>
                    <th colspan="2"></th>
                    <th colspan="3" class="blue center">Manpower</th>
                    <th colspan="3" class="red center">Contract</th>
                    <th colspan="3" class="green center">Overall Manpower and Contract Payment</th>
                    </tr>
                <tr>
                <th>SL No</th>
                <th>Vendor Name</th>

                <th class="blue">Total Amount</th>
                <th class="blue">Issued Amount</th>
                <th class="blue">Balance Amount</th>

                <th class="red">Total Amount</th>
                <th class="red">Issued Amount</th>
                <th class="red">Balance Amount</th>

                <th class="green">Overall Total Amount</th>
                <th class="green">Overall Issued Amount</th>
                <th class="green">Overall Balance Amount</th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
